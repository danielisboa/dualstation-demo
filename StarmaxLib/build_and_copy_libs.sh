#!/bin/bash

target=none
copy=false

[ $# -eq 0 ] && target="rebuild" && copy=true

[ "$1" == "-r" ] && target="rebuild"

[ "$1" == "-b" ] && target="build"

[ "$1" == "-x" ] && target="clean"

[ "$1" == "-c" ] && copy=true

if [ "$target" != "none" ]; then
    echo -e "Performing msbuild /t:$target\n"

	if ! msbuild /t:$target; then
	   echo -e "\n\n\n\n\n\n\n\n\n\t\t\tERROR ON BUILD\n\n"
	   exit 1
	fi
fi

! $copy && exit

dstdir=$(find .. -name 'Starmax\.dll.*' | grep 'Assets/Libs')
for dir in $dstdir; do
	dir=$(dirname $dir)
	cp -v AllLibs/bin/Debug/[!A]*.* $dir 2>/dev/null || echo "COPY FAILED"
done
