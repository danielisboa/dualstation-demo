﻿using System;
using System.Runtime.InteropServices;
using Infrastructure;

namespace GuiCommunication
{
	public static class GuiSimProtocol
	{
		public enum Opcodes : ushort
		{
			NoOpcode                    = 0x00,
			CommunicationStatus         = 0x01,
			GuiTrainingSelection        = 0x02,
			GuiFireTrainingConfig       = 0x03,
			GuiPantographTrainingConfig = 0x04,
			GuiTrainingControl          = 0x05,
			SimTrainingStatus           = 0x06,
			SimTrainingResults          = 0x07,
			SimFireTrainingStatus       = 0x08,
			TargetSectorStatus          = 0x09,
			SimTrainingStatusSnapshot   = 0x0A,
			SimStatus                   = 0xFD,
			SimCalibrations             = 0xFE,
			KeepAlive                   = 0xFF
		}

		//----------------------------

		public enum TrainingType : byte
		{
			Pantograph        = 0x00,
			OperationalStatic = 0x01,
			OperationalMobile = 0x02,
			Calibrations      = 0x03,
			Naval             = 0x04,
			Boresight		  = 0x05,
			NoTraining,
		}

		//----------------------------

		public enum TrainingControl : byte
		{
			Start   = 0x01,
			Stop    = 0x02,
			Pause   = 0x03,
			Resume  = 0x04,
			Failure = 0xFF,
		}

		//----------------------------

		public enum PantographPatterns
		{
			Horizontal = 0x00,
			Vertical   = 0x01,
			Elipsoidal = 0x02,
			Infinity   = 0x03,
			Saw        = 0x04,
			Square     = 0x05,
			Sinus      = 0x06,
		}

		//----------------------------

		public enum PantographCalibratedSpeed : byte
		{
			Slow   = 0x01,
			Normal = 0x02,
			Fast   = 0x03,
		}

		//----------------------------

		public enum OperationModes : byte
		{
			Manual           = 0x01,
			Power            = 0x02,
			Stab             = 0x03,
		}

		//----------------------------

		public enum CameraTypes : byte
		{
			Day              = 0x01,
			Thermal          = 0x02,
			Lrf              = 0x03,
		}

		//----------------------------

		[System.Serializable]
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct GuiTrainingSelection
		{
			public byte TrainingType;
			public byte TrainingSelection;
		}

		//----------------------------

		[System.Serializable]
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct GuiFireTrainingConfig
		{
			public float  TrainingDuration;
			public ushort NumberOfTargets;
			public byte   HitsToKillTargets;
			public byte   PopupTargets;
			public float  PopupInterval;
			public ushort SimultaneousTargets;
			public ushort AmmunitionCount;
			public byte   ReloadAmmoSafe;
			public byte   FireJamming;
			public byte   DaylightIntensity;
			public byte   FogIntensity;
			public byte   RainIntensity;
			public sbyte  WindSpeed;
			public float  VehicleSpeed;
			public bool   LrfEnabled;
		}

		//----------------------------

		[System.Serializable]
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct GuiPantographTrainingConfig
		{
			public float TrainingDuration;
			public float LaserReqInterval;
			public float FireReqInterval;
			public float RequestDuration;
			public byte  CalibratedTargetSpeed;
		}

		//----------------------------

		[System.Serializable]
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct GuiTrainingControl
		{
			public byte TrainingControl;
		}

		//----------------------------

		[System.Serializable]
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct SimCalibrations
		{
			public bool  WeaponCalibrationsOn;
			public float FireForceLimiter;
			public float WindForceScale;
			public bool  LrfCalibrationsOn;
			public float LrfPosX;
			public float LrfPosY;
			public float LrfRotX;
			public float LrfRotY;
			public bool  CameraCalibrationsOn;
			public bool  ReticleOn;
			public bool  ReticleOffsetsOn;
			public int   ReticleOffsetX;
			public int   ReticleOffsetY;
			public float ReticleScaleX;
			public float ReticleScaleY;
			public bool  UseTerrainDetail;
			public int   DetailDistance;
			public float DetailDensity;
		}

		//----------------------------

		[System.Serializable]
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct CommunicationStatus
		{
			public bool CommunicationOk;

			public CommunicationStatus(bool communicationOk)
			{
				CommunicationOk = communicationOk;
			}
		}

		//----------------------------

		[System.Serializable]
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct SimTrainingStatus
		{
			public byte TrainingStatus;

			public SimTrainingStatus(byte status)
			{
				TrainingStatus = status;
			}
		}

		//----------------------------

		[System.Serializable]
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct SimFireTrainingStatus
		{
			public ushort VehiclePositionX;
			public ushort VehiclePositionY;
			public ushort VehicleDirectionAz;
			public ushort TurretDirectionAz;
			public sbyte  TurretDirectionElv;
			public ushort TurretConfiguredRange;
			public byte TurretFiring;
			public byte TargetOnLOS;
			public ushort TargetHitId;
			public ushort RemainingAmmunition;
			public byte TurretOperationalMode;
			public byte ActiveCamera;
		}
	}
}

//----------------------------
