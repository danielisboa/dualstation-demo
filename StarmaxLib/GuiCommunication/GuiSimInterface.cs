﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading;
using Infrastructure;

namespace GuiCommunication
{
	public class GuiSimInterface : UdpChannelInterface<GuiSimInterface, GuiSimMsg>
	{
		override protected bool ParseReceivedData(byte[] data)
		{
			var msg = new GuiSimMsg(data);
			bool notifyData = IsEnabled();

			switch ((GuiSimProtocol.Opcodes) msg.Opcode)
			{
				case GuiSimProtocol.Opcodes.GuiTrainingSelection:
				case GuiSimProtocol.Opcodes.GuiFireTrainingConfig:
				case GuiSimProtocol.Opcodes.GuiPantographTrainingConfig:
				case GuiSimProtocol.Opcodes.GuiTrainingControl:
					Log.Print(LogLevel, $"Received opcode {msg.Opcode}, lenght {msg.Lenght}, timetag {msg.TimeTag}");
					break;

				case GuiSimProtocol.Opcodes.KeepAlive:
					notifyData = true;
					break;

				default:
					Log.Print(LogLevel, $"Received unknown opcode: {msg.Opcode}");
					NotifyError(InvalidMessageReceivedError);
					return false;
			}

			if (notifyData) NotifyData(msg);

			return true;
		}
	}
}

