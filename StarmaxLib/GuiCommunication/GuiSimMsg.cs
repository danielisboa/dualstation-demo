﻿using System;
using System.Text;
using Infrastructure;

namespace GuiCommunication
{
	public class GuiSimMsg
	{
//		#region privateStaticAttributes

		private static uint _defaultBufferSize = 50;

//		//#endregion

		//----------------------------

//		#region privateAttributes

		private byte[] _data;
		private uint _bufferSize;

//		//#endregion

		//----------------------------

//		#region publicProperties

		public GuiSimProtocol.Opcodes Opcode { get; set; }

		//----------------------------

		public uint Lenght { get; set; }

		//----------------------------

		public uint TimeTag { get; }

//		//#endregion

		//----------------------------

//		#region publicStaticMethods

		public static void SetDefaultBufferSize(uint defaultBufferSize)
		{
			_defaultBufferSize = defaultBufferSize;
		}

//		//#endregion

		//----------------------------

//		#region publicMethods

		//----------------------------

		public GuiSimMsg(GuiSimProtocol.Opcodes opcode)
		{
			Opcode = opcode;
			SetData(null);
		}

		//----------------------------

		public GuiSimMsg(GuiSimProtocol.Opcodes opcode, byte[] data)
		{
			Opcode = opcode;
			SetData(data);
		}

		//----------------------------

		public GuiSimMsg(GuiSimProtocol.Opcodes opcode, string json)
		{
			Opcode = opcode;
			SetData(Encoding.ASCII.GetBytes(json));
		}

		//----------------------------

		public GuiSimMsg(byte[] data)
		{
			var header = new Infrastructure.MsgHeader(data);
			Opcode = (GuiSimProtocol.Opcodes) header.Opcode;
			Lenght = header.Length;
			TimeTag = header.TimeTag;
			SetData(data, (uint) Infrastructure.MsgHeader.Size());
		}

		//----------------------------

		public void Serialize(ref byte[] buffer)
		{
			Serialize(ref buffer, 0);
		}

		//----------------------------

		public void Serialize(ref byte[] buffer, int offset)
		{
			var serializer = new Serializer(buffer);
			serializer.Offset = offset;

			var header = new Infrastructure.MsgHeader((ushort) Opcode, (ushort) Lenght);
			var headerBytes = header.Serialize();
			Log.Disabled(Serializer.BytesToString("GuiSimMsg|Header", headerBytes));
			serializer.Add(headerBytes);
			Log.Disabled(Serializer.BytesToString("GuiSimMsg|Data", _data));
			serializer.Add(_data, (int) Lenght);
			Log.Disabled(Serializer.BytesToString("GuiSimMsg|Buffer", buffer));
		}

		//----------------------------

		public byte[] Serialize()
		{
			var buffer = new byte[Size()];
			Serialize(ref buffer, 0);
			return buffer;
		}

		//----------------------------

		public int Size()
		{
			return Infrastructure.MsgHeader.Size() + (int) Lenght;
		}

		//----------------------------

		public void SetData(byte[] data)
		{
			SetData(data, 0);
		}

		//----------------------------

		public void SetData(byte[] data, uint offset)
		{
			Lenght = 0;
			if (data == null)
			{
				Reserve(_defaultBufferSize);
				return;
			}
			Lenght = (uint) data.Length;
			Reserve(Lenght);
			Buffer.BlockCopy(data, (int) offset, _data, 0, (int) Lenght);
		}

		//----------------------------

		public GuiSimProtocol.GuiTrainingSelection GetTrainingSelection()
		{
			if (Opcode != GuiSimProtocol.Opcodes.GuiTrainingSelection)
				throw new Exception("GuiTrainingSelection: Wrong Msg Type - expected {GuiSimProtocol.Opcodes.GuiTrainingSelection}, received {Opcode}\"");

			var trainingSelection = new GuiSimProtocol.GuiTrainingSelection();

			if (Lenght != trainingSelection.Size())
				throw new Exception($"GuiTrainingSelection: Wrong Msg Size - expected {trainingSelection.Size()}, received {Lenght}");

			trainingSelection.Deserialize(_data);

			return trainingSelection;
		}

		//----------------------------

		public GuiSimProtocol.GuiFireTrainingConfig GetFireTrainingConfig()
		{
			if (Opcode != GuiSimProtocol.Opcodes.GuiFireTrainingConfig)
				throw new Exception("GuiFireTrainingConfig: Wrong Msg Type - expected {GuiSimProtocol.Opcodes.GuiFireTrainingConfig}, received {Opcode}\"");

			var fireTrainingConfig = new GuiSimProtocol.GuiFireTrainingConfig();

			if (Lenght != fireTrainingConfig.Size())
				throw new Exception($"GuiFireTrainingConfig: Wrong Msg Size - expected {fireTrainingConfig.Size()}, received {Lenght}");

			fireTrainingConfig.Deserialize(_data);

			return fireTrainingConfig;
		}

		//----------------------------

		public GuiSimProtocol.GuiPantographTrainingConfig GetPantographTrainingConfig()
		{
			if (Opcode != GuiSimProtocol.Opcodes.GuiPantographTrainingConfig)
				throw new Exception("GuiPantographTrainingConfig: Wrong Msg Type - expected {GuiSimProtocol.Opcodes.GuiPantographTrainingConfig}, received {Opcode}\"");

			var pantographTrainingConfig = new GuiSimProtocol.GuiPantographTrainingConfig();

			if (Lenght != pantographTrainingConfig.Size())
				throw new Exception($"GuiPantographTrainingConfig: Wrong Msg Size - expected {pantographTrainingConfig.Size()}, received {Lenght}");

			pantographTrainingConfig.Deserialize(_data);

			return pantographTrainingConfig;
		}

		//----------------------------

		public GuiSimProtocol.GuiTrainingControl GetTrainingControl()
		{
			if (Opcode != GuiSimProtocol.Opcodes.GuiTrainingControl)
				throw new Exception("GuiTrainingControl: Wrong Msg Type - expected {GuiSimProtocol.Opcodes.GuiTrainingControl}, received {Opcode}\"");

			var trainingControl = new GuiSimProtocol.GuiTrainingControl();

			if (Lenght != trainingControl.Size())
				throw new Exception($"GuiTrainingControl: Wrong Msg Size - expected {trainingControl.Size()}, received {Lenght}");

			trainingControl.Deserialize(_data);

			return trainingControl;
		}

		//----------------------------

		public GuiSimProtocol.SimCalibrations GetSimCalibrations()
		{
			if (Opcode != GuiSimProtocol.Opcodes.SimCalibrations)
				throw new Exception("SimCalibrations: Wrong Msg Type - expected {GuiSimProtocol.Opcodes.SimCalibrations}, received {Opcode}\"");

			var simCalibrations = new GuiSimProtocol.SimCalibrations();

			if (Lenght != simCalibrations.Size())
				throw new Exception($"SimCalibrations: Wrong Msg Size - expected {simCalibrations.Size()}, received {Lenght}");

			simCalibrations.Deserialize(_data);

			return simCalibrations;
		}

		//----------------------------

		public void Reserve(uint bufferSize)
		{
			if (_bufferSize >= bufferSize)
				return;

			_bufferSize = bufferSize;
			_data = new byte[_bufferSize];
		}

//		//#endregion
	}
}

//----------------------------
