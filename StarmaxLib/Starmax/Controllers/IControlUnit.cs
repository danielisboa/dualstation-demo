﻿using System;

namespace Managers
{
	public interface IControlUnit
	{
		void Start(float delay = -1);
		void Pause(bool pause);
		void Stop();
	}
}

