﻿using System;
using Models;
using Common;

namespace Controllers
{
	public abstract class Controller
	{
		public abstract void SetModel(IModel model);

		protected ModelType RegisterModel<ModelType>(IModel model, UpdateEventDelegate preUpdate, UpdateEventDelegate postUpdate) where ModelType:class
		{
			var modelInterface = model as ModelType;

			if (modelInterface == null)
				throw new Exception("Model type mismatch: expected " + typeof(ModelType).ToString());

			model.AddPreUpdateCallback(preUpdate);
			model.AddPostUpdateCallback(postUpdate);

			return modelInterface;
		}
	}
}
