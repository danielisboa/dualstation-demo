﻿using System;
using System.Collections.Generic;
using Models;
using Common;

namespace Controllers
{
	public class PowerStabCommanderController : Controller
	{
		//#region publicAttributes

		public UpdateEventDelegate encodersValuesChanged;

		//#endregion

		//------------------------------

		//#region privateAttributes

		ICommanderModel _commanderModel;		
		IGyroModel _trvGyro;
		IGyroModel _elvGyro;

		// Gunner
		float _trvAngle;			
		float _elvAngle;
		float _trvCtrlAng;
		float _elvCtrlAng;
		float _lastTrvCtrlAng;
		float _lastElvCtrlAng;
		float _trvStabRef;
		float _elvStabRef;
		float _trvJmpAngle;
		float _elvJmpAngle;

		bool _stabilization = false;

		//#endregion

		//------------------------------

		//#region publicMethods

		public PowerStabCommanderController()
		{
		}

		//------------------------------

		public override void SetModel(Models.IModel commanderModel)
		{
			Log.Debug($"PowerStabControllerCommander.SetModel()");
			_commanderModel = RegisterModel<ICommanderModel>(commanderModel, this.SetTurretAngles, this.GetTurretAngles);
			_trvGyro = (IGyroModel) _commanderModel.GetTrvGyroModel();
			_elvGyro = (IGyroModel) _commanderModel.GetElvGyroModel();
			_trvStabRef = _trvGyro.GetAngle();
			_elvStabRef = _elvGyro.GetAngle();
		}

		//------------------------------		

		public void SetMode(Common.OperationalMode opModeCMDR)		
		{
			_stabilization = (opModeCMDR == Common.OperationalMode.Stab);
		}

		//------------------------------

		public void SetJumpToLaserPosOffsets(float trvOffset, float elvOffset)
		{
			Log.Trash("PowerStabControllerCommander.SetJumpToLaserPosOffsets(): trvOffset = " + trvOffset + ", elvOffset = " + elvOffset);

			const float factor = 0.3f;
			const float rad2deg = 180f / (float) Math.PI;

			float trvJmpAngle = trvOffset * factor * rad2deg;
			float elvJmpAngle = elvOffset * factor * rad2deg;

			_trvJmpAngle = trvJmpAngle;
			_elvJmpAngle = elvJmpAngle;
		}

		//------------------------------

		public void SetTrvValue(float value)
		{
			_trvCtrlAng = value + _trvJmpAngle; // use of jump offset according to ControlDispatcher::SendBallisticValues() in Remax code
			Log.Trash($"PowerStabControllerCommander.SetTrvValue(): Trv = {value}, Jump = {_trvJmpAngle}, CtrlAngle = {_trvCtrlAng}");
		}

		//------------------------------

		public void SetElvValue(float value)
		{
			_elvCtrlAng = -(value - _elvJmpAngle); // use of jump offset according to ControlDispatcher::SendBallisticValues() in Remax code
			Log.Trash($"PowerStabControllerCommander.SetElvValue(): Elv = {value}, Jump = {_elvJmpAngle}, CtrlAngle = {_elvCtrlAng}");
		}

		//------------------------------

		public float GetTrvValue()
		{
			return _trvAngle;
		}

		//------------------------------

		public float GetElvValue()
		{
			return _elvAngle;
		}

		//------------------------------

		public float GetTrvGyro()
		{
			return _trvGyro.GetRate();
		}

		//------------------------------

		public float GetElvGyro()
		{
			return _elvGyro.GetRate();
		}

		//#endregion

		//------------------------------

		//#region privateMethods

		void SetTurretAngles()
		{
			float rotation;

			Log.Disabled($"PowerStabCommanderController.SetTurretAngles()");

			rotation = CalculateRotation(_trvCtrlAng, ref _lastTrvCtrlAng, ref _trvStabRef, _trvGyro);
			if (_stabilization) Log.Trash($"PowerStabCommanderController.SetTurretAngles(): Trv rotation = {rotation}");
			_commanderModel.ApplyTrvRotation(rotation);

			rotation = CalculateRotation(_elvCtrlAng, ref _lastElvCtrlAng, ref _elvStabRef, _elvGyro);
			if (_stabilization) Log.Trash($"PowerStabCommanderController.SetTurretAngles(): Elv rotation = {rotation}");
			_commanderModel.ApplyElvRotation(rotation);
		}

		//------------------------------

		void GetTurretAngles()
		{
			_trvAngle = _commanderModel.GetTrvAngle();
			_elvAngle = _commanderModel.GetElvAngle();

			if (_elvAngle > 180)
				_elvAngle = _elvAngle - 360;

			if (encodersValuesChanged != null)
				encodersValuesChanged();
		}

		//------------------------------

		float CalculateRotation(float ctrlAng, ref float lastCtrlAng, ref float stabRef, IGyroModel gyro)
		{
			var gyroAngle = gyro.GetCurrentAngle();

			if (!_stabilization)
				stabRef = gyroAngle - lastCtrlAng;

			lastCtrlAng = ctrlAng;

			var stabAngle = gyroAngle - stabRef - ctrlAng;

			return -stabAngle;
		}

		//#endregion

		//------------------------------
	}
}
