﻿using System;
using Common;
using Models;
using Controllers;
using Managers;

namespace Controllers
{
	public class EnvironmentController : Controller
	{
		IEnvironmentModel _enviromentModel;
		
		//#region publicAttributes

		public UpdateValueDelegate daylightIntensityUpdate;
		public UpdateValueDelegate rainIntensityUpdate;
		public UpdateValueDelegate fogIntensityUpdate;

		//#endregion

		float _rain;
		float _fog;

		// used for restoring the light value for day camera
		float _configuredLight;
		float _currentFovAngle;
		float _farRainPosition;

		bool _rainEnabled;
	
		//-------------------------------------

		public EnvironmentController()
		{
			Log.Trace($"EnvironmentController.EnvironmentController()");

			_currentFovAngle = Common.Values.NarrowFov;

			_rainEnabled = false;
		}

		//-------------------------------------

		public override void SetModel(Models.IModel model)
		{
			Log.Trace($"EnvironmentController.SetModel({model})");

			_enviromentModel = RegisterModel<IEnvironmentModel>(model, this.UpdateModel, this.GetModelData);;
		}

		//-------------------------------------

		void UpdateModel()
		{
			//Log.Periodic($"EnvironmentController.UpdateModel(): _light = {_light}, _rain = {_rain}");

			_enviromentModel.SetDayLightValue (_configuredLight);
			_enviromentModel.SetRainValue (_rain);
			_enviromentModel.SetFogValue (_fog);
			_enviromentModel.SetFarRainPosition (_farRainPosition);
			_enviromentModel.EnableRain (_rainEnabled);
		}

		//-------------------------------------

		void GetModelData()
		{
		}

		//-------------------------------------

		public void SetDayLightValue(float value)
		{
			_configuredLight = value;
		
			if (daylightIntensityUpdate != null)
				daylightIntensityUpdate (_configuredLight);
		}

		//-------------------------------------

		public void SetRainValue(float value)
		{
			_rain = value;
			HandleRain();
			if (rainIntensityUpdate != null)
				rainIntensityUpdate (_rain);
		}

		//-------------------------------------

		public void SetFogValue(float value)
		{
			_fog = value;
			if (fogIntensityUpdate != null)
				fogIntensityUpdate (_fog);
		}

		//-------------------------------------

		public void ReceiveFovAngle(float value)
		{
			_currentFovAngle = value;

			HandleRain ();
		}

		//-------------------------------------

		void HandleRain()
		{
			if (_rain == 0f) {
				_rainEnabled = false;
				return;
			}

			_rainEnabled = true;

			const float minRainPos = 43.4f; //10.7f; //300;
			const float maxRainPos = 180f; //943;
			const float rainPosRange = maxRainPos - minRainPos;

			float zoomRange = Common.Values.WideFov - Common.Values.NarrowFov;

			_farRainPosition = (float)(((_currentFovAngle - Common.Values.NarrowFov) * rainPosRange) / zoomRange) + minRainPos;

			float temp = _farRainPosition - minRainPos;
			_farRainPosition = maxRainPos - temp;

			Log.Disabled ("EnviormentController: _currentFovAngle: " + _currentFovAngle + " | rain position: " + _farRainPosition);
		}
	}
}



