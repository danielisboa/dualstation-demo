﻿using System;
using Models;
using Controllers;
using Managers;
using Common;
using System.Collections.Generic;

namespace Controllers
{
	public class WeaponController : Controller
	{
		IWeaponModel _weaponModel;

		public UpdateEventDelegate WeaponCtrlrUpdateClbk;

		bool _fire;
		bool _fireCmd;
		bool _fireJammed;
		uint _shotsFired;
		uint _burstLength;
		uint _ammoAmountToLoad;
		uint _ammoAmount;

		uint _totalShotsFired;

		float _nextFireTime;
		float _fireInterval;
		float _fireRate;

		bool _outFiringZone;

		//--------------------------

		public WeaponController()
		{
		}

		//--------------------------

		public override void SetModel(Models.IModel model)
		{
			Log.Trace($"WeaponController.SetModel({model})");
			_weaponModel = RegisterModel<IWeaponModel>(model, this.UpdateModel, this.GetModelData);
			_fireRate = _weaponModel.GetFireRate(); // rounds per minute
			_fireInterval = 1f / (_fireRate / 60f);
			Log.Info("WeaponController.SetModel(): Fire rate = " + _fireRate + ", fire interval = " + _fireInterval);
		}

		//--------------------------

		void UpdateModel()
		{
			if (_outFiringZone)
				return;

			Log.Disabled($"WeaponController.UpdateModel(): _fire = {_fire}, _fireCmd = {_fireCmd}, _shotsFired = {_shotsFired}, _burstLength = {_burstLength}, _ammoAmount = {_ammoAmount}, time = {_weaponModel.GetTime()}, _nextFireTime = {_nextFireTime}");

			var currTime = _weaponModel.GetTime();

			if (!_fire)
				_shotsFired = 0;
			else if (_nextFireTime == 0)
				_nextFireTime = currTime;

			if (_shotsFired >= _burstLength || _ammoAmount <= 0)
			{
				if (_fire) Log.Disabled("WeaponController.UpdateModel(): Disabling fire, _shotsFired = " + _shotsFired + ", _burstLength = " + _burstLength + ", _ammoAmount = " + _ammoAmount);
				_fire = false;
			}

			if ((currTime >= _nextFireTime) && _fire)
			{
//				_nextFireTime = _weaponModel.GetTime() + _fireInterval;
				_nextFireTime += _fireInterval;

				_weaponModel.Fire();
				_totalShotsFired++;
				_shotsFired++;
				_ammoAmount--;

				Log.Info("WeaponController.Fire(): Fire event at " + currTime);
				Log.Debug("WeaponController.Fire(): Next fire at time " + _nextFireTime);
			}

			_fire = _fireCmd;
		}

		//--------------------------

		void GetModelData()
		{
			if (WeaponCtrlrUpdateClbk != null)
				WeaponCtrlrUpdateClbk ();
		}

		//------------------------------

		public void SelectBullet(int index)
		{
			if (_weaponModel != null)
				_weaponModel.SelectBullet(index);
		}

		//--------------------------

		public void SetTracerBullet(int interval)
		{
			if (_weaponModel != null)
				_weaponModel.SetTracerBullet(interval);
		}

		//------------------------------

		public void SetWindSpeed(float windSpeed)
		{
			if (_weaponModel != null)
				_weaponModel.SetWindSpeed(windSpeed);
		}

		//--------------------------

		public void Fire(bool cmd)
		{
			if (_fireJammed)
				cmd = false;

			_fireCmd = cmd;
			if (_fireCmd)
			{
				_nextFireTime = 0;
				_fire = true;
			}

			Log.Debug("WeaponController.Fire(): _fireCmd = " + _fireCmd + ", _fire = " + _fire);
		}

		//--------------------------

		public void SetBurstLength(uint value)
		{
			_burstLength = value;
		}

		//--------------------------

		public void SetAmmunitionAmount(uint value)
		{
			_ammoAmountToLoad = value;
		}

		//--------------------------

		public void SetFireJammed()
		{
			Log.Info("WeaponController.SetFireJammed(): Fire jammed");
			_fire = _fireCmd = false;
			_fireJammed = true;
		}

		//------------------------

		public void ReloadWeapon()
		{
			if (_fireJammed && _ammoAmount > 0)
			{
				Log.Info("WeaponController.ReloadWeapon(): Fire jammed, clearing");
				_ammoAmount--;
			}

			_fire = _fireCmd = _fireJammed = false;

			if (_ammoAmountToLoad == 0)
				return;

			_ammoAmount = _ammoAmountToLoad;
			_ammoAmountToLoad = 0;

			if (_ammoAmount > 0)
				Log.Info("WeaponController.ReloadWeapon(): _ammoAmount = " + _ammoAmount);
		}

		//------------------------

		public uint GetAmmoAmmount()
		{
			return _ammoAmount;
		}

		//------------------------

		public uint GetShotsFired()
		{
			return _totalShotsFired;
		}

		//------------------------

		public void SetFiringZone(bool flag)
		{
			_outFiringZone = flag;
		}

		//------------------------
	}
}

