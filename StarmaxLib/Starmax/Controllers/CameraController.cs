﻿using System;
using Common;
using Infrastructure;

namespace Controllers
{
	public class CameraController
	{
		private float _narrowFov;
		private float _wideFov;
		private float _veryWideFov;
		private float _fovRange;
		private float _minZoom;
		private float _maxZoom;

		private float _zoomSetPoint;

		private float _zoomValue;
		private float _focusSetPoint;
		private float _focusCurrentPosition;

		private AutoManualMode _focusMode;
		private InOutStopCmd _focusCmd;
		private FOVCmds _fovCmd;

		private AutoManualMode _irisMode;
		private float _irisValeu;
		private Polarity _polarity;

		private float _reticleX;
		private float _reticleY;

		AutoManualMode _gammaMode;
		InOutStopCmd _gammaCmd;
		AutoManualMode _levelMode;
		InOutStopCmd _levelCmd;
		AutoManualMode _gainMode;
		InOutStopCmd _gainCmd;
		bool _nucCmd;
		FieldOfView _fov;

		float _modelFeedbackFov;

		float _gammaValue;
		float _levelValue;
		float _gainValue;

		// focus speed in the camera model should be 0.6f
		const float manualFocusStep = 0.10f;
		const float manualZoomStep = 0.07f;

		const float irisMin = 0f;
		const float irisMax = 30f;
		const float irisIdeal = 15f;
		const float irisStep  = 3f;

		//------------------------------------------------

		public CameraController (float narrowFov, float wideFov, float veryWideFov, float minZoom, float maxZoom)
		{
			_narrowFov = narrowFov;
			_wideFov = wideFov;
			_veryWideFov = veryWideFov;

			_minZoom = minZoom;
			_maxZoom = maxZoom;

			// _maxZoom is the smaller value
			_fovRange = _minZoom - _maxZoom;

			_zoomSetPoint = _narrowFov;

			_focusMode = AutoManualMode.Auto;
			_focusCmd = InOutStopCmd.Stop;
			_fovCmd = FOVCmds.Stop;

			_irisMode = AutoManualMode.Auto;
			_polarity = Polarity.WhiteHot;

			_irisValeu = irisIdeal;
			_fov = FieldOfView.Narrow;

			_gammaValue = 0;
			_levelValue = 0;
			_gainValue = 0;
		}

		//------------------------------------------------

		public void Update()
		{
			AdustFOV();
			AdjustFocusSetPoint();
		}

		//------------------------------------------------

		public float GetZoomSetPoint()
		{
			return _zoomSetPoint;
		}

		//------------------------------------------------

		public float GetZoomPercent()
		{
			// max zoom has the minor value
			var percent = 1f - ((_modelFeedbackFov - _maxZoom) / _fovRange);
			return MathHelper.Clamp(percent, 0f, 1f);
		}

		//------------------------------------------------

		public float GetFocusSetPoint()
		{
			return _focusSetPoint;
		}

		//------------------------------------------------

		public void SetFocusMode(AutoManualMode mode)
		{
			_focusMode = mode;

			if (_focusMode == AutoManualMode.Auto)
				_focusCmd = InOutStopCmd.Stop;

			Log.Disabled("FOCUS MODE: " + _focusMode);
		}

		//------------------------------------------------

		public void SetFocusCmd(InOutStopCmd cmd)
		{
			if (_focusMode == AutoManualMode.Auto)
				return;

			_focusCmd = cmd;
		}

		//------------------------------------------------

		public void SetFovCmd(FOVCmds cmd)
		{
			_fovCmd = cmd;
		}

		//------------------------------------------------

		float AdjustFocusSetPoint()
		{
			switch (_focusCmd)
			{
				case InOutStopCmd.In:
					_focusCurrentPosition += manualFocusStep;
					break;

				case InOutStopCmd.Out:
					_focusCurrentPosition -= manualFocusStep;
					break;
			}

			if (_focusMode == AutoManualMode.Auto)
				_focusCurrentPosition = _zoomValue;

			_focusCurrentPosition = MathHelper.Clamp(_focusCurrentPosition, 0f, 1f);
			_focusSetPoint = Math.Abs(_zoomValue - _focusCurrentPosition);

			Log.Disabled($"CameraController.AdjustFocusSetPoint(): _focusCurrentPosition = {_focusCurrentPosition}, _zoomValue = {_zoomValue}, _focusSetPoint = {_focusSetPoint}");

			return _focusSetPoint;
		}

		//------------------------------------------------

		void AdustFOV()
		{
			switch (_fovCmd)
			{
			case FOVCmds.Out:
				_zoomSetPoint = _zoomSetPoint + manualZoomStep;
				break;

			case FOVCmds.In:
				_zoomSetPoint = _zoomSetPoint - manualZoomStep;
				break;

			case FOVCmds.Narrow:
				_zoomSetPoint = _narrowFov;
				break;

			case FOVCmds.Wide:
				_zoomSetPoint = _wideFov;
				break;

			case FOVCmds.VeryWide:
				_zoomSetPoint = _veryWideFov;
				break;
			}

			float diffToVeryWide = _zoomSetPoint - _veryWideFov;
			float diffToWide     = _zoomSetPoint - _wideFov;
			float diffToNarrow   = _zoomSetPoint - _narrowFov;
			const float MaxDiff  = 0.05f;

			if (diffToNarrow <= MaxDiff)
				_fov = FieldOfView.Narrow;
			else if (diffToWide <= MaxDiff)
				_fov = FieldOfView.Wide;
			else if (diffToVeryWide <= MaxDiff)
				_fov = FieldOfView.VeryWide;
			else
				_fov = FieldOfView.VeryWide; // TODO: IMPLEMENT VERY VERY WIDE FOV SUPPORT

			/*
			if (diffToWide > MaxDiff)
				_fov = FieldOfView.VeryWide;
			else if (diffToNarrow > MaxDiff)
				_fov = FieldOfView.Wide;
			else
				_fov = FieldOfView.Narrow;
			 */

			bool uncalibratedFov = (Math.Abs(diffToWide) > MaxDiff) && (Math.Abs(diffToNarrow) > MaxDiff);

			// maxZoom is the smallest value
			_zoomSetPoint = MathHelper.Clamp(_zoomSetPoint, _maxZoom, _minZoom);
			_zoomValue = GetZoomPercent();
		}

		//------------------------------------------------

		public void SetIrisMode(AutoManualMode mode)
		{
			_irisMode = mode;

			if (_irisMode == AutoManualMode.Auto)
				_irisValeu = irisIdeal;
		}

		//------------------------------------------------

		public float GetIrisValue()
		{
			var scale = 2f * ((_irisValeu - 1f) / (30f - 1f)) - 1;
			return scale;
		}

		//------------------------------------------------

		public void SetPolarityCmd(Polarity cmd)
		{
			_polarity = cmd;
		}

		//------------------------------------------------

		public Polarity GetPolarityCmd()
		{
			return _polarity;
		}

		//------------------------------------------------

		public void SetReticlePosition(float x, float y)
		{
			_reticleX = x;
			_reticleY = y;
		}

		//------------------------------------------------

		public float GetReticleX()
		{
			return _reticleX;
		}

		//------------------------------------------------

		public float GetReticleY()
		{
			return _reticleY;
		}

		//------------------------------------------------

		public void SetIrisCmd(InOutStopCmd cmd)
		{
			if (_irisMode == AutoManualMode.Auto) {
				_irisValeu = irisIdeal;
				return;
			}

			switch (cmd)
			{
			case InOutStopCmd.In:
				_irisValeu += irisStep;
			break;
			case InOutStopCmd.Out:
				_irisValeu -= irisStep;
				break;
			default:
				break;
			}

			if (_irisValeu < irisMin)
				_irisValeu = irisMin;

			if (_irisValeu > irisMax)
				_irisValeu = irisMax;
		}

		//----------------------------------------

		public void SetGammaMode(AutoManualMode mode)
		{
			_gammaMode = mode;
		}

		//----------------------------------------

		public void SetGammaCmd(InOutStopCmd cmd)
		{
			_gammaCmd = cmd;
		}

		//----------------------------------------

		public void SetLevelMode(AutoManualMode mode)
		{
			_levelMode = mode;
		}

		//----------------------------------------

		public void SetLevelCmd(InOutStopCmd cmd)
		{
			_levelCmd = cmd;
		}

		//----------------------------------------

		public void SetGainMode(AutoManualMode mode)
		{
			_gainMode = mode;
		}

		//----------------------------------------

		public void SetGainCmd(InOutStopCmd cmd)
		{
			_gainCmd = cmd;
		}

		//----------------------------------------

		public void SetNucCmd(bool cmd)
		{
			_nucCmd = cmd;
		}

		//----------------------------------------

		public bool GetNucCmd()
		{
			return _nucCmd;
		}

		//----------------------------------------

		public float GetGammaValue()
		{
			return _gammaValue;
		}

		//----------------------------------------

		public float GetLevelValue()
		{
			return _levelValue;
		}

		//----------------------------------------

		public float GetGainValue()
		{
			return _gainValue;
		}

		//----------------------------------------

		public sbyte GetFov()
		{
			return (sbyte)_fov;
		}

		//----------------------------------------

		public bool IsCalibratedFov()
		{
			return false;
		}

		//----------------------------------------

		public void SetModelFeedbackFov(float fov)
		{
			_modelFeedbackFov = fov;
		}

		//----------------------------------------

		public float GetCurrentFovAngle()
		{
			return _modelFeedbackFov;
		}

		//----------------------------------------

	}
}




