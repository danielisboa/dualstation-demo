﻿using System;
using Models;
using Controllers;
using Common;

namespace Managers
{
	public class TurretManager
	{
		//#region  publicAttributes

		public UpdateCounterDelegate laserCountUpdate;
		public UpdateCounterDelegate fireCountUpdate;
		public UpdateValueDelegate fovUpdate;

		//#endregion

		//------------------------------

		//#region  privateAttributes
		
		PowerStabController _powerStabController;
		
		PowerStabCommanderController _powerStabControllerCMDR;		
		OptronicsController _optronicsController;
		OptronicsController _optronicsControllerCMDR;
		WeaponController _weaponController;

		WSCommunicationManager _wsCommunicationManager;

		Camera _currentCamera;
		Camera _currentCameraCMDR;
		bool _measureRangeCmd;

		uint _laserCount;
		uint _fireCount;

		float _trvPos;
		float _elvPos;
		float _trvGyro;
		float _elvGyro;
		// CMDR
		float _trvPosCMDR;
		float _elvPosCMDR;
		float _trvGyroCMDR;
		float _elvGyroCMDR;

		float _windSpeed;

		float _range;

		//#endregion

		//-------------------------------------

		//#region  publicMethods

		public TurretManager(WSCommunicationManager wsCommunicationManager)
		{
			Log.Trace($"TurretManager.TurretManager()");

			_wsCommunicationManager = wsCommunicationManager;
			_wsCommunicationManager.SetEncoderSimulation(true);

			_powerStabController = new PowerStabController();
			_powerStabControllerCMDR = new PowerStabCommanderController();
	
			_optronicsController = new OptronicsController();
			_optronicsControllerCMDR = new OptronicsController();
			_weaponController = new WeaponController();

			_wsCommunicationManager.wsStatusReceived += this.GetWsStatusData;
			_wsCommunicationManager.wsFastStatusReceived += this.GetWsFastStatusData;
			_wsCommunicationManager.wsCmdReceived += this.GetWsCmdData;

			_powerStabController.encodersValuesChanged += this.GetEncodersData;
			_powerStabControllerCMDR.encodersValuesChanged += this.GetEncodersData;
			_optronicsController.OptronicsUpdateClbk += this.GetOptronicsUpdate;
			_weaponController.WeaponCtrlrUpdateClbk += this.GetWeaponUpdate;

			_currentCamera = Camera.Day;
			_currentCameraCMDR = Camera.Day;
		}

		//-------------------------------------

		~TurretManager()
		{
			_wsCommunicationManager.SetEncoderSimulation(false);
		}

		//-------------------------------------

		public void SetTurretModel(IModel model)
		{
			Log.Trace($"TurretManager.SetTurretModel({model})");

			ITurretModel turretModel = model as ITurretModel;

			if (turretModel == null)
				throw new Exception("Invalid TurretModel");

			var weaponType = _wsCommunicationManager.GetWeaponType();

			_powerStabController.SetModel(turretModel);
			_optronicsController.SetModel(turretModel.GetOptronicsModel());				
			_weaponController.SetModel(turretModel.GetWeaponModel(weaponType));
			_weaponController.SetTracerBullet(5);
			//CMDR
			_optronicsControllerCMDR.SetModel(turretModel.GetOptronicsModelCMDR());			
			_powerStabControllerCMDR.SetModel(turretModel.GetPodCommanderModel());			
		}

		//---------------------------------------

		public void SetAmmunitionAmount(uint value, bool reload = false)
		{
			_weaponController.SetAmmunitionAmount (value);
			if (reload) _weaponController.ReloadWeapon();
		}

		//---------------------------------------

		public void SetFireJammed()
		{
			_weaponController.SetFireJammed();
		}

		//---------------------------------------

		public void SetWindSpeed(float value)
		{
			_windSpeed = value;
			SetWindSpeed();
		}

		//---------------------------------------

		public void SetLrfEnabled(bool enabled)
		{
			_optronicsController.SetLrfEnabled(enabled);
		}

		//#endregion

		//-------------------------------------

		//#region  privateMethods

		void GetEncodersData()
		{
			_trvPos = _powerStabController.GetTrvValue();
			_elvPos = _powerStabController.GetElvValue();

			_trvGyro = _powerStabController.GetTrvGyro();
			_elvGyro = _powerStabController.GetElvGyro();

			// CMDR
			_trvPosCMDR = _powerStabControllerCMDR.GetTrvValue();			
			_elvPosCMDR = _powerStabControllerCMDR.GetElvValue();

			_trvGyroCMDR = _powerStabControllerCMDR.GetTrvGyro();
			_elvGyroCMDR = _powerStabControllerCMDR.GetElvGyro();
			
			_wsCommunicationManager.UpdateEncoderValues(_trvPos, _elvPos);
			_wsCommunicationManager.UpdateEncoderValuesCMDR(_trvPosCMDR, _elvPosCMDR);
			
			_wsCommunicationManager.UpdateGyroValues(_trvGyro, _elvGyro);
			_wsCommunicationManager.UpdateGyroValuesCMDR(_trvGyroCMDR, _elvGyroCMDR);

			SetWindSpeed();
		}

		//-------------------------------------

		void GetWsStatusData()
		{
			Log.Disabled("TurretManager.GetWsStatusData()");
			// get status data from WsCommManager
			var opMode = _wsCommunicationManager.GetOpMode ();
			var opModeCMDR = _wsCommunicationManager.GetOpModeCMDR ();
			var activeCamera = _wsCommunicationManager.GetActiveCamera ();
			var activeCameraCMDR = _wsCommunicationManager.GetActiveCameraCMDR ();

			float reticlePositionX, reticlePositionY;
			_wsCommunicationManager.GetReticlePos (out reticlePositionX, out reticlePositionY);

			short minRange, maxRange;
			_wsCommunicationManager.GetMinMaxRange (out minRange, out maxRange);

			_range = _wsCommunicationManager.GetRange();

			var focusMode = _wsCommunicationManager.GetFocusMode();

			//TODO: forward do relevant packages
			var outFiringZone = _wsCommunicationManager.GetOutFiringZone ();
			_weaponController.SetFiringZone (outFiringZone);
//			var armSw = _wsCommunicationManager.GetArmSw ();
//			var safeSw = _wsCommunicationManager.GetSafeSw ();
//			var overrideSw = _wsCommunicationManager.GetOverrideSw ();

			float trvLrfJmp, elvLrfJmp, trvLrfJmpCMDR, elvLrfJmpCMDR;
			_wsCommunicationManager.GetLrfJmpOffsets(out trvLrfJmp, out elvLrfJmp);
			_wsCommunicationManager.GetLrfJmpOffsetsCMDR(out trvLrfJmpCMDR, out elvLrfJmpCMDR);

			Log.Trash("TurretManager.GetWsStatusData(): TrvJump = " + trvLrfJmp + ", ElvJump = " + elvLrfJmp + ", TrvJumpCMDR = " + trvLrfJmpCMDR + ", ElvJumpCMDR = " + elvLrfJmpCMDR);
			_powerStabController.SetJumpToLaserPosOffsets(trvLrfJmp, elvLrfJmp);
			_powerStabControllerCMDR.SetJumpToLaserPosOffsets(trvLrfJmpCMDR, elvLrfJmpCMDR);

			// set relevant data to PowerStabController
			_powerStabController.SetMode (opMode);
			_powerStabControllerCMDR.SetMode (opModeCMDR);

			_optronicsController.SetActiveCamera (activeCamera);
			_optronicsControllerCMDR.SetActiveCamera(activeCameraCMDR);

			// CMDR
			if (_currentCameraCMDR != activeCameraCMDR)				
				_currentCameraCMDR = activeCameraCMDR;
						
			// gunner
			if (_currentCamera != activeCamera)
			{
				_currentCamera = activeCamera;

				if (activeCamera != Camera.Thermal)
				{
					_weaponController.SelectBullet(0);
					_weaponController.SetTracerBullet(3);
				}
				else
				{
					_weaponController.SelectBullet(1);
					_weaponController.SetTracerBullet(1);
				}
			}
			_optronicsController.SetReticlePositionXY (reticlePositionX, reticlePositionY);
			_optronicsController.SetRangeLimits (minRange, maxRange);
			_optronicsController.SetFocusMode(focusMode);
		}

		//-------------------------------------

		void GetWsFastStatusData()
		{
			Log.Print(Log.Level.Disabled, "TurretManager.GetWsFastStatusData()");
			// get status data from WsCommManager
			float trvEncVal, elvEncVal;
			_wsCommunicationManager.GetEncodersValues(out trvEncVal, out elvEncVal);
			// set relevant data to PowerStabController
			Log.Trash("TurretManager.GetWsFastStatusData(): Trv = " + trvEncVal + ", Elv = " + elvEncVal);
			_powerStabController.SetTrvValue(trvEncVal);			
			_powerStabController.SetElvValue(elvEncVal);

			// Commander
			float trvEncValCMDR, elvEncValCMDR;
			_wsCommunicationManager.GetEncodersCommanderValues(out trvEncValCMDR, out elvEncValCMDR);
			Log.Trash("TurretManager.GetWsFastStatusData(): Trv = " + trvEncValCMDR + ", Elv = " + elvEncValCMDR);
			_powerStabControllerCMDR.SetTrvValue(trvEncValCMDR);
			_powerStabControllerCMDR.SetElvValue(elvEncValCMDR);		
		}

		//-------------------------------------

		void GetWsCmdData()
		{
			Log.Disabled("TurretManager.GetWsCmdData()");

			_measureRangeCmd = _wsCommunicationManager.GetMeasureRangeCmd();
			var fireCmd = _wsCommunicationManager.GetFireCmd();
			var jumpToLaserPosCmd = _wsCommunicationManager.GetJumpToLaserPosCmd();
			var burstLength = _wsCommunicationManager.GetBurstLength();
			var polarityMode = _wsCommunicationManager.GetPolarityMode();
			var reloadWeapon = _wsCommunicationManager.GetReloadWeapon();
			var focusCmd = _wsCommunicationManager.GetFocusCmd();
			var focusCmdCMDR = _wsCommunicationManager.GetFocusCmdCMDR();
			var irisMode = _wsCommunicationManager.GetIrisMode();
			var irisCmd = _wsCommunicationManager.GetIrisCmd();
			var gammaMode = _wsCommunicationManager.GetGammaMode();
			var gammaCmd = _wsCommunicationManager.GetGammaCmd();
			var levelMode = _wsCommunicationManager.GetLevelMode();
			var levelCmd = _wsCommunicationManager.GetLevelCmd();
			var gainMode = _wsCommunicationManager.GetGainMode();
			var gainCmd = _wsCommunicationManager.GetGainCmd();
			var nucCmd = _wsCommunicationManager.GetNucCmd();
			var fovCmd = _wsCommunicationManager.GetFovCmd();
			var fovCmdCMDR = _wsCommunicationManager.GetFovCmdCMDR();
			var enableReticle = _wsCommunicationManager.GetEnableReticleCmd();

			Log.Disabled("TurretManager.GetWsCmdData(): fireCmd = " + fireCmd + ", measureRangeCmd = " + _measureRangeCmd);

			_optronicsController.MeasureRangeCmd(_measureRangeCmd);
			_optronicsController.SetFocusCmd(focusCmd);
			_optronicsController.SetPolarityMode(polarityMode);
			_optronicsController.SetIrisCmd(irisCmd);
			_optronicsController.SetIrisMode(irisMode);
			_optronicsController.SetGammaMode(gammaMode);
			_optronicsController.SetGammaCmd(gammaCmd);
			_optronicsController.SetLevelMode(levelMode);
			_optronicsController.SetLevelCmd(levelCmd);
			_optronicsController.SetGainMode(gainMode);
			_optronicsController.SetGainCmd(gainCmd);
			_optronicsController.SetNucCmd(nucCmd);
			_optronicsController.SetFovCmd(fovCmd);
			_optronicsController.EnableReticle(enableReticle);

			// CMDR Pod
			_optronicsControllerCMDR.SetFovCmd(fovCmdCMDR);
			_optronicsControllerCMDR.SetFocusCmd(focusCmdCMDR);

			_weaponController.Fire(fireCmd);
			_weaponController.SetBurstLength(burstLength);

			// if (jumpToLaserPosCmd)
			// 	_powerStabController.SetJumpToLaserPos(jumpToLaserPosCmd, _range);

			if (reloadWeapon)
				_weaponController.ReloadWeapon();
		}

		//--------------------------------------

		void GetOptronicsUpdate()
		{
			float fovVertAngle;
			float zoomPercent;
			sbyte fov;
			bool uncalibratedFov;

			_optronicsController.GetFovAngle(out fovVertAngle, out zoomPercent, out fov, out uncalibratedFov);

			if (fovUpdate != null) {
				Log.Disabled ("OptronicsUpdate:: Calling Delegate for FOV update");
				fovUpdate (fovVertAngle);
			}

			_wsCommunicationManager.UpdateFovValues(fovVertAngle, zoomPercent, fov, uncalibratedFov);

			float range;
			byte freshRange;

			_optronicsController.GetRange(out range, out freshRange);

			Log.Periodic("TurretManager:: Range: " + range + " - Fresh: " + freshRange);

			_wsCommunicationManager.UpdateMeasuredDistance(range, freshRange);
			_wsCommunicationManager.UpdatePolarityMode (_optronicsController.GetPolarityMode());

			var laserCount = _optronicsController.GetNumRangeRequests();

			if (laserCountUpdate != null && laserCount != _laserCount)
				laserCountUpdate(laserCount);

			_laserCount = laserCount;
		}

		//---------------------------------------

		void GetWeaponUpdate()
		{
			_wsCommunicationManager.UpdateAmmunitionCount(_weaponController.GetAmmoAmmount());

			var fireCount = _weaponController.GetShotsFired();

			if (fireCountUpdate != null && fireCount != _fireCount)
				fireCountUpdate(fireCount);

			_fireCount = fireCount;
		}

		//---------------------------------------

		void SetWindSpeed()
		{
			const double DegToRad = Math.PI / 180.0;
			float windSpeed = _windSpeed * (float) Math.Cos(_trvPos * DegToRad);
			_weaponController.SetWindSpeed(windSpeed);
		}

		//---------------------------------------

		//#endregion
	}
}

