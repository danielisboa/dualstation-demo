﻿using System;
using Models;
using Common;

namespace Controllers
{
	public class VehicleController : Controller
	{
		//#region publicAttributes

		public UpdateValueDelegate vehicleVelocityUpdate;
		public PositionUpdateDelegate positionUpdate;

		//#endregion

		//------------------------------

		//#region privateAttributes

		IVehicleModel _model;

		//#endregion

		//------------------------------

		//#region publicMethods

		public VehicleController()
		{
			Log.Trace($"VehicleController.VehicleController()");
		}

		//------------------------------

		public override void SetModel(IModel model)
		{
			Log.Trace($"VehicleController.SetModel({model})");
			_model = RegisterModel<IVehicleModel>(model, null, this.UpdateVehiclePos);
		}

		//------------------------------

		public void SetVehiclePosition(IGameObject reference)
		{
			Log.Trace($"VehicleController.SetVehiclePosition({reference})");
			if (_model != null) _model.SetVehiclePosition(reference);
		}

		//------------------------------

		public void SetDriveVelocity(float velocity)
		{
			Log.Trace($"VehicleController.SetDriveVelocity({velocity})");
			if (_model != null) _model.SetDriveVelocity(velocity);
		}

		//------------------------------

		public void SetTrajectory(IGameObject trajectory)
		{
			Log.Trace($"VehicleController.SetTrajectory({trajectory})");
			if (_model != null) _model.SetTrajectory(trajectory);
		}

		//------------------------------

		public void SetHeadlightsOn(bool headlightsOn)
		{
			Log.Trace($"VehicleController.SetHeadlightsOn({headlightsOn})");
			if (_model != null) _model.SetHeadlights(headlightsOn);
		}

		//------------------------------

		public void Drive()
		{
			Log.Trace($"VehicleController.Drive()");

			if (_model == null)
				return;

			_model.Drive();

			if (vehicleVelocityUpdate != null)
				vehicleVelocityUpdate(GetDriveVelocity());
		}

		//------------------------------

		public void Stop()
		{
			Log.Trace($"VehicleController.Stop()");

			if (_model == null)
				return;

			_model.Stop();

			if (vehicleVelocityUpdate != null)
				vehicleVelocityUpdate(0f);
		}

		//------------------------------

		public float GetDriveVelocity()
		{
			Log.Trace($"VehicleController.GetDriveVelocity()");
			if (_model == null) return 0f;
			return _model.GetDriveVelocity();
		}

		//#endregion

		//------------------------------

		//#region privateMethods

		~VehicleController()
		{
			Log.Trace($"VehicleController.~VehicleController()");
			if (_model != null) _model.Stop();
		}

		//------------------------------

		void UpdateVehiclePos()
		{
			float x, y, dir;
			_model.GetPos(out x, out y, out dir);

			if (positionUpdate != null)
				positionUpdate(x, y, dir);
		}

		//#endregion
	}
}

