﻿using System;
using Models;
using Common;

namespace Controllers
{
	public class TargetController : Controller
	{
		//#region publicTypes

		public delegate void TargetUpdateDelegate(TargetController controller);

		//#endregion

		//------------------------------

		//#region publicAttributes

		public TargetUpdateDelegate targetKilled;
		public TargetUpdateDelegate targetHit;

		//#endregion

		//------------------------------

		//#region privateAttributes

		ITargetModel _model;
		uint _maxHits;
		bool _killed;

		static uint _targetCount;
		uint _targetIndex;

		uint _hitsCount;
		uint _lastCount;

		bool _toEnable;
		bool _toDisable;
		bool _toRaise;
		bool _toDrop;
		bool _imediate;

		//#endregion

		//------------------------------

		//#region publicMethods

		public TargetController()
		{
			_targetIndex = _targetCount++;
			Log.Trace($"TargetController({_targetIndex}).TargetController()");
		}

		//------------------------------

		public override void SetModel(Models.IModel targetModel)
		{
			Log.Trace($"TargetController({_targetIndex}).SetModel({targetModel})");
			_model = RegisterModel<ITargetModel>(targetModel, this.SetTargetState, this.GetTargetHitsCount);
		}

		//------------------------------

		public void SetEnabled(bool enabled)
		{
			Log.Trace($"TargetController({_targetIndex}).SetEnabled({enabled})");

			_toEnable = enabled;
			_toDisable = !enabled;

			if (_toDisable) Reset();
		}

		//------------------------------

		public void SetDeathHitCount(uint count)
		{
			Log.Trace($"TargetController({_targetIndex}).SetDeathHitCount({count})");
			_maxHits = count;
		}

		//------------------------------

		public uint GetDeathHitCount()
		{
			return _maxHits;
		}

		//------------------------------

		public uint GetNewHitsCount()
		{
			var count = _hitsCount - _lastCount;
			_lastCount = _hitsCount;
			return count;
		}

		//------------------------------

		public uint GetHitsCount()
		{
			return _hitsCount;
		}

		//------------------------------

		public void Reset()
		{
			Log.Trace($"TargetController({_targetIndex}).Reset()");

			_killed = false;
			_model.ResetHitsCount();
		}

		//------------------------------

		public void Drop(bool imediate = false)
		{
			Log.Trace($"TargetController({_targetIndex}).Drop({imediate})");

			if (_model.IsMobile())
				return;

			_toDrop = true;
			_toRaise = false;
			_imediate = imediate;
		}

		//------------------------------

		public void Raise(bool imediate = false)
		{
			Log.Trace($"TargetController({_targetIndex}).Raise({imediate})");

			if (_killed) return;
			_toRaise = true;
			_toDrop = false;
			_imediate = imediate;
		}

		//------------------------------

		public bool IsTargetMobile()
		{
			return _model.IsMobile();
		}

		//------------------------------

		public bool IsTargetDropped()
		{
			return _model.IsDropped();
		}

		//------------------------------

		public bool IsTargetDead()
		{
			return _killed;
		}

		//#endregion

		//------------------------------

		//#region privateMethods

		void SetTargetState()
		{
			if (_toEnable)
				_model.SetEnabled(true);

			if (_toRaise)
				_model.Raise(_imediate);

			if (_toDrop)
				_model.Drop(_imediate);

			if (_toDisable)
				_model.SetEnabled(false);

			_toDisable = _toEnable = _toRaise = _toDrop = false;
		}

		//------------------------------

		void GetTargetHitsCount()
		{
			_hitsCount = _model.GetHitsCount();

			if (_model.IsHit())
			{
				Log.Info($"TargetController({_targetIndex}).GetTargetHitsCount(): target hit, hit count = {_hitsCount}");
				if (targetHit != null) targetHit(this);
				_model.SetHitIndication(.25f);
			}

			if (_maxHits > 0 && _hitsCount >= _maxHits)
			{
				Log.Info($"TargetController({_targetIndex}).GetTargetHitsCount(): target killed, hit count = {_hitsCount}");
				if (!_killed && targetKilled != null) targetKilled(this);
				_killed = true;
				_model.Drop();
			}
		}

		//#endregion
	}
}
