﻿using System;
using System.Threading;
using System.Collections.Generic;
using Controllers;
using Models;
using Common;

using Timer = System.Threading.Timer;

namespace Managers
{
	public class TargetManager : IControlUnit
	{
		#region  privateTypes

		delegate void TargetOperation(TargetController controller);

		#endregion

		//------------------------------

		//#region  publicAttributes

		public UpdateCounterDelegate hitsCountUpdate;
		public UpdateCounterDelegate killCountUpdate;
		public UpdateCounterDelegate totalTargetsUpdate;
		public UpdateEventDelegate   targetsCleared;
		public UpdateEventDelegate   trainingStarted;
		public UpdateEventDelegate   trainingEnded;

		//#endregion

		//------------------------------

		//#region  privateAttributes

		List<TargetController> _mobileTargets;
		List<TargetController> _unselectedTargets;
		List<TargetController> _selectedTargets;

		uint _numTargets;
		uint _numSimultaneous;
		uint _numUsedTargets;

		uint _hitsToKill;

		bool _popupEnabled;
		float _popupTime;

		Timer _popupTimer;

		uint _totalHits;
		uint _totalKills;

		//#endregion

		//------------------------------

		//#region  publicMethods

		public TargetManager()
		{
			Log.Trace("TargetManager.TargetManager()");
			_popupTimer = new Timer(this.TimerCallback);
		}

		//------------------------------

		public void Start(float delay = -1)
		{
			Log.Trace("TargetManager.Start()");
			SetupPopupTimer();
			SelectTargets();

			if (trainingStarted != null)
				trainingStarted();
		}

		//------------------------------

		public void Pause(bool pause)
		{
			int timeMs = (pause) ? Timeout.Infinite : (int) (_popupTime * 1000);
			SetupPopupTimer(timeMs);
		}

		//------------------------------

		public void Stop()
		{
			Log.Trace("TargetManager.Stop()");
			_popupTimer.Change(Timeout.Infinite, Timeout.Infinite);
			_popupTimer = null;
			DropAll();

			if (trainingEnded != null)
				trainingEnded();
		}

		//------------------------------

		public void SetTargets(uint numTargets, List<IModel> targetModels)
		{
			Log.Trace($"TargetManager.SetTargets({numTargets}, {targetModels})");

			_selectedTargets = null;
			_unselectedTargets = new List<TargetController>();
			_mobileTargets = new List<TargetController>();

			if (numTargets == 0) numTargets = (uint) targetModels.Count;

			Log.Debug("TargetManager.SetTargets(): numTargets = " + numTargets + ", targetModels.Count = " + targetModels.Count);

			_numTargets = (uint) Math.Min(numTargets, targetModels.Count);

			Log.Debug("TargetManager.SetTargets(): _numTargets = " + _numTargets);

			foreach (var model in targetModels)
			{
				var controller = new TargetController();
				controller.targetKilled += this.RemoveKilledTarget;
				controller.targetHit += this.UpdateHitsCount;
				controller.SetDeathHitCount(_hitsToKill);
				controller.SetModel(model);

				if (controller.IsTargetMobile())
					_mobileTargets.Add(controller);
				else
					_unselectedTargets.Add(controller);
			}

			Log.Debug("TargetManager.SetTargets(): _unselectedTargets.Count = " + _unselectedTargets.Count);
			Log.Debug("TargetManager.SetTargets(): _mobileTargets.Count = " + _mobileTargets.Count);

			if (_numTargets < _mobileTargets.Count)
				_numTargets = (uint) _mobileTargets.Count;
	
			_unselectedTargets = RandomizeList<TargetController>(_unselectedTargets);

			int totalCount = targetModels.Count;

			if (_numTargets < totalCount && _unselectedTargets.Count > 0)
			{
				int unusedCount = totalCount - (int) _numTargets;
				Log.Debug($"TargetManager.SetTargets(): " + _numTargets + " selected with " + totalCount + " available, need to discard " + unusedCount);

				if (unusedCount < _unselectedTargets.Count)
				{
					var startIndex = _unselectedTargets.Count - unusedCount;
					Log.Debug($"TargetManager.SetTargets(): getting range of {unusedCount} target(s) from index {startIndex} from _unselectedTargets that has {_unselectedTargets.Count} targets");
					var unusedTargets = _unselectedTargets.GetRange(startIndex, unusedCount);
					ForeachTarget(unusedTargets, delegate(TargetController target) { target.SetEnabled(false); });
					_unselectedTargets.RemoveRange((int) _numTargets, unusedCount);
				}
				else
				{
					ForeachTarget(_unselectedTargets, delegate(TargetController target) { target.SetEnabled(false); });
					_unselectedTargets = null;
				}
			}

			// the mobile targets are already shown
			_numUsedTargets = (uint) _mobileTargets.Count;

			if (totalTargetsUpdate != null) totalTargetsUpdate(_numUsedTargets);
		}

		//------------------------------

		public void SetPopup(bool popupEnabled, float popupTime = 0, uint numSimultaneousTargets = 0)
		{
			Log.Trace($"TargetManager.SetPopup({popupEnabled}, {popupTime}, {numSimultaneousTargets})");

			if ((int) numSimultaneousTargets >= _numTargets)
			{
				numSimultaneousTargets = (uint) _numTargets;
				popupEnabled = false;
				_popupTime = 0;
			}

			_popupEnabled = popupEnabled;
			_popupTime = popupTime;
			_numSimultaneous = numSimultaneousTargets;
		}

		//------------------------------

		public void SetDeathHitCount(uint count)
		{
			Log.Trace($"TargetManager.SetDeathHitCount({count})");
			_hitsToKill = count;
			TargetOperation method = delegate(TargetController controller) { controller.SetDeathHitCount(count); };
			ForeachTarget(_selectedTargets, method);
			ForeachTarget(_unselectedTargets, method);
		}

		//#endregion

		//------------------------------

		//#region  privateMethods

		void ForeachTarget(List<TargetController> targets, TargetOperation op)
		{
			if (targets == null) return;
			foreach (var target in targets) op(target);
		}

		//------------------------------

		void UpdateTotalTargetsCounter(int selectedTargets)
		{
			uint numUsedTargets = _numUsedTargets + (uint) selectedTargets;
			numUsedTargets = Infrastructure.MathHelper.Clamp(numUsedTargets, 0, _numTargets);
			if (numUsedTargets != _numUsedTargets && totalTargetsUpdate != null)
				totalTargetsUpdate(numUsedTargets);
			Log.Debug($"TargetManager.UpdateTotalTargetsCounter(): selectedTargets = {selectedTargets}, numUsedTargets = {numUsedTargets}");
			_numUsedTargets = numUsedTargets;
		}

		//------------------------------

		void RaiseAll()
		{
			Log.Trace($"TargetManager.RaiseAll()");
			UpdateTotalTargetsCounter(_selectedTargets.Count);
			ForeachTarget(_selectedTargets, delegate(TargetController target) { target.SetEnabled(true); target.Raise(); });
		}

		//------------------------------

		void DropAll()
		{
			Log.Trace($"TargetManager.DropAll()");
			ForeachTarget(_unselectedTargets, delegate(TargetController target) { target.Drop(true); });
			ForeachTarget(_selectedTargets, delegate(TargetController target) { target.Drop(); });
		}

		//------------------------------

		void UpdateHitsCount(TargetController target)
		{
			Log.Trace($"TargetManager.UpdateHitsCount({target})");
			_totalHits += target.GetNewHitsCount();
			if (hitsCountUpdate != null) hitsCountUpdate(_totalHits);
		}

		//------------------------------

		void RemoveKilledTarget(TargetController target)
		{
			Log.Trace($"TargetManager.RemoveKilledTarget({target})");

			_selectedTargets.Remove(target);

			_totalKills++;

			if (killCountUpdate != null)
				killCountUpdate(_totalKills);

			if (_selectedTargets.Count == 0)
				SelectTargets();
		}

		//------------------------------

		void SelectTargets()
		{
			if (_unselectedTargets == null)
				return;
	
			Log.Trace($"TargetManager.SelectTargets()");

			DropAll();

			if (_selectedTargets != null)
				_unselectedTargets.AddRange(_selectedTargets);

			if (!_popupEnabled)
				_numSimultaneous = (uint) _unselectedTargets.Count;

			int numSimultaneous = (int) Math.Min(_numSimultaneous, _unselectedTargets.Count);
			_selectedTargets = _unselectedTargets.GetRange(0, numSimultaneous);
			_unselectedTargets.RemoveRange(0, numSimultaneous);

			Log.Info($"TargetManager.SelectTargets(): selected = {_selectedTargets.Count}, unselected = {_unselectedTargets.Count}");

			if (_selectedTargets.Count > 0)
			{
				RaiseAll();
				SetupPopupTimer();
			}
			else if (targetsCleared != null)
			{
				Log.Info($"TargetManager.SelectTargets(): all targets cleared");
				targetsCleared();
			}
		}

		//------------------------------

		void SetupPopupTimer()
		{
			Log.Trace($"TargetManager.SetupPopupTimer()");
			int timeMs = (int) _popupTime * 1000;
			SetupPopupTimer(timeMs);
		}

		//------------------------------

		void SetupPopupTimer(int timeMs)
		{
			Log.Trace($"TargetManager.SetupPopupTimer(" + timeMs + ")");
			if (!_popupEnabled) return;
			_popupTimer.Change(timeMs, timeMs);
		}

		//------------------------------

		void TimerCallback(Object stateInfo)
		{
			Log.Trace($"TargetManager.TimerCallback()");
			SelectTargets();
		}

		//------------------------------

		List<Type> RandomizeList<Type>(List<Type> originalList)
		{
			Log.Trace($"TargetManager.RandomizeList({originalList})");

			Random random = new Random();
			List<Type> randomList = new List<Type>();

			while (originalList.Count > 0)
			{
				var length = originalList.Count - 1;
				int index = (int) Math.Round((decimal) random.Next(0, length));
				randomList.Add(originalList[index]);
				originalList.RemoveAt(index);
			}

			return randomList;
		}

		//#endregion
	}
}
