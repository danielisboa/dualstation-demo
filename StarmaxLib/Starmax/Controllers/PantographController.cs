﻿using System;
using System.Threading;
using Managers;
using Models;
using Common;

using Timer = Common.Timer;

namespace Controllers
{
	public class PantographController : Controller, IControlUnit
	{
		//#region  privateTypes

		enum Actions
		{
			NoAction,
			Start,
			StartDelay,
			LaserDelay,
			LaserRequest,
			LaserCheck,
			FireDelay,
			FireRequest,
			FireCheck,
			Stop,
			StopDelay,
			End,
		}

		//#endregion

		//-------------------------------------

		//#region  publicAttributes

		public UpdateValueDelegate   trainingStartTime;
		public UpdateCounterDelegate laserCountUpdate;
		public UpdateCounterDelegate fireCountUpdate;
		public UpdateCounterDelegate laserHitsCountUpdate;
		public UpdateCounterDelegate fireHitsCountUpdate;
		public UpdateValueDelegate   followPercentageUpdate;
		public UpdateEventDelegate   trainingStarted;
		public UpdateEventDelegate   trainingEnded;

		//#endregion

		//------------------------------

		//#region  privateAttributes

		Actions _currentAction;

		WSCommunicationManager _wsCommunicationManager;
		IPantographModel _model;

		Timer _actionTimer;

		float _laserReqInterval;
		float _fireReqInterval;

		bool _firing;
		bool _lasing;

		bool _lastFiring;
		bool _lastLasing;

		bool _started;
		float _startTime;
		float _timeOnTarget;
		float _modelTime;
		float _updateTime;

		float _startDelay;
		float _requestDelay;

		float _trainingDuration;
		float _trainingTime;

		uint _laserRequests;
		uint _fireRequests;
		uint _laserHitsCount;
		uint _fireHitsCount;

		//#endregion

		//-------------------------------------

		//#region  publicMethods

		public PantographController(WSCommunicationManager wsCommunicationManager)
		{
			Log.Trace($"PantographController.PantographController({wsCommunicationManager})");

			_currentAction = Actions.NoAction;
			_actionTimer = new Timer(this.TimerCallback);

			_startDelay = 3f; // seconds
			_requestDelay = 5f; // seconds

			_wsCommunicationManager = wsCommunicationManager;
			_wsCommunicationManager.wsCmdReceived += this.GetWsCmds;
			_wsCommunicationManager.UpdateFovValues(Values.WideFov, Values.WideFovDayZoomLevel, (sbyte) FieldOfView.Wide, false);
			_wsCommunicationManager.SetEncoderSimulation(false);
		}

		//-------------------------------------

		public override void SetModel(Models.IModel pantographModel)
		{
			Log.Trace($"PantographController.SetModel({pantographModel})");
			_model = RegisterModel<IPantographModel>(pantographModel, this.ModelPreUpdate, null);
			_actionTimer.Update(_model.GetTime());
		}

		//-------------------------------------

		public void SetPattern(PantographPatterns pattern)
		{
			Log.Trace($"PantographController.SetPattern({pattern})");
			if (_model != null) _model.SetPattern(pattern);
		}

		//---------------------------------------

		public void SetCalibratedTargetSpeed(PantographCalibratedSpeed speed)
		{
			Log.Trace($"PantographController.SetCalibratedTargetSpeed({speed})");
			if (_model != null) _model.SetCalibratedTargetSpeed(speed);
		}

		//-------------------------------------

		public void SetLaserReqInterval(float interval)
		{
			Log.Trace($"PantographController.SetLaserReqInterval({interval})");
			_laserReqInterval = interval;
		}

		//-------------------------------------

		public void SetFireReqInterval(float interval)
		{
			Log.Trace($"PantographController.SetFireReqInterval({interval})");
			_fireReqInterval = interval;
		}

		//-------------------------------------

		public void SetRequestDuration(float duration)
		{
			Log.Trace($"PantographController.SetRequestDuration({duration})");
			_requestDelay = duration;
		}

		//-------------------------------------

		public void SetTrainingDuration(float duration)
		{
			_trainingDuration = duration;
		}

		//-------------------------------------

		public void Start(float delay = -1)
		{
			Log.Trace($"PantographController.Start()");

			_fireRequests = 0;
			_laserRequests = 0;
			_laserHitsCount = 0;
			_fireHitsCount = 0;
			_timeOnTarget = 0;

			if (delay >= 0)
				_startDelay = delay;

			Log.Debug($"PantographController.Start(): _trainingDuration = {_trainingDuration}, _startDelay = {_startDelay}");

			SetAction(Actions.Start);
		}

		//-------------------------------------

		public void Pause(bool pause)
		{
		}

		//-------------------------------------

		public void Stop()
		{
			Log.Trace($"PantographController.Stop()");
			if (_startTime == 0 || _modelTime < _startTime)
			{
				if (trainingStartTime != null)
					trainingStartTime(_modelTime);
			}
			UpdateFollowPercentage();
			SetAction(Actions.Stop);
		}

		//#endregion

		//-------------------------------------

		//#region  privateMethods

		void ModelPreUpdate()
		{
			_actionTimer.Update(_model.GetTime());

			UpdateStatus();
			ManageOnTargetTime();

			switch (_currentAction)
			{
				case Actions.Start:
					ActionStart();
					break;

				case Actions.LaserRequest:
					ActionLaserRequest();
					break;

				case Actions.LaserCheck:
					ActionLaserCheck();
					break;

				case Actions.FireRequest:
					ActionFireRequest();
					break;

				case Actions.FireCheck:
					ActionFireCheck();
					break;

				case Actions.LaserDelay:
				case Actions.FireDelay:
					ActionDelay();
					break;

				case Actions.Stop:
					ActionStop();
					break;

				case Actions.End:
					// End action, send trainingEnded notification
					Log.Info($"PantographController.ModelUpdate(): Training ended");
					if (trainingEnded != null) trainingEnded();
					break;
			}

			if (_lastLasing != _lasing)
				Log.Debug($"PantographController.ModelUpdate(): _lasing = {_lasing}, _lastLasing = {_lastLasing}");

			if (_lastFiring != _firing)
				Log.Debug($"PantographController.ModelUpdate(): _firing = {_firing}, _lastFiring = {_lastFiring}");

			_lastLasing = _lasing;
			_lastFiring = _firing;

			_lasing = false;
			_firing = false;
		}

		//------------------------------

		void ActionStart()
		{
			// Start action, start training and wait StartDelay
			if (_model.IsOnTarget())
			{
				_model.StartTraining(_startDelay);
				SetAction(Actions.StartDelay);

				if (trainingStartTime != null)
				{
					_startTime = _modelTime + _startDelay;
					trainingStartTime(_startTime);
					Log.Disabled("PantographController.ActionStart(): time = " + _model.GetTime() + ", _startTime = " + _startTime);
				}
			}
		}

		//------------------------------

		void ActionDelay()
		{
			// laser and fire requests delay, make sure
			// it's not displaying the request marks
			_model.ShowLaserRequest(false);
			_model.ShowFireRequest(false);
		}

		//------------------------------

		void  ActionLaserRequest()
		{
			// LaserRequest action, show laser target

			_model.ShowLaserRequest(true);
			UpdateLaserCount(++_laserRequests);

			SetAction(Actions.LaserCheck);
		}

		//------------------------------

		void ActionLaserCheck()
		{
			// LaserCheck action, check if lasing

			if (!_lastLasing && _lasing && _model.IsOnTarget())
			{
				UpdateLaserHitsCount(++_laserHitsCount);
				_model.ShowLaserRequest(false);
				SetAction(Actions.FireDelay);
			}
		}

		//------------------------------

		void ActionFireRequest()
		{
			// FireRequest action, show fire target

			_model.ShowFireRequest(true);
			UpdateFireCount(++_fireRequests);

			SetAction(Actions.FireCheck);
		}

		//------------------------------

		void  ActionFireCheck()
		{
			// FireCheck action, check if hit

			if (!_lastFiring && _firing && _model.IsOnTarget())
			{
				UpdateFireHitsCount(++_fireHitsCount);
				_model.ShowFireRequest(false);
				SetAction(Actions.LaserDelay);
			}
		}

		//------------------------------

		void ActionStop()
		{
			// Stop action, stop training

			_started = false;

			_model.StopTraining();
			_model.ShowOnTargetIndication(false);
			_model.ShowLaserRequest(false);
			_model.ShowFireRequest(false);

			UpdateFollowPercentage();

			SetAction(Actions.StopDelay);
		}

		//------------------------------

		void UpdateLaserCount(uint laserCount)
		{
			if (laserCountUpdate != null)
				laserCountUpdate(laserCount);
		}

		//------------------------------

		void UpdateLaserHitsCount(uint laserHitsCount)
		{
			if (laserHitsCountUpdate != null)
				laserHitsCountUpdate(laserHitsCount);
		}

		//------------------------------

		void UpdateFireCount(uint fireCount)
		{
			if (fireCountUpdate != null)
				fireCountUpdate(fireCount);
		}

		//------------------------------

		void UpdateFireHitsCount(uint fireHitsCount)
		{
			if (fireHitsCountUpdate != null)
				fireHitsCountUpdate(_fireHitsCount);
		}

		//------------------------------

		void UpdateFollowPercentage()
		{
			if (followPercentageUpdate != null)
			{
				float duration = _modelTime - _startTime;
				float followPercentage = _timeOnTarget / duration;
				if (followPercentage < 0) followPercentage = 0;
				followPercentage = (float) Math.Round(followPercentage, 2);
				Log.Disabled("PantographController.ActionStop(): followPercentage = " + followPercentage);
				followPercentageUpdate(followPercentage);
			}
		}

		//------------------------------

		void UpdateStatus()
		{
			float reticlePositionX, reticlePositionY;

			_wsCommunicationManager.GetReticlePos(out reticlePositionX, out reticlePositionY);
			_model.SetReticlePosition((int) reticlePositionX, (int) reticlePositionY);

			float trvEncVal, elvEncVal;

			_wsCommunicationManager.GetEncodersValues(out trvEncVal, out elvEncVal);
			_model.SetEncodersValue(trvEncVal, elvEncVal);

			_trainingTime = _model.GetTrainingElapsedTime();

			_modelTime = _model.GetTime();

			if (_started && (_modelTime - _updateTime) > 1)
			{
				UpdateFollowPercentage();
				UpdateLaserCount(_laserRequests);
				UpdateLaserHitsCount(_laserHitsCount);
				UpdateFireCount(_fireRequests);
				UpdateFireHitsCount(_fireHitsCount);
				_updateTime = _modelTime;
			}
		}

		//------------------------------

		void GetWsCmds()
		{
			if (_wsCommunicationManager.GetFireCmd()) _firing = true;
			if (_wsCommunicationManager.GetMeasureRangeCmd()) _lasing = true;
		}

		//------------------------------

		void SetAction(Actions action)
		{
			Log.Trace($"PantographController.SetAction({action})");

			float delay = (float) Timeout.Infinite / 1000f;

			switch (action)
			{
				case Actions.StartDelay:
					delay = _startDelay;
					break;

				case Actions.LaserDelay:
					if (HasTimeForRequests())
						delay = _laserReqInterval;
					break;

				case Actions.LaserCheck:
					delay = _requestDelay;
					break;

				case Actions.FireDelay:
					delay = _fireReqInterval;
					break;

				case Actions.FireCheck:
					delay = _requestDelay;
					break;

				case Actions.Stop:
					break;

				case Actions.StopDelay:
					delay = _startDelay * 2;
					break;
			}

			_actionTimer.Change((int) (delay * 1000), Timeout.Infinite);
			_currentAction = action;
		}

		//------------------------------

		bool HasTimeForRequests()
		{
			var remainingTime   = _trainingDuration - _trainingTime;
			var maxRequestsTime = _laserReqInterval + _fireReqInterval + _requestDelay * 2;

			return (remainingTime > maxRequestsTime);
		}

		//------------------------------

		void TimerCallback(Object stateInfo)
		{
			Log.Trace($"PantographController.TimerCallback()");
			Log.Debug($"PantographController.TimerCallback(): _currentAction = {_currentAction}");

			switch (_currentAction)
			{
				case Actions.StartDelay:
					// timeout from StartDelay, start waiting for LaserDelay
					SetAction(Actions.LaserDelay);
					if (trainingStarted != null)
						trainingStarted();
					_started = true;
					break;

				case Actions.LaserDelay:
					// timeout from StartDelay, initiate LaserRequest
					SetAction(Actions.LaserRequest);
					break;

				case Actions.LaserCheck:
					// timeout from LaserCheck, no laser on target, go back to LaserDelay
					SetAction(Actions.LaserDelay);
					break;

				case Actions.FireDelay:
					// timeout from FireDelay, initiate FireRequest
					SetAction(Actions.FireRequest);
					break;

				case Actions.FireCheck:
					// timeout from FireCheck, no fire on target, go back to LaserDelay
					SetAction(Actions.LaserDelay);
					break;

				case Actions.StopDelay:
					// timeout from StopDelay, go to End
					SetAction(Actions.End);
					break;
			}
		}

		//------------------------------

		void StopTraining(Object stateInfo)
		{
			SetAction(Actions.Stop);
		}

		//-------------------------------------

		void ManageOnTargetTime()
		{
			if (_currentAction == Actions.NoAction)
				return;

			bool onTarget = _model.IsOnTarget();

			if (_started && onTarget)
				_timeOnTarget += _model.GetDeltaTime();

			if (_currentAction == Actions.StopDelay || _currentAction == Actions.End)
				onTarget = false;

			_model.ShowOnTargetIndication(onTarget);

			Log.Trash("PantographController.ManageOnTargetTime(): _timeOnTarget = " + _timeOnTarget);
		}

		//#endregion
	}
}

