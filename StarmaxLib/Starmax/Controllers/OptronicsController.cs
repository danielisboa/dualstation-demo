﻿using System;
using Models;
using Controllers;
using Managers;
using Common;
using System.Collections.Generic;

namespace Controllers
{
	public class OptronicsController : Controller
	{
		IOptronicsModel _optronicsModel;

		public UpdateEventDelegate OptronicsUpdateClbk;

		Camera _activeCamera;
		CameraController[] _cameraController;
		CameraController _currCamController;

		float   _range;
		byte   _freshRangeCount;
		bool   _measureRangeCmd;
		bool   _measureRange;
		bool   _lrfEnabled;
		short _minRange;
		short _maxRange;
		bool _enableReticle;
		uint _totalRangeRequests;

		bool _nucCmd;
		bool _nuc;

		public OptronicsController()
		{
			_cameraController = new CameraController[(int)Camera.NumCameras];

			_cameraController [(int)Camera.Day] = new CameraController (Values.NarrowFov, Values.WideFov, Values.VeryWideFov, Values.MinDayZoomFov, Values.MaxDayZoomFov);
			_cameraController [(int)Camera.Day].SetFocusMode (AutoManualMode.Auto);

			_cameraController [(int)Camera.Thermal] = new CameraController (Values.NarrowFov, Values.WideFov, Values.WideFov, Values.MinThermalZoomFov, Values.MaxThermalZoomFov);
			_cameraController [(int)Camera.Thermal].SetFocusMode (AutoManualMode.Manual);

			_cameraController [(int)Camera.Boresight] = new CameraController (Values.BoresightFov, Values.BoresightFov, Values.BoresightFov, Values.BoresightFov, Values.BoresightFov);
			_cameraController [(int)Camera.Boresight].SetFocusMode (AutoManualMode.None);

			SetActiveCamera (Camera.Day);

			_minRange = (short) Values.MinLrfRange;
			_maxRange = (short) Values.MaxLrfRange;

			_lrfEnabled = true;
		}

		//----------------------------------------

		public override void SetModel(Models.IModel model)
		{
			Log.Trace($"OptronicsController.SetModel({model})");
			_optronicsModel = RegisterModel<IOptronicsModel>(model, this.UpdateModel, this.GetModelData);
		}

		//----------------------------------------

		void UpdateModel()
		{
			_currCamController.Update();
			_optronicsModel.SetActiveCamera (_activeCamera);
			_optronicsModel.SetCameraFOVAngle (_currCamController.GetZoomSetPoint());
			_optronicsModel.SetFocusValue (_currCamController.GetFocusSetPoint ());
			_optronicsModel.SetReticlePosition (_currCamController.GetReticleX(), _currCamController.GetReticleY());
			_optronicsModel.SetIrisValue (_currCamController.GetIrisValue ());
			_optronicsModel.SetPolarityCmd (_currCamController.GetPolarityCmd ());
			_optronicsModel.SetRangeLimits (_minRange, _maxRange);
			//_optronicsModel.SetNucCmd (_currCamController.GetNucCmd ());
			_optronicsModel.SetGammaValue (_currCamController.GetGammaValue ());
			_optronicsModel.SetLevelValue (_currCamController.GetLevelValue ());
			_optronicsModel.SetGainValue (_currCamController.GetGainValue ());
			_optronicsModel.EnableReticle(_enableReticle);

			if (_measureRange)
			{
				_totalRangeRequests++;
				_optronicsModel.MeasureRangeCmd();
				_freshRangeCount = 1;
			}
			_measureRange = _measureRangeCmd;

			if (_nuc)
			{
				_optronicsModel.SetNucCmd (_nuc);
			}
			_nuc = _nucCmd;

		}

		//----------------------------------------

		void GetModelData()
		{
			_range = _optronicsModel.GetTargetRange();

			_currCamController.SetModelFeedbackFov(_optronicsModel.GetCurrentFov());

			if (OptronicsUpdateClbk != null)
				OptronicsUpdateClbk();
		}

		//----------------------------------------

		public void SetLrfEnabled(bool enabled)
		{
			_lrfEnabled = enabled;
		}

		//----------------------------------------

		public void SetActiveCamera(Camera camera)
		{
			if ((int) camera >= _cameraController.Length)
				return;
			if (_cameraController[(int) camera] == null)
				return;
			_activeCamera = camera;
			_currCamController = _cameraController[(int)_activeCamera];
		}

		//----------------------------------------

		public void SetReticlePositionXY(float reticleX, float reticleY)
		{
			if (_currCamController == null)
				return;

			_currCamController.SetReticlePosition (reticleX, reticleY);
		}

		//----------------------------------------

		public void SetFocusMode(AutoManualMode mode)
		{
			if (_currCamController == null)
				return;

			_currCamController.SetFocusMode (mode);
		}

		//----------------------------------------

		public void SetFocusCmd(InOutStopCmd cmd)
		{
			if (_currCamController == null)
				return;

			_currCamController.SetFocusCmd(cmd);
		}

		//--------------------------------------

		public void SetFovCmd(FOVCmds cmd)
		{
			if (_currCamController == null)
				return;

			//Log.Debug ("Optronics Controller: SET FOV CMD = " + cmd);

			_currCamController.SetFovCmd(cmd);
		}

		//----------------------------------------

		public void SetIrisMode(AutoManualMode mode)
		{

			if (_currCamController == null)
				return;

			_currCamController.SetIrisMode (mode);
		}

		//----------------------------------------

		public void SetPolarityMode(Polarity cmd)
		{
			if (_currCamController == null)
				return;

			_currCamController.SetPolarityCmd (cmd);
		}

		//----------------------------------------

		public void SetRangeLimits(short minRange, short maxRange)
		{
			_minRange = minRange;
			_maxRange = maxRange;
		}

		//----------------------------------------

		public void MeasureRangeCmd(bool cmd)
		{
			_measureRangeCmd = cmd;

			if (_measureRangeCmd)
				_measureRange = true;

			Log.Disabled("OptronicsController.MeasureRangeCmd(): RCVD _measureRangeCmd = " + _measureRangeCmd + ", _measureRange = " + _measureRange);
		}

		//----------------------------------------

		public void GetRange(out float range, out byte freshRange)
		{
			range = (_lrfEnabled) ? _optronicsModel.GetTargetRange() : 0;
			freshRange = (byte) ((_freshRangeCount != 0) ? 1 : 0);

			const int FreshRangeCount = 50;
			// keep fresh range in 1 for 3 messages
			if (freshRange == 1 && ++_freshRangeCount >= FreshRangeCount)
				_freshRangeCount = 0;
		}

		//----------------------------------------

		public void SetIrisCmd(InOutStopCmd cmd)
		{
			if (_currCamController == null)
				return;

			_currCamController.SetIrisCmd (cmd);
		}

		//----------------------------------------

		public void SetGammaMode(AutoManualMode mode)
		{
			if (_currCamController == null)
				return;

			_currCamController.SetGammaMode (mode);
		}

		//----------------------------------------

		public void SetGammaCmd(InOutStopCmd cmd)
		{
			if (_currCamController == null)
				return;

			_currCamController.SetGammaCmd (cmd);
		}

		//----------------------------------------

		public void SetLevelMode(AutoManualMode mode)
		{
			if (_currCamController == null)
				return;

			_currCamController.SetLevelMode (mode);
		}

		//----------------------------------------

		public void SetLevelCmd(InOutStopCmd cmd)
		{
			if (_currCamController == null)
				return;

			_currCamController.SetLevelCmd (cmd);
		}

		//----------------------------------------

		public void SetGainMode(AutoManualMode mode)
		{
			if (_currCamController == null)
				return;

			_currCamController.SetGainMode (mode);
		}

		//----------------------------------------

		public void SetGainCmd(InOutStopCmd cmd)
		{
			if (_currCamController == null)
				return;

			_currCamController.SetGainCmd (cmd);
		}

		//----------------------------------------

		public void SetNucCmd(bool cmd)
		{
			if (_currCamController == null)
				return;

			_nucCmd = cmd;

			if (_nucCmd)
				_nuc = true;

			//_currCamController.SetNucCmd (cmd);
		}

		//----------------------------------------

		public void GetFovAngle(out float fovVertAngle, out float zoomPercent, out sbyte fov, out bool uncalibratedFov)
		{
			fovVertAngle =  _currCamController.GetCurrentFovAngle ();
			zoomPercent = _currCamController.GetZoomPercent ();
			fov = _currCamController.GetFov ();
			uncalibratedFov = !_currCamController.IsCalibratedFov();

			Log.Disabled ("OptronicsController: fovVertAngle: " + fovVertAngle + " | zoomPercent = " + zoomPercent + " | fov = " + fov + " | uncalibratedFov = " + uncalibratedFov);
		}

		//----------------------------------------

		public void EnableReticle(bool cmd)
		{
			_enableReticle = cmd;
		}

		//------------------------

		public uint GetNumRangeRequests()
		{
			return _totalRangeRequests;
		}

		//----------------------------------------

		public sbyte GetPolarityMode()
		{
			return (sbyte)_currCamController.GetPolarityCmd ();
		}

		//----------------------------------------
	}
}

