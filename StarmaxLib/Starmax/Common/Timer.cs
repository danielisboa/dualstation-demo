﻿using System;

namespace Common
{
	public class Timer
	{
		#region publicAttributes

		public const int Infinite = System.Threading.Timeout.Infinite;

		#endregion

		//-------------------------------------

		#region privateAttributes

		UpdateValueDelegate<object> _callback;
		int _currTime;
		int _dueTime;
		int _period;

		#endregion

		//-------------------------------------

		#region publicMethods

		public Timer(UpdateValueDelegate<object> callback)
		{
			_currTime = 0;
			_period = Infinite;
			_dueTime = Infinite;
			_callback = callback;
		}

		//-------------------------------------

		public void Change(int dueTime, int period)
		{
			// dueTime can be infinite, zero or positive
			if (dueTime < 0 && dueTime != Infinite)
				dueTime = Infinite;

			// period can be infinite or positive
			if (period <= 0 && period != Infinite)
				period = Infinite;

			_period = period;

			if (dueTime != Infinite)
				_dueTime = _currTime + dueTime;
			else if (period != Infinite)
				_dueTime = _currTime + _period;
			else
				_dueTime = Infinite;

			Log.Info(
				"Timer.Change(" + dueTime + ", " + period + "): " +
				"_currTime = " + _currTime +
				", _dueTime = " + _dueTime +
				", _period = " + _period);

			Trigger();
		}

		//-------------------------------------

		public void Update(float time)
		{
			_currTime = (int) (time * 1000);
			Trigger();
		}

		#endregion

		//-------------------------------------

		#region privateMethods

		void Trigger()
		{
			if (_dueTime == Infinite)
				return; // due time not set

			if (_dueTime > _currTime)
				return; // due time not yet passed

			// due time passed

			Log.Info(
				"Timer.Trigger(): " +
				"_currTime = " + _currTime +
				", _dueTime = " + _dueTime);

			// calculate next due time

			if (_period != Infinite)
				_dueTime += _period; // add period
			else
				_dueTime = Infinite; // disable

			// call the callback function

			if (_callback != null)
				_callback(_currTime);
		}

		#endregion
	}
}

