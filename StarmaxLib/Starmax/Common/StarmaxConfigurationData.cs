﻿using Infrastructure;

namespace Common
{
	/*
	 * Although it should be a struct,
	 * it cannot be a struct if we want
	 * to use the LoadFromJsonFile()
	 * extension method.
	 */
	[System.Serializable]
	public class StarmaxConfigurationData
	{
		//#region  publicAttributes

		public int wsCommLocalPort;
		public int wsCommRemotePort;
		public string wsCommLocalIP;
		public string wsCommRemoteIP;

		public int guiCommSimPort;
		public int guiCommGuiPort;

		public int   terrainDetailDistance;
		public float terrainDetailDensity;
		public int terrainBillboardDistance;
		public int baseMapDistance;

		public bool dynamicsSimulation;

		//#endregion

		//------------------------------

		//#region  publicMethods

		public StarmaxConfigurationData()
		{
			wsCommLocalIP = "127.0.0.1";
			wsCommRemoteIP = "";
		}

		//------------------------------

		public StarmaxConfigurationData(string filename) : this()
		{
			LoadConfiguration(filename);
		}

		//------------------------------

		public bool LoadDefaultConfiguration()
		{
			// C# supports short-circuit evaluation, so the second call only takes place if the first fail
			return LoadConfiguration(DefaultFilename()) || LoadConfiguration(DefaultPath() + DefaultFilename());
		}

		//------------------------------

		public bool LoadConfiguration(string filename)
		{
			// var ok = this.LoadFromJsonFile(filename);
			// Log.Info("StarmaxConfigurationData(): Loaded configuration file " + filename + " - " + ok);
			// return;

			if (this.LoadFromJsonFile(filename))
			{
				Log.Info("StarmaxConfigurationData(): Loaded configuration file " + filename);
				return true;
			}

			filename = DefaultPath() + filename;
			if (this.LoadFromJsonFile(filename))
			{
				Log.Info("StarmaxConfigurationData(): Loaded configuration file " + filename);
				return true;
			}

			Log.Error("StarmaxConfigurationData(): Unable to load configuration file " + filename);

			// throw new System.Exception("Unable to load configuration");

			return false;
		}

		//#endregion

		//------------------------------

		//#region  publicStaticMethods

		public static string DefaultFilename()
		{
			return "config.json";
		}

		//------------------------------

		public static string DefaultPath()
		{
			return "/etc/starmax/";
		}

		//#endregion
	}
}

