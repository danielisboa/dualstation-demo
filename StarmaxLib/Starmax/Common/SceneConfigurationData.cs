﻿using System;

namespace Common
{
	public struct SceneConfigurationData
	{
		public uint numberOfTargets;
		public bool popupTargets;
		public float popupInterval;
		public uint simultaneousTargets;
		public uint hitsToKillTargets;
		public float vehicleVelocity;
		public float daylightIntensity;
		public float rainIntensity;
		public float fogIntensity;
		public float windSpeed;
		public uint ammoAmount;
		public bool reloadAmmoSafe;
		public bool fireJammed;
		public float laserReqInterval;
		public float fireReqInterval;
		public float requestDuration;
		public float trainingDuration;
		public byte calibratedSpeed;
		public bool lrfEnabled;
	}

	public struct SimCalibrationsData
	{
		public bool  WeaponCalibrationsOn;
		public float FireForceLimiter;
		public float WindForceScale;
		public bool  LrfCalibrationsOn;
		public float LrfPosX;
		public float LrfPosY;
		public float LrfRotX;
		public float LrfRotY;
		public bool  CameraCalibrationsOn;
		public bool  reticleOn;
		public bool  reticleOffsetsOn;
		public int   reticleOffsetX;
		public int   reticleOffsetY;
		public float reticleScaleX;
		public float reticleScaleY;
		public bool  UseTerrainDetail;
		public int   DetailDistance;
		public float DetailDensity;
		public int 	 BillboardDistance;
		public int 	 BaseMapDistance;
	}
}
