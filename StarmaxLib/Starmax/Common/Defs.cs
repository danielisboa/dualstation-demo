﻿using System;

namespace Common
{
	public delegate void UpdateEventDelegate();
	public delegate void CommunicationStatusDelegate(bool communicationOk);
	public delegate void PositionUpdateDelegate(float x, float y, float dir);
	public delegate void UpdateCounterDelegate(uint counter);
	public delegate void UpdateValueDelegate(float value);
	public delegate void UpdateIntValueDelegate(uint value);
	public delegate void JsonDataDelegate(string jsonData);
	public delegate void UpdateValueDelegate<Type>(Type value);

	//-------------------------------------

	public enum TrainingType
	{
		Pantograph        = GuiCommunication.GuiSimProtocol.TrainingType.Pantograph,
		OperationalStatic = GuiCommunication.GuiSimProtocol.TrainingType.OperationalStatic,
		OperationalMobile = GuiCommunication.GuiSimProtocol.TrainingType.OperationalMobile,
		Calibrations      = GuiCommunication.GuiSimProtocol.TrainingType.Calibrations,
		Naval             = GuiCommunication.GuiSimProtocol.TrainingType.Naval,
		Boresight	  = GuiCommunication.GuiSimProtocol.TrainingType.Boresight,
		NoType
	}

	//----------------------------

	public enum PantographPatterns
	{
		Horizontal = GuiCommunication.GuiSimProtocol.PantographPatterns.Horizontal,
		Vertical   = GuiCommunication.GuiSimProtocol.PantographPatterns.Vertical,
		Elipsoidal = GuiCommunication.GuiSimProtocol.PantographPatterns.Elipsoidal,
		Infinity   = GuiCommunication.GuiSimProtocol.PantographPatterns.Infinity,
		Saw        = GuiCommunication.GuiSimProtocol.PantographPatterns.Saw,
		Square,
		Sinus,
		NoPattern
	}

	//----------------------------

	public enum PantographCalibratedSpeed
	{
		Slow   = GuiCommunication.GuiSimProtocol.PantographCalibratedSpeed.Slow,
		Normal = GuiCommunication.GuiSimProtocol.PantographCalibratedSpeed.Normal,
		Fast   = GuiCommunication.GuiSimProtocol.PantographCalibratedSpeed.Fast,
	}

	//----------------------------

	public enum TrainingControl
	{
		Start   = GuiCommunication.GuiSimProtocol.TrainingControl.Start,
		Stop    = GuiCommunication.GuiSimProtocol.TrainingControl.Stop,
		Pause   = GuiCommunication.GuiSimProtocol.TrainingControl.Pause,
		Resume  = GuiCommunication.GuiSimProtocol.TrainingControl.Resume,
		Failure = GuiCommunication.GuiSimProtocol.TrainingControl.Failure,
	}

	//-------------------------------------

	public enum OperationalMode
	{
		Manual = WSCommunication.WSSimProtocol.OperationModes.Manual,
		Power  = WSCommunication.WSSimProtocol.OperationModes.Power,
		Stab   = WSCommunication.WSSimProtocol.OperationModes.Stab,
	}

	//-------------------------------------

	public enum RangeMode
	{
		Manual = WSCommunication.WSSimProtocol.RangeModes.Manual,
		Battle = WSCommunication.WSSimProtocol.RangeModes.Battle,
		Lrf    = WSCommunication.WSSimProtocol.RangeModes.Lrf,
	}

	//-------------------------------------

	public enum Axis
	{
		Traverse,
		Elevation,
	}

	//-------------------------------------

	public enum Camera
	{
		Day     	= WSCommunication.WSSimProtocol.CameraTypes.Day,
		Thermal	 	= WSCommunication.WSSimProtocol.CameraTypes.Thermal,
		Lrf     	= WSCommunication.WSSimProtocol.CameraTypes.Lrf,
		Boresight   = WSCommunication.WSSimProtocol.CameraTypes.Boresight,
		NumCameras,
	}

	//-------------------------------------

	public enum AutoManualMode
	{
		None   = WSCommunication.WSSimProtocol.AutoManualMode.None,
		Manual = WSCommunication.WSSimProtocol.AutoManualMode.Manual,
		Auto   = WSCommunication.WSSimProtocol.AutoManualMode.Auto,
	}

	//-------------------------------------

	public enum FOVCmds
	{
		NoFov	 = WSCommunication.WSSimProtocol.FovCmds.NoFov,
		Narrow	 = WSCommunication.WSSimProtocol.FovCmds.Narrow,
		Wide 	 = WSCommunication.WSSimProtocol.FovCmds.Wide,
		VeryWide = WSCommunication.WSSimProtocol.FovCmds.VeryWide,
		Out		 = WSCommunication.WSSimProtocol.FovCmds.Out,
		Stop     = WSCommunication.WSSimProtocol.FovCmds.Stop,
		In	     = WSCommunication.WSSimProtocol.FovCmds.In
	}

	//-------------------------------------

	public enum Polarity
	{
		BlackHot = WSCommunication.WSSimProtocol.Polarity.BlackHot,
		WhiteHot = WSCommunication.WSSimProtocol.Polarity.WhiteHot,
	}

	//-------------------------------------

	public enum ZoomConfig
	{
		Fixed = 0x00,
		Continuous,
		Discrete,
	}

	//-------------------------------------

	public enum FireMode
	{
		Single = WSCommunication.WSSimProtocol.FireModes.Single,
		Burst_5 = WSCommunication.WSSimProtocol.FireModes.Burst_5,
		Burst_15 = WSCommunication.WSSimProtocol.FireModes.Burst_15,
	}

	//-------------------------------------

	public enum EnabledDisabled
	{
		Disabled =  WSCommunication.WSSimProtocol.EnabledDisabled.Disabled,
		Enabled =  WSCommunication.WSSimProtocol.EnabledDisabled.Enabled,
	}

	//-------------------------------------

	public enum InOutStopCmd
	{
		Out		= WSCommunication.WSSimProtocol.InOutStopCmd.Out,
		Stop	= WSCommunication.WSSimProtocol.InOutStopCmd.Stop,
		In	 	= WSCommunication.WSSimProtocol.InOutStopCmd.In,
	}

	//-------------------------------------

	public enum FieldOfView
	{
		NoFov			= WSCommunication.WSSimProtocol.FieldOfView.NoFov,
		Narrow			= WSCommunication.WSSimProtocol.FieldOfView.Narrow,
		Wide 			= WSCommunication.WSSimProtocol.FieldOfView.Wide,
		VeryWide		= WSCommunication.WSSimProtocol.FieldOfView.VeryWide,
		VeryVeryWide	= WSCommunication.WSSimProtocol.FieldOfView.VeryVeryWide,
	}

	//-------------------------------------

	public enum WeaponType
	{
		NoWeapon = WSCommunication.WSSimProtocol.WeaponType.NoWeapon,
		Gun      = WSCommunication.WSSimProtocol.WeaponType.Gun,
		Mag      = WSCommunication.WSSimProtocol.WeaponType.Mag,
	}

	//-------------------------------------

}
