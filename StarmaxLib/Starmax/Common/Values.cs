﻿namespace Common
{
	public static class Values
	{
		public static float NarrowFov;
		public static float WideFov;
		public static float VeryWideFov;

		public static float DayFovRange;
		public static float ThermalFovRange;

		public static float BoresightFov;

		public static float NarrowFovDayZoomLevel;
		public static float WideFovDayZoomLevel;
		public static float VeryWideFovDayZoomLevel;

		public static float NarrowFovThermalZoomLevel;
		public static float WideFovThermalZoomLevel;
		public static float VeryWideFovThermalZoomLevel;

		public static float MinDayZoomFov;
		public static float MaxDayZoomFov;
		public static float MinThermalZoomFov;
		public static float MaxThermalZoomFov;

		public static float MinLrfRange;
		public static float MaxLrfRange;

		static Values()
		{
			// TODO: READ CONFIG FILE FOR WEAPON STATION

			MinDayZoomFov = 41.48916f;
			MaxDayZoomFov = 1.754002f;

			NarrowFov = 4.613655f;
			WideFov = 14.61864f;
			VeryWideFov = MaxDayZoomFov * 2; // For REMAX we don't want to use very wide

			BoresightFov = 2f;

			MinThermalZoomFov = 14.61864f;
			MaxThermalZoomFov = 4.613655f;

			MinLrfRange = 30f;
			MaxLrfRange = 2500f;

			DayFovRange = MaxDayZoomFov - MinDayZoomFov;
			ThermalFovRange = MaxThermalZoomFov - MinThermalZoomFov;

			NarrowFovDayZoomLevel = NarrowFov - MinDayZoomFov / DayFovRange;
			WideFovDayZoomLevel = WideFov - MinDayZoomFov / DayFovRange;
			VeryWideFovDayZoomLevel = VeryWideFov - MinDayZoomFov / DayFovRange;

			NarrowFovThermalZoomLevel = NarrowFov - MinThermalZoomFov / ThermalFovRange;
			WideFovThermalZoomLevel = WideFov - MinThermalZoomFov / ThermalFovRange;
			VeryWideFovThermalZoomLevel = VeryWideFov - MinThermalZoomFov / ThermalFovRange;
		}
	}
}
