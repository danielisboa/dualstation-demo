﻿using System;

namespace Models
{
	public interface ITargetModel : IModel
	{
		uint GetHitsCount();
		void ResetHitsCount();
		void SetHitIndication(float time);
		void SetEnabled(bool enabled);
		void Raise(bool imediate = false);
		void Drop(bool imediate = false);
		bool IsMobile();
		bool IsRaised();
		bool IsDropped();
		bool IsHit();
	}
}
