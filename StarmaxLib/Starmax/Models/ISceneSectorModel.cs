﻿using System;
using System.Collections.Generic;

namespace Models
{
	public interface ISceneSectorModel
	{
		IGameObject GetVehiclePosition();
		IGameObject GetTrajectory();
		List<IModel> GetTargets();
	}
}
