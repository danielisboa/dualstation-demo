﻿using System;

namespace Models
{
	public interface IVehicleModel : IModel
	{
		void SetVehiclePosition(IGameObject reference);
		void SetTrajectory(IGameObject trajectory);
		void SetHeadlights(bool headlightsOn);
		void SetDriveVelocity(float velocity);
		void GetPos(out float x, out float y, out float dir);
		float GetDriveVelocity();
		void Drive();
		void Stop();
	}
}
