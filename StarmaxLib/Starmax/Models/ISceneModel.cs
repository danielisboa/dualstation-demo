﻿using System;

namespace Models
{
	public interface ISceneModel
	{
		ITurretModel GetTurretModel();
		ISceneSectorModel GetSceneSectorModel(uint sectorNumber);
		IModel GetEnvironmentModel();
		IVehicleModel GetVehicleModel();
		IModel GetPantographModel();
	}
}
