﻿using System;

namespace Models
{
	public interface ICommanderModel : IModel
	{
		IModel GetOptronicsModel();		
		IModel GetTrvGyroModel();
		IModel GetElvGyroModel();
		void ApplyTrvRotation(float trv);
		void ApplyElvRotation(float elv);
		float GetTrvAngle();
		float GetElvAngle();
	}
}

