﻿using System;
using Common;

namespace Models
{
	public interface IOptronicsModel : IModel
	{
		void SetActiveCamera(Camera camera);
		void SetCameraFOVAngle(float fov);
		void SetReticlePosition(float x, float y, bool force = false);
		void SetRange(float range);
		void SetFocusValue(float value);
		void SetIrisValue(float value);
		void SetPolarityCmd(Polarity cmd);
		void SetRangeLimits(short minRange, short maxRange);
		void MeasureRangeCmd();
		float GetTargetRange();
		void SetNucCmd(bool cmd);
		void SetGammaValue(float value);
		void SetLevelValue(float value);
		void SetGainValue(float value);
		void EnableReticle(bool enable, bool force = false);
		float GetCurrentFov();
		byte GetPolarityMode();
	}
}





