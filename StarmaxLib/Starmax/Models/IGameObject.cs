﻿using System;

namespace Models
{
	public interface IGameObject
	{
		object GetGameObject();
	}
}
