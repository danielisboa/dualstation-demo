﻿using System;
using Common;

namespace Models
{
	public interface IEnvironmentModel : IModel
	{
		void SetDayLightValue(float value);
		void SetRainValue(float value);
		void SetFogValue(float value);
		void SetFarRainPosition(float value);
		void EnableRain(bool flag);
		void EnablerThermalLight(bool flag);
	}
}

