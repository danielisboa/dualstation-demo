﻿using System;
using Common;

//------------------------------------------------

namespace Models
{
	public interface IModel
	{
		float GetTime();
		float GetDeltaTime();
		void AddPreUpdateCallback(UpdateEventDelegate callback);
		void AddPostUpdateCallback(UpdateEventDelegate callback);
	}
}

//------------------------------------------------