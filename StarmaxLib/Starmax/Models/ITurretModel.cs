﻿using System;

namespace Models
{
	public interface ITurretModel : IModel
	{
		IModel GetOptronicsModel();
		IModel GetOptronicsModelCMDR();		
		IModel GetPodCommanderModel();
		IModel GetWeaponModel(Common.WeaponType weaponType);
		IModel GetTrvGyroModel();
		IModel GetElvGyroModel();
		void ApplyTrvRotation(float trv);
		void ApplyElvRotation(float elv);
		float GetTrvAngle();
		float GetElvAngle();
	}
}

