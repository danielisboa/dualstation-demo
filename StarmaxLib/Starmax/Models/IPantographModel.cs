﻿using System;
using Common;

namespace Models
{
	public interface IPantographModel : IModel
	{
		float GetTrainingElapsedTime();
		void SetPattern(PantographPatterns pattern);
		void SetReticlePosition(int x, int y);
		void SetEncodersValue(float trv, float elv);
		void SetCalibratedTargetSpeed(PantographCalibratedSpeed speed);
		void ShowLaserRequest(bool show);
		void ShowFireRequest(bool show);
		void ShowOnTargetIndication(bool show);
		void StartTraining(float delay = 0);
		void StopTraining();
		bool IsOnTarget();
		bool IsRunning();
	}
}

