﻿using System;
using Common;

namespace Models
{
	public interface IWeaponModel : IModel
	{
		void Fire();
		void SelectBullet(int index);
		void SetTracerBullet(int interval);
		void SetWindSpeed(float windSpeed);
		uint GetFireRate();
	}
}