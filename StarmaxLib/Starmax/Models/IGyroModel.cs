﻿using System;

namespace Models
{
	public interface IGyroModel : IModel
	{
		float GetCurrentAngle();
		float GetAngle();
		float GetRate();
	}
}

