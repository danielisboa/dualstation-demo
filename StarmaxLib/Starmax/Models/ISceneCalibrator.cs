﻿using System;
using Common;

namespace Models
{
	public interface ISceneCalibrator
	{
		void SetCalibrationData(SimCalibrationsData data);
	}
}

