﻿using System;
using Common;
using Managers;
using Utils;

namespace Controllers
{
	public class SceneController
	{
		//#region publicAttributes

		public UpdateEventDelegate sceneStopped;

		//#endregion

		//-------------------------------------

		//#region privateAttributes

		ISceneInterface _sceneInterface;
		SceneResults _sceneResults;

		PantographController _pantographController;
		EnvironmentController _environmentController;
		VehicleController _vehicleController;
		TurretManager _turretManager;
		TargetManager _targetManager;

		TrainingTimer _trainingTimer;

		//#endregion

		//-------------------------------------

		//#region publicMethods

		public SceneController(ISceneInterface sceneInterface, SceneResults sceneResults)
		{
			Log.Trace("SceneController.SceneController()");
			_sceneInterface = sceneInterface;
			_sceneResults = sceneResults;
		}

		//-------------------------------------

		public void SetPantographController(PantographController pantographController)
		{
			Log.Trace("SceneController.SetPantographController()");
			_pantographController = pantographController;
		}

		//-------------------------------------

		public void SetEnvironmentController(EnvironmentController environmentController)
		{
			Log.Trace("SceneController.SetEnvironmentController()");
			_environmentController = environmentController;
		}

		//-------------------------------------

		public void SetVehicleController(VehicleController vehicleController)
		{
			Log.Trace("SceneController.SetVehicleController()");
			_vehicleController = vehicleController;
		}

		//-------------------------------------

		public void SetTurretManager(TurretManager turretManager)
		{
			Log.Trace("SceneController.SetTurretManager()");
			_turretManager = turretManager;
		}

		//-------------------------------------

		public void SetTargetManager(TargetManager targetManager)
		{
			Log.Trace("SceneController.SetTargetManager()");
			_targetManager = targetManager;
		}

		//------------------------------

		public void StartScene(float trainingTime)
		{
			Log.Trace("SceneController.StartScene()");

			Log.Info("SceneController.StartScene(): Creating a timer for " + trainingTime + " seconds");
			_trainingTimer = new TrainingTimer(trainingTime, SoftStopScene);

			if (_pantographController != null)
				_pantographController.Start();

			if (_targetManager != null)
				_targetManager.Start();
		}

		//------------------------------

		public void SceneStarted()
		{
			Log.Trace("SceneController.SceneStarted()");

			Log.Info("SceneController.SceneStarted(): Starting training timer");
			if (_trainingTimer != null)
				_trainingTimer.Start();
			if (_sceneResults != null)
				_sceneResults.SetStartTime();
		}

		//------------------------------

		public void SceneEnded()
		{
			Log.Trace("SceneController.SceneEnded()");

			if (_trainingTimer != null)
				_trainingTimer = null;

			if (_sceneResults != null)
				_sceneResults.Stop();
		}

		//------------------------------

		public void PauseScene(bool pause)
		{
			Log.Trace("SceneController.PauseScene(" + pause + ")");

			if (_trainingTimer != null && _trainingTimer.IsRunning())
				_trainingTimer.Pause(pause);

			if (_sceneInterface != null)
				_sceneInterface.PauseScene(pause);

			if (_pantographController != null)
				_pantographController.Pause(pause);

			if (_targetManager != null)
				_targetManager.Pause(pause);
		}

		//------------------------------

		public void SoftStopScene()
		{
			Log.Trace("SceneController.SoftStopScene()");

			PauseScene(true);

			if (_trainingTimer != null && _trainingTimer.IsRunning())
				_trainingTimer.Stop();

			const float delay = 5; // seconds
			Log.Info("SceneController.SoftStopScene(): Scene paused, waiting " + delay + " more seconds");
			_trainingTimer = new TrainingTimer(delay, StopScene);
			_trainingTimer.Start();

			if (_sceneResults != null)
				_sceneResults.Stop();
		}

		//------------------------------

		public void StopScene()
		{
			Log.Trace("SceneController.StopScene()");

			if (_trainingTimer != null && _trainingTimer.IsRunning())
				_trainingTimer.Stop();

			if (_sceneInterface.IsScenePaused())
				_sceneInterface.PauseScene(false);

			if (_targetManager != null)
				_targetManager.Stop();

			if (_pantographController != null)
				_pantographController.Stop();

			_turretManager = null;
			_targetManager = null;
			_vehicleController = null;
			_environmentController = null;
			_pantographController = null;
			_trainingTimer = null;

			if (sceneStopped != null)
				sceneStopped();
		}

		//-------------------------------------

		public void SetDaylightValue(float value)
		{
			Log.Trace("SceneController.SetDaylightValue(" + value + ")");
			Log.Debug($"SceneController.SetDaylightValue({value})");
			const float DarknessLevel = 15;
			_vehicleController.SetHeadlightsOn(value <= DarknessLevel);
			_environmentController.SetDayLightValue (value);
		}

		//-------------------------------------

		public void SetFogValue(float value)
		{
			Log.Trace("SceneController.SetFogValue(" + value + ")");
			_environmentController.SetFogValue (value);
		}

		//-------------------------------------

		public void SetRainValue(float value)
		{
			Log.Trace("SceneController.SetRainValue(" + value + ")");
			_environmentController.SetRainValue (value);
		}

		//-------------------------------------

		public void SetLrfEnabled(bool lrfEnabled)
		{
			Log.Trace("SceneController.SetLrfEnabled(" + lrfEnabled + ")");
			_turretManager.SetLrfEnabled(lrfEnabled);
		}

		//-------------------------------------

		public void SetAmmunitionCount(uint count)
		{
			Log.Trace("SceneController.SetAmmunitionCount(" + count + ")");

			Log.Debug("SceneController: AmmoCount: " + count);
			_turretManager.SetAmmunitionAmount (count);
		}

		//-------------------------------------

		public void SetFireJamming()
		{
			Log.Trace("SceneController.SetFireJamming()");
			_turretManager.SetFireJammed();
		}

		//-------------------------------------

		public void SetVehicleVelocity(float velocity)
		{
			Log.Trace("SceneController.SetVehicleVelocity(" + velocity + ")");
			_vehicleController.SetDriveVelocity(velocity);
		}

		//-------------------------------------

		public void SetVehicleStart(bool vehicleStart)
		{
			Log.Trace("SceneController.SetVehicleStart(" + vehicleStart + ")");

			if (vehicleStart)
				_vehicleController.Drive();
			else
				_vehicleController.Stop();
		}

		//-------------------------------------

		public void UpdateSceneConfig(SceneConfigurationData config)
		{
			Log.Trace("SceneController.UpdateSceneConfig(" + config + ")");

			if (config.reloadAmmoSafe)
				SetAmmunitionCount(config.ammoAmount);
			
			if (config.fireJammed)
				SetFireJamming();

			SetDaylightValue(config.daylightIntensity);
			SetFogValue(config.fogIntensity);
			SetRainValue(config.rainIntensity);
			SetVehicleVelocity(config.vehicleVelocity);
			SetVehicleStart(config.vehicleVelocity != 0);
			SetLrfEnabled(config.lrfEnabled);
		}

		//#endregion
	}
}
