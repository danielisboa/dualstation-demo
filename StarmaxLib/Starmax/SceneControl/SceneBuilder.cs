﻿using System;
using Models;
using Managers;
using Controllers;
using Common;

namespace Utils
{
	public class SceneBuilder
	{
		//#region privateAttributes

		WSCommunicationManager _wsCommunicationManager;

		TurretManager _turretManager;
		TargetManager _targetManager;
		VehicleController _vehicleController;
		EnvironmentController _environmentController;
		PantographController _pantographController;

		ISceneModel _sceneModel;

		//#endregion

		//------------------------------

		//#region publicMethods

		public SceneBuilder(WSCommunicationManager wsCommunicationManager)
		{
			Log.Trace($"SceneBuilder.SceneBuilder()");
			_wsCommunicationManager = wsCommunicationManager;
		}

		//------------------------------

		public void SetSceneModel(ISceneModel sceneModel)
		{
			Log.Trace($"SceneBuilder.SetSceneModel({sceneModel})");
			if (sceneModel == null) throw new Exception("Invalid SceneModel");
			_sceneModel = sceneModel;
		}

		//------------------------------

		public bool BuildScene(SceneController sceneController, SceneResults sceneResults,
		                       TrainingType trainingType, uint trainingSelection,
		                       SceneConfigurationData sceneConfig)
		{
			Log.Trace($"SceneBuilder.BuildScene({trainingType}, {trainingSelection}, {sceneConfig})");

			if (_sceneModel == null) throw new Exception("No SceneModel");

			try
			{
				switch (trainingType)
				{
					case TrainingType.Pantograph:
						BuildPantographScene(sceneController, sceneResults, trainingSelection, sceneConfig);
						break;

					case TrainingType.OperationalStatic:
					case TrainingType.OperationalMobile:
					case TrainingType.Calibrations:
					case TrainingType.Naval:
					case TrainingType.Boresight:
						BuildOperationalScene(sceneController, sceneResults, trainingSelection, sceneConfig);
						break;
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex.Message);
				Log.Info(ex.StackTrace);
				return false;
			}

			return true;
		}

		//#endregion

		//------------------------------

		//#region privateMethods

		public void BuildPantographScene(SceneController sceneController, SceneResults sceneResults,
		                                 uint trainingSelection, SceneConfigurationData sceneConfig)
		{
			Log.Trace($"SceneBuilder.BuildPantographScene({sceneConfig})");

			var pantographModel = _sceneModel.GetPantographModel() as IPantographModel;
			if (pantographModel == null) throw new Exception("SceneBuilder.BuildPantographScene(): No PantographModel");

			_pantographController = new PantographController(_wsCommunicationManager);

			_pantographController.SetModel(pantographModel);
			_pantographController.SetPattern((PantographPatterns) trainingSelection);
			_pantographController.SetCalibratedTargetSpeed((PantographCalibratedSpeed) sceneConfig.calibratedSpeed);
			_pantographController.SetLaserReqInterval(sceneConfig.laserReqInterval);
			_pantographController.SetFireReqInterval(sceneConfig.fireReqInterval);
			_pantographController.SetRequestDuration(sceneConfig.requestDuration);
			_pantographController.SetTrainingDuration(sceneConfig.trainingDuration);

			_pantographController.laserCountUpdate += sceneResults.ReceiveLaserCount;
			_pantographController.fireCountUpdate += sceneResults.ReceiveFireCount;
			_pantographController.laserHitsCountUpdate += sceneResults.ReceiveHitsCount;
			_pantographController.fireHitsCountUpdate += sceneResults.ReceiveKillCount;
			_pantographController.followPercentageUpdate += sceneResults.ReceiveAccuracy;
			_pantographController.trainingStartTime += sceneResults.SetStartTime;
			_pantographController.trainingStarted += sceneController.SceneStarted;
			_pantographController.trainingEnded += sceneController.SceneEnded;

			sceneResults.ReceiveTotalTargets(1);
			sceneController.SetPantographController(_pantographController);
		}

		//------------------------------

		public void BuildOperationalScene(SceneController sceneController, SceneResults sceneResults,
		                             uint trainingSelection, SceneConfigurationData sceneConfig)
		{
			Log.Trace($"SceneBuilder.BuildOperationalScene({sceneConfig})");

			var sceneSector = _sceneModel.GetSceneSectorModel(trainingSelection) as ISceneSectorModel;
			if (sceneSector == null) throw new Exception("SceneBuilder.BuildOperationalScene(): No SceneSector " + trainingSelection);

			var turretModel = _sceneModel.GetTurretModel() as ITurretModel;
			if (turretModel == null) throw new Exception("SceneBuilder.BuildOperationalScene(): No TurretModel");

			var vehicleModel = _sceneModel.GetVehicleModel() as IVehicleModel;
			if (vehicleModel == null) throw new Exception("SceneBuilder.BuildOperationalScene(): No VehicleModel");

			var environmentModel = _sceneModel.GetEnvironmentModel() as IEnvironmentModel;
			if (environmentModel == null) throw new Exception("SceneBuilder.BuildOperationalScene(): No EnvironmentModel");

			_turretManager = new TurretManager(_wsCommunicationManager);
			_turretManager.SetTurretModel(turretModel);
			_turretManager.SetAmmunitionAmount(sceneConfig.ammoAmount, true);
			_turretManager.SetWindSpeed(sceneConfig.windSpeed);

			_vehicleController = new VehicleController();
			_vehicleController.SetModel(vehicleModel);
			_vehicleController.SetVehiclePosition(sceneSector.GetVehiclePosition());
			_vehicleController.SetTrajectory(sceneSector.GetTrajectory());

			_targetManager = new TargetManager();
			_targetManager.SetDeathHitCount(sceneConfig.hitsToKillTargets);

			_targetManager.SetTargets(sceneConfig.numberOfTargets, sceneSector.GetTargets());
			_targetManager.SetPopup(sceneConfig.popupTargets, sceneConfig.popupInterval, sceneConfig.simultaneousTargets);
			_targetManager.targetsCleared += sceneController.SoftStopScene;
			_targetManager.trainingStarted += sceneController.SceneStarted;
			_targetManager.trainingEnded += sceneController.SceneEnded;

			_environmentController = new EnvironmentController();
			_environmentController.SetModel(environmentModel);

			_turretManager.fovUpdate += _environmentController.ReceiveFovAngle;

			sceneController.SetEnvironmentController(_environmentController);
			sceneController.SetVehicleController(_vehicleController);
			sceneController.SetTurretManager(_turretManager);
			sceneController.SetTargetManager(_targetManager);

			sceneController.UpdateSceneConfig (sceneConfig);

			_turretManager.laserCountUpdate += sceneResults.ReceiveLaserCount;
			_turretManager.fireCountUpdate += sceneResults.ReceiveFireCount;

			_vehicleController.vehicleVelocityUpdate += sceneResults.ReceiveVehicleVelocity;

			_targetManager.totalTargetsUpdate += sceneResults.ReceiveTotalTargets;
			_targetManager.hitsCountUpdate += sceneResults.ReceiveHitsCount;
			_targetManager.killCountUpdate += sceneResults.ReceiveKillCount;

			_environmentController.daylightIntensityUpdate += sceneResults.ReceiveDaylightValue;
			_environmentController.rainIntensityUpdate += sceneResults.ReceiveRainValue;
			_environmentController.fogIntensityUpdate += sceneResults.ReceiveFogValue;
		}

		//#endregion

		//------------------------------
	}
}
