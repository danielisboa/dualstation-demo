﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using Newtonsoft.Json;
using Common;

using Timer = System.Threading.Timer;

namespace Utils
{
	public class SceneResults
	{
		//#region  privateTypes

		struct Status
		{
			public float Accuracy;
			public uint LaserCount;
			public uint FireCount;
			public uint HitsCount;
			public uint KillCount;
			public float Time;

			public Status(float time)
			{
				this.Time = time;
				Accuracy = 0;
				LaserCount = 0;
				FireCount = 0;
				HitsCount = 0;
				KillCount = 0;
			}

			public Status(float time, Status other) : this(time)
			{
				Accuracy = other.Accuracy;
				LaserCount = other.LaserCount;
				FireCount = other.FireCount;
				HitsCount = other.HitsCount;
				KillCount = other.KillCount;
			}
		}

		//------------------------------

		struct Config
		{
			public float VehicleVelocity;
			public float DaylightValue;
			public float RainValue;
			public float FogValue;
			public float Time;

			public Config(float time)
			{
				this.Time = time;
				VehicleVelocity = 0;
				DaylightValue = 0;
				RainValue = 0;
				FogValue = 0;
			}

			public Config(float time, Config other) : this(time)
			{
				VehicleVelocity = other.VehicleVelocity;
				DaylightValue = other.DaylightValue;
				RainValue = other.RainValue;
				FogValue = other.FogValue;
			}
		}

		//------------------------------

		[System.Serializable]
		struct Snapshot
		{
			public int CurrentTime;
			public int PresentedTargets;
			public Status Status;
			public Config Config;

			public Snapshot(int tm, int tgts, Status st, Config cf)
			{
				CurrentTime = tm;
				PresentedTargets = tgts;
				Status = st;
				Config = cf;
			}
		}

		//------------------------------

		[System.Serializable]
		struct Results
		{
			public int TotalTime;
			public int TotalTargets;
			public Status[] Statuses;
			public Config[] Configs;
		}

		//------------------------------

		delegate void SetEventParam(ref Status st);
		delegate void SetConfigParam(ref Config cf);

		//#endregion

		//------------------------------

		//#region  publicAttributes

		public JsonDataDelegate updateSceneResults;

		//#endregion

		//------------------------------

		//#region  privateAttributes

		uint _totalTargets;
		List<Status> _events;
		List<Config> _configs;

		ISceneInterface _sceneInterface;

		float _startTime;
		float _stopTime;
		bool _started;

		bool _formatJson;

		Timer _timer;

		//#endregion

		//------------------------------

		//#region  publicMethods

		public SceneResults(ISceneInterface sceneInterface, bool formatJson = false)
		{
			Log.Trace("SceneResults.SceneResults()");

			_sceneInterface = sceneInterface;
			_formatJson = formatJson;

			_started = false;

			float startTime = (_sceneInterface != null) ? _sceneInterface.GetRunningTime() : 0;

			ResetRecords(startTime);

			const int periodicTime = 1000; //ms
			_timer = new Timer(this.TimerCallback);
			_timer.Change(periodicTime, periodicTime);
		}

		//-------------------------------------

		public void SetStartTime(float startTime = -1)
		{
			Log.Trace("SceneResults.SetStartTime(" + startTime + ")");
			if (startTime == -1) startTime = _sceneInterface.GetRunningTime();
			Log.Info("SceneResults.SetStartTime(): Training start time is " + startTime);
			ResetRecords(startTime);
			_started = true;
		}

		//-------------------------------------

		public void SetStopTime(float stopTime = -1)
		{
			Log.Trace($"SceneResults.SetStopTime(" + stopTime + ")");
			if (stopTime == -1) stopTime = _sceneInterface.GetRunningTime();
			Log.Info($"SceneResults.SetStopTime(): Training stop time is " + stopTime);
		}

		//-------------------------------------

		public void Stop()
		{
			Log.Trace("SceneResults.Stop()");

			_started = false;
			updateSceneResults = null;

			SetStopTime();

			if (_timer == null)
				return;

			Log.Debug("SceneResults.Stop(): disposing timer");
			_timer.Change(Timeout.Infinite, Timeout.Infinite);
			_timer.Dispose();
			_timer = null;
		}

		//------------------------------

		public void ReceiveTotalTargets(uint totalTargets)
		{
			Log.Trace("SceneResults.ReceiveTotalTargets(" + totalTargets + ")");
			_totalTargets = totalTargets;
		}

		//------------------------------

		public void ReceiveAccuracy(float accuracy)
		{
			Log.Trace("SceneResults.ReceiveAccuracy(" + accuracy + ")");
			if (LastEvent().Accuracy == accuracy) return;
			AddEvent(delegate(ref Status st) { st.Accuracy = accuracy; });
		}

		//------------------------------

		public void ReceiveLaserCount(uint laserCount)
		{
			Log.Trace("SceneResults.ReceiveLaserCount(" + laserCount + ")");
			if (LastEvent().LaserCount == laserCount) return;
			AddEvent(delegate(ref Status st) { st.LaserCount = laserCount; });
		}

		//------------------------------

		public void ReceiveFireCount(uint fireCount)
		{
			Log.Trace("SceneResults.ReceiveFireCount(" + fireCount + ")");
			if (LastEvent().FireCount == fireCount) return;
			AddEvent(delegate(ref Status st) { st.FireCount = fireCount; });
		}

		//------------------------------

		public void ReceiveHitsCount(uint hitsCount)
		{
			Log.Trace("SceneResults.ReceiveFireCount(" + hitsCount + ")");
			if (LastEvent().HitsCount == hitsCount) return;
			AddEvent(delegate(ref Status st) { st.HitsCount = hitsCount; });
		}

		//------------------------------

		public void ReceiveKillCount(uint killCount)
		{
			Log.Trace("SceneResults.ReceiveFireCount(" + killCount + ")");
			if (LastEvent().KillCount == killCount) return;
			AddEvent(delegate(ref Status st) { st.KillCount = killCount; });
		}

		//------------------------------

		public void ReceiveVehicleVelocity(float vehicleVelocity)
		{
			Log.Trace("SceneResults.ReceiveFireCount(" + vehicleVelocity + ")");
			if (LastConfig().VehicleVelocity == vehicleVelocity) return;
			AddConfig(delegate(ref Config cf) { cf.VehicleVelocity = vehicleVelocity; });
		}

		//------------------------------

		public void ReceiveDaylightValue(float daylightValue)
		{
			Log.Trace("SceneResults.ReceiveFireCount(" + daylightValue + ")");
			if (LastConfig().DaylightValue == daylightValue) return;
			AddConfig(delegate(ref Config cf) { cf.DaylightValue = daylightValue; });
		}

		//------------------------------

		public void ReceiveRainValue(float rainValue)
		{
			Log.Trace("SceneResults.ReceiveFireCount(" + rainValue + ")");
			if (LastConfig().RainValue == rainValue) return;
			AddConfig(delegate(ref Config cf) { cf.RainValue = rainValue; });
		}

		//------------------------------

		public void ReceiveFogValue(float fogValue)
		{
			Log.Trace("SceneResults.ReceiveFireCount(" + fogValue + ")");
			if (LastConfig().FogValue == fogValue) return;
			AddConfig(delegate(ref Config cf) { cf.FogValue = fogValue; });
		}

		//------------------------------

		public string GetTrainingResultsJson()
		{
			Log.Trace("SceneResults.GetTrainingResultsJson()");

			Results results;
			int totalTime = (int) Math.Round(Time());
			if (_stopTime > _startTime)
				totalTime = (int) Math.Round(_stopTime - _startTime);
			results.TotalTime = totalTime;
			results.TotalTargets = (int) _totalTargets;
			results.Statuses = _events.ToArray();
			results.Configs = _configs.ToArray();

			var format = (_formatJson) ? Formatting.Indented : Formatting.None;
			return JsonConvert.SerializeObject(results, format);
		}

		//#endregion

		//------------------------------

		//#region  privateMethods

		void AddEvent(SetEventParam param)
		{
			Log.Trace("SceneResults.AddEvent(" + param + ")");

			float time = Time();
			if (time < 0) return;
			Log.Disabled("SceneResults.AddEvent(): param = " + param);
			if (param == null || _events == null || _sceneInterface == null) return;
			var ev = new Status(time, _events.Last());
			param(ref ev);
			_events.Add(ev);
		}

		//------------------------------

		Status LastEvent()
		{
			Log.Trace("SceneResults.LastEvent()");
			return _events.Last();
		}

		//------------------------------

		void AddConfig(SetConfigParam param)
		{
			Log.Trace("SceneResults.AddConfig(" + param + ")");
			float time = Time();
			if (time < 0) return;
			Log.Disabled("SceneResults.AddConfig(): param = " + param);
			if (param == null || _configs == null || _sceneInterface == null) return;
			var sc = new Config(time, _configs.Last());
			param(ref sc);
			_configs.Add(sc);
		}

		//------------------------------

		Config LastConfig()
		{
			Log.Trace("SceneResults.LastConfig()");
			return _configs.Last();
		}

		//------------------------------

		void ResetRecords(float startTime)
		{
			Log.Trace("SceneResults.ResetRecords(" + startTime + ")");
			_stopTime = 0;
			_startTime = startTime;
			_events = new List<Status>();
			_configs = new List<Config>();
			_configs.Add(new Config(0));
			_events.Add(new Status(0));
		}

		//------------------------------

		void TimerCallback(Object state = null)
		{
			Log.Trace("SceneResults.TimerCallback()");
			if (_started) UpdateResults();
		}

		//------------------------------

		void UpdateResults()
		{
			Log.Trace("SceneResults.UpdateResults()");
			if (updateSceneResults == null) return;
			var currResults = new Snapshot((int) Time(), (int) _totalTargets, _events.Last(), _configs.Last());
			var format = (_formatJson) ? Formatting.Indented : Formatting.None;
			var json = JsonConvert.SerializeObject(currResults, format);
			updateSceneResults(json);
		}

		//------------------------------

		float Time()
		{
			Log.Trace("SceneResults.Time()");
			return _sceneInterface.GetRunningTime() - _startTime;
		}

		//#endregion
	}
}

