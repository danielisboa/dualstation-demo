﻿using System;
using Common;

namespace Utils
{
	public interface ISceneInterface
	{
		void AddLevelLoadedCallback(UpdateEventDelegate callback);
		void LoadTrainingScene(Common.TrainingType trainingType);
		void SetErrorMessage(string message);
		void PauseScene(bool pause);
		bool IsScenePaused();
		float GetRunningTime();
	}
}
