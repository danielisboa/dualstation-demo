﻿using System;
using Models;
using Controllers;
using Utils;
using Common;
using Newtonsoft.Json;

namespace Managers
{
	public class SceneManager
	{
		//#region  privateAttributes

		StarmaxConfigurationData _starmaxConfig;
		SceneConfigurationData _sceneConfig;

		ISceneInterface _sceneInterface;
		ISceneCalibrator _sceneCalibrator;

		WSCommunicationManager  _wsCommunicationManager;
		GuiCommunicationManager _guiCommunicationManager;

		SceneBuilder _sceneBuilder;
		SceneController _sceneController;
		SceneResults _sceneResults;

		TrainingType _trainingType;
		uint _trainingSelection;
		bool _sceneStarted;

		//#endregion

		//------------------------------

		//#region  publicMethods

		public SceneManager(WSCommunicationManager wsCommunicationManager, GuiCommunicationManager guiCommunicationManager)
		{
			Log.Trace($"SceneManager.SceneManager(" + wsCommunicationManager + ", " + guiCommunicationManager + ")");

			_wsCommunicationManager  = wsCommunicationManager;
			_guiCommunicationManager = guiCommunicationManager;

			_starmaxConfig = new StarmaxConfigurationData();
			_starmaxConfig.LoadDefaultConfiguration();

			_wsCommunicationManager.DynamicsSimulation = _starmaxConfig.dynamicsSimulation;
			Log.Info("SceneManager.SceneManager(): DynamicsSimulation is " + ((_starmaxConfig.dynamicsSimulation) ? "enabled" : "disabled"));

			_wsCommunicationManager.SetupConnection(_starmaxConfig.wsCommLocalPort, _starmaxConfig.wsCommRemoteIP, _starmaxConfig.wsCommRemotePort);
			_guiCommunicationManager.SetupConnection(_starmaxConfig.guiCommSimPort, _starmaxConfig.wsCommLocalIP, _starmaxConfig.guiCommGuiPort);

			_guiCommunicationManager.trainingSelectionReceived += this.GetTrainingSelection;
			_guiCommunicationManager.trainingControlReceived += this.GetTrainingControl;
			_guiCommunicationManager.fireTrainingConfigReceived += this.GetFireTrainingConfig;
			_guiCommunicationManager.pantographTrainingConfigReceived += this.GetPantographTrainingConfig;
			_guiCommunicationManager.simCalibrationsReceived += this.GetSimCalibrations;

			// Uncomment line below to enable debug sending of SimStatus to GUI
			// WARNING: THIS GENERATES A LOT OF DATA EXCHANGE
			// // // _wsCommunicationManager.simStatusSent += this.GetSimStatus;

			_sceneBuilder = new SceneBuilder(_wsCommunicationManager);
		}

		//------------------------------

		~SceneManager()
		{
			_sceneController = null;
			_sceneResults = null;
			_sceneBuilder = null;
		}

		//------------------------------

		public void SetSceneCalibrator(ISceneCalibrator sceneCalibrator)
		{
			Log.Trace("SceneManager.SetSceneCalibrator(" + sceneCalibrator + ")");
			_sceneCalibrator = sceneCalibrator;

			if (_sceneCalibrator != null)
			{
				SimCalibrationsData calib = new SimCalibrationsData();
				calib.DetailDistance = _starmaxConfig.terrainDetailDistance;
				calib.DetailDensity = _starmaxConfig.terrainDetailDensity;
				calib.BillboardDistance = _starmaxConfig.terrainBillboardDistance;
				calib.BaseMapDistance = _starmaxConfig.baseMapDistance;
				calib.UseTerrainDetail = true;
				Log.Debug($"SceneManager.SetSceneCalibrator(): DetailDistance = {calib.DetailDistance}, DetailDensity = {calib.DetailDensity}");
				_sceneCalibrator.SetCalibrationData(calib);
			}
		}

		//------------------------------

		public void SetSceneInterface(ISceneInterface sceneInterface)
		{
			Log.Trace("SceneManager.SetSceneInterface(" + sceneInterface + ")");
			_sceneInterface = sceneInterface;
			_sceneInterface.AddLevelLoadedCallback(this.SceneLoaded);
		}

		//------------------------------

		public void SetSceneModel(ISceneModel sceneModel)
		{
			Log.Trace($"SceneManager.SetSceneModel({sceneModel})");
			if (sceneModel == null) throw new Exception("Invalid SceneModel");
			_sceneBuilder.SetSceneModel(sceneModel);
		}

		//------------------------------

		public void ReceiveCommunicationStatus(bool communicationOk)
		{
			Log.Trace($"SceneManager.GetCommunicationStatus({communicationOk})");
			Log.Debug($"SceneManager.GetCommunicationStatus(): Communication status is " + ((communicationOk) ? "ok" : "not ok"));

			if (!communicationOk && _sceneStarted)
				StopScene();
		}

		//------------------------------

		public void StartScene()
		{
			Log.Trace("SceneManager.StartScene()");

			_sceneConfig = _guiCommunicationManager.GetSceneConfig();

			_sceneResults = new SceneResults(_sceneInterface);
			_sceneResults.updateSceneResults += _guiCommunicationManager.SendTrainingSnapshotMsg;

			_sceneController = new SceneController(_sceneInterface, _sceneResults);
			_sceneController.sceneStopped += this.StopScene;

			if (_sceneBuilder == null || !_sceneBuilder.BuildScene(_sceneController, _sceneResults,
			                                                       _trainingType, _trainingSelection,
			                                                       _sceneConfig))
			{
				_sceneInterface.SetErrorMessage("Falha carregando cena");
				_sceneInterface.LoadTrainingScene(TrainingType.NoType);
				_guiCommunicationManager.SendSimTrainingStatusMsg(TrainingControl.Failure);

				_sceneResults.Stop();

				_sceneResults = null;
				_sceneController = null;

				return;
			}

			Log.Event("Starting training");

			_sceneController.StartScene(_sceneConfig.trainingDuration);

			ConfigScene();

			_wsCommunicationManager.EnableSafetySwitch();
			_guiCommunicationManager.SendSimTrainingStatusMsg(TrainingControl.Start);

			_sceneStarted = true;
		}

		//------------------------------

		public void PauseScene(bool pause)
		{
			Log.Trace("SceneManager.PauseScene(" + pause + ")");

			if (!_sceneStarted)
				return;

			_sceneController.PauseScene(pause);

			var status = (pause) ? TrainingControl.Pause : TrainingControl.Resume;
			_guiCommunicationManager.SendSimTrainingStatusMsg(status);
		}

		//------------------------------

		public void StopScene()
		{
			Log.Trace("SceneManager.StopScene()");

			_wsCommunicationManager.SetEncoderSimulation(false);
			_wsCommunicationManager.DisableSafetySwitch();

			if (!_sceneStarted)
				return;

			_sceneStarted = false;

			_guiCommunicationManager.SendSimTrainingStatusMsg(TrainingControl.Stop);

			if (_sceneController != null)
			{
				_sceneController.StopScene();
				_sceneController = null;
			}

			if (_sceneResults != null)
			{
				var results = _sceneResults.GetTrainingResultsJson();
				_sceneResults.Stop();
				_sceneResults = null;
				Log.Disabled("SceneManager.StopScene(): resultsJson =\n" + results);
				_guiCommunicationManager.SendTrainingResultsMsg(results);
			}

			LoadScene(TrainingType.NoType);
		}

		//#endregion

		//------------------------------

		//#region  privateMethods

		void GetSimStatus()
		{
			Log.Trace("SceneManager.GetSimStatus()");
			var simStatus = _wsCommunicationManager.GetSimStatusMsg();
			var simStatusJson = JsonConvert.SerializeObject(simStatus, Formatting.Indented);
			_guiCommunicationManager.SendSimStatus(simStatusJson);
		}

		//------------------------------

		void GetTrainingSelection()
		{
			Log.Trace("SceneManager.GetTrainingSelection()");

			_trainingType = _guiCommunicationManager.GetTrainingType();
			_trainingSelection = _guiCommunicationManager.GetTrainingSelection();

			Log.Debug("SceneManager.GetTrainingSelection() - TYPE: " + _trainingType + " - SELECTION: " + _trainingSelection);
		}

		//------------------------------

		void GetTrainingControl()
		{
			Log.Trace("SceneManager.GetTrainingControl()");

			var control = _guiCommunicationManager.GetTrainingControl();

			switch (control)
			{
				case TrainingControl.Start:
					Log.Event("Loading training scene with type " + _trainingType);
					LoadScene(_trainingType);
					break;

				case TrainingControl.Pause:
					Log.Event("Pausing training");
					PauseScene(true);
					break;

				case TrainingControl.Resume:
					Log.Event("Resuming training");
					PauseScene(false);
					break;

				case TrainingControl.Stop:
					Log.Event("Stoping training");
					StopScene();
					break;

				default:
					Log.Debug("SceneManager.GetTrainingControl(): Unknown control " + control);
					break;
			}
		}

		//------------------------------

		void GetFireTrainingConfig()
		{
			Log.Trace("SceneManager.GetFireTrainingConfig()");

			_sceneConfig = _guiCommunicationManager.GetSceneConfig();

			if (_sceneStarted && _sceneController != null)
				_sceneController.UpdateSceneConfig (_sceneConfig);
			else
				Log.Debug ($"SceneManager.GetFireTrainingConfig(): _sceneStarted = {_sceneStarted}, _sceneController = {_sceneController}");
		}

		//------------------------------

		void GetPantographTrainingConfig()
		{
			Log.Trace("SceneManager.GetPantographTrainingConfig()");
			_sceneConfig = _guiCommunicationManager.GetSceneConfig();
		}

		//------------------------------

		void GetSimCalibrations()
		{
			Log.Trace("SceneManager.GetSimCalibrations()");

			var simCalibration = _guiCommunicationManager.GetSimCalibrations ();
			simCalibration.BillboardDistance = _starmaxConfig.terrainBillboardDistance;
			simCalibration.BaseMapDistance = _starmaxConfig.baseMapDistance;

			if (_sceneCalibrator != null)
				_sceneCalibrator.SetCalibrationData(simCalibration);
		}

		//------------------------------

		void LoadScene(TrainingType trainingType)
		{
			if (_sceneInterface != null)
				_sceneInterface.LoadTrainingScene(trainingType);
		}

		//------------------------------

		void SceneLoaded()
		{
			Log.Trace($"SceneManager.SceneLoaded()");
			Log.Event("Training scene loaded");
			StartScene();
		}

		//------------------------------

		void ConfigScene()
		{
			Log.Trace($"SceneManager.ConfigScene()");
			switch (_trainingType)
			{
				case TrainingType.Pantograph:
					GetPantographTrainingConfig();
					break;

				case TrainingType.OperationalStatic:
				case TrainingType.OperationalMobile:
				case TrainingType.Calibrations:
				case TrainingType.Boresight:
					GetFireTrainingConfig();
					break;
			}
		}

		//#endregion
	}
}
