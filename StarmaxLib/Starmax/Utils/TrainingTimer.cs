﻿using Common;
using System;
using System.Collections;
using System.Threading;

namespace Utils
{
	public class TrainingTimer
	{
		#region privateAttributes

		UpdateEventDelegate _timerExpired;
		Thread _thread;
		float _waitTime;
		float _alreadySleptTime;
		DateTime _pauseTime;
		bool _running;
		bool _paused;

		#endregion

		//-------------------------------------

		#region publicMethods

		public TrainingTimer(float time, UpdateEventDelegate callback)
		{
			_waitTime = time;
			_timerExpired = callback;
			_thread = new Thread(new ThreadStart(WaitTime));
			Log.Info("TrainingTimer.TrainingTimer(): Setting timer for " + _waitTime + " seconds");
		}

		//-------------------------------------

		public void Start()
		{
			Log.Info("TrainingTimer.TrainingTimer(): Starting timer for " + _waitTime + " seconds");
			_thread.Start();
		}

		//-------------------------------------

		public void Pause(bool pause)
		{
			Log.Info("TrainingTimer.Pause(" + pause + ")");

			if (pause)
			{
				_pauseTime = DateTime.Now;
				Log.Info("TrainingTimer.Pause(): _pauseTime is " + _pauseTime);
			}
			else
			{
				var addedTime = TimeFromPause();
				_waitTime += addedTime - _alreadySleptTime;
				Log.Info("TrainingTimer.Pause():" +
				         " addedTime = " + addedTime +
				         ", _alreadySleptTime = " + _alreadySleptTime +
				         ", _waitTime is now " + _waitTime);
			}

			_paused = pause;
		}

		//-------------------------------------

		public bool IsRunning()
		{
			return _running;
		}

		//-------------------------------------

		public void Stop()
		{
			_running = false;
			_paused = false;
		}

		#endregion

		//-------------------------------------

		#region privateMethods

		~TrainingTimer()
		{
			Log.Info("TrainingTimer.WaitTime(): Aborting thread execution");
			_running = false;
			_thread.Abort();
		}

		//-------------------------------------

		float TimeFromPause()
		{
			var now = DateTime.Now;
			var timeSpan = now - _pauseTime;
			var diffTime = (float) (timeSpan.TotalMilliseconds) / 1000f;
			return diffTime;
		}

		//-------------------------------------

		void WaitTime()
		{
			_running = true;
			_paused = false;
			_alreadySleptTime = 0;

			while (_running && _waitTime > 0)
			{
				float waitTime = _waitTime;
				_waitTime = 0;

				if (!_paused)
					Log.Info("TrainingTimer.WaitTime(): Sleeping for " + waitTime + " seconds");

				bool wasPausedBeforeSleep = _paused;

				Thread.Sleep((int) (waitTime * 1000));

				if (_paused)
				{
					if (!wasPausedBeforeSleep)
						_alreadySleptTime = TimeFromPause();
					_waitTime = 0.2f;
				}
				else if (_running)
					Log.Info("TrainingTimer.WaitTime(): Timer of " + waitTime + " expired");
				else
					Log.Info("TrainingTimer.WaitTime(): Thread stopped while sleeping");
			}

			if (!_running)
				return;

			if (_timerExpired != null)
				_timerExpired();

			Log.Debug("TrainingTimer.WaitTime(): Bailing out");

			_running = false;
		}

		#endregion
	}
}

