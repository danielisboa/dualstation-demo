﻿using System;
using Managers;
using Common;

namespace Utils
{
	public class NetworkStatus
	{
		//#region  publicAttributes

		public CommunicationStatusDelegate communicationStatusChanged;

		//#endregion

		//----------------------------

		//#region  privateAttributes

		bool _wsCommOk;
		bool _guiCommOk;
		bool _commOk;

		//#endregion

		//----------------------------

		//#region  publicMethods

		public NetworkStatus()
		{
			Log.Trace("NetworkStatus.NetworkStatus()");
		}

		//----------------------------

		public void ReceiveWsCommunicationStatus(bool communicationOk)
		{
			Log.Trace($"NetworkStatus.RcvWsCommStatus({communicationOk})");
			Log.Debug("NetworkStatus.ReceiveWsCommunicationStatus(): WS communication status is " + ((communicationOk) ? "ok" : "not ok"));
			CheckCommStatus(communicationOk, _guiCommOk);
		}

		//----------------------------

		public void ReceiveGuiCommunicationStatus(bool communicationOk)
		{
			Log.Trace($"NetworkStatus.RcvGuiCommStatus({communicationOk})");
			Log.Debug("NetworkStatus.ReceiveGuiCommunicationStatus(): GUI communication status is " + ((communicationOk) ? "ok" : "not ok"));
			CheckCommStatus(_wsCommOk, communicationOk);
		}

		//----------------------------

		public void NotifyCommunicationStatus()
		{
			Log.Info("NetworkStatus.NotifyCommunicationStatus(): Communication status is " + ((_commOk) ? "ok" : "not ok"));
			if (communicationStatusChanged != null)
				communicationStatusChanged(_commOk);
		}

		//#endregion

		//----------------------------

		//#region  privateMethods

		void CheckCommStatus(bool wsCommOk, bool guiCommOk)
		{
			Log.Trace($"NetworkStatus.CheckCommStatus({wsCommOk}, {guiCommOk})");

			_wsCommOk = wsCommOk;
			_guiCommOk = guiCommOk;

			bool commOk = wsCommOk && guiCommOk;

			if (commOk != _commOk)
			{
				_commOk = commOk;
				NotifyCommunicationStatus();
			}
		}

		//#endregion
	}
}
