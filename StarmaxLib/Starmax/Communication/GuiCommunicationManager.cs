﻿using System;
using System.Threading;
using GuiCommunication;
using Infrastructure;
using Common;

using Timer = System.Threading.Timer;

namespace Managers
{
	public class GuiCommunicationManager : Observer<GuiSimMsg>
	{
		//#region  publicAttributes

		public CommunicationStatusDelegate communicationStatusChanged;
		public UpdateEventDelegate trainingSelectionReceived;
		public UpdateEventDelegate fireTrainingConfigReceived;
		public UpdateEventDelegate pantographTrainingConfigReceived;
		public UpdateEventDelegate trainingControlReceived;
		public UpdateEventDelegate simCalibrationsReceived;

		//#endregion

		//-------------------------------------

		//#region  privateAttributes

		bool _connected;
		bool _phonyConnection;

		TcpJsonComm _tcpJsonComm;

		SceneConfigurationData _sceneConfig;
		SimCalibrationsData _simCalibrations;

		Common.TrainingType _selectedTrainingType;
		uint _selectedTrainingNumber;

		Common.TrainingControl _trainingControl;

		// GuiSimProtocol.SimFireTrainingStatus SimFireTrainingStatus;

		int _remoteCommPort;

		Timer _timer;

		//#endregion

		//-------------------------------------

		//#region  publicMethods

		public GuiCommunicationManager()
		{
			Log.Trace("GuiCommunicationManager.GuiCommunicationManager()");
			TcpJsonComm.SetLogLevel(Log.Level.Disabled);
			_tcpJsonComm = TcpJsonComm.Instance();
		}

		//-------------------------------------

		~GuiCommunicationManager()
		{
			Log.Trace("GuiCommunicationManager.~GuiCommunicationManager()");
			Disconnect();
		}

		//-------------------------------------

		public void SetupConnection(int localPort, string remoteAddress, int remotePort)
		{
			string args = $"{localPort}, {remoteAddress}, {remotePort}";
			Log.Trace("GuiCommunicationManager.SetupConnection(" + args + ")");

			_timer = new Timer(this.TimerCallback);
			_timer.Change(1000, 1000);

			lock (_tcpJsonComm)
			{
				_tcpJsonComm.messageReceived = this.GetJsonMsg;

				if (!_tcpJsonComm.IsConnected())
					_tcpJsonComm.Connect(_remoteCommPort = remotePort);

				NotifyCommStatus(_tcpJsonComm.IsConnected());
			}
		}

		//-------------------------------------

		public void Disconnect()
		{
			Log.Trace("GuiCommunicationManager.Disconnect()");
			_timer.Change(Timeout.Infinite, Timeout.Infinite);
			lock (_tcpJsonComm) _tcpJsonComm.Close();
			_tcpJsonComm.messageReceived = null;
			NotifyCommStatus(false);
			_timer = null;
		}

		//-------------------------------------

		public void GetJsonMsg()
		{
			Log.Trace("GuiCommunicationManager.GetJsonMsg()");

			Log.Trash("GuiCommunicationManager.GetJsonMsg(): Getting received messages");

			NotifyCommStatus(true);

			while (_tcpJsonComm.HasMessage())
			{
				var opcode = _tcpJsonComm.GetNextMsgId();

				Log.Trash("GuiCommunicationManager.GetJsonMsg(): Got opcode " + opcode);

				switch (opcode)
				{
					case (int) GuiSimProtocol.Opcodes.GuiTrainingSelection:
						GuiSimProtocol.GuiTrainingSelection trainingSelection;
						if (_tcpJsonComm.GetNextMsg(out trainingSelection))
						{
							Log.Debug("GuiCommunicationManager.GetJsonMsg(): trainingSelection = " + trainingSelection);
							HandleGuiTrainingSelection(trainingSelection);
						}
						else
							Log.Debug("GuiCommunicationManager.GetJsonMsg(): trainingSelection invalid");
						break;

					case (int) GuiSimProtocol.Opcodes.GuiTrainingControl:
						GuiSimProtocol.GuiTrainingControl trainingControl;
						_tcpJsonComm.GetNextMsg(out trainingControl);
						Log.Debug("GuiCommunicationManager.GetJsonMsg(): trainingControl = " + trainingControl);
						HandleGuiTrainingControl(trainingControl);
						break;

					case (int) GuiSimProtocol.Opcodes.GuiFireTrainingConfig:
						GuiSimProtocol.GuiFireTrainingConfig fireTrainingConfig;
						_tcpJsonComm.GetNextMsg(out fireTrainingConfig);
						Log.Debug("GuiCommunicationManager.GetJsonMsg(): fireTrainingConfig = " + fireTrainingConfig);
						HandleGuiFireTrainingConfig(fireTrainingConfig);
						break;

					case (int) GuiSimProtocol.Opcodes.GuiPantographTrainingConfig:
						GuiSimProtocol.GuiPantographTrainingConfig pantographTrainingConfig;
						_tcpJsonComm.GetNextMsg(out pantographTrainingConfig);
						Log.Debug("GuiCommunicationManager.GetJsonMsg(): pantographTrainingConfig = " + pantographTrainingConfig);
						HandleGuiPantographTrainingConfig(pantographTrainingConfig);
						break;

					case (int) GuiSimProtocol.Opcodes.SimCalibrations:
						GuiSimProtocol.SimCalibrations simCalibrations;
						_tcpJsonComm.GetNextMsg(out simCalibrations);
						Log.Debug("GuiCommunicationManager.GetJsonMsg(): simCalibrations = " + simCalibrations);
						HandleSimCalibrations(simCalibrations);
						break;

					default:
						Log.Debug("GuiCommunicationManager.GetJsonMsg(): Unhandled opcode " + opcode);
						break;
				}
			}

			Log.Trash("GuiCommunicationManager.GetJsonMsg(): Finished getting messages");
		}

		//-------------------------------------

		public void NotifyCommStatus(bool commOk)
		{
			if (commOk == _connected) return;
			Log.Warning("GuiCommunicationManager.NotifyCommStatus(): GUI connection status is " + ((commOk) ? "ok" : "not ok"));
			if (communicationStatusChanged != null)
				communicationStatusChanged(commOk);
			_connected = commOk;
		}

		//-------------------------------------

		public void SendCommunicationStatus(bool communicationOk)
		{
			Log.Trace($"GuiCommunicationManager.SendCommunicationStatus({communicationOk})");

			var commStatus = new GuiSimProtocol.CommunicationStatus(communicationOk);

			lock (_tcpJsonComm)
			{
				if (!_tcpJsonComm.SendMsg((int) GuiSimProtocol.Opcodes.CommunicationStatus, commStatus))
					Log.Error("GuiCommunicationManager.SendCommunicationStatus(): Sending commStatus failed");
				else
					Log.Warning($"GuiCommunicationManager.SendCommunicationStatus(): Sent communication status " + communicationOk);
			}
		}

		//-------------------------------------

		public void SendSimTrainingStatusMsg(Common.TrainingControl status)
		{
			Log.Trace($"GuiCommunicationManager.SendSimTrainingStatusMsg({status})");
			var simStatus = new GuiSimProtocol.SimTrainingStatus((byte) status);
			Log.Debug($"GuiCommunicationManager.SendSimTrainingStatusMsg(): Sending simulation status " + status);

			lock (_tcpJsonComm)
			{
				if (!_tcpJsonComm.SendMsg((int) GuiSimProtocol.Opcodes.SimTrainingStatus, simStatus))
					Log.Error("GuiCommunicationManager.SendSimTrainingStatusMsg(): Sending simStatus failed");
			}
		}

		//-------------------------------------

		// public void SendSimFireTrainingStatusMsg()
		// {
		// 	lock (_tcpJsonComm) _tcpJsonComm.SendMsg((int) GuiSimProtocol.Opcodes.SimFireTrainingStatus, SimFireTrainingStatus);
		// }

		//-------------------------------------

		public void SendTrainingResultsMsg(string resultsJson)
		{
			lock (_tcpJsonComm) _tcpJsonComm.SendMsg((int) GuiSimProtocol.Opcodes.SimTrainingResults, resultsJson);
		}

		//-------------------------------------

		public void SendTrainingSnapshotMsg(string snapshotJson)
		{
			lock (_tcpJsonComm) _tcpJsonComm.SendMsg((int) GuiSimProtocol.Opcodes.SimTrainingStatusSnapshot, snapshotJson);
		}

		//-------------------------------------

		public void SendSimStatus(string statusJson)
		{
			lock (_tcpJsonComm) _tcpJsonComm.SendMsg((int) GuiSimProtocol.Opcodes.SimStatus, statusJson);
		}

		//-------------------------------------

		// Observer
		override public void OnNext(GuiSimMsg msg)
		{
			NotifyCommStatus(true);

			switch (msg.Opcode)
			{
				case GuiSimProtocol.Opcodes.GuiTrainingSelection:
					HandleGuiTrainingSelection(msg.GetTrainingSelection());
					break;

				case GuiSimProtocol.Opcodes.GuiFireTrainingConfig:
					HandleGuiFireTrainingConfig(msg.GetFireTrainingConfig());
					break;

				case GuiSimProtocol.Opcodes.GuiPantographTrainingConfig:
					HandleGuiPantographTrainingConfig(msg.GetPantographTrainingConfig());
					break;

				case GuiSimProtocol.Opcodes.GuiTrainingControl:
					HandleGuiTrainingControl(msg.GetTrainingControl());
					break;

				case GuiSimProtocol.Opcodes.SimCalibrations:
					HandleSimCalibrations(msg.GetSimCalibrations());
					break;
			}
		}

		//-------------------------------------

		// public GuiSimProtocol.SimFireTrainingStatus GetSimStatusMsg()
		// {
		// 	Log.Periodic("GuiCommunicationManager.GetSimStatusMsg()");
		// 	return SimFireTrainingStatus;
		// }

		//-------------------------------------

		public void SetPhonyGuiTrainingSelection(GuiSimProtocol.GuiTrainingSelection trainingSelection)
		{
			_phonyConnection = true;
			Log.Trace("GuiCommunicationManager.SetPhonyGuiTrainingSelection()");
			HandleGuiTrainingSelection(trainingSelection);
			NotifyCommStatus(true);
		}

		//-------------------------------------

		public void SetPhonyGuiTrainingControl(GuiSimProtocol.GuiTrainingControl trainingControl)
		{
			_phonyConnection = true;
			Log.Trace("GuiCommunicationManager.SetPhonyGuiTrainingControl()");
			HandleGuiTrainingControl(trainingControl);
			NotifyCommStatus(true);
		}

		//-------------------------------------

		public void SetPhonyGuiFireTrainingConfig(GuiSimProtocol.GuiFireTrainingConfig trainingConfig)
		{
			_phonyConnection = true;
			Log.Trace("GuiCommunicationManager.()");
			HandleGuiFireTrainingConfig(trainingConfig);
			NotifyCommStatus(true);
		}

		//-------------------------------------

		public void SetPhonyGuiPantographTrainingConfig(GuiSimProtocol.GuiPantographTrainingConfig trainingConfig)
		{
			Log.Trace("GuiCommunicationManager.SetPhonyGuiPantographTrainingConfig()");
			HandleGuiPantographTrainingConfig(trainingConfig);
		}

		//-------------------------------------

		public TrainingControl GetTrainingControl()
		{
			Log.Trace("GuiCommunicationManager.GetTrainingControl()");
			return _trainingControl;
		}

		//-------------------------------------

		public TrainingType GetTrainingType()
		{
			Log.Trace("GuiCommunicationManager.GetTrainingType()");
			return _selectedTrainingType;
		}

		//-------------------------------------

		public uint GetTrainingSelection()
		{
			Log.Trace("GuiCommunicationManager.GetTrainingSelection()");
			return _selectedTrainingNumber;
		}

		//-------------------------------------

		public SceneConfigurationData GetSceneConfig()
		{
			Log.Trace("GuiCommunicationManager.GetSceneConfig()");
			return _sceneConfig;
		}

		//-------------------------------------

		public SimCalibrationsData GetSimCalibrations()
		{
			Log.Trace("GuiCommunicationManager.GetSimCalibrations()");
			return _simCalibrations;
		}

		//#endregion

		//-------------------------------------

		//#region  privateMethods

		void TimerCallback(Object state = null)
		{
			lock (_tcpJsonComm)
			{
				if (_tcpJsonComm.IsConnected())
				{
					Log.Periodic("GuiCommunicationManager.TimerCallback(): Sendind KeepAlive msg");
					_tcpJsonComm.SendMsg((int) GuiSimProtocol.Opcodes.KeepAlive, "");
				}
				else
					_tcpJsonComm.Connect(_remoteCommPort);

				if (!_phonyConnection)
					NotifyCommStatus(_tcpJsonComm.IsConnected());
			}
		}

		//-------------------------------------

		void HandleGuiTrainingSelection(GuiSimProtocol.GuiTrainingSelection selMsg)
		{
			Log.Disabled("GuiCommunicationManager.HandleGuiTrainingSelection()");

			_selectedTrainingType = (Common.TrainingType) selMsg.TrainingType;
			_selectedTrainingNumber = (uint) selMsg.TrainingSelection;

			Log.Disabled ("GuiCommunicationManager.HandleGuiTrainingSelection() - Type = " + _selectedTrainingType + " - Selection: " + _selectedTrainingNumber);

			if (trainingSelectionReceived != null) trainingSelectionReceived();
		}

		//-------------------------------------

		void HandleGuiFireTrainingConfig(GuiSimProtocol.GuiFireTrainingConfig confMsg)
		{
			Log.Debug("GuiCommunicationManager.HandleGuiFireTrainingConfig()");

			_sceneConfig.trainingDuration = confMsg.TrainingDuration;

			_sceneConfig.numberOfTargets = confMsg.NumberOfTargets;

			_sceneConfig.popupTargets = confMsg.PopupTargets == 1;
			_sceneConfig.popupInterval = confMsg.PopupInterval;
			_sceneConfig.simultaneousTargets = confMsg.SimultaneousTargets;

			_sceneConfig.hitsToKillTargets = confMsg.HitsToKillTargets;
			_sceneConfig.ammoAmount = confMsg.AmmunitionCount;
			_sceneConfig.reloadAmmoSafe = confMsg.ReloadAmmoSafe == 1;
			_sceneConfig.fireJammed = confMsg.FireJamming == 1;

			_sceneConfig.vehicleVelocity = confMsg.VehicleSpeed;

			_sceneConfig.windSpeed = confMsg.WindSpeed;
			_sceneConfig.daylightIntensity = confMsg.DaylightIntensity;
			_sceneConfig.rainIntensity = confMsg.RainIntensity;
			_sceneConfig.fogIntensity = confMsg.FogIntensity;
			_sceneConfig.lrfEnabled = confMsg.LrfEnabled;

			if (fireTrainingConfigReceived != null) fireTrainingConfigReceived();
		}

		//-------------------------------------

		void HandleGuiPantographTrainingConfig(GuiSimProtocol.GuiPantographTrainingConfig confMsg)
		{
			Log.Disabled("GuiCommunicationManager.HandleGuiPantographTrainingConfig()");

			_sceneConfig.trainingDuration = confMsg.TrainingDuration;
			_sceneConfig.laserReqInterval = confMsg.LaserReqInterval;
			_sceneConfig.fireReqInterval  = confMsg.FireReqInterval;
			_sceneConfig.requestDuration  = confMsg.RequestDuration;
			_sceneConfig.calibratedSpeed  = confMsg.CalibratedTargetSpeed;

			if (pantographTrainingConfigReceived != null) pantographTrainingConfigReceived();
		}

		//-------------------------------------

		void HandleGuiTrainingControl(GuiSimProtocol.GuiTrainingControl ctrlMsg)
		{
			Log.Disabled("GuiCommunicationManager.HandleGuiTrainingControl()");

			_trainingControl = (Common.TrainingControl) ctrlMsg.TrainingControl;

			if (trainingControlReceived != null) trainingControlReceived();
		}

		//-------------------------------------

		void HandleSimCalibrations(GuiSimProtocol.SimCalibrations calibMsg)
		{
			Log.Disabled("GuiCommunicationManager.HandleSimCalibrations()");

			_simCalibrations.WeaponCalibrationsOn = calibMsg.WeaponCalibrationsOn;
			_simCalibrations.FireForceLimiter = calibMsg.FireForceLimiter;
			_simCalibrations.WindForceScale = calibMsg.WindForceScale;
			_simCalibrations.LrfCalibrationsOn = calibMsg.LrfCalibrationsOn;
			_simCalibrations.LrfPosX = calibMsg.LrfPosX;
			_simCalibrations.LrfPosY = calibMsg.LrfPosY;
			_simCalibrations.LrfRotX = calibMsg.LrfRotX;
			_simCalibrations.LrfRotY = calibMsg.LrfRotY;
			_simCalibrations.CameraCalibrationsOn = calibMsg.CameraCalibrationsOn;
			_simCalibrations.reticleOn = calibMsg.ReticleOn;
			_simCalibrations.reticleOffsetsOn = calibMsg.ReticleOffsetsOn;
			_simCalibrations.reticleOffsetX = calibMsg.ReticleOffsetX;
			_simCalibrations.reticleOffsetY = calibMsg.ReticleOffsetY;
			_simCalibrations.reticleScaleX = calibMsg.ReticleScaleX;
			_simCalibrations.reticleScaleY = calibMsg.ReticleScaleY;

			_simCalibrations.UseTerrainDetail = calibMsg.UseTerrainDetail;
			_simCalibrations.DetailDistance = calibMsg.DetailDistance;
			_simCalibrations.DetailDensity = calibMsg.DetailDensity;

			Log.Info("GuiCommunicationManager.HandleSimCalibrations(): New calibrations received");

			if (simCalibrationsReceived != null)
			{
				Log.Trash("GuiCommunicationManager.HandleSimCalibrations(): Calling delegate");
				simCalibrationsReceived();
				Log.Trash("GuiCommunicationManager.HandleSimCalibrations(): Delegate called");
			}
		}

		//#endregion
	}
}
