﻿using System;
using System.Collections;
using System.Threading;
using WSCommunication;
using Common;

using Timer = System.Threading.Timer;

namespace Managers
{
	public class WSCommunicationManager : WSSimulation
	{
		//#region  publicAttributes

		public CommunicationStatusDelegate communicationStatusChanged;
		public UpdateEventDelegate wsStatusReceived;
		public UpdateEventDelegate wsFastStatusReceived;
		public UpdateEventDelegate wsCmdReceived;
		public UpdateEventDelegate simStatusSent;
		public UpdateEventDelegate simFastStatusSent;

		public bool DynamicsSimulation;

		public bool Debug;

		//#endregion

		//-------------------------------------

		//#region  privateAttributes

		private float _encTrv;
		private float _encElv;
		// daniel
		private float _encTrvCMDR;
		private float _encElvCMDR;

		//status msg
		private OperationalMode _opMode;
		private OperationalMode _opModeCMDR;
		private WeaponType _weaponType;
		private Camera _activeCamera;
		private Camera _activeCameraCMDR;
		private short  _reticleX;
		private short  _reticleY;
		private uint  _nRange;
		private short _minRange;
		private short _maxRange;
		private float _trvLrfJmp;
		private float _elvLrfJmp;
		private float _trvLrfJmpCMDR;
		private float _elvLrfJmpCMDR;
		private bool  _outFiringZone;
		private EnabledDisabled  _armSw;
		private EnabledDisabled  _safeSw;
		private EnabledDisabled  _overrideSw;

		//cmd msg
		private bool _measureRangeCmd = false;
		private bool _fireCmd;
		private FireMode _nFireMode;
		private bool _jumpToLaserPosCmd;
		private RangeMode _rangeMode;
		private Polarity _polarityMode;
		private bool _reloadWeapon;
		private InOutStopCmd _focusCmd;
		private InOutStopCmd _focusCmdCMDR;
		private AutoManualMode _focusMode;
		private AutoManualMode _irisMode;
		private InOutStopCmd _irisCmd;
		private AutoManualMode _gammaMode;
		private InOutStopCmd _gammaCmd;
		private AutoManualMode _levelMode;
		private InOutStopCmd _levelCmd;
		private AutoManualMode _gainMode;
		private InOutStopCmd _gainCmd;
		private bool _nucCmd;
		private FOVCmds _fovCmd;
		private FOVCmds _fovCmdCMDR;
		private bool _enableReticleCmd;

		Timer _timer;
		uint _periodicTime;

		uint _keepAliveCounter;
		uint _fastStatusCounter;
		uint _statusCounter;

		bool _connected;

		//#endregion

		//-------------------------------------

		//#region  publicMethods

		public WSCommunicationManager()
		{
			Log.Trace("WSCommunicationManager.WSCommunicationManager()");

			Debug = false;
			Subscribe(WSSimInterface.Instance());

			_periodicTime = 10; //ms
			_timer = new Timer(this.SendPeriodicMsgs);
			_timer.Change(_periodicTime, _periodicTime);

			SimStatus.UseEncSimulation = 0;

			SimCmds.DisableSafetySw = 1;
		}

		//----------------------------

		public void Enable(bool enable)
		{
			WSSimInterface.Instance().Enable(enable);
			NotifyCommStatus(_connected, true);
		}

		//-------------------------------------

		public void SendPeriodicMsgs(Object state = null)
		{
			if (!_connected)
				return;

			_statusCounter++;
			_fastStatusCounter++;
			_keepAliveCounter++;

			const uint fastStatusMsgTime = 10; //ms

			if ((_fastStatusCounter * _periodicTime) >= fastStatusMsgTime)
			{
				SendSimFastStatusMsg();
				_fastStatusCounter = 0;
			}

			const uint statusMsgTime = 100; //ms

			if ((_statusCounter * _periodicTime) >= statusMsgTime)
			{
				Log.Periodic("WSCommunicationManager.SendPeriodicMsgs: Sending status msg");
				SendSimStatusMsg();
				SimStatus.FreshRange = 0;
				_statusCounter = 0;

				if (simStatusSent != null)
					simStatusSent();
			}

			const uint keepAliveMsgTime = 100; //ms

			if ((_keepAliveCounter * _periodicTime) >= keepAliveMsgTime)
			{
				SendKeepAliveMsg();
				_keepAliveCounter = 0;
			}
		}

		//-------------------------------------

		public void SetupConnection(int localPort, string remoteAddress, int remotePort)
		{
			SetupConnection("any", localPort, remoteAddress, remotePort);
		}

		//-------------------------------------

		public void SetupConnection(string localAddress, int localPort, string remoteAddress, int remotePort)
		{
			Log.Trace("WSCommunicationManager.SetupConnection()");

			var commInterface = WSSimInterface.Instance();

			_connected = false;

			if (!commInterface.IsCommOk())
			{
				commInterface.SetupConnection(localAddress, localPort, remoteAddress, remotePort);
				Thread.Sleep(1000);
			}

			if (commInterface.IsCommOk())
				SendSimStatusMsg();
		}

		//-------------------------------------

		public void Disconnect()
		{
			if (_connected)
			{
				TcpJsonComm.Instance().Close();
				var commInterface = WSSimInterface.Instance();
				commInterface.CloseConnection();
				Unsubscribe(commInterface);
				NotifyCommStatus(false);
			}
		}

		//-------------------------------------

		override public void UpdateFovValues(float fovAngle, float zoomPercent, sbyte fov, bool uncalibratedFov)
		{
			SimStatus.FovHorzAngle = fovAngle;
			SimStatus.FovVertAngle = fovAngle * 0.75f;
			SimStatus.ZoomPercent = zoomPercent;
			SimStatus.UncalibratedFov = (byte) ((uncalibratedFov) ? 1 : 0);
			SimStatus.Fov = fov;
		}

		//-------------------------------------

		override public void UpdateMeasuredDistance(float distance, byte freshRange)
		{
			SimStatus.Range = (ushort) Math.Round(distance);
			SimStatus.FreshRange = freshRange;
		}

		//-------------------------------------

		override public void UpdateAmmunitionCount(uint count)
		{
			SimStatus.AmmoCount = (ushort) count;
		}

		//-------------------------------------

		public void EnableSafetySwitch(bool enable = true)
		{
			DisableSafetySwitch(!enable);
		}

		//-------------------------------------

		override public void DisableSafetySwitch(bool disable = true)
		{
			SimCmds.DisableSafetySw = (byte) ((disable) ? 1 : 0);
			SendSimCmdMsg();
		}

		//-------------------------------------

		override public void SetEncoderSimulation(bool simulation)
		{
			SimStatus.UseEncSimulation = (byte) ((simulation) ? 1 : 0);
		}

		//-------------------------------------		

		// Gunner
		override public void UpdateEncoderValues(float traverse, float elevation)
		{
			SimFastStatus.TrvEncoder = traverse;
			SimFastStatus.ElvEncoder = elevation;

			Log.Trash(String.Format("WSCommunicationManager.UpdateEncoderValues(): TrvDeg = {0}, ElvDeg  = {1}", traverse, elevation));
		}

		// Commander
		override public void UpdateEncoderValuesCMDR(float traverse, float elevation)
		{
			SimFastStatus.TrvEncoderCMDR = traverse;			
			SimFastStatus.ElvEncoderCMDR = elevation;

			Log.Trash(String.Format("WSCommunicationManager.UpdateEncoderValuesCMDR(): TrvDegCMDR = {0}, ElvDegCMDR  = {1}", traverse, elevation));
		}

		//-------------------------------------

		// Gunner
		override public void UpdateGyroValues(float traverse, float elevation)
		{
			SimFastStatus.TrvGyro = traverse;
			SimFastStatus.ElvGyro = elevation;			

			Log.Trash(String.Format("WSCommunicationManager.UpdateGyroValues(): TrvRate = {0}, ElvRate  = {1}", traverse, elevation));
		}

		// Commander
		override public void UpdateGyroValuesCMDR(float traverse, float elevation)
		{
			SimFastStatus.TrvGyroCMDR = traverse;
			SimFastStatus.ElvGyroCMDR = elevation;

			Log.Trash(String.Format("WSCommunicationManager.UpdateGyroValuesCMDR(): TrvRateCMDR = {0}, ElvRateCMDR  = {1}", traverse, elevation));
		}

		//-------------------------------------

		override public void UpdateInertialValues()
		{
		}

		//-------------------------------------

		public void UpdatePolarityMode(sbyte mode)
		{
			SimStatus.PolarityMode = mode;
		}

		//-------------------------------------

		private string ByteArrayToHexString(byte[] data)
		{
			string delimiter = " ";
			string hex = BitConverter.ToString(data);
			return hex.Replace("-", delimiter);
		}

		//-------------------------------------

		public void GetEncodersValues(out float encTrv, out float encElv)
		{
			encTrv = _encTrv;
			encElv = _encElv;
		}

		// Commadner
		public void GetEncodersCommanderValues(out float encTrv, out float encElv)
		{
			encTrv = _encTrvCMDR;
			encElv = _encElvCMDR;
		}

		//-------------------------------------

		public void GetReticlePos(out float x, out float y)
		{
			x = _reticleX;
			y = _reticleY;
		}

		//-------------------------------------

		public bool GetFireCmd()
		{
			return _fireCmd;
		}

		//-------------------------------------

		public bool GetJumpToLaserPosCmd()
		{
			return _jumpToLaserPosCmd;
		}

		//-------------------------------------

		public void GetLrfJmpOffsets(out float trvLrfJmp, out float elvLrfJmp)
		{
			trvLrfJmp = _trvLrfJmp;
			elvLrfJmp = _elvLrfJmp;
	
		}

		//-------------------------------------

		public void GetLrfJmpOffsetsCMDR(out float trvLrfJmp, out float elvLrfJmp)
		{
			trvLrfJmp = _trvLrfJmpCMDR;
			elvLrfJmp = _elvLrfJmpCMDR;		
		}

		//-------------------------------------

		public bool GetMeasureRangeCmd()
		{
			return _measureRangeCmd;
		}

		//-------------------------------------

		public FOVCmds GetFovCmd ()
		{
			return _fovCmd;
		}

		//-------------------------------------

		public FOVCmds GetFovCmdCMDR ()
		{
			return _fovCmdCMDR;
		}

		//-------------------------------------

		public Camera GetActiveCamera()
		{
			return _activeCamera;
		}

		//-------------------------------------

		public Camera GetActiveCameraCMDR()
		{
			return _activeCameraCMDR;
		}

		//-------------------------------------

		public OperationalMode GetOpMode()
		{
			return _opMode;
		}

		//-------------------------------------

		public OperationalMode GetOpModeCMDR()
		{
			return _opModeCMDR;
		}

		//-------------------------------------

		public WeaponType GetWeaponType()
		{
			Log.Disabled("WSComunication.GetWeaponType(): weaponType = " + _weaponType);
			return _weaponType;
		}

		//-------------------------------------

		public AutoManualMode GetFocusMode()
		{
			return _focusMode;
		}

		//-------------------------------------

		public InOutStopCmd GetFocusCmd()
		{
			return _focusCmd;
		}

		//-------------------------------------

		public AutoManualMode GetFocusModeCMDR()
		{
			return _focusMode;
		}

		//-------------------------------------

		public InOutStopCmd GetFocusCmdCMDR()
		{
			return _focusCmd;
		}

		//-------------------------------------

		public Common.FireMode GetFireMode()
		{
			return _nFireMode;
		}

		//-------------------------------------

		public RangeMode GetRangeMode()
		{
			return _rangeMode;
		}

		//-------------------------------------

		public bool GetEnableReticleCmd()
		{
			return _enableReticleCmd;
		}

		//-------------------------------------

		public uint GetBurstLength()
		{
			if (_nFireMode == FireMode.Single)
				return 1;
			else if (_nFireMode == FireMode.Burst_5)
				return 5;
			else
				return 15;
		}

		//-------------------------------------

		public uint GetRange()
		{
			return _nRange;
		}

		//-------------------------------------

		public void NotifyCommStatus(bool commOk, bool force = false)
		{
			if (commOk == _connected && !force) return;
			Log.Info("WSCommunicationManager.NotifyCommStatus(): WS connection status is " + ((commOk) ? "ok" : "not ok"));
			if (communicationStatusChanged != null)
				communicationStatusChanged(commOk);
			_connected = commOk;
		}

		//-------------------------------------

		public FOVCmds GetCameraFov()
		{
			return _fovCmd;
		}

		//-------------------------------------

		public FOVCmds GetCameraFovCMDR()
		{
			return _fovCmdCMDR;
		}

		//-------------------------------------

		override public void OnError(Exception ex)
		{
			var msg = ex.Message;

			if (msg == WSSimInterface.Instance().CommunicationTimeoutError)
			{
				Log.Error("Timeout on WS communication channel");
				NotifyCommStatus(false);
			}
			else if (msg == WSSimInterface.Instance().InvalidMessageReceivedError)
			{
				Log.Error("Invalid message received on WS communication channel");
			}
		}

		//-------------------------------------

		public Polarity GetPolarityMode()
		{
			return _polarityMode;
		}

		//-------------------------------------

		public void GetMinMaxRange(out short minRange, out short maxRange)
		{
			minRange = _minRange;
			maxRange = _maxRange;
		}

		//-------------------------------------

		public bool GetOutFiringZone()
		{
			return _outFiringZone;
		}

		//-------------------------------------

		public EnabledDisabled GetArmSw()
		{
			return _armSw;
		}

		//-------------------------------------

		public EnabledDisabled GetSafeSw()
		{
			return _safeSw;
		}

		//-------------------------------------

		public EnabledDisabled GetOverrideSw()
		{
			return _overrideSw;
		}

		//-------------------------------------

		public bool GetReloadWeapon()
		{
			return _reloadWeapon;
		}

		//-------------------------------------

		public AutoManualMode GetIrisMode()
		{
			return _irisMode;
		}

		//-------------------------------------

		public InOutStopCmd GetIrisCmd()
		{
			return _irisCmd;
		}

		//-------------------------------------

		public AutoManualMode GetGammaMode()
		{
			return _gammaMode;
		}

		//-------------------------------------

		public InOutStopCmd GetGammaCmd()
		{
			return _gammaCmd;
		}

		//-------------------------------------

		public AutoManualMode GetLevelMode()
		{
			return _levelMode;
		}

		//-------------------------------------

		public InOutStopCmd GetLevelCmd()
		{
			return _levelCmd;
		}

		//-------------------------------------

		public AutoManualMode GetGainMode()
		{
			return _gainMode;
		}

		//-------------------------------------

		public InOutStopCmd GetGainCmd()
		{
			return _gainCmd;
		}

		//-------------------------------------

		public bool GetNucCmd()
		{
			return _nucCmd;
		}

		//-------------------------------------

		// Observer
		override public void OnNext(WSSimMsg msg)
		{
			NotifyCommStatus(true);

			switch (msg.Opcode)
			{
				case WSSimProtocol.Opcodes.WSStatus:
					HandleWSStatusMsg(msg.GetWsStatusMsg());
					break;

				case WSSimProtocol.Opcodes.WSFastStatus:
					HandleWSFastStatusMsg(msg.GetWsFastStatusMsg());
					break;

				case WSSimProtocol.Opcodes.WSCmds:
					HandleWSCmdsMsg(msg.GetWsCmdsMsg());
					break;

				case WSSimProtocol.Opcodes.KeepAlive:
					break;
			}
		}

		//-------------------------------------

		public WSSimProtocol.SimStatusMsg GetSimStatusMsg()
		{
			Log.Disabled("WSCommunicationManager.GetSimStatusMsg()");
			return SimStatus;
		}

		//-------------------------------------

		public WSSimProtocol.SimFastStatusMsg GetSimFastStatusMsg()
		{
			Log.Disabled("WSCommunicationManager.GetSimFastStatusMsg()");
			return SimFastStatus;
		}

		//-------------------------------------

		public void SetPhonyWSStatusMsg(WSSimProtocol.WsStatusMsg status)
		{
			Log.Disabled("WSCommunicationManager.SetPhonyWSStatusMsg()");
			WSSimInterface.Instance().Enable(false);
			HandleWSStatusMsg(status);
			NotifyCommStatus(true);
		}

		//-------------------------------------

		public void SetPhonyWSFastStatusMsg(WSSimProtocol.WsFastStatusMsg fastStatus)		
		{
			Log.Disabled("WSCommunicationManager.SetPhonyWSFastStatusMsg()");
			WSSimInterface.Instance().Enable(false);
			HandleWSFastStatusMsg(fastStatus);
			NotifyCommStatus(true);
		}

		//-------------------------------------

		public void SetPhonyWSCmdsMsg(WSSimProtocol.WsCmdsMsg cmds)
		{
			Log.Disabled("WSCommunicationManager.SetPhonyWSCmdsMsg()");
			WSSimInterface.Instance().Enable(false);
			HandleWSCmdsMsg(cmds);
			NotifyCommStatus(true);
		}

		//-------------------------------------

		override public void OnCompleted()
		{
		}

		//#endregion

		//-------------------------------------

		//#region  privateMethods

		void HandleWSStatusMsg(WSSimProtocol.WsStatusMsg status)
		{
			Log.Ptrace ("WSComunication.HandleWSStatusMsg()");

			if (_weaponType != (WeaponType) status.SelectedWeapon)
				Log.Disabled("WSComunication.HandleWSStatusMsg(): weaponType changed from " + _weaponType + " to " + ((WeaponType) status.SelectedWeapon));

			_opMode = (OperationalMode) status.OpMode;
			_opModeCMDR = (OperationalMode) status.OpModeCMDR;
			_weaponType = (WeaponType) status.SelectedWeapon;
			_activeCamera = (Camera)status.ActiveCamera;
			_activeCameraCMDR = (Camera)status.ActiveCameraCMDR;

			_reticleX = status.ReticlePosX;
			_reticleY = status.ReticlePosY;
			_nRange = (uint)status.Range;

			_minRange = status.MinRange;
			_maxRange = status.MaxRange;

			_trvLrfJmp = status.TrvLrfJmpOffset;
			_elvLrfJmp = status.ElvLrfJmpOffset;

			_trvLrfJmpCMDR = status.TrvLrfJmpOffset;
			_elvLrfJmpCMDR = status.ElvLrfJmpOffset;

			_outFiringZone = (bool)(status.OutFiringZone == 1);
			_armSw = (EnabledDisabled) status.Arm;
			_safeSw = (EnabledDisabled) status.Safe;
			_overrideSw = (EnabledDisabled) status.Override;

			_focusMode = (AutoManualMode)status.FocusMode;

			if (DynamicsSimulation && _opMode == OperationalMode.Stab)
				_opMode = OperationalMode.Power;

			if (DynamicsSimulation && _opModeCMDR == OperationalMode.Stab)
				_opModeCMDR = OperationalMode.Power;

			Log.Disabled ("Received WSStatus: " + status.OpMode + " " + status.OpModeCMDR + " " + status.ActiveCamera + " " + status.ReticlePosX + " " + status.ReticlePosY + " " + status.OutFiringZone + " " +
						status.Range + " " + status.Arm + " " + status.Safe + " " + status.Override + " " + status.MinRange + " " + status.MaxRange + " " + status.ProtocolVersion +
			              " " + _focusMode);

			if (wsStatusReceived != null)
			{
				Log.Disabled("WSCommunicationManager.HandleWSStatusMsg(): Calling wsStatusReceived()");
				wsStatusReceived();
			}
		}

		//-------------------------------------

		void HandleWSFastStatusMsg(WSSimProtocol.WsFastStatusMsg fastStatus)
		{
			if (DynamicsSimulation)
			{
				const float RadToDeg = 180f / (float) Math.PI;
				_encTrv = fastStatus.TrvEncoderEmuPos * RadToDeg;
				_encElv = fastStatus.ElvEncoderEmuPos * RadToDeg;
				//daniel
				const float RadToDegCMDR = 180f / (float) Math.PI;
				_encTrvCMDR = fastStatus.TrvEncoderEmuPosCMDR * RadToDegCMDR;
				_encElvCMDR = fastStatus.ElvEncoderEmuPosCMDR * RadToDegCMDR;
			}
			else
			{
				const float MilsToDeg = 360f / 6400f;
				_encElv = fastStatus.ElvEncoderPos * MilsToDeg;
				_encTrv = fastStatus.TrvEncoderPos * MilsToDeg;
				//daniel
				const float MilsToDegCMDR = 360f / 6400f;
				_encElvCMDR = fastStatus.ElvEncoderPosCMDR * MilsToDegCMDR;
				_encTrvCMDR = fastStatus.TrvEncoderPosCMDR * MilsToDegCMDR;
			}

			Log.Trash(
				String.Format("WSCommunicationManager.HandleWSFastStatusMsg(): TrvMils = {0}, ElvMils = {1}, TrvDeg = {2}, ElvDeg  = {3}",
				              fastStatus.TrvEncoderPos, fastStatus.ElvEncoderPos, fastStatus.TrvEncoderPosCMDR, fastStatus.ElvEncoderPosCMDR, _encTrv, _encElv, _encTrvCMDR, _encElvCMDR));

			if (wsFastStatusReceived != null)
			{
				Log.Disabled("WSCommunicationManager.HandleWSFastStatusMsg(): Calling wsFastStatusReceived()");
				wsFastStatusReceived();
			}
		}

		//-------------------------------------

		void HandleWSCmdsMsg(WSSimProtocol.WsCmdsMsg cmds)
		{
			_fireCmd = cmds.FireCmd == 1;
			_measureRangeCmd = cmds.MeasureRange == 1;
			_jumpToLaserPosCmd = cmds.JumpToLaserPos == 1;
			_nFireMode = (Common.FireMode) cmds.FireMode;
			_rangeMode = (RangeMode) cmds.RangeMode;
			_polarityMode = (Polarity)cmds.PolarityMode;
			_reloadWeapon = (bool)(cmds.ReloadWeapon == 1);
			_focusCmd = (InOutStopCmd) cmds.FocusCmd;
			_focusCmdCMDR = (InOutStopCmd) cmds.FocusCmdCMDR;
			_irisMode = (AutoManualMode)cmds.IrisMode;
			_irisCmd = (InOutStopCmd)cmds.IrisCmd;
			_gammaMode = (AutoManualMode)cmds.GammaMode;
			_gammaCmd = (InOutStopCmd)cmds.GammaCmd;
			_levelMode = (AutoManualMode)cmds.LevelMode;
			_levelCmd = (InOutStopCmd)cmds.LevelCmd;
			_gainMode = (AutoManualMode)cmds.GainMode;
			_gainCmd = (InOutStopCmd)cmds.GainCmd;
			_nucCmd = cmds.NucCmd == 1;
			_fovCmd = (FOVCmds)cmds.ZoomCmd;
			_fovCmdCMDR = (FOVCmds)cmds.ZoomCmdCMDR;
			_enableReticleCmd = (cmds.EnableReticle == 1) ? true : false;

			Log.Debug ("Received WSCmds: " + cmds.MeasureRange + " " + cmds.FireCmd + " " + cmds.FireMode + " " + cmds.RangeMode + " " + cmds.PolarityMode + " " + cmds.ReloadWeapon + " " +
						cmds.FocusCmd + " " + cmds.FocusCmdCMDR + " " + cmds.IrisMode + " " + cmds.IrisCmd + " " + cmds.GammaMode + " " + cmds.GammaCmd + " " +
						cmds.LevelMode + " " + cmds.LevelCmd + " " + cmds.GainMode + " " + cmds.GainCmd + " " + cmds.NucCmd + " " + cmds.ZoomCmd + " " + cmds.ZoomCmdCMDR);

			if (wsCmdReceived != null)
			{
				Log.Disabled("WSCommunicationManager.HandleWSCmdsMsg(): Calling wsCmdReceived() with fire = " + _fireCmd + " and measureRange = " + _measureRangeCmd);
				wsCmdReceived();
			}
		}

		//-------------------------------------

		~WSCommunicationManager()
		{
			Log.Trace("WSCommunicationManager.~WSCommunicationManager()");

			SimStatus.UseEncSimulation = 0;
			SendSimStatusMsg();

			Disconnect();
		}

		//-------------------------------------

		//#endregion
	}
}
