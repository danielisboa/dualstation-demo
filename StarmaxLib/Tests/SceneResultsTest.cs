﻿using System;
using Common;
using Utils;

namespace Tests
{
	public class SceneInterface : ISceneInterface
	{
		float _runningTime;

		public void AddLevelLoadedCallback(UpdateEventDelegate callback)
		{
		}

		public void LoadTrainingScene(Common.TrainingType trainingType)
		{
		}

		public void SetErrorMessage(string message)
		{
		}

		public void PauseScene(bool pause)
		{
		}

		public bool IsScenePaused()
		{
			return false;
		}

		public float GetRunningTime()
		{
			_runningTime += 1f;
			return _runningTime;
		}
	}

	public class SceneResultsTest
	{
		public SceneResultsTest()
		{
			Log.SetConsolePrinter(new SystemConsolePrinter());
			Log.SetLogLevel(Log.Level.Debug);
		}

		public void RunTest()
		{
			uint fireCounter = 0;
			uint hitsCounter = 0;
			uint killCounter = 0;

			bool formatJson = true;
			var sceneResults = new SceneResults(new SceneInterface(), formatJson);

			sceneResults.ReceiveDaylightValue(1f);
			sceneResults.ReceiveRainValue(0f);
			sceneResults.ReceiveFogValue(0f);

			sceneResults.ReceiveFireCount(++fireCounter);
			sceneResults.ReceiveFireCount(++fireCounter);
			sceneResults.ReceiveHitsCount(++hitsCounter);
			sceneResults.ReceiveFireCount(++fireCounter);
			sceneResults.ReceiveHitsCount(++hitsCounter);
			sceneResults.ReceiveFireCount(++fireCounter);
			sceneResults.ReceiveFireCount(++fireCounter);
			sceneResults.ReceiveHitsCount(++hitsCounter);
			sceneResults.ReceiveKillCount(++killCounter);

			sceneResults.ReceiveDaylightValue(.6f);
			sceneResults.ReceiveFireCount(++fireCounter);
			sceneResults.ReceiveFireCount(++fireCounter);
			sceneResults.ReceiveFireCount(++fireCounter);
			sceneResults.ReceiveFireCount(++fireCounter);
			sceneResults.ReceiveHitsCount(++hitsCounter);
			sceneResults.ReceiveFireCount(++fireCounter);
			sceneResults.ReceiveFireCount(++fireCounter);
			sceneResults.ReceiveHitsCount(++hitsCounter);
			sceneResults.ReceiveKillCount(++killCounter);

			Log.Info("\n" + sceneResults.GetTrainingResultsJson());
		}
	}
}
