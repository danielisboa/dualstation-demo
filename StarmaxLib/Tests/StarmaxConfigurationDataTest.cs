﻿using System;
using Common;
using Infrastructure;

namespace Tests
{
	public class StarmaxConfigurationDataTest
	{
		public StarmaxConfigurationDataTest()
		{
			Log.SetConsolePrinter(new SystemConsolePrinter());
			Log.SetLogLevel(Log.Level.Debug);
		}

		public void RunTest()
		{
			string filename = "/home/bruno/Tmp/StarmaxConfig.json";
			var orig = new StarmaxConfigurationData();
			orig.wsCommLocalPort = 1;
			orig.wsCommRemotePort = 2;
			orig.wsCommRemoteIP = "3";
			orig.guiCommSimPort = 4;
			orig.guiCommGuiPort = 5;
			orig.StoreOnJsonFile(filename);
			filename = StarmaxConfigurationData.DefaultPath();
			var dest = new StarmaxConfigurationData(filename);
			dest.guiCommSimPort = 789;
			dest.LoadFromJsonFile(filename);
			var output = dest.AsString();
			Log.Info($"Stored config:\n{output}");
		}
	}
}
