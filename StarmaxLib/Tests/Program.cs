﻿using System;

namespace Tests
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var test = new StarmaxConfigurationDataTest();
			test.RunTest();
		}
	}
}
