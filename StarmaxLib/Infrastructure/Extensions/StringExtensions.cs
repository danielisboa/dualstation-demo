﻿using System;
using System.Reflection;

namespace Infrastructure
{
	public static class StringExtensions
	{
		//-------------------------------------

		public static string AsString<T>(this T obj)
		{
			string output = "";

			foreach(var prop in obj.GetType().GetProperties())
				output += (prop.Name + ": " + prop.GetValue(obj, null)) + "\n";

			foreach(var field in obj.GetType().GetFields())
				output += (field.Name + ": " + field.GetValue(obj)) + "\n";

			return output;
		}

	}
}
