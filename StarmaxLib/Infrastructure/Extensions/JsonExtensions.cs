﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace Infrastructure
{
	public static class JsonExtensions
	{
		public static bool LoadFromJsonFile<T>(this T obj, string filename) where T:class
		{
			var type = typeof(T);

			if (!File.Exists(filename))
				return false;

			var json = File.ReadAllText(filename);

			if (json.Length == 0)
				return false;

			var newObj = JsonConvert.DeserializeObject<T>(json);

			foreach (var prop in type.GetProperties())
			{
				var propertyInfo = type.GetProperty(prop.Name);
				propertyInfo.SetValue(obj, prop.GetValue(newObj, null), null);
			}

			foreach (var field in type.GetFields())
			{
				var propertyInfo = type.GetField(field.Name);
				propertyInfo.SetValue(obj, field.GetValue(newObj));
			}

			return true;
		}

		//-------------------------------------

		public static void StoreOnJsonFile<T>(this T obj, string filename)
		{
			var json = JsonConvert.SerializeObject(obj, Formatting.Indented) + "\n";
			File.WriteAllText(filename, json);
		}

	}
}

