﻿using System;
using System.Runtime.InteropServices;

namespace Infrastructure
{
	public static class StructExtensions
	{
		public static int Size<T>(this T obj) where T:struct
		{
			return Marshal.SizeOf(obj);
		}

		//-------------------------------------

		public static bool Deserialize<T>(this T obj, byte[] rawdata, int offset = 0) where T:struct
		{
			int rawsize = Marshal.SizeOf(obj);
			if (rawsize > rawdata.Length - offset)
				return false;
			IntPtr buffer = Marshal.AllocHGlobal(rawsize);
			Marshal.Copy(rawdata, offset, buffer, rawsize);
			obj = (T) Marshal.PtrToStructure(buffer, typeof(T));
			Marshal.FreeHGlobal(buffer);
			return true;
		}

		//-------------------------------------

		public static byte[] Serialize<T>(this T obj) where T:struct
		{
			int rawsize = Marshal.SizeOf(obj);
			IntPtr buffer = Marshal.AllocHGlobal(rawsize);
			Marshal.StructureToPtr(obj, buffer, false);
			byte[] rawdata = new byte[rawsize];
			Marshal.Copy(buffer, rawdata, 0, rawsize);
			Marshal.FreeHGlobal(buffer);
			return rawdata;
		}
	}
}

