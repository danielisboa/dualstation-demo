﻿using System;

public interface IConsolePrinter
{
	void Print(string sText);
}
