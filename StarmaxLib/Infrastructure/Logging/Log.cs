﻿using System;

//---------------------------------

public class Log
{
	public enum Level
	{
		Disabled = 0,
		Logging  = 0x0001,
		Periodic = Logging << 1,
		Ptrace   = Periodic << 1,
		Trash    = Ptrace << 1,
		Trace    = Trash << 1,
		Debug    = Trace << 1,
		Info     = Debug << 1,
		Event    = Info << 1,
		Warning  = Event << 1,
		Error    = Warning << 1,
		Focused  = Error << 1,
		NoLog    = Focused << 1,
	}

	//---------------------------------

	static uint _nActiveLevels;
	static IConsolePrinter _rConsolePrinter;

	//---------------------------------

	static Log()
	{
		_nActiveLevels = 0;
		_rConsolePrinter = new SystemConsolePrinter();
	}

	//---------------------------------

	public static void SetLogLevel(Level eLevel)
	{
		_nActiveLevels = 0;
		for (uint i = (uint) Level.NoLog >> 1; i >= (uint) eLevel;)
		{
			_nActiveLevels |= i;
			i >>= 1;
		}
	}

	//---------------------------------

	public static void EnableLevel(Level eLevel, bool bEnabled = true)
	{
		if (bEnabled)
			_nActiveLevels |=  ((uint) eLevel);
		else
			_nActiveLevels &= ~((uint) eLevel);
	}

	//---------------------------------

	public static void SetConsolePrinter(IConsolePrinter rConsolePrinter)
	{
		_rConsolePrinter = rConsolePrinter;
	}

	//---------------------------------

	public static void Print(Level eLevel, string sMessage)
	{
		if (((uint) eLevel & _nActiveLevels) != 0)
		{
			var levelStr = eLevel.ToString().ToUpper() + ":";
			_rConsolePrinter.Print(levelStr.PadRight(12) + sMessage);
		}
	}

	//---------------------------------

	public static void Disabled(string sMessage)
	{
	}

	//---------------------------------

	public static void Periodic(string sMessage)
	{
		Print(Level.Periodic, sMessage);
	}

	//---------------------------------

	public static void Ptrace(string sMessage)
	{
		Print(Level.Ptrace, sMessage);
	}

	//---------------------------------

	public static void Trash(string sMessage)
	{
		Print(Level.Trash, sMessage);
	}

	//---------------------------------

	public static void Trace(string sMessage)
	{
		Print(Level.Trace, sMessage);
	}

	//---------------------------------

	public static void Debug(string sMessage)
	{
		Print(Level.Debug, sMessage);
	}

	//---------------------------------

	public static void Info(string sMessage)
	{
		Print(Level.Info, sMessage);
	}

	//---------------------------------

	public static void Event(string sMessage)
	{
		Print(Level.Event, sMessage);
	}

	//---------------------------------

	public static void Warning(string sMessage)
	{
		Print(Level.Warning, sMessage);
	}

	//---------------------------------

	public static void Error(string sMessage)
	{
		Print(Level.Error, sMessage);
	}

	//---------------------------------

	public static void Focused(string sMessage)
	{
		Print(Level.Focused, sMessage);
	}

	//---------------------------------

	public static void Logging(string sMessage)
	{
		Print(Level.Logging, sMessage);
	}
}

//---------------------------------
