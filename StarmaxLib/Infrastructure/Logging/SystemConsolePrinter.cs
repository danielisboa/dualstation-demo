﻿using System;

public class SystemConsolePrinter : IConsolePrinter
{
	public SystemConsolePrinter()
	{
	}

	public void Print(string sText)
	{
		Console.Write(sText + "\n");
	}
}

