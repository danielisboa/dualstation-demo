﻿using System;

namespace Infrastructure
{
	public static class MathHelper
	{
		public static float Clamp(float value, float min, float max)
		{
			return ((value < min) ? min : ((value > max) ? max : value));
		}

		public static double Clamp(double value, double min, double max)
		{
			return ((value < min) ? min : ((value > max) ? max : value));
		}

		public static int Clamp(int value, int min, int max)
		{
			return ((value < min) ? min : ((value > max) ? max : value));
		}

		public static uint Clamp(uint value, uint min, uint max)
		{
			return ((value < min) ? min : ((value > max) ? max : value));
		}
	}
}

