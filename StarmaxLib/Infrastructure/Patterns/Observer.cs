using System;
using System.Collections.Generic;

//-------------------------------------

namespace Infrastructure
{
	public class Observer<T>
	{
		private Dictionary<Observable<T>, IDisposable> _subscriptions;

		//-------------------------------------

		public Observer()
		{
			_subscriptions = new Dictionary<Observable<T>, IDisposable>();
		}

		//-------------------------------------

		public bool Subscribe(Observable<T> obj)
		{
			if (obj == null) return false;
			var disposable = obj.Subscribe(this);
			if (disposable == null) return false;
			_subscriptions[obj] = disposable;
			return true;
		}

		//-------------------------------------

		public void Unsubscribe(Observable<T> obj)
		{
			_subscriptions[obj].Dispose();
			_subscriptions.Remove(obj);
		}

		//-------------------------------------

		~Observer()
		{
			foreach (var s in _subscriptions)
			{
				var disposable = s.Value;
				if (disposable != null)
					disposable.Dispose();
			}
		}

		//-------------------------------------

		public virtual void OnNext(T t) {}
		public virtual void OnError(Exception e) {}
		public virtual void OnCompleted() {}
	}

	//-------------------------------------

	public class Observable<T>
	{
		private class Unsubscriber : IDisposable
		{
			private Observer<T> _observer;
			private Observable<T> _observable;

			//-------------------------------------

			public Unsubscriber(Observable<T> observable, Observer<T> observer) 
			{
				_observable = observable;
				_observer = observer;
			}

			//-------------------------------------

			public void Dispose()
			{
				if (_observable != null)
					_observable.Unsubscribe(_observer);
			}
		}

		//-------------------------------------

		private List<Observer<T>> _observers;

		//-------------------------------------

		public Observable()
		{
			_observers = new List<Observer<T>>();
		}

		//-------------------------------------

		public IDisposable Subscribe(Observer<T> obj) 
		{
			if (!_observers.Contains(obj))
				_observers.Add(obj); 
			return new Unsubscriber(this, obj);
		}

		//-------------------------------------

		public void Unsubscribe(Observer<T> obj)
		{
			if (obj != null && _observers.Contains(obj))
				_observers.Remove(obj);
		}

		//-------------------------------------

		public virtual void NotifyData(T data)
		{
			foreach (var observer in _observers)
				observer.OnNext(data);
		}

		//-------------------------------------

		public virtual void NotifyError(string error = "")
		{
			Exception ex = new Exception(error);
			foreach (var observer in _observers)
				observer.OnError(ex);
		}

		//-------------------------------------

		public virtual void NotifyCompleted()
		{
			foreach (var observer in _observers)
				observer.OnCompleted();
		}
	}
}

//-------------------------------------
