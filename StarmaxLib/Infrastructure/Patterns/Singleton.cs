﻿using System;

//----------------------------

namespace Infrastructure
{
	public class Singleton<T> where T : class, new()
	{
		static T _pInstance = new T();
		public static T Instance() { return _pInstance; }
		protected Singleton() {}
	}

	//----------------------------

	public class SingletonObserver<ClassType, MsgType> : Observer<MsgType> where ClassType : class, new()
	{
		public static ClassType Instance() { return Singleton<ClassType>.Instance(); }
	}

	//----------------------------

	public class SingletonObservable<ClassType, MsgType> : Observable<MsgType> where ClassType : class, new()
	{
		public static ClassType Instance() { return Singleton<ClassType>.Instance(); }
	}
}

//----------------------------
