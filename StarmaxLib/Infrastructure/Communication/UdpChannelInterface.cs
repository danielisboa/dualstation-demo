﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading;
using Infrastructure;

namespace Infrastructure
{
	public abstract class UdpChannelInterface<DerivedType, MsgType> : SingletonObservable<DerivedType, MsgType>  where DerivedType : class, new()
	{
		public static Log.Level LogLevel = Log.Level.Disabled;
		public string CommunicationTimeoutError { get; private set; }
		public string InvalidMessageReceivedError { get; private set; }

		IPEndPoint _remoteEndPoint;
		UdpClient _udpClient;

		protected uint _rcvdCount;
		bool _enabled;

		Timer _watchdogTimer;

		//----------------------------

		private string ByteArrayToHexString(byte[] data)
		{
			string hex = BitConverter.ToString(data);
			return hex.Replace("-", " ");
		}

		//----------------------------

		public UdpChannelInterface()
		{
			CommunicationTimeoutError = "Communication Timeout";
			InvalidMessageReceivedError = "Invalid Message Received";
			_udpClient = new UdpClient(AddressFamily.InterNetwork);
			_watchdogTimer = new Timer(this.WatchDog);
			_watchdogTimer.Change(1000, 1000);
		}

		//----------------------------

		~UdpChannelInterface()
		{
			CloseConnection();
		}

		//----------------------------

		void WatchDog(Object stateInfo)
		{
			if (_rcvdCount == 0 && _enabled)
				NotifyError(CommunicationTimeoutError);
			_rcvdCount = 0;
		}

		//----------------------------

		public void Enable(bool enable)
		{
			_enabled = enable;
		}

		//----------------------------

		public bool IsEnabled()
		{
			return _enabled;
		}

		//----------------------------

		protected abstract bool ParseReceivedData(byte[] data);

		//----------------------------

		void ReceiveData(IAsyncResult result)
		{
			var endPoint = new IPEndPoint(IPAddress.Any, 0);
			var data = _udpClient.EndReceive(result, ref endPoint);

			if (endPoint.Address.Equals(_remoteEndPoint.Address))
			{
				if (ParseReceivedData(data)) _rcvdCount++;
			}
			else
				Log.Print(LogLevel, "Unknown message from " + endPoint);

			_udpClient.BeginReceive(new AsyncCallback(ReceiveData), null);
		}

		//----------------------------

		public void SetupConnection(int localPort, string remoteAddress, int remotePort)
		{
			SetupConnection("any", localPort, remoteAddress, remotePort);
		}

		//----------------------------

		public void SetupConnection(string localAddress, int localPort, string remoteAddress, int remotePort)
		{
			Log.Trace("UdpChannelInterface.SetupConnection(local: " + localAddress + ":" + localPort + ", remote: " + remoteAddress + ":" + remotePort);

			var localAddr = ((localAddress != "any") ? IPAddress.Parse(localAddress) : IPAddress.Any);
			Bind(new IPEndPoint(localAddr, localPort));

			SetupConnection(remoteAddress, remotePort);
		}

		//----------------------------

		public void SetupConnection(string remoteAddress, int remotePort)
		{
			Log.Trace($"UdpChannelInterface.SetupConnection({remoteAddress}, {remotePort})");
			var remote = new IPEndPoint(IPAddress.Parse(remoteAddress), remotePort);
			SetupConnection(remote);
		}

		//----------------------------

		public void Bind(IPEndPoint local)
		{
			_udpClient.Client.Bind(local);
		}

		//----------------------------

		public void SetupConnection(IPEndPoint local, IPEndPoint remote)
		{
			Bind(local);
			SetupConnection(remote);
		}

		//----------------------------

		public void SetupConnection(IPEndPoint remote)
		{
			_udpClient.Connect(remote);
			_remoteEndPoint = remote;
			_udpClient.BeginReceive(new AsyncCallback(ReceiveData), null);
		}

		//----------------------------

		public void CloseConnection()
		{
			_udpClient.Close();
		}

		//----------------------------

		public bool IsCommOk()
		{
			return _udpClient.Client.IsBound;
		}

		//----------------------------

		public bool SendMsg(byte[] msg)
		{
			if (!_enabled || !IsCommOk()) return false;
			return _udpClient.Send(msg, msg.Length) == msg.Length;
		}
	}
}

