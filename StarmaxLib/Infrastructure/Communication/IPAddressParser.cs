﻿using System.Net;
using System.Net.Sockets;

namespace Infrastructure
{
	public static class IPAddressParser
	{
		public static IPEndPoint Parse(int port)
		{
			return new IPEndPoint(IPAddress.Any, port);
		}

		//----------------------------

		public static IPEndPoint Parse(string address, int port)
		{
			return new IPEndPoint(IPAddress.Parse(address), port);
		}

		//----------------------------

		public static string Resolve(string hostname)
		{
			string address = "";

			try
			{
				var host = Dns.GetHostEntry(hostname);
				address = host.AddressList[0].AsString();
			}
			catch
			{
			}

			return address;
		}
	}
}

