﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Net.Sockets;
using System.Text;
using System.Net;
using System;

//---------------------------------

public class TcpTextDatagramComm
{
	public delegate void Event();

	public Event messageReceived;

	public enum Mode
	{
		Client,
		Server
	}

	//---------------------------------

	protected static Log.Level _eLogLevel;

	//---------------------------------

	struct MsgData
	{
		public int Id;
		public string Msg;
	}

	List<MsgData> _textMsgs;
	TcpListener _tcpServer;
	TcpClient _tcpClient;
	String _rcvData;
	byte[] _rcvBuffer;
	Mode _mode;

	static TcpTextDatagramComm _instance;

	const string _sStartMarkup = "<msg id=([0-9]*)>";
	const string _sEndMarkup  = "</msg>";

	//---------------------------------

	public static void SetLogLevel(Log.Level eLevel)
	{
		_eLogLevel = eLevel;
	}

	//---------------------------------

	static TcpTextDatagramComm()
	{
		_eLogLevel = Log.Level.Disabled;
		_instance = new TcpTextDatagramComm();
	}

	//---------------------------------

	public static TcpTextDatagramComm Instance()
	{
		return _instance;
	}

	//---------------------------------

	public TcpTextDatagramComm()
	{
		_mode = Mode.Client;
		const int rcvBuffSize = 1000;
		_rcvBuffer = new byte[rcvBuffSize];
		_textMsgs = new List<MsgData>();
		_rcvData = "";
		Log.Print(_eLogLevel, "TcpTextDatagramComm.TcpTextDatagramComm(): Creating instance of TcpTextDatagramComm");
	}

	//---------------------------------

	~TcpTextDatagramComm()
	{
		Close();
	}

	//---------------------------------

	public void Close()
	{
		if (_tcpClient != null)
			_tcpClient.Close();
		if (_tcpServer != null)
			_tcpServer.Stop();
		_tcpClient = null;
		_tcpServer = null;
	}

	//---------------------------------

	public void Flush()
	{
		_rcvData = "";
	}

	//---------------------------------

	public void Connect(int port)
	{
		Log.Print(_eLogLevel, $"TcpTextDatagramComm.Connect({port})");
		Init(Mode.Client, port);
	}

	//---------------------------------

	public void Connect(string hostname, int port)
	{
		Log.Print(_eLogLevel, $"TcpTextDatagramComm.Connect({hostname}, {port})");
		Init(Mode.Client, hostname, port);		
	}

	//---------------------------------

	public void Connect(IPEndPoint ipEndPoint)
	{
		Init(Mode.Client, ipEndPoint);
	}

	//---------------------------------

	public void Listen(int port)
	{
		Log.Print(_eLogLevel, $"TcpTextDatagramComm.Listen({port})");
		Init(Mode.Server, port);
	}

	//---------------------------------

	public void Listen(string hostname, int port)
	{
		Log.Print(_eLogLevel, $"TcpTextDatagramComm.Listen({hostname}, {port})");
		Init(Mode.Server, hostname, port);		
	}

	//---------------------------------

	public void Listen(IPEndPoint ipEndPoint)
	{
		Init(Mode.Server, ipEndPoint);
	}

	//---------------------------------

	public void Init(Mode mode, int port)
	{
		Init(mode, Dns.GetHostName(), port);
	}

	//---------------------------------

	public void Init(Mode mode, string hostname, int port)
	{
		IPAddress ipAddress = Dns.GetHostEntry(hostname).AddressList[0];
		IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, port);
		Init(mode, ipEndPoint);		
	}

	//---------------------------------

	public void Init(Mode mode, IPEndPoint ipEndPoint)
	{
		if (_mode == Mode.Server)
		{
			Log.Print(_eLogLevel, $"TcpTextDatagramComm.Init(): Calling init with mode == {mode}, but _mode is already server");
			return;
		}

		if (IsConnected())
		{
			Log.Print(_eLogLevel, $"TcpTextDatagramComm.Init(): Calling init with mode == {mode}, but already connected");
			return;
		}

		_rcvData = "";

		_mode = mode;

		if (_mode == Mode.Client)
			InitClient(ipEndPoint);
		else
			InitServer(ipEndPoint);
	}

	//---------------------------------

	public bool IsConnected()
	{
		if (_tcpClient == null) return false;
		if (_tcpClient.Client == null) return false;
		return _tcpClient.Client.Connected;
	}

	//---------------------------------

	public bool HasMessage()
	{
		bool ret = false;
		lock (_textMsgs)
			ret = _textMsgs.Count > 0;
		return ret;
	}

	//---------------------------------

	public int GetNextMsgId()
	{
		return (HasMessage()) ? _textMsgs[0].Id : -1;
	}

	//---------------------------------

	public bool GetNextMsg(out string sMsg)
	{
		sMsg = "";

		if (!HasMessage())
			return false;

		try
		{
			lock (_textMsgs)
			{
				sMsg = _textMsgs[0].Msg;
				_textMsgs.RemoveAt(0);
			}
			return true;
		}
		catch
		{
			return false;
		}
	}

	//---------------------------------

	public bool SendMsg(string textMsg)
	{
		return SendMsg(0, textMsg);
	}

	//---------------------------------

	public bool SendMsg(int id, string textMsg)
	{
		if (!IsConnected())
		{
			Log.Print(_eLogLevel, "TcpTextDatagramComm.SendMsg(): Not connected to send message");
			return false;
		}

		try
		{
			// TODO: Add id to msg
			var datagram = "<msg id=" + id + ">" + textMsg + "</msg>\n";
			if (id != 255) Log.Disabled("TcpTextDatagramComm.SendMsg(" + id + "):\n" + datagram);
			var msgBytes = System.Text.Encoding.UTF8.GetBytes(datagram);
			lock (_tcpClient) _tcpClient.Client.Send(msgBytes);
			return true;
		}
		catch
		{
			return false;
		}
	}

	//---------------------------------

	void InitClient(IPEndPoint ipEndPoint)
	{
		Log.Print(_eLogLevel, $"TcpTextDatagramComm.InitClient({ipEndPoint})");
		try
		{
			_tcpClient = new TcpClient();
			_tcpClient.Connect(ipEndPoint);
			BeginReceive();
		}
		catch (Exception ex)
		{
			Log.Print(_eLogLevel, "TcpTextDatagramComm.InitClient(): Unable to connect to config server: " + ex.Message);
		}
	}

	//---------------------------------

	void InitServer(IPEndPoint ipEndPoint)
	{
		Log.Print(_eLogLevel, "TcpTextDatagramComm.InitServer()");
		try
		{
			_tcpServer = new TcpListener(ipEndPoint);
			_tcpServer.Start();
			BeginAccept();
		}
		catch (Exception e)
		{
			Log.Print(_eLogLevel, $"TcpTextDatagramComm.InitServer(): Unable to start config server: {e.Message}");
		}
	}

	//---------------------------------

	void BeginAccept()
	{
		Log.Print(_eLogLevel, "TcpTextDatagramComm.BeginAccept(): Waiting for connections");
		_tcpServer.BeginAcceptTcpClient(new AsyncCallback(TcpTextDatagramComm.AcceptCallback), this);
	}

	//---------------------------------

	public static void AcceptCallback(IAsyncResult ar)
	{
		var pthis = (TcpTextDatagramComm) ar.AsyncState;
		pthis.EndAccept(ar);
	}

	//---------------------------------

	void EndAccept(IAsyncResult ar)
	{
		_tcpClient = _tcpServer.EndAcceptTcpClient(ar);
		if (_tcpClient != null)
		{
			Log.Print(_eLogLevel, "TcpTextDatagramComm.EndAccept(): Connection accepted");
			BeginReceive();
		}
		else
			BeginAccept();
	}

	//---------------------------------

	void BeginReceive()
	{
		Log.Print(_eLogLevel, "TcpTextDatagramComm.BeginReceive(): Waiting messages, connected = " + IsConnected());
		if (!IsConnected()) return;
		_tcpClient.Client.BeginReceive(_rcvBuffer, 0, _rcvBuffer.Length, 0, new AsyncCallback(TcpTextDatagramComm.ReceiveCallback), this);
	}

	//---------------------------------

	public static void ReceiveCallback(IAsyncResult ar)
	{
		var pthis = (TcpTextDatagramComm) ar.AsyncState;
		pthis.EndReceive(ar);
	}

	//---------------------------------

	void EndReceive(IAsyncResult ar)
	{
		var nbytes = _tcpClient.Client.EndReceive(ar);

		if (nbytes == 0)
		{
			Disconnect();
			return;
		}

		Log.Print(_eLogLevel, "TcpTextDatagramComm.EndReceive(): Message received");

		var sMsg = Encoding.UTF8.GetString(_rcvBuffer, 0, nbytes);
		Log.Print(_eLogLevel, $"TcpTextDatagramComm.EndReceive(): Received {nbytes} bytes message:\n{sMsg}");

		var nParsedMsgCount = ParseRcvdData(sMsg);

		if (nParsedMsgCount > 0)
		{
			Log.Print(_eLogLevel, $"TcpTextDatagramComm.EndReceive(): {nParsedMsgCount} message(s) parsed\n");
			if (messageReceived != null)
			{
				Log.Print(_eLogLevel, $"TcpTextDatagramComm.EndReceive(): Calling delegate\n");
				messageReceived();
				Log.Print(_eLogLevel, $"TcpTextDatagramComm.EndReceive(): Delegate called\n");
			}
		}

		if (!_tcpClient.Connected)
		{
			Log.Print(_eLogLevel, $"TcpTextDatagramComm.EndReceive(): Client disconnected");
			Disconnect();
		}
		else
		{
			Log.Print(_eLogLevel, $"TcpTextDatagramComm.EndReceive(): Preparing to receive msgs");
			BeginReceive();
		}

		Log.Print(_eLogLevel, $"TcpTextDatagramComm.EndReceive(): End");
	}

	//---------------------------------

	void Disconnect()
	{
		Log.Print(_eLogLevel, $"TcpTextDatagramComm.Disconnect()");
		_tcpClient.Close();
		_tcpClient = null;
		if (_mode == Mode.Server)
			BeginAccept();
	}

	//---------------------------------

	public int ParseRcvdData(string sNewData)
	{
		// append new data at the end of buffer
		_rcvData += sNewData;

		// if _rcvData buffer is empty, return
		if (_rcvData.Length == 0)
			return 0;

		int nMsgCount = 0;

		while (true)
		{
			int nIndex = _rcvData.IndexOf(_sEndMarkup);

			// if markup not found, return
			if (nIndex == -1)
				break;

			// retrieve the packet with the markups from the buffer
			int nLength = nIndex + _sEndMarkup.Length;
			string sPacket = _rcvData.Substring(0, nLength);

			// remove it from the buffer
			_rcvData = _rcvData.Remove(0, nLength);

			// match the msg data from the packet with regex
			const string sPattern = _sStartMarkup + "(.*)" + _sEndMarkup;
			var oMatch = Regex.Match(sPacket, sPattern);

			// if packet is malformed, return
			if (!oMatch.Success)
				break;

			// get the id NUM of the message embedded in tag: <msg id=NUM>
			string sId = oMatch.Groups[1].Value;
			// get only the msg part from the match
			string sTextMsg = oMatch.Groups[2].Value;

			MsgData stMsgData = new MsgData();
			stMsgData.Id = Int32.Parse(sId);
			stMsgData.Msg = sTextMsg;

			Log.Disabled("TcpTextDatagramComm.ParseRcvdData(" + stMsgData.Id + "):\n" + stMsgData.Msg);

			Log.Print(_eLogLevel, $"TcpTextDatagramComm.ParseRcvdData(): Message parsed with id = " + sId + ":\n" + sTextMsg);
			// add it to the msg queue
			lock (_textMsgs)
			{
				_textMsgs.Add(stMsgData);
				Log.Print(_eLogLevel, $"TcpTextDatagramComm.ParseRcvdData(): Messages waiting: " + _textMsgs.Count);
			}

			nMsgCount++;
		}
	
		return nMsgCount;
	}
}

//---------------------------------
