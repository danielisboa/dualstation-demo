using System;
using System.Runtime.InteropServices;

//----------------------------

namespace Infrastructure
{
	public class Serializer
	{
		private const int _defaultSize = 50;

		//----------------------------

		private byte[] _array;
		private int _index;
		private int _size;

		//----------------------------

		public static void PrintBytes(string name, byte[] bytes)
		{
			Console.WriteLine(BytesToString(name, bytes));
		}

		//----------------------------

		public static string BytesToString(string name, byte[] bytes)
		{
			string res = $"{name} ({bytes.Length}): ";
			foreach (var b in bytes) { res += string.Format("0x{0:X2} ", b); }
			return res;
		}

		//----------------------------

		public Serializer() : this(_defaultSize) {}
		
		//----------------------------
		
		public Serializer(int size) : this(new byte[size]) {}

		//----------------------------
		
		public Serializer(byte[] array) : this(array, 0, 0) {}

		//----------------------------

		public Serializer(byte[] array, int offset) : this(array, array.Length, offset) {}

		//----------------------------

		public Serializer(byte[] array, int size, int offset)
		{
			_array = array;
			_index = offset;
			_size = (size >= offset) ? size : offset;
		}
		
		//----------------------------

		public byte[] Array
		{
			get { return _array; }
		}

		//----------------------------

		public int Capacity
		{
			get { return _array.Length; }
		}

		//----------------------------

		public int Size
		{
			get { return _size; }
		}

		//----------------------------

		public int Index
		{
			get { return _index; }
		}

		//----------------------------

		public int Offset
		{
			set { _index = value; }
		}

		//----------------------------

		public bool Add<T>(T data)
		{
			return Add(_index, data);
		}

		//----------------------------

		public bool Add<T>(int index, T data)
		{
			bool grow = true;
			_index = index;
			bool ok = Add(ref _array, ref _index, data, grow);
			if (ok && _size < _index)
				_size = _index;
			return ok;
		}
		
		//----------------------------

		public bool Add(byte[] data)
		{
			return Add(data, data.Length, 0);
		}

		//----------------------------
		
		public bool Add(byte[] data, int size)
		{
			return Add(data, size, 0);
		}

		//----------------------------

		public bool Add(byte[] data, int size, int offset)
		{
			bool grow = true;
			bool ok = Add(ref _array, ref _index, data, offset, size, grow);
			if (ok && _size < _index)
				_size = _index;
			return ok;
		}

		//----------------------------

		public bool Get<T>(ref T data)
		{
			return Get(_array, ref _index, ref data);
		}

		//----------------------------

		public bool Get<T>(int index, ref T data)
		{
			_index = index;
			return Get(ref data);
		}
		
		//----------------------------
		
		public bool Get(ref byte[] data, int size)
		{
			return Get(ref data, size, 0);
		}

		//----------------------------

		public bool Get(ref byte[] data, int size, int offset)
		{
			bool grow = false;
			return Add(ref data, ref offset, _array, _index, size, grow);
		}
		
		//----------------------------
		
		public bool Get(int index, ref byte[] data, int size)
		{
			return Get(index, ref data, size, 0);
		}

		//----------------------------

		public bool Get(int index, ref byte[] data, int size, int offset)
		{
			bool grow = false;
			_index = index;
			return Add(ref data, ref offset, _array, _index, size, grow);
		}

		//----------------------------

		public void Reset()
		{
			Seek(0);
		}

		//----------------------------

		public void Seek(int offset)
		{
			_index = offset;
		}

		//----------------------------

		public void Trim()
		{
			System.Array.Resize(ref _array, _size);
		}

		//----------------------------

		public void Resize(int size)
		{
			bool force = false;
			if (size < _array.Length && !force) return;
			System.Array.Resize(ref _array, size);
		}

		//----------------------------

		public void Grow(int size)
		{
			System.Array.Resize(ref _array, _array.Length + size);
		}

		//----------------------------

		public static int SizeOf<T>(T data)
		{
			return Marshal.SizeOf(data);
		}

		//----------------------------

		public static int SizeOf(Type t)
		{
			return Marshal.SizeOf(t);
		}

		//----------------------------

		public static byte[] Serialize<T>(T data)
		{
			int offset = 0;
			byte[] buffer = new byte[SizeOf(data)];
			Add(ref buffer, ref offset, data, false);
			return buffer;
		}

		//----------------------------

		public static bool Add(ref byte[] dst, ref int dstOffset, byte[] src, int srcOffset, int count, bool grow)
		{
			if (dst == null)
			{
				if (!grow) return false;
				dst = new byte[count + dstOffset];
			}

			if ((dst.Length - dstOffset) < count)
			{
				if (!grow) return false;
				System.Array.Resize(ref dst, dst.Length + count);
			}

			Buffer.BlockCopy(src, srcOffset, dst, dstOffset, count);
			dstOffset += count;

			return true;
		}

		//----------------------------

		public static bool Add<T>(ref byte[] dst, ref int offset, T src, bool grow)
		{
			int size = Marshal.SizeOf(src);

//			Type t = typeof(T);
//			Console.WriteLine($"Serializer.Add(): offset = {offset}, src = {src}, size = {size}, type = {t.FullName}");

			if (dst == null)
			{
				if (!grow) return false;
				dst = new byte[size + offset];
			}

			if ((dst.Length - offset) < size)
			{
				if (!grow) return false;
				System.Array.Resize(ref dst, dst.Length + size);
			}

			IntPtr ptr = Marshal.AllocHGlobal(size);
			Marshal.StructureToPtr(src, ptr, true);
			Marshal.Copy(ptr, dst, offset, size);
			Marshal.FreeHGlobal(ptr);
			offset += size;
			return true;
		}
		
		//----------------------------

		public static bool Get<T>(byte[] src, ref int offset, ref T data)
		{
			int size = Marshal.SizeOf(typeof(T));
			if ((src.Length - offset) < size) return false;
			IntPtr ptr = Marshal.AllocHGlobal(size);
			Marshal.Copy(src, offset, ptr, size);
			data = (T) Marshal.PtrToStructure(ptr, typeof(T));
			Marshal.FreeHGlobal(ptr);
			offset += size;
			return true;
		}
	}
}

//----------------------------
