﻿using System;
using Newtonsoft.Json;

public class TcpJsonComm : TcpTextDatagramComm
{
	static TcpJsonComm _pInstance;

	//---------------------------------

	new public static TcpJsonComm Instance()
	{
		return _pInstance;
	}

	//---------------------------------

	static TcpJsonComm()
	{
		_pInstance = new TcpJsonComm();
	}

	//---------------------------------

	public TcpJsonComm()
	{
	}

	//---------------------------------

	public bool GetNextMsg<T>(out T oMsg)
	{
		string sMsg;
		oMsg = default(T);

		if (!base.GetNextMsg(out sMsg))
			return false;

		try
		{
			oMsg = JsonConvert.DeserializeObject<T>(sMsg);
			return true;
		}
		catch
		{
			return false;
		}
	}

	//---------------------------------

	public bool SendMsg<T>(T oMsg)
	{
		return SendMsg(0, oMsg);
	}

	//---------------------------------

	public bool SendMsg<T>(int id, T oMsg)
	{
		try
		{
			string sJsonMsg;
			if (!typeof(T).Equals(typeof(string)))
				sJsonMsg = JsonConvert.SerializeObject(oMsg);
			else
				sJsonMsg = oMsg as string;
			return base.SendMsg(id, sJsonMsg);
		}
		catch
		{
			return false;
		}
	}
}

//---------------------------------
