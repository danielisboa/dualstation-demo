﻿using System;
using System.Runtime.InteropServices;

namespace Infrastructure
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct MsgHeader
	{
		// the MsgHeader fields must be equal to the AppMsg struct in Elbit's Infrastructure
		// the TimeTag field will be changed to the UnitCode inside the WS code
		public ushort TimeTag;
		public ushort Opcode;
		public ushort Length;

		//----------------------------

		public MsgHeader(ushort opcode) : this(opcode, 0) {}

		//----------------------------

		public MsgHeader(ushort opcode, ushort length)
		{
			Length  = length;
			Opcode  = opcode;
			TimeTag = (ushort) DateTime.Now.Ticks;
		}

		//----------------------------

		public MsgHeader(byte[] data) : this(data, 0) {}

		//----------------------------

		public MsgHeader(byte[] data, int offset) : this(0,0)
		{
			Deserialize(data, offset);
		}

		//----------------------------

		public void Deserialize(byte[] data)
		{
			Deserialize(data, 0);
		}

		//----------------------------

		public void Deserialize(byte[] data, int offset)
		{
			var serializer = new Serializer(data, data.Length, offset);
			serializer.Get(ref Length);
			serializer.Get(ref Opcode);
			serializer.Get(ref TimeTag);
		}

		//----------------------------

		public void Serialize(ref byte[] buffer)
		{
			Serialize(ref buffer, 0);
		}

		//----------------------------

		public void Serialize(ref byte[] buffer, int offset)
		{
			var serializer = new Serializer(buffer);
			serializer.Add(offset, this);
		}

		//----------------------------

		public byte[] Serialize()
		{
			return Serializer.Serialize(this);
		}

		//----------------------------

		public static int Size()
		{
			return sizeof(ushort) * 3;
		}
	}
}

