using System;
using System.Threading;
using Infrastructure;
using WSCommunication;

//-------------------------------------

namespace CommTest
{
	public class CommTest : Observer<WSSimMsg>
	{
		WSSimProtocol.Opcodes PrintOpcode;
		WSSimProtocol.SimStatusMsg SimStatus;
		WSSimProtocol.SimFastStatusMsg SimFastStatus;
		bool Verbose;

		//-------------------------------------

		public CommTest()
		{
			Verbose = false;
			PrintOpcode = WSSimProtocol.Opcodes.NoOpcode;
			var commInterface = WSSimInterface.Instance();
			commInterface.SetupConnection(2223, "192.168.1.4", 2223);
			commInterface.Subscribe(this);
			commInterface.Enable (true);
			SimStatus = new WSSimProtocol.SimStatusMsg();

			Console.WriteLine ("CommTest: END OF CONSTRUCTOR");
		}

		//-------------------------------------

		public void HandlePeriodicMsg(Object stateInfo)
		{
			SendKeepAliveMsg();
		}

		//-------------------------------------

		public void SendKeepAliveMsg()
		{
			var msg = new WSSimMsg(WSSimProtocol.Opcodes.KeepAlive);
			var bytes = msg.Serialize();
			if (Verbose && PrintOpcode == WSSimProtocol.Opcodes.NoOpcode)
				Console.WriteLine("SendKeepAliveMsg");
			WSSimInterface.Instance().SendMsg(bytes);
		}

		//-------------------------------------

		bool SendSimStatusMsg()
		{
			Console.WriteLine($"SendSimStatusMsg: range = {SimStatus.Range}, freshRange = {SimStatus.FreshRange}, ammoCount = {SimStatus.AmmoCount}");
			var statuBytes = new byte[WSSimProtocol.SimStatusMsg.Size()];
			if (!SimStatus.Serialize(statuBytes))
				Console.WriteLine("SendSimStatusMsg: Failure serializing status");
			Serializer.PrintBytes("SendSimStatusMsg|Status", statuBytes);
			var msg = new WSSimMsg(WSSimProtocol.Opcodes.SimStatus, statuBytes);
			var bytes = msg.Serialize();
			Serializer.PrintBytes("SendSimStatusMsg|Buffer", bytes);
			return WSSimInterface.Instance().SendMsg(bytes);
		}

		//-------------------------------------

		bool SendSimFastStatusMsg()
		{
			Console.WriteLine($"SendSimFastStatusMsg: Trv = {SimFastStatus.TrvEncoder}, Elv = {SimFastStatus.ElvEncoder}");

			var statuBytes = new byte[WSSimProtocol.SimFastStatusMsg.Size()];

			if (!SimFastStatus.Serialize(statuBytes))
				Console.WriteLine("SendSimFastStatusMsg: Failure serializing status");

			Serializer.PrintBytes("SendSimFastStatusMsg|Status", statuBytes);

			var msg = new WSSimMsg(WSSimProtocol.Opcodes.SimFastStatus, statuBytes);
			var bytes = msg.Serialize();

			Serializer.PrintBytes("SendSimFastStatusMsg|Buffer", bytes);

			return WSSimInterface.Instance().SendMsg(bytes);
		}

		//-------------------------------------

		override public void OnNext(WSSimMsg msg)
		{
			//Console.WriteLine ("OnNext: NEW MSG RCVD");

			var opcode = (WSSimProtocol.Opcodes) msg.Opcode;

			if (opcode != PrintOpcode)
				return;

			switch (opcode)
			{
			case WSSimProtocol.Opcodes.WSStatus:
				var statusMsg = msg.GetWsStatusMsg();
				Console.WriteLine("Received WSStatus: OM: {0}, AC: {1}, FM: {12}, RX: {2}, RY: {3}, OFZ: {4}, RNG: {5}, ARM: {6}, SAFE: {7}, OVR: {8}, MinR: {9}, MaxR: {10}, TJ: {13}, EJ: {14}, V: {11}",
					statusMsg.OpMode, statusMsg.ActiveCamera, statusMsg.ReticlePosX, statusMsg.ReticlePosY, statusMsg.OutFiringZone,
				                  statusMsg.Range, statusMsg.Arm, statusMsg.Safe, statusMsg.Override, statusMsg.MinRange, statusMsg.MaxRange,
				                  statusMsg.ProtocolVersion, statusMsg.FocusMode, statusMsg.TrvLrfJmpOffset, statusMsg.ElvLrfJmpOffset);
				break;

			case WSSimProtocol.Opcodes.WSFastStatus:
				var fastStatusMsg = msg.GetWsFastStatusMsg();
				Console.WriteLine("Received WSFastStatus: TRV: {0}, ELV: {1}, EmTRV: {2}, EmELV: {3}, HndlTRV: {4}, HndlELV: {5}",
					fastStatusMsg.TrvEncoderPos, fastStatusMsg.ElvEncoderPos,
					fastStatusMsg.TrvEncoderPosCMDR, fastStatusMsg.ElvEncoderPosCMDR,
					fastStatusMsg.TrvEncoderEmuPos, fastStatusMsg.ElvEncoderEmuPos,
					fastStatusMsg.TrvEncoderEmuPosCMDR, fastStatusMsg.ElvEncoderEmuPosCMDR,
					fastStatusMsg.TrvHandleValue, fastStatusMsg.ElvHandleValue,
					fastStatusMsg.TrvHandleValueCMDR, fastStatusMsg.ElvHandleValueCMDR);
				break;

			case WSSimProtocol.Opcodes.WSCmds:
				//Console.WriteLine ("Received WSCmds");
				var cmdsMsg = msg.GetWsCmdsMsg ();
				Console.WriteLine("Received WSCmds: RNG: {0}, FIRE: {1}, FM: {2}, RM: {3}, POL: {4}, RCK: {5}, FC: {6}, IRM: {7}, IRC: {8}, GMM: {9}, GMC: {10}, LVM: {11}, LVC: {12}, GNM: {13}, GNC: {14}, NUC: {15}, ZOOM: {16}",
					cmdsMsg.MeasureRange, cmdsMsg.FireCmd, cmdsMsg.FireMode, cmdsMsg.RangeMode, cmdsMsg.PolarityMode, cmdsMsg.ReloadWeapon,
					cmdsMsg.FocusCmd, cmdsMsg.IrisMode, cmdsMsg.IrisCmd, cmdsMsg.GammaMode, cmdsMsg.GammaCmd,
					cmdsMsg.LevelMode, cmdsMsg.LevelCmd, cmdsMsg.GainMode, cmdsMsg.GainCmd, cmdsMsg.NucCmd, cmdsMsg.ZoomCmd);
				break;
			}
		}

		//-------------------------------------

		public void Run()
		{
			Console.WriteLine($"WSSimProtocol.WsCmdsMsg.Size()       = {WSSimProtocol.WsCmdsMsg.Size()}");
			Console.WriteLine($"WSSimProtocol.WsStatusMsg.Size()     = {WSSimProtocol.WsStatusMsg.Size()}");
			Console.WriteLine($"WSSimProtocol.WsFastStatusMsg.Size() = {WSSimProtocol.WsFastStatusMsg.Size()}");

			SimFastStatus.TrvEncoder = 0;
			SimFastStatus.ElvEncoder = 0;
			SimFastStatus.TrvEncoderCMDR = 0;
			SimFastStatus.ElvEncoderCMDR = 0;
			SendSimFastStatusMsg();
			SimStatus.UseEncSimulation = 0;
			SimStatus.AmmoCount = 100;
			SimStatus.FovHorzAngle = Common.Values.WideFov;
			SimStatus.FovVertAngle = Common.Values.WideFov * 0.75f;
			SimStatus.ZoomPercent = Common.Values.WideFovDayZoomLevel;
			SimStatus.Fov = (sbyte) Common.FieldOfView.Wide;
			SendSimStatusMsg();

			while (true)
			{
				System.Threading.Thread.Sleep(1000);
				var key = Console.ReadKey(true).KeyChar;
				var opcode = PrintOpcode;
				PrintOpcode = WSSimProtocol.Opcodes.NoOpcode;

				SendKeepAliveMsg();

				var verbose = Verbose;
				Verbose = false;

				switch (key)
				{
					case 's':
						Console.WriteLine("\nPrinting WSStatus");
						opcode = WSSimProtocol.Opcodes.WSStatus;
						break;
					case 'f':
						Console.WriteLine("\nPrinting WSFastStatus");
						opcode = WSSimProtocol.Opcodes.WSFastStatus;
						break;
					case 'c':
						Console.WriteLine("\nPrinting WSCmds");
						opcode = WSSimProtocol.Opcodes.WSCmds;
						break;
					case 'r':
						Console.Write("\nEnter range: ");
						var range = Console.ReadLine();
						SimStatus.Range = UInt16.Parse(range);
						SimStatus.FreshRange = 1;
						Console.WriteLine("Sending" + ((SendSimStatusMsg()) ? "OK" : "FAILED"));
						SimStatus.FreshRange = 0;
						break;
					case 'a':
						Console.Write("\nEnter ammo count: ");
						var count = Console.ReadLine();
						SimStatus.AmmoCount = UInt16.Parse(count);
						Console.WriteLine("Sending" + ((SendSimStatusMsg()) ? "OK" : "FAILED"));
						break;
					case 'p':
						Console.Write("\nEnter trv: ");
						var trv = Console.ReadLine();
						SimFastStatus.TrvEncoder = (float) Double.Parse(trv);
						Console.Write("Enter elv: ");
						var elv = Console.ReadLine();
						SimFastStatus.ElvEncoder = (float) Double.Parse(elv);
						Console.WriteLine("Sending" + ((SendSimFastStatusMsg()) ? "OK" : "FAILED"));
						SimStatus.UseEncSimulation = 1;
						Console.WriteLine("Sending" + ((SendSimStatusMsg()) ? "OK" : "FAILED"));
						break;
					case 'v':
						Console.WriteLine("\nVerbose");
						verbose = !verbose;
						break;
					case 'q':
						SimStatus.UseEncSimulation = 0;
						SendSimStatusMsg();
						Console.WriteLine("\nQuit");
						return;
					default:
						Console.WriteLine("\nStop");
						opcode = WSSimProtocol.Opcodes.NoOpcode;
						break;
				}

				Verbose = verbose;
				PrintOpcode = opcode;
			}
		}
	}

	//-------------------------------------

	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("CommTest: MainClass - START");
			try
			{
				Console.WriteLine("CommTest");
				var commTest = new CommTest();
				var timer = new Timer(commTest.HandlePeriodicMsg);
				timer.Change(500, 500);
				commTest.Run();
			}
			catch (Exception e)
			{
				// just to avoid warning
				Console.WriteLine("CommTest: MainClass - EXCEPTION");
				e.ToString();
			}
		}
	}
}

//-------------------------------------
