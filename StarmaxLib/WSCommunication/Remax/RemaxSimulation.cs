using System;
using System.Collections;
using System.Threading;
using WSCommunication;
using Infrastructure;

//-------------------------------------

namespace WSCommunication
{
	public class RemaxSimulation : WSSimulation
	{
		private float m_encTrv;
		private float m_encElv;
		// private float m_encTrvCMDR;
		// private float m_encElvCMDR;

//		private float m_dayHFov;
//		private float m_dayVFov;
//		private float m_nightHFov;
//		private float m_nightVFov;
		private int   m_aimingX;
		private int   m_aimingY;
		private bool  m_fire     = false;
		private bool  m_fireCmd  = false;
		private bool  m_range    = false;
		private bool  m_rangeCmd = false;

		//private uint  m_nFocusMode;
		//private int   m_nFocusCmd;
		//private int   m_nFireMode;
		//private int   m_nRangeMode;

		// status
		private uint  m_opMode;
		private uint  m_activeCamera;
		private uint m_InFiringZone;
		private uint m_nArm;
		private uint m_nSafe;
		private uint m_nOverride;
		private uint m_nMinRange;
		private uint m_nMaxRange;
		private uint  m_nRange;

		//Cmds
		public byte m_nFireCmd;
		public byte m_nFireMode;
		public byte m_nRangeMode;
		public byte m_nPolarityMode;
		public byte m_nReload;
		public sbyte m_nFocusCmd;
		public byte m_nFocusMode;
		public byte m_nIrisMode;
		public sbyte m_nIrisCmd;
		public byte m_nGammaMode;
		public sbyte m_nGammaCmd;
		public byte m_nLevelMode;
		public sbyte m_nLevelCmd;
		public byte m_nGainMode;
		public sbyte m_nGainCmd;
		public byte m_nNucCmd;
		public sbyte m_nFovCmd;

		public bool Debug;
		Timer m_timer;

		//-------------------------------------

		public RemaxSimulation()
		{
			Debug = false;
			Subscribe(WSSimInterface.Instance());
			m_timer = new Timer(this.SendKeepAliveMsg);
			m_timer.Change(500, 500);
			SimStatus.UseEncSimulation = 1;
		}

		//-------------------------------------

		~RemaxSimulation()
		{
			SimStatus.UseEncSimulation = 0;
			SendSimStatusMsg();

			var commInterface = WSSimInterface.Instance();
			commInterface.CloseConnection();
			Unsubscribe(commInterface);
		}

		//-------------------------------------

		public void SendKeepAliveMsg(Object state)
		{
			var msg = new WSSimMsg(WSSimProtocol.Opcodes.KeepAlive);
			WSSimInterface.Instance().SendMsg(msg.Serialize());
		}

		//-------------------------------------

		public void SetupConnection(int localPort, string remoteAddress, int remotePort)
		{
			var commInterface = WSSimInterface.Instance();
			if (!commInterface.IsCommOk ())
			{
				commInterface.SetupConnection (localPort, remoteAddress, remotePort);
				Thread.Sleep(1000);
			}
			SendSimStatusMsg();
		}

		//-------------------------------------

		override public void UpdateMeasuredDistance(float distance, byte freshRange)
		{
			SimStatus.Range = (ushort) distance;
			SimStatus.FreshRange = 1;
			SendSimStatusMsg();
			SimStatus.FreshRange = 0;
		}

		//-------------------------------------

		override public void UpdateAmmunitionCount(uint count)
		{
			SimStatus.AmmoCount = (ushort) count;
			SendSimStatusMsg();
		}

		//-------------------------------------

		override public void DisableSafetySwitch(bool disable)
		{
			SimCmds.DisableSafetySw = (byte) ((disable) ? 1 : 0);
			SendSimCmdMsg();
		}

		//-------------------------------------

		override public void SetEncoderSimulation(bool simulation)
		{
			SimStatus.UseEncSimulation = (byte) ((simulation) ? 1 : 0);
			SendSimStatusMsg();
		}

		//-------------------------------------

		override public void UpdateEncoderValues(float traverse, float elevation /*, float _trvPosCMDR, float _elvPosCMDR*/)
		{
			SimFastStatus.TrvEncoder = traverse;
			SimFastStatus.ElvEncoder = elevation;

			// SimFastStatus.TrvEncoderCMDR = _trvPosCMDR;
			// SimFastStatus.ElvEncoderCMDR = _elvPosCMDR;

			SendSimFastStatusMsg();
		}

		//-------------------------------------

		override public void UpdateEncoderValuesCMDR(float traverse, float elevation)
		{
			SimFastStatus.TrvEncoderCMDR = traverse;
			SimFastStatus.ElvEncoderCMDR = elevation;

			SendSimFastStatusMsg();
		}

		//-------------------------------------

		override public void UpdateGyroValues(float traverse, float elevation)
		{
		}

		override public void UpdateGyroValuesCMDR(float traverse, float elevation)
		{
		}

		//-------------------------------------

		override public void UpdateInertialValues()
		{
		}

		//-------------------------------------

		private string ByteArrayToHexString(byte[] data)
		{
			string delimiter = " ";
			string hex = BitConverter.ToString(data);
			return hex.Replace("-", delimiter);
		}

		//-------------------------------------

		public void GetEncodersValues(out float encTrv, out float encElv)
		{
			encTrv = m_encTrv;
			encElv = m_encElv;
		}
		// Commander
		// public void GetEncodersCommanderValues(out float encTrv, out float encElv)
		// {
		// 	encTrv = m_encTrvCMDR;
		// 	encElv = m_encElvCMDR;
		// }

		//-------------------------------------

		public void GetAimingValues(out float aimX, out float aimY)
		{
			aimX = m_aimingX;
			aimY = m_aimingY;
		}

		//-------------------------------------

		public bool GetFireCmd()
		{
			bool fire = m_fireCmd;
			if (m_nFireMode == 0)
				m_fireCmd = false;
			return fire;
		}

		//-------------------------------------

		public bool GetMeasureRangeCmd()
		{
			bool range = m_range;
			m_range = m_rangeCmd;
			return range;
		}

		//-------------------------------------

//		public void GetCameraFOV(out float hfov, out float vfov)
//		{
//			bool thermal = (m_activeCamera == (uint) WSSimProtocol.CameraTypes.Thermal);
//			hfov = (thermal) ? m_nightHFov : m_dayHFov;
//			vfov = (thermal) ? m_nightVFov : m_dayVFov;
//		}

		//-------------------------------------

//		public float GetCameraHFOV()
//		{
//			return (m_activeCamera == (uint) WSSimProtocol.CameraTypes.Day) ? m_dayHFov : m_nightHFov;
//		}
//
//		//-------------------------------------
//
//		public float GetCameraVFOV()
//		{
//			return (m_activeCamera == (uint) WSSimProtocol.CameraTypes.Day) ? m_dayVFov : m_nightVFov;
//		}

		//-------------------------------------

		public uint GetActiveCamera()
		{
			return m_activeCamera;
		}

		//-------------------------------------

		public uint GetOpMode()
		{
			return m_opMode;
		}

		//-------------------------------------

		public uint GetFocusMode()
		{
			return m_nFocusMode;
		}

		//-------------------------------------

		public int GetFocusCmd()
		{
			return m_nFocusCmd;
		}

		//-------------------------------------

		public int GetFireMode()
		{
			return m_nFireMode;
		}

		//-------------------------------------

		public int GetRangeMode()
		{
			return m_nRangeMode;
		}

		//-------------------------------------

		public int GetBurstLength()
		{
			if (m_nFireMode == 0)
				return 1;
			else if (m_nFireMode == 1)
				return 5;
			else
				return 15;
		}

		//-------------------------------------

		public uint GetRange()
		{
			return m_nRange;
		}

		//-------------------------------------

		public uint GetInFiringZone()
		{
			return m_InFiringZone;
		}

		//-------------------------------------

		public uint GetArmSw()
		{
			return m_nArm;
		}

		//-------------------------------------

		public uint GetSafeSw()
		{
			return m_nSafe;
		}

		//-------------------------------------

		public uint GetOverrideSw()
		{
			return m_nOverride;
		}

		//-------------------------------------

		public void GetMinMaxRange(out float min, out float max)
		{
			min = m_nMinRange;
			max = m_nMaxRange;
		}

		//-------------------------------------

		public byte GetPolarityMode()
		{
			return m_nPolarityMode;
		}

		//-------------------------------------

		public byte GetReloadWeapon()
		{
			return m_nReload;
		}

		//-------------------------------------

		public byte GetIrisMode()
		{
			return m_nIrisMode;
		}

		//-------------------------------------

		public sbyte GetIrisCmd()
		{
			return m_nIrisCmd;
		}

		//-------------------------------------

		public byte GetGammaMode()
		{
			return m_nGammaMode;
		}

		//-------------------------------------

		public sbyte GetGammaCmd()
		{
			return m_nGammaCmd;
		}

		//-------------------------------------

		public byte GetLevelMode()
		{
			return m_nLevelMode;
		}

		//-------------------------------------

		public sbyte GetLevelCmd()
		{
			return m_nLevelCmd;
		}

		//-------------------------------------

		public byte GetGainMode()
		{
			return m_nGainMode;
		}

		//-------------------------------------

		public sbyte GetGainCmd()
		{
			return m_nGainCmd;
		}

		//-------------------------------------

		public byte GetNucCmd()
		{
			return m_nNucCmd;
		}

		//-------------------------------------

		public sbyte GetFovCmd()
		{
			return m_nFovCmd;
		}

		//-------------------------------------

		// Observer
		override public void OnNext(WSSimMsg msg)
		{
			if (Debug) Console.WriteLine("OnNext");

			switch (msg.Opcode)
			{
			case WSSimProtocol.Opcodes.WSStatus:
				HandleWSStatusMsg(msg.GetWsStatusMsg());
				break;

			case WSSimProtocol.Opcodes.WSFastStatus:
				HandleWSFastStatusMsg(msg.GetWsFastStatusMsg());
				break;

			case WSSimProtocol.Opcodes.WSCmds:
				HandleWSCmdsMsg(msg.GetWsCmdsMsg());
				break;
			}
		}

		//-------------------------------------

		void HandleWSStatusMsg(WSSimProtocol.WsStatusMsg status)
		{
//			if (m_activeCamera == (uint)WSSimProtocol.CameraTypes.Day)
//			{
//				m_dayHFov = status.CameraHorzFov;
//				m_dayVFov = status.CameraVertFov;
//			}
//			else if (m_activeCamera == (uint)WSSimProtocol.CameraTypes.Thermal)
//			{
//				m_nightHFov = status.CameraHorzFov;
//				m_nightVFov = status.CameraVertFov;
//			}
			//m_nFocusMode = (uint)status.FocusMode;

			m_activeCamera = (uint)status.ActiveCamera;

			m_aimingX = status.ReticlePosX;
			m_aimingY = status.ReticlePosY;

			m_opMode = (uint)status.OpMode;

			m_InFiringZone = (uint)status.OutFiringZone;

			m_nRange = (uint)status.Range;

			m_nArm = (uint)status.Arm;
			m_nSafe = (uint)status.Safe;
			m_nOverride = (uint)status.Override;
			m_nMinRange = (uint)status.MinRange;
			m_nMaxRange = (uint)status.MaxRange;
			m_nFocusMode = (byte)status.FocusMode;
		}

		//-------------------------------------

		void HandleWSFastStatusMsg(WSSimProtocol.WsFastStatusMsg fastStatus)
		{
			m_encElv = fastStatus.ElvEncoderPos * 360f / 6400f;
			m_encTrv = fastStatus.TrvEncoderPos * 360f / 6400f;
		}

		//-------------------------------------

		void HandleWSCmdsMsg(WSSimProtocol.WsCmdsMsg cmds)
		{
			m_nFireMode = cmds.FireMode;

			m_nRangeMode = cmds.RangeMode;

			if (m_nFireMode == 0) // Single
			{
				if (cmds.FireCmd == 1)
					m_fireCmd = true;
			}
			else // Burst or Full
				m_fireCmd = cmds.FireCmd == 1;
			//-------
			m_rangeCmd = cmds.MeasureRange == 1;

			if (!m_range) m_range = m_rangeCmd;
			if (!m_fire) m_fire = m_fireCmd;
			//-------

			m_nPolarityMode = (byte)cmds.PolarityMode;
			m_nReload = (byte)cmds.ReloadWeapon;
			m_nFocusCmd = (sbyte)cmds.FocusCmd;
			m_nIrisMode = (byte)cmds.IrisMode;
			m_nIrisCmd = (sbyte)cmds.IrisCmd;
			m_nGammaMode = (byte)cmds.GammaMode;
			m_nGammaCmd = (sbyte)cmds.GammaCmd;
			m_nLevelMode = (byte)cmds.LevelMode;
			m_nLevelCmd = (sbyte)cmds.LevelCmd;
			m_nGainMode = (byte)cmds.GainMode;
			m_nGainCmd = (sbyte)cmds.GainCmd;
			m_nNucCmd = (byte)cmds.NucCmd;
			m_nFovCmd = (sbyte)cmds.ZoomCmd;
		}

		//-------------------------------------

		override public void UpdateFovValues(float fovAngle, float zoomPercent, sbyte fov, bool uncalibratedFov)
		{
			SimStatus.FovHorzAngle = fovAngle;
			SimStatus.FovVertAngle = fovAngle * (float)0.75;
			SimStatus.ZoomPercent = zoomPercent;
			SimStatus.Fov = fov;

			SendSimStatusMsg();
		}

		override public void OnError(Exception e) {}
		override public void OnCompleted() {}
	}
}
