using System;

namespace WSCommunication
{
	public static class RemaxConstants
	{
		// ACTIVE_CAMERA
		// CMD
		public const uint ACTIVE_CAMERA_CMD_DB = 12;
		public const uint ACTIVE_CAMERA_CMD_DB_ENTRY = 0;
		// STATUS
		public const uint ACTIVE_CAMERA_STS_DB = 11;
		public const uint ACTIVE_CAMERA_STS_DB_ENTRY = 20;

		enum CameraSelectionEnum
		{
			DAY_CAM = 0,
			THERMAL_CAM
		};

		// DAY_CAMERA_FOV
		// CMD
		public const uint DAY_CAM_CMD_DB = 1; 
		public const uint DAY_CAM_FOV_CMD_DB_ENTRY = 6;
		// STS
		public const uint DAY_CAM_STS_DB = 0;
		public const uint DAY_CAM_FOV_STS_DB_ENTRY = 6;

		// THERMAL_CAMERA_FOV
		// CMD
		public const uint NIGHT_CAM_CMD_DB = 4;
		public const uint NIGHT_CAM_FOV_CMD_DB_ENTRY = 6;
		// STS
		public const uint NIGHT_CAM_STS_DB = 3;
		public const uint NIGHT_CAM_FOV_STS_DB_ENTRY = 6;

		enum CameraFOVEnum
		{
			NARROW_FOV = 0,
			WIDE_FOV
		};

		// DISTANCE
		public const uint LRF_STS_DB = 54;
		public const uint LRF_MEASURED_RANGE_DB_ENTRY = 1;
		public const uint MAX_NUM_OF_RANGES = 3;

		// SET DEBUG STATE FOR LRF MEASURE RANGE
		public const uint MEOP_STS_DB = 7;
		public const uint MEOP_LRF_ECHO_STS_DB_ENTRY = 30;

		enum LrfEchoEnum
		{
			ECHO_FAIL = -1,
			NO_ECHOS,
			SINGLE_ECHO,
			TWO_ECHOES,
		};

//		struct MeasuredRangeStruct 
//		{
//			public int[] ranges; // = new int[3];
//			public int NumOfRanges;
//		};

		// HANDLES
		public const uint HANDLES_STS_DB = 32;
		public const uint HANDLES_ELV_VALUE_DB_ENTRY = 0;
		public const uint HANDLES_TRV_VALUE_DB_ENTRY = 1;

		// PMU
		public const uint PMU_STS_DB = 75;
		public const uint PMU_ELV_ENC_VALUE_DB_ENTRY = 8;
		public const uint PMU_TRV_ENC_VALUE_DB_ENTRY = 9;

		// PMU FAST MOTION
		public const uint PMU_FAST_MOTION_STS_DB = 72;
		public const uint PMU_FAST_TRV_ENC_VALUE_DB_ENTRY = 93;
		public const uint PMU_FAST_ELV_ENC_VALUE_DB_ENTRY = 94;

		// DISCRETES
		public const uint DISCRETES_DB = 21;
		public const uint DISCRETES_FIRE_DB_ENTRY = 1;

		// UPDATE TRV/ELV VALUES --- VERIFY GRAPH ON COMBAT SCREEN
	}
}

