using System;
//using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;

using ObserverLib;

namespace WSIMLib
{
	static class Constants
	{
		public const string RemoteIp = "192.168.1.4";
		public const int RemotePort = 2222;
		public const int LocalPort = 2222;
	}

	class WSIM : NotifierMsg
	{
		private UdpClient udpClient;
		private IPEndPoint remoteEndPoint;

		public WSIM(string RemoteIp, int RemotePort, int LocalPort)
		{
			if (!SetupUdpClient (RemoteIp, RemotePort, LocalPort)) 
				Console.WriteLine ("WSIM::UDPSetup NOK!");
			else
				Console.WriteLine ("WSIM::UDPSetup OK!");
		}

		public bool SetupUdpClient(string remoteIp, int remotePort, int localPort)
		{
			remoteEndPoint = new IPEndPoint(IPAddress.Parse(remoteIp), remotePort);

			if (remoteEndPoint.Address.Equals(IPAddress.Any))
				return false;

			if (remoteEndPoint.Port <= 0)
				return false;

			if (localPort <= 0)
				return false;

			IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, localPort);
			udpClient = new UdpClient(endPoint);
			udpClient.Connect(remoteEndPoint);
			return true;
		}

		public void RunReceiver()
		{
			udpClient.BeginReceive(new AsyncCallback(DataReceived), null);
		}

		private void DataReceived(IAsyncResult result)
		{
			IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
			byte[] data = udpClient.EndReceive(result, ref endPoint);
//
//			if (endPoint.Address.Equals(remoteEndPoint.Address) && endPoint.Port == remoteEndPoint.Port)
//			{
//				string msg = Encoding.ASCII.GetString(data);
//
//				if (msg == "quit")
//				{
//					Console.WriteLine("chat terminated by remote");
//					Environment.Exit(0);
//				}
//

//			}
			Console.WriteLine("received: ");
			NotifyObserver (data);

			udpClient.BeginReceive(new AsyncCallback(DataReceived), null);
		}

		public void RunSender()
		{
			string msg = "";
			byte[] newMsg = new byte[3];

			newMsg [0] = 0x03;
			newMsg [1] = 0x00;
			newMsg [2] = 0x05;WSSLib

			while (msg != "quit")
			{
				//msg = Console.ReadLine();
				udpClient.Send(newMsg, 3);
				Thread.Sleep (500);
			}
		}
	}
}



//		public override void Notify(string data)
//		{
//			//	string msg = data;//System.Text.Encoding.ASCII.GetString(data);
//			//Console.WriteLine(m_Name +": "+ data);
//		}
		
		// Setup 
		//construtor
				
		// Interface -----
		//RegisterEntry
		//UnregisterEntry
		//GetData
		//SetData
		//SetDebugState
		//ResetDebugState
		//GetCommStatus
		//GetProtocolVersion
		//SetProtocolVersion

		//UDP
		//SetupUdpClient
		//ReceiveUDPMsg
		//HandleReceivedMsg
		//SendUDPMsg
		
		// meacanismo keepAlive
		// SendkeepAlive
		// ResumeConnection
		// HandleConnectionFailure
	//}	
//}

