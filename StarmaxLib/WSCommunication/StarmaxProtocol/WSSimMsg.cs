using System;
using Infrastructure;
using WSCommunication;

//----------------------------

namespace WSCommunication
{
	public class WSSimMsg
	{
		private byte[] _data;
		private uint _bufferSize;
		private static uint _defaultBufferSize = 50;
		public bool Debug;

		//----------------------------

		public static void SetDefaultBufferSize(uint defaultBufferSize)
		{
			_defaultBufferSize = defaultBufferSize;
		}

		//----------------------------

		public WSSimProtocol.Opcodes Opcode { get; set; }

		//----------------------------

		public uint Lenght { get; set; }

		//----------------------------

		public uint TimeTag { get; }

		//----------------------------

		public void Serialize(ref byte[] buffer)
		{
			Serialize(ref buffer, 0);
		}

		//----------------------------

		public void Serialize(ref byte[] buffer, int offset)
		{
			var serializer = new Serializer(buffer);
			serializer.Offset = offset;

			var header = new Infrastructure.MsgHeader((ushort) Opcode, (ushort) Lenght);
			var headerBytes = header.Serialize();
			if (Debug) Serializer.PrintBytes("WSSimMsg|Header", headerBytes);
			serializer.Add(headerBytes);
			if (Debug) Serializer.PrintBytes("WSSimMsg|Data", _data);
			serializer.Add(_data, (int) Lenght);
			if (Debug) Serializer.PrintBytes("WSSimMsg|Buffer", buffer);
		}

		//----------------------------

		public byte[] Serialize()
		{
			var buffer = new byte[Size()];
			Serialize(ref buffer, 0);
			return buffer;
		}

		//----------------------------

		public int Size()
		{
			return Infrastructure.MsgHeader.Size() + (int) Lenght;
		}

		//----------------------------

		public void SetData(byte[] data)
		{ 
			SetData(data, 0);
		}

		//----------------------------
		
		public void SetData(byte[] data, uint offset)
		{ 
			Lenght = 0;
			if (data == null)
			{
				Reserve(_defaultBufferSize);
				return;
			}
			Lenght = (uint) data.Length - offset;
			Reserve(Lenght);

			Buffer.BlockCopy (data, (int)offset, _data, 0, (int)(Lenght));
		}
		
		//----------------------------

		public WSSimMsg(byte[] data)
		{
			var header = new Infrastructure.MsgHeader(data);
			Opcode = (WSSimProtocol.Opcodes) header.Opcode;
			Lenght = header.Length;
			TimeTag = header.TimeTag;

			SetData(data, (uint) Infrastructure.MsgHeader.Size());
		}

		//----------------------------
		
		public WSSimMsg(WSSimProtocol.Opcodes opcode)
		{
			Opcode = opcode;
			SetData(null);
		}

		//----------------------------
		
		public WSSimMsg(WSSimProtocol.Opcodes opcode, byte[] data)
		{
			Opcode = opcode;
			SetData(data);
		}
		
		//----------------------------
		
		public WSSimProtocol.WsStatusMsg GetWsStatusMsg()
		{
			if (Opcode != WSSimProtocol.Opcodes.WSStatus)
				throw new Exception("StatusMsg: Wrong Msg Type - expected {WSSimProtocol.Opcodes.WSStatus}, received {Opcode}\"");

			if (Lenght != WSSimProtocol.WsStatusMsg.Size())
				throw new Exception($"StatusMsg: Wrong Msg Size - expected {WSSimProtocol.WsStatusMsg.Size()}, received {Lenght}");

			return new WSSimProtocol.WsStatusMsg(_data);
		}
		
		//----------------------------
		
		public WSSimProtocol.WsFastStatusMsg GetWsFastStatusMsg()
		{
			if (Opcode != WSSimProtocol.Opcodes.WSFastStatus)
				throw new Exception("FastStatusMsg: Wrong Msg Type - expected {WSSimProtocol.Opcodes.WSFastStatus}, received {Opcode}\"");

			if (Lenght != WSSimProtocol.WsFastStatusMsg.Size())
				throw new Exception($"FastStatusMsg: Wrong Msg Size - expected {WSSimProtocol.WsFastStatusMsg.Size()}, received {Lenght}");
			
			return new WSSimProtocol.WsFastStatusMsg(_data);
		}
		
		//----------------------------
		
		public WSSimProtocol.WsCmdsMsg GetWsCmdsMsg()
		{
			if (Opcode != WSSimProtocol.Opcodes.WSCmds)
				throw new Exception("CmdMsg: Wrong Msg Type - expected {WSSimProtocol.Opcodes.WSCmds}, received {Opcode}\"");

			if (Lenght != WSSimProtocol.WsCmdsMsg.Size())
				throw new Exception($"CmdMsg: Wrong Msg Size - expected {WSSimProtocol.WsCmdsMsg.Size()}, received {Lenght}");
			return new WSSimProtocol.WsCmdsMsg(_data);
		}
		
		//----------------------------

		public void Reserve(uint bufferSize)
		{
			if (_bufferSize >= bufferSize)
				return;

			_bufferSize = bufferSize;
			_data = new byte[_bufferSize];
		}
	}
}

//----------------------------
