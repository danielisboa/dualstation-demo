using System;
using Infrastructure;
using WSCommunication;

namespace WSCommunication
{
	abstract public class WSSimulation : Observer<WSSimMsg>
	{
		abstract public void UpdateFovValues (float fovAngle, float zoomPercent, sbyte fov, bool uncalibratedFov);
		abstract public void UpdateMeasuredDistance(float distance, byte freshRange);
		abstract public void UpdateAmmunitionCount(uint count);
		abstract public void DisableSafetySwitch(bool running);
		abstract public void SetEncoderSimulation(bool simulation);		
		abstract public void UpdateEncoderValues(float traverse, float elevation);
		abstract public void UpdateEncoderValuesCMDR(float traverse, float elevation);
		abstract public void UpdateGyroValues(float traverse, float elevation);
		abstract public void UpdateGyroValuesCMDR(float traverseCMDR, float elevationCMDR);
		virtual  public void UpdateInertialValues() { }

		//-------------------------------------

		protected WSSimProtocol.SimCmdMsg SimCmds;
		protected WSSimProtocol.SimStatusMsg SimStatus;
		protected WSSimProtocol.SimFastStatusMsg SimFastStatus;

		//-------------------------------------

		protected void SendKeepAliveMsg()
		{
			var msg = new WSSimMsg(WSSimProtocol.Opcodes.KeepAlive);
			WSSimInterface.Instance().SendMsg(msg.Serialize());
		}

		//-------------------------------------

		protected void SendSimCmdMsg()
		{
			var msg = new WSSimMsg(WSSimProtocol.Opcodes.SimCmds, SimCmds.Serialize());
			WSSimInterface.Instance().SendMsg(msg.Serialize());
		}

		//-------------------------------------

		protected void SendSimStatusMsg()
		{
			var msg = new WSSimMsg(WSSimProtocol.Opcodes.SimStatus, SimStatus.Serialize());
			WSSimInterface.Instance().SendMsg(msg.Serialize());
		}

		//-------------------------------------

		protected void SendSimFastStatusMsg()
		{
			var msg = new WSSimMsg(WSSimProtocol.Opcodes.SimFastStatus, SimFastStatus.Serialize());
			WSSimInterface.Instance().SendMsg(msg.Serialize());
		}
	}
}
