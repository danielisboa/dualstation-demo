using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading;

using Infrastructure;

//----------------------------

namespace WSCommunication
{
	public class WSSimInterface : UdpChannelInterface<WSSimInterface, WSSimMsg>
	{
		override protected bool ParseReceivedData(byte[] data)
		{
			var msg = new WSSimMsg(data);
			bool notifyData = IsEnabled();

			switch ((WSSimProtocol.Opcodes) msg.Opcode)
			{
				case WSSimProtocol.Opcodes.WSStatus:
				case WSSimProtocol.Opcodes.WSFastStatus:
				case WSSimProtocol.Opcodes.WSCmds:
					Log.Print(LogLevel, $"Received opcode {msg.Opcode}, lenght {msg.Lenght}, timetag {msg.TimeTag}");
					break;

				case WSSimProtocol.Opcodes.KeepAlive:
					notifyData = true;
					break;

				default:
					Log.Print(LogLevel, $"Received unknown opcode: {msg.Opcode}");
					NotifyError(InvalidMessageReceivedError);
					return false;
			}

			if (notifyData) NotifyData(msg);

			return true;
		}
	}
}

//----------------------------
