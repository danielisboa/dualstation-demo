using System;
using System.Runtime.InteropServices;
using Infrastructure;

//----------------------------

namespace WSCommunication
{
	public static class WSSimProtocol
	{
		public enum Opcodes : ushort
		{
			NoOpcode         = 0x00,
			WSStatus         = 0x01,
			WSFastStatus     = 0x02,
			WSCmds           = 0x03,
			SimStatus        = 0x04,
			SimFastStatus    = 0x05,
			SimCmds          = 0x06,
			KeepAlive		 = 0x07,
		}

		//----------------------------

		public enum OperationModes : byte
		{
			NoMode           = 0x00,
			Manual           = 0x01,
			Power            = 0x02,
			Stab             = 0x03,
		}

		//----------------------------

		public enum WeaponType : byte
		{
			NoWeapon	= 0x00,
			Gun			= 0x01,
			Mag			= 0x02,
		}

		//----------------------------

		public enum RangeModes : byte
		{
			Manual			= 0x00,
			Battle			= 0x01,
			Lrf				= 0x02,
		}

		//----------------------------

		public enum CameraTypes : byte
		{
			Fault     = 0x00,
			Day       = 0x01,
			Thermal   = 0x02,
			Lrf		  = 0x03,
			Boresight = 0x04,
		}

		//----------------------------

		public enum AutoManualMode : byte
		{
			None			= 0xFF,
			Manual			= 0x00,
			Auto			= 0x01
		}

		//----------------------------

		public enum FocusCmd : sbyte
		{
			Near			= -1,
			Stop			=  0,
			Far 			= +1
		}

		//----------------------------

		public enum InOutStopCmd : sbyte
		{
			Out				= -1,
			Stop			=  0,
			In	 			= +1
		}

		//----------------------------

		public enum FovCmds : byte
		{
			NoFov			= 0,
			Narrow			= 1,
			Wide 			= 2,
			VeryWide		= 3,
			Out				= 4,
			Stop			= 5,
			In				= 6,
		}

		//----------------------------

		public enum FieldOfView : sbyte
		{
			NoFov			= -1,
			Narrow			= 0,
			Wide 			= 1,
			VeryWide		= 2,
			VeryVeryWide	= 3,
		}

		//----------------------------

		public enum Polarity : byte
		{
			NoPolarity 		= 0xFF,
			WhiteHot		= 0x00,
			BlackHot		= 0x01,
		}

		//----------------------------

		public enum FireModes : byte
		{
			Single	 		= 0x00,
			Burst_5			= 0x01,
			Burst_15		= 0x02,
		}

		//----------------------------

		public enum EnabledDisabled
		{
			Disabled = 0x00,
			Enabled = 0x01
		}

		//----------------------------

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct WsStatusMsg
		{
			public byte   OpMode;
			public byte   OpModeCMDR;
			public byte   SelectedWeapon;
			public byte   ActiveCamera;
			public byte   ActiveCameraCMDR;
			public short  ReticlePosX;
			public short  ReticlePosY;
			public byte   OutFiringZone;
			public ushort Range;
			public byte   Arm;
			public byte   Safe;
			public byte   Override;
			public short  MinRange;
			public short  MaxRange;
			public byte	  ProtocolVersion;
			public byte   FocusMode;
			public float  TrvLrfJmpOffset;
			public float  ElvLrfJmpOffset;
			public float  TrvLrfJmpOffsetCMDR;
			public float  ElvLrfJmpOffsetCMDR;

			public WsStatusMsg(Opcodes opcode)
			{
				ReticlePosX  = 0;
				ReticlePosY  = 0;
				OpMode = (byte) OperationModes.Manual;
				OpModeCMDR = (byte) OperationModes.Manual;
				SelectedWeapon = (byte) WeaponType.NoWeapon;
				ActiveCamera = (byte) CameraTypes.Fault;
				ActiveCameraCMDR = (byte) CameraTypes.Fault;
				Range = 750;
				MinRange = 0;
				MaxRange = 0;
				OutFiringZone = 0;
				Arm = (byte)EnabledDisabled.Disabled;
				Safe = (byte)EnabledDisabled.Disabled;
				Override = (byte)EnabledDisabled.Disabled;
				ProtocolVersion = 0;
				FocusMode = (byte) AutoManualMode.Auto;
				TrvLrfJmpOffset = 0;
				ElvLrfJmpOffset = 0;
				TrvLrfJmpOffsetCMDR = 0;
				ElvLrfJmpOffsetCMDR = 0;
			}

			public WsStatusMsg(byte[] data) : this(data, 0)
			{
			}

			public WsStatusMsg(byte[] data, int offset) : this(Opcodes.NoOpcode)
			{
				Deserialize(data, offset);
			}

			public void Deserialize(byte[] data)
			{
				Deserialize(data, 0);
			}

			public void Deserialize(byte[] data, int offset)
			{
				var serializer = new Serializer(data, data.Length, offset);
				serializer.Get(ref OpMode);
				serializer.Get(ref OpModeCMDR);
				serializer.Get(ref SelectedWeapon);
				serializer.Get(ref ActiveCamera);
				serializer.Get(ref ActiveCameraCMDR);
				serializer.Get(ref ReticlePosX);
				serializer.Get(ref ReticlePosY);
				serializer.Get(ref OutFiringZone);
				serializer.Get(ref Range);
				serializer.Get(ref Arm);
				serializer.Get(ref Safe);
				serializer.Get(ref Override);
				serializer.Get(ref MinRange);
				serializer.Get(ref MaxRange);
				serializer.Get(ref ProtocolVersion);
				serializer.Get(ref FocusMode);
				serializer.Get(ref TrvLrfJmpOffset);
				serializer.Get(ref ElvLrfJmpOffset);
				serializer.Get(ref TrvLrfJmpOffsetCMDR);
				serializer.Get(ref ElvLrfJmpOffsetCMDR);
			}

			public byte[] Serialize()
			{
				return Serializer.Serialize(this);
			}

			public static int Size()
			{
				return sizeof(byte) * 11 + sizeof(short) * 5 + sizeof(float) * 4;
			}
		}

		//----------------------------

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct WsFastStatusMsg
		{
			public float TrvEncoderPos;
			public float ElvEncoderPos;
			public float TrvEncoderPosCMDR;
			public float ElvEncoderPosCMDR;
			public float TrvEncoderEmuPos;
			public float ElvEncoderEmuPos;
			public float TrvEncoderEmuPosCMDR;
			public float ElvEncoderEmuPosCMDR;
			public float TrvHandleValue;
			public float ElvHandleValue;
			public float TrvHandleValueCMDR;
			public float ElvHandleValueCMDR;
			public bool  HandlePalmSw;

			public WsFastStatusMsg(byte[] data)
				: this(data, 0) {}

			public WsFastStatusMsg(byte[] data, int offset)
			{
				TrvEncoderPos = 0;
				ElvEncoderPos = 0;
				TrvEncoderPosCMDR = 0;
				ElvEncoderPosCMDR = 0;
				TrvEncoderEmuPos = 0;
				ElvEncoderEmuPos = 0;
				TrvEncoderEmuPosCMDR = 0;
				ElvEncoderEmuPosCMDR = 0;
				TrvHandleValue = 0;
				ElvHandleValue = 0;
				TrvHandleValueCMDR = 0;
				ElvHandleValueCMDR = 0;
				HandlePalmSw = false;
				Deserialize(data, offset);
			}

			public void Deserialize(byte[] data)
			{
				Deserialize(data, 0);
			}

			public void Deserialize(byte[] data, int offset)
			{
				var serializer = new Serializer(data, data.Length, offset);
				serializer.Get(ref TrvEncoderPos);
				serializer.Get(ref ElvEncoderPos);
				serializer.Get(ref TrvEncoderPosCMDR);
				serializer.Get(ref ElvEncoderPosCMDR);
				serializer.Get(ref TrvEncoderEmuPos);
				serializer.Get(ref ElvEncoderEmuPos);
				serializer.Get(ref TrvEncoderEmuPosCMDR);
				serializer.Get(ref ElvEncoderEmuPosCMDR);
				serializer.Get(ref TrvHandleValue);
				serializer.Get(ref ElvHandleValue);
				serializer.Get(ref TrvHandleValueCMDR);
				serializer.Get(ref ElvHandleValueCMDR);
				serializer.Get(ref HandlePalmSw);
			}

			public static int Size()
			{
				return sizeof(float) * /* 12 */ 13 + sizeof(bool);
			}
		}

		//----------------------------

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct WsCmdsMsg
		{
			public byte MeasureRange;
			public byte FireCmd;
			public byte FireMode;
			public byte JumpToLaserPos;
			public byte RangeMode;
			public byte PolarityMode;
			public byte ReloadWeapon;
			public sbyte FocusCmd;
			public sbyte FocusCmdCMDR;
			public byte IrisMode;
			public sbyte IrisCmd;
			public byte GammaMode;
			public sbyte GammaCmd;
			public byte LevelMode;
			public sbyte LevelCmd;
			public byte GainMode;
			public sbyte GainCmd;
			public byte NucCmd;
			public byte ZoomCmd;
			public byte ZoomCmdCMDR;
			public byte EnableReticle;

			public WsCmdsMsg(Opcodes opcode)
			{
				MeasureRange = 0;
				FireCmd = 0;
				FireMode = (byte)FireModes.Single;
				JumpToLaserPos = 0;
				RangeMode = (byte)RangeModes.Lrf;
				PolarityMode = (byte)Polarity.BlackHot;
				ReloadWeapon = 0;
				FocusCmd = (sbyte) InOutStopCmd.Stop;
				FocusCmdCMDR = (sbyte) InOutStopCmd.Stop;
				IrisMode = (byte) AutoManualMode.Auto;
				IrisCmd = (sbyte) InOutStopCmd.Stop;
				GammaMode = (byte) AutoManualMode.Auto;
				GammaCmd = (sbyte) InOutStopCmd.Stop;
				LevelMode = (byte) AutoManualMode.Auto;
				LevelCmd = (sbyte) InOutStopCmd.Stop;
				GainMode = (byte) AutoManualMode.Auto;
				GainCmd = (sbyte) InOutStopCmd.Stop;
				NucCmd = 0;
				ZoomCmd = (byte) FovCmds.Narrow;
				ZoomCmdCMDR = (byte) FovCmds.Narrow;
				EnableReticle = 0;
			}

			public WsCmdsMsg(byte[] data) : this(data, 0)
			{
			}

			public WsCmdsMsg(byte[] data, int offset) : this(Opcodes.NoOpcode)
			{
				Deserialize(data, offset);
			}

			public void Deserialize(byte[] data)
			{
				Deserialize(data, 0);
			}

			public void Deserialize(byte[] data, int offset)
			{
				Deserialize(data, data.Length, offset);
			}

			public void Deserialize(byte[] data, int size, int offset)
			{
				var serializer = new Serializer(data, size, offset);
				serializer.Get(ref MeasureRange);
				serializer.Get(ref FireCmd);
				serializer.Get(ref FireMode);
				serializer.Get(ref JumpToLaserPos);
				serializer.Get(ref RangeMode);
				serializer.Get(ref PolarityMode);
				serializer.Get(ref ReloadWeapon);
				serializer.Get(ref FocusCmd);
				serializer.Get(ref FocusCmdCMDR);
				serializer.Get(ref IrisMode);
				serializer.Get(ref IrisCmd);
				serializer.Get(ref GammaMode);
				serializer.Get(ref GammaCmd);
				serializer.Get(ref LevelMode);
				serializer.Get(ref LevelCmd);
				serializer.Get(ref GainMode);
				serializer.Get(ref GainCmd);
				serializer.Get(ref NucCmd);
				serializer.Get(ref ZoomCmd);
				serializer.Get(ref ZoomCmdCMDR);
				serializer.Get(ref EnableReticle);
			}

			public static int Size()
			{
				return sizeof(byte) * 21;
			}
		}

		//----------------------------

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct SimCmdMsg
		{
			public byte OpModeReq;
			public byte OpModeReqCMDR;
			public byte DisableSafetySw;
			public byte RandomizeBoresightValues;

			public byte[] Serialize()
			{
				var data = new byte[Size()];
				if (!Serialize(data, 0))
					throw new Exception("Serialization Failure");
				return data;
			}

			public bool Serialize(byte[] data)
			{
				return Serialize(data, 0);
			}

			public bool Serialize(byte[] data, int offset)
			{
				var serializer = new Serializer(data, data.Length, offset);
				return	serializer.Add(OpModeReq) &&
						serializer.Add(OpModeReqCMDR) &&
						serializer.Add(DisableSafetySw) &&
						serializer.Add(RandomizeBoresightValues);
			}

			public static int Size()
			{
				return sizeof(byte) * 4;
			}
		}

		//----------------------------

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct SimStatusMsg
		{
			public ushort Range;
			public byte FreshRange;
			public ushort AmmoCount;
			public byte UseEncSimulation;
			public float FovHorzAngle;
			public float FovVertAngle;
			public float ZoomPercent;
			public byte UncalibratedFov;
			public sbyte Fov;
			public sbyte PolarityMode;

			public byte[] Serialize()
			{
				var data = new byte[Size()];
				if (!Serialize(data, 0))
					throw new Exception("Serialization Failure");
				return data;
			}

			public bool Serialize(byte[] data)
			{
				return Serialize(data, 0);
			}

			public bool Serialize(byte[] data, int offset)
			{
				var serializer = new Serializer(data, offset);
				return 	serializer.Add (Range) &&
					serializer.Add (FreshRange) &&
					serializer.Add (AmmoCount) &&
					serializer.Add (UseEncSimulation) &&
					serializer.Add (FovHorzAngle) &&
					serializer.Add (FovVertAngle) &&
					serializer.Add (ZoomPercent) &&
					serializer.Add (UncalibratedFov) &&
					serializer.Add (Fov) &&
					serializer.Add (PolarityMode);
			}

			public static int Size()
			{
				return sizeof(byte) * 5 + sizeof(ushort) * 2 + sizeof(float) * 3;
			}
		}

		//----------------------------

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct SimFastStatusMsg
		{
			public float TrvEncoder;
			public float ElvEncoder;
			public float TrvEncoderCMDR;
			public float ElvEncoderCMDR;
			public float TrvGyro;
			public float ElvGyro;
			public float TrvGyroCMDR;
			public float ElvGyroCMDR;

			public byte[] Serialize()
			{
				var data = new byte[Size()];
				if (!Serialize(data, 0))
					throw new Exception("Serialization Failure");
				return data;
			}

			public bool Serialize(byte[] data)
			{
				return Serialize(data, 0);
			}

			public bool Serialize(byte[] data, int offset)
			{
				var serializer = new Serializer(data, data.Length, offset);
				return	serializer.Add(TrvEncoder) &&
						serializer.Add(ElvEncoder) &&
						serializer.Add(TrvEncoderCMDR) &&
						serializer.Add(ElvEncoderCMDR) &&
						serializer.Add(TrvGyro) &&
						serializer.Add(ElvGyro) &&
						serializer.Add(TrvGyroCMDR) &&
						serializer.Add(ElvGyroCMDR);						
			}

			public static int Size()
			{
				return sizeof(float) * 8;
			}
		}
	}
}

//----------------------------
