using System;
using System.Runtime.InteropServices;
using Infrastructure;

//----------------------------

namespace WSCommunication
{
	//----------------------------

	public enum WSDbgEnvOpcodes : byte
	{
		NoOpcode           = 0xFF,

		// From Simulator to WS
		GetKeepAlive       = 0x05,
		RegisterEntry      = 0x08,
		UnregisterEntry    = 0x09,
		GetData            = 0x01,
		SetData            = 0x03,
		UpdateDebugState   = 0x07,
		RegisterEntryEx    = 0x18,
		UnregisterEntryEx  = 0x19,
		GetDataEx          = 0x11,
		SetDataEx          = 0x13,
		UpdateDebugStateEx = 0x17,
		GetDebugState      = 0x20,

		// From WS to Simulator
		SendKeepAlive      = 0x05,
		SingleMsg          = 0x08,
		SendKeepAliveEx    = 0x15,
		SingleMsgEx        = 0x18,
		DebugState         = 0x20,
	}

	//----------------------------

	static class WSDbgEnvProtocol
	{
		private static bool _newProtocol = false;

		//----------------------------

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct MsgHeader
		{
			public UInt16 Length;
			public byte Opcode;

			public MsgHeader(WSDbgEnvOpcodes opcode) : this(opcode, 0) {}

			public MsgHeader(WSDbgEnvOpcodes opcode, ushort length)
			{
				Opcode = (byte) opcode;
				Length = length;
			}

			public MsgHeader(byte[] data) : this(data, 0) {}

			public MsgHeader(byte[] data, int offset) : this(0,0)
			{
				Deserialize(data, offset);
			}
			
			public void Deserialize(byte[] data)
			{
				Deserialize(data, 0);
			}

			public void Deserialize(byte[] data, int offset)
			{
				var serializer = new Serializer(data);
				serializer.Seek(offset);
				serializer.Get(ref Length);
				serializer.Get(ref Opcode);
			}
			
			public void Serialize(ref byte[] buffer)
			{
				Serialize(ref buffer, 0);
			}

			public void Serialize(ref byte[] buffer, int offset)
			{
				var serializer = new Serializer(buffer);
				serializer.Add(offset, this);
			}

			public byte[] Serialize()
			{
				return Serializer.Serialize(this);
			}

			public int Size()
			{
				return Serializer.SizeOf(this);
			}
		}

		//----------------------------

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct EntrySpec<T>
		{
			public T DB;
			public T Entry;
			
			public int Size() { return Serializer.SizeOf(this); }
		}

		//----------------------------

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct RespMsg<T> where T:IConvertible
		{
			public MsgHeader Header;
			public UInt32 TimeTag;
			public EntrySpec<T> Index;
			public byte[] Data;

			public RespMsg(WSDbgEnvOpcodes opcode)
			{
				TimeTag = 0;
				Header.Opcode = (byte) opcode;
				Header.Length = 0;
				Index.DB = (T) Convert.ChangeType(0, typeof(T));
				Index.Entry = (T) Convert.ChangeType(0, typeof(T));
				Data = null;
			}
			
			public RespMsg(byte[] data) : this(data, data.Length) {}

			public RespMsg(byte[] data, int size) : this(data, size, 0) {}

			public RespMsg(byte[] data, int size, int offset) : this(WSDbgEnvOpcodes.NoOpcode)
			{
				Deserialize(data, size, offset);
			}
			
			public void Deserialize(byte[] data, int size)
			{
				Deserialize(data, size, 0);
			}
			
			public void Deserialize(byte[] data, int size, int offset)
			{
				var serializer = new Serializer(data, size, offset);
				serializer.Get(ref Header);
				serializer.Get(ref TimeTag);
				serializer.Get(ref Index);
				int dataSize = Header.Length - serializer.Index;
				Data = new byte[dataSize];
				serializer.Get(ref Data, dataSize);
			}
		}

		//----------------------------

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		private struct ReqMsg<T> where T:IConvertible
		{
			public MsgHeader Header;
			public EntrySpec<T> Index;

			public ReqMsg(WSDbgEnvOpcodes opcode, UInt32 db, UInt32 entry)
			{
				Header.Opcode = (byte) opcode;
				Header.Length = 0;
				Index.DB = (T) Convert.ChangeType(db, typeof(T));
				Index.Entry = (T) Convert.ChangeType(entry, typeof(T));
			}

			public byte[] Serialize() { return Serializer.Serialize(this); }
		}

		//----------------------------

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		private struct SetDataMsg<T> where T:IConvertible
		{
			public MsgHeader Header;
			public EntrySpec<T> Index;
			public byte[] Data;

			public SetDataMsg(WSDbgEnvOpcodes opcode, UInt32 db, UInt32 entry, byte[] data, int size)
			{
				Header.Opcode = (byte) opcode;
				Header.Length = 0;

				Index.DB = (T) Convert.ChangeType(db, typeof(T));
				Index.Entry = (T) Convert.ChangeType(entry, typeof(T));

				Data = new byte[size];
				Buffer.BlockCopy(data, 0, Data, 0, size);
			}

			public byte[] Serialize()
			{
				var serializer = new Serializer();
				serializer.Add(Header);
				serializer.Add(Index);
				serializer.Add(Data, Data.Length);
				return serializer.Array;
			}
		}

		//----------------------------

		public static void SetNewProtocol(bool newProtocol)
		{
			_newProtocol = newProtocol;
		}

		//----------------------------

		public static byte[] BuildGetKeepAliveMsg()
		{
			return new MsgHeader(WSDbgEnvOpcodes.GetKeepAlive, 3).Serialize();
		}
		
		//----------------------------

		public static byte[] BuildRegisterEntryMsg(uint db, uint entry)
		{
			return (_newProtocol)
				? new ReqMsg<UInt32>(WSDbgEnvOpcodes.RegisterEntryEx, db, entry).Serialize()
				: new ReqMsg <Byte> (WSDbgEnvOpcodes.RegisterEntry,   db, entry).Serialize();
		}
		
		//----------------------------

		public static byte[] BuildUnregisterEntryMsg(uint db, uint entry)
		{
			return (_newProtocol)
				? new ReqMsg<UInt32>(WSDbgEnvOpcodes.UnregisterEntryEx, db, entry).Serialize()
				: new ReqMsg <Byte> (WSDbgEnvOpcodes.UnregisterEntry,   db, entry).Serialize();
		}
		
		//----------------------------

		public static byte[] BuildGetDataMsg(uint db, uint entry)
		{
			return (_newProtocol)
				? new ReqMsg<UInt32>(WSDbgEnvOpcodes.GetDataEx, db, entry).Serialize()
				: new ReqMsg <Byte> (WSDbgEnvOpcodes.GetData,   db, entry).Serialize();
		}

		//----------------------------

		public static byte[] BuildSetDataMsg(uint db, uint entry, byte[] data, int size)
		{
			return (_newProtocol)
				? new SetDataMsg<UInt32>(WSDbgEnvOpcodes.SetDataEx, db, entry, data, size).Serialize()
				: new SetDataMsg <Byte> (WSDbgEnvOpcodes.SetData,   db, entry, data, size).Serialize();
		}

		//----------------------------

		public static byte[] BuildUpdateDebugStateMsg(uint db, uint entry)
		{
			return (_newProtocol)
				? new ReqMsg<UInt32>(WSDbgEnvOpcodes.UpdateDebugStateEx, db, entry).Serialize()
				: new ReqMsg <Byte> (WSDbgEnvOpcodes.UpdateDebugState,   db, entry).Serialize();
		}
		
		//----------------------------

		public static byte[] BuildGetDebugStateMsg(uint db, uint entry)
		{
			return new ReqMsg<UInt32>(WSDbgEnvOpcodes.GetDebugState, db, entry).Serialize();
		}
	}
}

//----------------------------
