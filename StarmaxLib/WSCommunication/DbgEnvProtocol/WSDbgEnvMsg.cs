using System;

//----------------------------

namespace WSCommunication
{
	public class WSDbgEnvMsg
	{
		private byte[] _data;
		private uint _bufferSize;
		private static uint _defaultBufferSize = 50;

		//----------------------------

		public static void SetDefaultBufferSize(uint defaultBufferSize)
		{
			_defaultBufferSize = defaultBufferSize;
		}

		//----------------------------

		public uint DBIndex { get; set; }

		//----------------------------

		public uint DBEntryIndex { get; set; }

		//----------------------------

		public uint DataSize { get; private set; }

		//----------------------------

		public byte[] Data
		{ 
			get { return _data; }

			set
			{
				DataSize = 0;
				if (value == null) return;
				DataSize = (uint) value.Length;
				Reserve(DataSize);
				Buffer.BlockCopy(value, 0, _data, 0, (int) DataSize);
			}
		}

		//----------------------------

		public WSDbgEnvMsg(uint dbIndex, uint dbEntryIndex, byte[] data)
		{
			DataSize = 0;
			DBIndex = dbIndex;
			DBEntryIndex = dbEntryIndex;
			Reserve((data != null) ? (uint) data.Length : _defaultBufferSize);
			Data = data;
		}

		//----------------------------

		public void Reserve(uint bufferSize)
		{
			if (_bufferSize >= bufferSize)
				return;

			_bufferSize = bufferSize;
			_data = new byte[_bufferSize];
		}
	}
}

//----------------------------
