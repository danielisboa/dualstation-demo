using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading;

using Infrastructure;

//----------------------------

namespace WSCommunication
{
	public class WSDbgEnvInterface : Observable<WSDbgEnvMsg>
	{
		private static WSDbgEnvInterface _instance;

		private IPEndPoint _remoteEndPoint;
		private UdpClient _udpClient;
		private bool _commOk;
		private bool _extended;

		private Dictionary<KeyValuePair<uint, uint>, bool> _debugState;

		//----------------------------

		private string ByteArrayToHexString(byte[] data)
		{
			string delimiter = " ";
			string hex = BitConverter.ToString(data);
			return hex.Replace("-", delimiter);
		}

		//----------------------------

		static WSDbgEnvInterface()
		{
			_instance = new WSDbgEnvInterface();
		}

		//----------------------------

		private WSDbgEnvInterface()
		{
			_commOk = false;
			_extended = false;
			_udpClient = new UdpClient(AddressFamily.InterNetwork);
			_debugState = new Dictionary<KeyValuePair<uint, uint>, bool>();
		}

		//----------------------------

		static public WSDbgEnvInterface Instance()
		{
			return _instance;
		}

		//----------------------------

		private void ParseReceivedData(byte[] data)
		{
			var msg = new WSDbgEnvProtocol.MsgHeader(data);

//			Console.WriteLine("CommInterface::ParseReceivedData: {0}", msg.Opcode);

			switch ((WSDbgEnvOpcodes) msg.Opcode)
			{
			case WSDbgEnvOpcodes.SendKeepAliveEx:
//				Console.WriteLine("Received SendKeepAliveEX");
				WSDbgEnvProtocol.SetNewProtocol(true);
				_extended = true;
				_commOk = true;
				NotifyError();
				break;

			case WSDbgEnvOpcodes.SendKeepAlive:
//				Console.WriteLine("Received SendKeepAliveEX");
				_commOk = true;
				NotifyError();
				break;

			case WSDbgEnvOpcodes.DebugState:
				break;

			case WSDbgEnvOpcodes.SingleMsg:
//				Console.WriteLine("Received SingleMsg");
				ParseSingleDataMsg(data, _extended);
				break;

			case WSDbgEnvOpcodes.SingleMsgEx:
//				Console.WriteLine("Received SingleMsgEx");
				ParseSingleDataMsg(data, _extended);
				break;
			}
		}

		//----------------------------

		private void ParseSingleDataMsg(byte[] data, bool ex)
		{
//			Console.WriteLine("ParseSingleDataMsg: data = {0}", ByteArrayToHexString(data));

			WSDbgEnvMsg wsMsg = null;
			if (ex)
			{
				var respMsg = new WSDbgEnvProtocol.RespMsg<UInt32>(data);
				wsMsg = new WSDbgEnvMsg(respMsg.Index.DB, respMsg.Index.Entry, respMsg.Data);
			}
			else
			{
				var respMsg = new WSDbgEnvProtocol.RespMsg<byte>(data);
				wsMsg = new WSDbgEnvMsg(respMsg.Index.DB, respMsg.Index.Entry, respMsg.Data);
			}

//			Console.WriteLine("Notify WSMsg({0}, {1}, {2}, {3})", wsMsg.DBIndex, wsMsg.DBEntryIndex, wsMsg.DataSize, ByteArrayToHexString(wsMsg.Data));

			NotifyData(wsMsg);
		}

		//----------------------------

		private void SendKeepAlive(IAsyncResult result)
		{
			_udpClient.EndSend(result);
			Thread.Sleep(100);
			var msg = WSDbgEnvProtocol.BuildGetKeepAliveMsg();
			//Console.WriteLine("CommInterface::SendKeepAlive: {0} - {1}", ByteArrayToHexString(msg), msg.Length);
			_udpClient.BeginSend(msg, msg.Length, new AsyncCallback(SendKeepAlive), null);
		}

		//----------------------------

		private void ReceiveData(IAsyncResult result)
		{
			var endPoint = new IPEndPoint(IPAddress.Any, 0);
			var data = _udpClient.EndReceive(result, ref endPoint);

			if (endPoint.Address.Equals(_remoteEndPoint.Address))
				ParseReceivedData(data);

			_udpClient.BeginReceive(new AsyncCallback(ReceiveData), null);
		}

		//----------------------------

		public void SetupConnection(int localPort, string remoteAddress, int remotePort)
		{
			var local = new IPEndPoint(IPAddress.Any, localPort);
			var remote = new IPEndPoint(IPAddress.Parse(remoteAddress), remotePort);
			SetupConnection(local, remote);
		}

		//----------------------------

		public void SetupConnection(IPEndPoint local, IPEndPoint remote)
		{
			_udpClient.Client.Bind(local);
			_udpClient.Connect(remote);

			_remoteEndPoint = remote;

			_udpClient.BeginReceive(new AsyncCallback(ReceiveData), null);

			var msg = WSDbgEnvProtocol.BuildGetKeepAliveMsg();
			_udpClient.BeginSend(msg, msg.Length, new AsyncCallback(SendKeepAlive), null);
		}

		//----------------------------

		public void RegisterEntry(uint dbIndex, uint entryIndex)
		{
			SendMsg(WSDbgEnvProtocol.BuildRegisterEntryMsg(dbIndex, entryIndex));
		}
		
		//----------------------------

		public void UnregisterEntry(uint dbIndex, uint entryIndex)
		{
			SendMsg(WSDbgEnvProtocol.BuildUnregisterEntryMsg(dbIndex, entryIndex));
		}
		
		//----------------------------

		public void GetData(uint dbIndex, uint entryIndex)
		{
			SendMsg(WSDbgEnvProtocol.BuildGetDataMsg(dbIndex, entryIndex));
		}
		
		//----------------------------

		public void SetData(uint dbIndex, uint entryIndex, byte[] data, int size)
		{
			SendMsg(WSDbgEnvProtocol.BuildSetDataMsg(dbIndex, entryIndex, data, size));
		}
		
		//----------------------------

		public void SetDebugState(uint dbIndex, uint entryIndex, bool state)
		{
			if (UpdateDebugState(dbIndex, entryIndex, state))
				SendMsg(WSDbgEnvProtocol.BuildUpdateDebugStateMsg(dbIndex, entryIndex));
		}

		//----------------------------

		public bool IsCommOk()
		{
			return _commOk;
		}

		//----------------------------

		private void SendMsg(byte[] msg)
		{
			_udpClient.Send(msg, msg.Length);
		}

		//----------------------------

		private bool UpdateDebugState(uint db, uint entry, bool state)
		{
			var key = new KeyValuePair<uint, uint>(db, entry);

			if (!_debugState.ContainsKey(key))
			{
				_debugState.Add(key, state);
				return state;
			}
			else if (_debugState[key] != state)
			{
				_debugState[key] = state;
				return true;
			}

			return false;
		}
	}
}

//----------------------------
