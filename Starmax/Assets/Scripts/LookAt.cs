﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour 
{
	public Transform lookingObject;
	public Transform lookAtObject;

	void Update () 
	{
		if (lookingObject && lookAtObject)
			lookingObject.transform.LookAt(lookAtObject);
	}
}
