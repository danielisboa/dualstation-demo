﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Infrastructure;

public class ChangeMaterial : MonoBehaviour
{
	public GameObject ObjectToChange;
	public Material[] Materials;
	public bool Recursive;
	Renderer _renderer;

	uint _selection;

	//------------------------------

	void Start()
	{
		Log.Disabled(ObjectToChange.name);
		if (ObjectToChange == null) ObjectToChange = this.gameObject;
		_renderer = ObjectToChange.GetComponent<Renderer>();
	}

	//------------------------------

	public void SelectMaterial(uint selection)
	{
		if (_selection == selection) return;
		_selection = selection;

		Change(_renderer);
		if (!Recursive) return;
		ChangeChilds(ObjectToChange.transform);
	}

	//------------------------------

	void ChangeChilds(Transform t)
	{
		var tc = t.childCount;
		for (var i = 0; i < tc; i++)
		{
			var c = t.GetChild(i);
			ChangeChilds(c);
			Change(c);
		}
	}

	//------------------------------

	bool Change(Transform t)
	{
		var ret = Change(t.gameObject.GetComponent<Renderer>());
		if (ret) Log.Disabled("Changed material for object " + t.gameObject.name);
		return ret;
	}

	//------------------------------

	bool Change(Renderer renderer)
	{
		if (!renderer) return false;
		renderer.enabled = true;
		_selection = (uint) Mathf.Clamp(_selection, 0, Materials.Length - 1);
		renderer.sharedMaterial = Materials[_selection];
		return true;
	}
}
