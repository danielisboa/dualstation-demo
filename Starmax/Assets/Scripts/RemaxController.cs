﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Infrastructure;
using WSCommunication;

public class RemaxController : MonoBehaviour 
{
	public GameObject TurretTraverse;
	public GameObject TurretElevation;
	public GameObject Optronics;
	public GameObject Weapon;

	OptronicsController _optronicsController;
	PowerStabAxisController _traverseController;
	PowerStabAxisController _elevationController;
	WeaponShooter _weaponShooter;

	private RemaxSimulation weaponStation;
	private float aimPosX;
	private float aimPosY;

	private bool _bLaser;
	private bool _bFire;

	private uint _nRounds;

	bool debug = false;

	//------------------------------

	void Awake()
	{
	}

	//------------------------------

	void Start()
	{
		weaponStation = new RemaxSimulation();
		weaponStation.SetupConnection(2223, "192.168.1.4", 2223);

		_optronicsController = Optronics.GetComponent<OptronicsController>();
		_elevationController = TurretElevation.GetComponent<PowerStabAxisController>();
		_traverseController = TurretTraverse.GetComponent<PowerStabAxisController>();
		_weaponShooter = Weapon.GetComponent<WeaponShooter> (); 

		if (_weaponShooter)
		{
			_nRounds = _weaponShooter.Rounds;
			weaponStation.UpdateAmmunitionCount((uint) _nRounds);
		}

		weaponStation.SetEncoderSimulation(true);

		_bLaser = false;
		_bFire = false;
	}
		
	//------------------------------

	void FixedUpdate()
	{
		if (_weaponShooter != null)
		{
			var nRounds = _weaponShooter.Rounds;

//			if (nRounds < _nRounds)
//				weaponStation.FireFeedbackPulse();

			if (nRounds != _nRounds)
				weaponStation.UpdateAmmunitionCount((uint) nRounds);

			_nRounds = nRounds;
		}
	}

	//------------------------------

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.F))
			Screen.fullScreen = !Screen.fullScreen; 

		if (_elevationController && _traverseController)
		{
			HandleEnconders();	
			HandleOperationalMode();

			var trvEncVal = _traverseController.EncoderValue;
			var elvEncVal = _elevationController.EncoderValue;
			if (elvEncVal > 180) elvEncVal -= 360;
			//Debug.Log($"UpdateEncoderValues({trvEncVal}, {-elvEncVal})");
			//weaponStation.UpdateEncoderValues(trvEncVal, -elvEncVal);
		}

		if (_optronicsController)
		{
			HandleAimingPosition();
			HandleLaserFireCmd();
			HandleActiveCamera();
			HandleActiveCameraFov();
			HandleFocusMode();
			HandleFocusCmd ();
		}

		if (_weaponShooter) 
		{
			HandleFire();
			HandleFireMode();
			HandleRange();
		}
	}

	//------------------------------

	void HandleEnconders()
	{
		float trv = 0, elv = 0;
		weaponStation.GetEncodersValues (out trv, out elv);

		if (debug) 
			Debug.Log ("trv: " + trv + "Elv: " + elv);
		
		_traverseController.ControlAngle = trv;
		_elevationController.ControlAngle = elv;
	}

	//------------------------------

	void HandleActiveCamera()
	{
		uint activeCamera = weaponStation.GetActiveCamera ();

		if (activeCamera == (uint)WSSimProtocol.CameraTypes.Thermal) 
			_optronicsController.ActiveCamera = OptronicsController.CamerasEnum.Thermal;
		else
			_optronicsController.ActiveCamera = OptronicsController.CamerasEnum.Day;
	}

	//------------------------------

	void HandleActiveCameraFov()
	{
		//var vFOV = weaponStation.GetCameraVFOV();
		//_optronicsController.SetActiveFieldOfView(vFOV);
	}

	//------------------------------

	void HandleFocusMode()
	{
		FocusModeEnum eFocusMode = (FocusModeEnum)weaponStation.GetFocusMode ();
		_optronicsController.SetFocusMode (eFocusMode);
	}

	//------------------------------

	void HandleFire()
	{
		bool bFire = (weaponStation.GetFireCmd () ? true : false);

		if (_bFire != bFire)
		{
			_weaponShooter.Fire = bFire;
			_bFire = bFire;
		}
	}

	//------------------------------

	void HandleAimingPosition()
	{
		weaponStation.GetAimingValues (out aimPosX, out aimPosY);

		aimPosX *= Screen.width / 700f;
		aimPosY *= Screen.height / 578f;

		_optronicsController.SetAimingPostion ((int)aimPosX, (int)aimPosY);
	}

	//------------------------------

	void HandleFocusCmd()
	{
		FocusCmdEnum nFocusCmd = (FocusCmdEnum)weaponStation.GetFocusCmd ();
		_optronicsController.FocusCommand = nFocusCmd;
		if (debug) Debug.Log ("Focus Cmd: " + nFocusCmd);
	}

	//------------------------------

	void HandleLaserFireCmd()
	{
		bool bLaser = weaponStation.GetMeasureRangeCmd ();

		if (_bLaser != bLaser)
		{
			_optronicsController.MeasureRange = bLaser;
			weaponStation.UpdateMeasuredDistance(_optronicsController.MeasuredRange, 1);
			_bLaser = bLaser;
		}
	}

	//------------------------------

	void HandleOperationalMode()
	{
		var nOpMode = weaponStation.GetOpMode();
		var bStab = (nOpMode == (uint) WSSimProtocol.OperationModes.Stab);

		_elevationController.Stabilization = bStab;
		_traverseController.Stabilization = bStab;		
	}

	//------------------------------

	void HandleFireMode()
	{
		int burstLength = weaponStation.GetBurstLength();
		_weaponShooter.BurstLenght = (uint)burstLength;

	}

	//------------------------------

	void HandleRange()
	{
		_weaponShooter.ShootingRange = weaponStation.GetRange();
	}

	//------------------------------
}
