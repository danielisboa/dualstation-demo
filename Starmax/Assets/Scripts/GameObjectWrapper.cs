﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;

public class GameObjectWrapper : IGameObject
{
	GameObject _gameObject;

	public GameObjectWrapper(GameObject gameObject)
	{
		_gameObject = gameObject;
	}

	public object GetGameObject()
	{
		return _gameObject;
	}
}
