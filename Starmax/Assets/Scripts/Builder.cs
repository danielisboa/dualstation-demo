﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Infrastructure;
using Managers;
using Common;
using Utils;

public class Builder : MonoBehaviour
{
	public GameObject simulatedWeaponStation;
	public GameObject simulatedGUI;

	SceneManager _sceneManager;

	GameObject _sceneModel;

	WSCommunicationManager _wsCommunicationManager;
	GuiCommunicationManager _guiCommunicationManager;
	NetworkStatus _networkStatus;

	//------------------------------

	void Awake()
	{
		UnityUtils.DestroyMeIfDuplicate(this.gameObject);

		Log.SetConsolePrinter(new UnityConsolePrinter());
		Log.SetLogLevel(Log.Level.Event);
		// Log.EnableLevel(Log.Level.Logging);
		// Log.EnableLevel(Log.Level.Info);
		// Log.EnableLevel(Log.Level.Debug);
		// Log.EnableLevel(Log.Level.Trace);
		// Log.EnableLevel(Log.Level.Trash);
		// Log.EnableLevel(Log.Level.Periodic);
		// Log.EnableLevel(Log.Level.Focused, true);
		Log.Trace("Builder.Awake()");

		DontDestroyOnLoad(transform.gameObject);

		if (simulatedWeaponStation != null)
			DontDestroyOnLoad(simulatedWeaponStation.gameObject);

		UnityUtils.sceneLoaded += this.SceneLoaded;
	}

	//------------------------------

	void OnApplicationQuit()
	{
		Log.Trace("Builder.OnApplicationQuit()");
		Log.Info("Builder.OnApplicationQuit(): Closing communication channels");

		if (_sceneManager != null)
		{
			_sceneManager.StopScene();
			_sceneManager = null;
		}

		if (_guiCommunicationManager != null)
		{
			_guiCommunicationManager.Disconnect();
			_guiCommunicationManager = null;
		}

		if (_wsCommunicationManager != null)
		{
			_wsCommunicationManager.Disconnect();
			_wsCommunicationManager = null;
		}
	}

	//------------------------------

	void SceneLoaded()
	{
		Log.Trace("Builder.SceneLoaded()");

		if (_sceneManager != null)
			_sceneManager.SetSceneCalibrator(null);

		GetSceneModel();

		if (simulatedWeaponStation != null)
		{
			var simWS = simulatedWeaponStation.GetComponent<SimulatedWeaponStation>();
			if (simWS != null) simWS.Force = true;
		}
	}

	//------------------------------

	void SendDefaultWsConfig()
	{
		if (_wsCommunicationManager != null)
		{
			_wsCommunicationManager.UpdateFovValues(Values.WideFov, Values.WideFovDayZoomLevel, (sbyte) FieldOfView.Wide, false);
			_wsCommunicationManager.SetEncoderSimulation(false);
		}
	}

	//------------------------------

	void GetSceneModel()
	{
		Log.Trace("Builder.GetSceneModel()");

		if (_sceneManager == null)
			return;

		_sceneModel = GameObject.Find("SceneModel");

		if (_sceneModel != null)
			_sceneManager.SetSceneModel(_sceneModel.GetComponent<SceneModel>());
		else
			SendDefaultWsConfig();

		var calibrator = GameObject.Find("Calibrator");

		if (calibrator != null)
		{
			var calibratorScript = calibrator.GetComponent<Calibrator>();
			if (calibratorScript) Log.Info("Builder.GetSceneModel(): Found Calibrator");
			_sceneManager.SetSceneCalibrator(calibratorScript);
		}
	}

	//------------------------------

	void Start()
	{
		Log.Trace("Builder.Start()");

		_wsCommunicationManager = new WSCommunicationManager();
		_guiCommunicationManager = new GuiCommunicationManager();

		_networkStatus = new NetworkStatus();

		_wsCommunicationManager.communicationStatusChanged += _networkStatus.ReceiveWsCommunicationStatus;
		_guiCommunicationManager.communicationStatusChanged += _networkStatus.ReceiveGuiCommunicationStatus;
		_guiCommunicationManager.communicationStatusChanged += _wsCommunicationManager.Enable;

		_networkStatus.communicationStatusChanged += _guiCommunicationManager.SendCommunicationStatus;

		_sceneManager = new SceneManager(_wsCommunicationManager, _guiCommunicationManager);
		_networkStatus.communicationStatusChanged += _sceneManager.ReceiveCommunicationStatus;

		_networkStatus.NotifyCommunicationStatus();

		var sceneInterface = gameObject.GetComponent<SceneInterface>();

		if (_sceneManager != null && sceneInterface != null)
			_sceneManager.SetSceneInterface(sceneInterface);

		if (simulatedWeaponStation != null)
		{
			var simWS = simulatedWeaponStation.GetComponent<SimulatedWeaponStation>();
			if (simWS != null) simWS.SetWsCommunicationManager(_wsCommunicationManager);
		}

		if (simulatedGUI != null)
		{
			var simGUI = simulatedGUI.GetComponent<SimulatedGUI>();
			if (simGUI != null) simGUI.SetGuiCommunicationManager(_guiCommunicationManager);
		}

		GetSceneModel();
	}
}
