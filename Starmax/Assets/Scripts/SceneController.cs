﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
	public GameObject TurretTraverse;
	public GameObject TurretElevation;
	public GameObject Optronics;
	public GameObject Weapon;

	[Range(.1f,1f)]
	public float TurretControlGain;

	const float TurretAngularSpeed = 20;

	OptronicsController _optronicsController;
	PowerStabAxisController _traverseController;
	PowerStabAxisController _elevationController;
	WeaponShooter _weaponController;

	bool _zoomCmd;
	bool _focusCmd;
	float _lastKeyPressTime;

	// Use this for initialization
	void Start()
	{
		_zoomCmd = false;
		_focusCmd = false;

		if (TurretControlGain == 0)
			TurretControlGain = 0.3f;

		_optronicsController = Optronics.GetComponent<OptronicsController>();
		_elevationController = TurretElevation.GetComponent<PowerStabAxisController>();
		_traverseController = TurretTraverse.GetComponent<PowerStabAxisController>();
		_weaponController = Weapon.GetComponent<WeaponShooter>();

		_lastKeyPressTime = 0;
	}
	
	// Update is called once per frame
	void Update()
	{
		ContinuousKeyPressHandler();
		DiscreteKeyPressHandler();
	}

	void ContinuousKeyPressHandler()
	{
		float angularStep = TurretControlGain * TurretAngularSpeed * Time.deltaTime;

		if (Input.GetKey(KeyCode.Keypad8))
			_elevationController.ControlAngle += angularStep;

		if (Input.GetKey(KeyCode.Keypad5))
			_elevationController.ControlAngle -= angularStep;

		if (Input.GetKey(KeyCode.Keypad6))
			_traverseController.ControlAngle += angularStep;

		if (Input.GetKey(KeyCode.Keypad4))
			_traverseController.ControlAngle -= angularStep;

		_weaponController.Fire = Input.GetKey(KeyCode.LeftControl);
	}

	void DiscreteKeyPressHandler()
	{
		if ((Time.time - _lastKeyPressTime) < 0.5)
			return;

		_lastKeyPressTime = (Input.anyKeyDown) ? Time.time : 0;

		KeyPressReleaseHandler();
		KeyPressOnlyHandler();
	}

	void KeyPressReleaseHandler()
	{
		if (Input.GetKey(KeyCode.PageUp))
		{
			_optronicsController.ZoomCommand = ZoomCmdEnum.ZoomIn;
			_zoomCmd = true;
		}
		else if (Input.GetKey(KeyCode.PageDown))
		{
			_optronicsController.ZoomCommand = ZoomCmdEnum.ZoomOut;
			_zoomCmd = true;
		}
		else if (_zoomCmd)
		{
			_optronicsController.ZoomCommand = ZoomCmdEnum.ZoomStop;
			_zoomCmd = false;
		}

		if (Input.GetKey(KeyCode.KeypadPlus))
		{
			_optronicsController.FocusCommand = FocusCmdEnum.FocusFar;
			_focusCmd = true;
		}
		else if (Input.GetKey(KeyCode.KeypadPeriod))
		{
			_optronicsController.FocusCommand = FocusCmdEnum.FocusNear;
			_focusCmd = true;
		}
		else if (_focusCmd)
		{
			_optronicsController.FocusCommand = FocusCmdEnum.FocusStop;
			_focusCmd = false;
		}
	}

	void KeyPressOnlyHandler()
	{
		if (!Input.anyKeyDown)
			return;

//		if (Input.GetKeyDown(KeyCode.Space))
//			_weaponController.Fire = true;
//		else if (Input.GetKeyUp(KeyCode.Space))
//			_weaponController.Fire = false;

		if (Input.GetKey(KeyCode.Insert))
		{
			_elevationController.Stabilization = !_elevationController.Stabilization;
			_traverseController.Stabilization = !_traverseController.Stabilization;
		}

		var ActiveCamera = _optronicsController.ActiveCamera;

		if (Input.GetKey(KeyCode.Delete))
		{
			if (ActiveCamera == OptronicsController.CamerasEnum.Day)
				_optronicsController.ActiveCamera = OptronicsController.CamerasEnum.Thermal;
			else
				_optronicsController.ActiveCamera = OptronicsController.CamerasEnum.Day;
		}

		var FocusMode = _optronicsController.FocusMode;

		if (Input.GetKey(KeyCode.Keypad0))
		{
			if (FocusMode == FocusModeEnum.Auto)
				_optronicsController.FocusMode = FocusModeEnum.Manual;
			else
				_optronicsController.FocusMode = FocusModeEnum.Auto;
		}

		var ActiveFieldOfView = _optronicsController.ActiveFieldOfView;

		if (Input.GetKey(KeyCode.Home))
		{
			if (ActiveFieldOfView == FieldOfViewEnum.Narrow)
				_optronicsController.ActiveFieldOfView = FieldOfViewEnum.VeryWide;
			else if (ActiveFieldOfView == FieldOfViewEnum.VeryWide)
				_optronicsController.ActiveFieldOfView = FieldOfViewEnum.Wide;
			else if (ActiveFieldOfView == FieldOfViewEnum.Wide)
				_optronicsController.ActiveFieldOfView = FieldOfViewEnum.Narrow;
		}

		if (Input.GetKey(KeyCode.End))
		{
			if (ActiveFieldOfView == FieldOfViewEnum.Narrow)
				_optronicsController.ActiveFieldOfView = FieldOfViewEnum.Wide;
			else if (ActiveFieldOfView == FieldOfViewEnum.Wide)
				_optronicsController.ActiveFieldOfView = FieldOfViewEnum.VeryWide;
			else if (ActiveFieldOfView == FieldOfViewEnum.VeryWide)
				_optronicsController.ActiveFieldOfView = FieldOfViewEnum.Narrow;
		}

		if (Input.GetKey(KeyCode.KeypadEnter))
			_optronicsController.MeasureRange = true;

		if (Input.GetKey(KeyCode.KeypadDivide))
			_optronicsController.ActiveFieldOfView = FieldOfViewEnum.Narrow;

		if (Input.GetKey(KeyCode.KeypadMultiply))
			_optronicsController.ActiveFieldOfView = FieldOfViewEnum.Wide;

		if (Input.GetKey(KeyCode.KeypadMinus))
			_optronicsController.ActiveFieldOfView = FieldOfViewEnum.VeryWide;
	}
}
