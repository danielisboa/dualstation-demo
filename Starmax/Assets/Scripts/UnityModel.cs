﻿using System;
using UnityEngine;
using Models;
using Common;

public abstract class UnityModel : MonoBehaviour, IModel
{
	UpdateEventDelegate _preUpdateCallbacks;
	UpdateEventDelegate _postUpdateCallbacks;

	public UnityModel()
	{
	}

	public void AddPreUpdateCallback(UpdateEventDelegate callback)
	{
		if (callback != null)
			_preUpdateCallbacks += callback;
	}

	public void AddPostUpdateCallback(UpdateEventDelegate callback)
	{
		if (callback != null)
			_postUpdateCallbacks += callback;
	}

	protected void PerformUpdate()
	{
		if (_preUpdateCallbacks != null)
			_preUpdateCallbacks();

		ModelUpdate();

		if (_postUpdateCallbacks != null)
			_postUpdateCallbacks();
	}

	public float GetTime()
	{
		return Time.time;
	}

	public abstract float GetDeltaTime();

	public abstract void ModelUpdate();
}

//---------------------------------------------

public abstract class Model : UnityModel
{
	public void Update()
	{
		PerformUpdate();
	}

	public override float GetDeltaTime()
	{
		return Time.deltaTime;
	}
}

//---------------------------------------------

public abstract class PhysicsModel : UnityModel
{
	public void FixedUpdate()
	{
		PerformUpdate();
	}

	public override float GetDeltaTime()
	{
		return Time.fixedDeltaTime;
	}
}

