﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;

public class SceneModel : MonoBehaviour, ISceneModel
{
	public GameObject turretModel;
	public GameObject environmentModel;
	public GameObject vehicleModel;
	public GameObject pantographModel;
	public GameObject[] sceneSectorModel;

	//------------------------------

	void Awake()
	{
		var builder = GameObject.Find("Builder");
		if (builder == null || !builder.activeSelf)
			UnityUtils.LoadScene("Launcher");
	}

	//------------------------------

	ModelType GetModel<ModelType>(GameObject model)
	{
		return model.GetComponent<ModelType>();
	}

	//------------------------------

	public ITurretModel GetTurretModel()
	{
		return GetModel<TurretModel>(turretModel);
	}

	//------------------------------

	public IVehicleModel GetVehicleModel()
	{
		return GetModel<VehicleModel>(vehicleModel);
	}

	//------------------------------

	public ISceneSectorModel GetSceneSectorModel(uint sectorNumber)
	{
		Log.Debug ("SceneModel.GetSceneSectorModel() SectorNumber: " + sectorNumber + " - SectorLength: " + sceneSectorModel.Length);
		if (sceneSectorModel.Length <= --sectorNumber) return null;
		for (int i = 0; i < sceneSectorModel.Length; i++)
			if (i != sectorNumber) sceneSectorModel[i].SetActive(false);
		sceneSectorModel[sectorNumber].SetActive(true);
		return GetModel<SceneSectorModel>(sceneSectorModel[sectorNumber]);
	}

	//------------------------------

	public IModel GetEnvironmentModel()
	{
		EnvironmentModel model = GetModel<EnvironmentModelNaval>(environmentModel);

		if (model == null)
			model = GetModel<EnvironmentModel>(environmentModel);

		return model;
	}

	//------------------------------

	public IModel GetPantographModel()
	{
		return GetModel<PantographModel>(pantographModel);
	}
}
