﻿using System;

[Serializable]
public struct ConfigStruct
{
	public int nTrainingType;
	public int nSceneIndex;
	public bool bLoadScene;
	public int nTotalTime;
	public int nTargetSpeed;
	public int nFireTime;
	public int nFireDuration;
	public int nStartStop;
	public bool bLoadTargetSpeed;
	public bool bLoadBasicConfig;
	//public bool bFireTime;
}

