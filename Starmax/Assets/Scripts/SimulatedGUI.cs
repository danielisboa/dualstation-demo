﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Managers;
using Common;

using GuiCommunication;

public class SimulatedGUI : MonoBehaviour
{
	public Common.TrainingType trainingType;

	public Common.TrainingControl trainingControl;

	[System.Serializable]
	public struct FireTrainingConfig
	{
		[Range(0,600)]
		public float trainingDuration;

		[Range(1,4)]
		public byte targetSector;
		[Range(0,100)]
		public uint ammunitionCount;
		public bool reloadAmmoSafe;

		[Range(0,100)]
		public float daylightIntensity;
		[Range(0,100)]
		public float fogIntensity;
		[Range(0,100)]
		public float rainIntensity;
		[Range(-99,99)]
		public float windSpeed;

		[Range(0,100)]
		public uint numberOfTargets;
		[Range(0,20)]
		public uint hitsToKillTargets;
		public bool popupTargets;
		[Range(0,60)]
		public float popupInterval;
		[Range(0,10)]
		public uint simultaneousTargets;

		[Range(0,70)]
		public float vehicleVelocity;

		public bool LrfEnabled;
	}

	public FireTrainingConfig fireTrainingConfig;

	[System.Serializable]
	public struct PantographTrainingConfig
	{
		public PantographPatterns pattern;
		[Range(0,300)]
		public float trainingDuration;
		[Range(1,3)]
		public byte calibratedSpeed;
		[Range(0,60)]
		public float laserResquestInterval;
		[Range(0,60)]
		public float fireResquestInterval;
		[Range(0,60)]
		public float requestDuration;
	}

	public PantographTrainingConfig pantographTrainingConfig;

	GuiSimProtocol.GuiTrainingSelection _trainingSelection;
	GuiSimProtocol.GuiFireTrainingConfig _fireTrainingConfig;
	GuiSimProtocol.GuiPantographTrainingConfig _pantographTrainingConfig;
	GuiSimProtocol.GuiTrainingControl _trainingControl;

	GuiCommunicationManager _guiCommManager;

	//------------------------------

	GuiSimProtocol.GuiTrainingSelection GetTrainingSelection()
	{
		GuiSimProtocol.GuiTrainingSelection selection;
		selection.TrainingType = (byte) trainingType;

		switch (trainingType)
		{
			case TrainingType.Pantograph:
				selection.TrainingSelection = (byte) pantographTrainingConfig.pattern;
				break;

			default:
				selection.TrainingSelection = fireTrainingConfig.targetSector;
				break;
		}

		return selection;
	}

	//------------------------------

	GuiSimProtocol.GuiTrainingControl GetTrainingControl()
	{
		GuiSimProtocol.GuiTrainingControl control;
		control.TrainingControl = (byte) trainingControl;
		return control;
	}

	//------------------------------

	GuiSimProtocol.GuiFireTrainingConfig GetFireTrainingConfig()
	{
		GuiSimProtocol.GuiFireTrainingConfig config;
		config.TrainingDuration = fireTrainingConfig.trainingDuration;
		config.NumberOfTargets = (ushort) fireTrainingConfig.numberOfTargets;
		config.HitsToKillTargets = (byte) fireTrainingConfig.hitsToKillTargets;
		config.PopupTargets = (byte) (fireTrainingConfig.popupTargets ? 1 : 0);
		config.PopupInterval = fireTrainingConfig.popupInterval;
		config.SimultaneousTargets = (ushort) fireTrainingConfig.simultaneousTargets;
		config.AmmunitionCount = (ushort) fireTrainingConfig.ammunitionCount;
		config.ReloadAmmoSafe = (byte) (fireTrainingConfig.reloadAmmoSafe ? 1 : 0);
		config.DaylightIntensity = (byte) fireTrainingConfig.daylightIntensity;
		config.FogIntensity = (byte) fireTrainingConfig.fogIntensity;
		config.RainIntensity = (byte) fireTrainingConfig.rainIntensity;
		config.WindSpeed = (sbyte) fireTrainingConfig.windSpeed;
		config.VehicleSpeed = fireTrainingConfig.vehicleVelocity;
		config.LrfEnabled = fireTrainingConfig.LrfEnabled;
		config.FireJamming = 0;
		return config;
	}

	//------------------------------

	GuiSimProtocol.GuiPantographTrainingConfig GetPantographTrainingConfig()
	{
		GuiSimProtocol.GuiPantographTrainingConfig config;

		config.TrainingDuration = pantographTrainingConfig.trainingDuration;
		config.LaserReqInterval = pantographTrainingConfig.laserResquestInterval;
		config.FireReqInterval = pantographTrainingConfig.fireResquestInterval;
		config.RequestDuration = pantographTrainingConfig.requestDuration;
		config.CalibratedTargetSpeed = pantographTrainingConfig.calibratedSpeed;

		return config;
	}

	//------------------------------

	void Awake()
	{
		UnityUtils.DestroyMeIfDuplicate(this.gameObject);
		DontDestroyOnLoad(transform.gameObject);
	}

	//------------------------------

	void Update()
	{
		if (_guiCommManager == null)
			return;
		
		var trainingSelection = GetTrainingSelection();

		if (!trainingSelection.Equals(_trainingSelection))
		{
			Log.Debug("SimulatedGUI.Update(): trainingSelection.TrainingType = " + trainingSelection.TrainingType);
			_trainingSelection = trainingSelection;
			if (trainingSelection.TrainingType != (byte) Common.TrainingType.NoType)
				_guiCommManager.SetPhonyGuiTrainingSelection(trainingSelection);
		}

		var fireTrainingConfig = GetFireTrainingConfig();

		if (!fireTrainingConfig.Equals(_fireTrainingConfig))
		{
			Log.Debug("SimulatedGUI.Update(): fireTrainingConfig.NumberOfTargets = " + fireTrainingConfig.NumberOfTargets);
			_fireTrainingConfig = fireTrainingConfig;
			_guiCommManager.SetPhonyGuiFireTrainingConfig(fireTrainingConfig);
		}

		var pantographTrainingConfig = GetPantographTrainingConfig();

		if (!pantographTrainingConfig.Equals(_pantographTrainingConfig))
		{
			Log.Debug("SimulatedGUI.Update(): pantographTrainingConfig.CalibratedTargetSpeed = " + pantographTrainingConfig.CalibratedTargetSpeed);
			_pantographTrainingConfig = pantographTrainingConfig;
			_guiCommManager.SetPhonyGuiPantographTrainingConfig(pantographTrainingConfig);
		}

		var trainingControl = GetTrainingControl();

		if (!trainingControl.Equals(_trainingControl))
		{
			Log.Debug("SimulatedGUI.Update(): trainingControl.TrainingControl = " + trainingControl.TrainingControl);
			_trainingControl = trainingControl;
			_guiCommManager.SetPhonyGuiTrainingControl(trainingControl);
		}
	}

	//------------------------------

	public void SetGuiCommunicationManager(GuiCommunicationManager guiCommManager)
	{
		_guiCommManager = guiCommManager;
	}
}
