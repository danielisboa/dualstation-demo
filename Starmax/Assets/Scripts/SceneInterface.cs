﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using Common;

public class SceneInterface : MonoBehaviour, Utils.ISceneInterface
{
//	#region privateAttributes

	UpdateEventDelegate _levelLoaded;

	Common.TrainingType _trainingType;
	string _sceneToLoad;
	bool _pause;
	float _time;
	float _fixedDeltaTime;

//	#endregion

	//------------------------------

//	#region publicMethods

	public void LoadTrainingScene(Common.TrainingType trainingType)
	{
		Log.Debug("SceneInterface.LoadTrainingScene(" + trainingType + ")");

		switch (trainingType)
		{
			case Common.TrainingType.Pantograph:
				_sceneToLoad = "Pantograph";
				break;

			case Common.TrainingType.OperationalStatic:
				_sceneToLoad = "OperationalStatic";
				break;

			case Common.TrainingType.OperationalMobile:
				//_sceneToLoad = "OperationalMobile";
				_sceneToLoad = "VideoSimulator";
				break;

			case Common.TrainingType.Calibrations:
				_sceneToLoad = "Calibrations";
				break;

			case Common.TrainingType.Naval:
				_sceneToLoad = "Naval";
				break;

			case Common.TrainingType.Boresight:
				_sceneToLoad = "Boresight";
				break;

			default:
				_sceneToLoad = "Launcher";
				break;
		}

		_trainingType = trainingType;
	}

	//------------------------------

	public void SetErrorMessage(string message)
	{
	}

	//------------------------------

	public void SceneLoaded()
	{
		Log.Trace("SceneInterface.SceneLoaded()");
		Log.Info("SceneInterface.SceneLoaded(): Scene " + SceneManager.GetActiveScene().name + " loaded");
		if (_levelLoaded != null && _trainingType != Common.TrainingType.NoType)
			_levelLoaded();
	}

	//------------------------------

	public void AddLevelLoadedCallback(UpdateEventDelegate callback)
	{
		_levelLoaded += callback;
	}

	//------------------------------

	public void PauseScene(bool pause)
	{
		Log.Info("SceneInterface.PauseScene(): pause = " + pause);
		_pause = pause;
	}

	//------------------------------

	public bool IsScenePaused()
	{
		return _pause;
	}

	//------------------------------

	public float GetRunningTime()
	{
		return _time;
	}

//	#endregion

	//------------------------------

//	#region privateMethods

	//------------------------------

	void Start()
	{
		_trainingType = TrainingType.NoType;

		_fixedDeltaTime = Time.fixedDeltaTime;

		if (Time.timeScale > 0)
			_fixedDeltaTime /= Time.timeScale;

		UnityUtils.sceneLoaded += this.SceneLoaded;
	}

	//------------------------------

	void Update()
	{
		if (_sceneToLoad != null && _sceneToLoad != "")
		{
			Log.Info("SceneInterface.Update(): Loading scene " + _sceneToLoad);
			SceneManager.LoadScene(_sceneToLoad);
			_sceneToLoad = null;
		}

		Time.timeScale = (_pause) ? 0 : 1;
		Time.fixedDeltaTime = _fixedDeltaTime * Time.timeScale;

		_time = Time.time;
	}

//	#endregion
}
