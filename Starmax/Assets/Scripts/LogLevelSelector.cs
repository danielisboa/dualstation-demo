﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogLevelSelector : MonoBehaviour
{
	new bool enabled;

	bool l_pressed;

	void Awake()
	{
		UnityUtils.DestroyMeIfDuplicate(gameObject);
		DontDestroyOnLoad(transform.gameObject);
	}

	// Update is called once per frame
	void Update ()
	{
		var lc = Input.GetKey(KeyCode.LeftControl);
		var ls = Input.GetKey(KeyCode.LeftShift);
		var l  = Input.GetKey(KeyCode.L);

		var f  = Input.GetKey(KeyCode.F);
		var t  = Input.GetKey(KeyCode.T);
		var r  = Input.GetKey(KeyCode.R);
		var d  = Input.GetKey(KeyCode.D);
		var i  = Input.GetKey(KeyCode.I);
		var v  = Input.GetKey(KeyCode.V);
		var e  = Input.GetKey(KeyCode.E);

		if (lc && l && !l_pressed)
		{
			if (enabled) Debug.Log("Disabling log level setting mode");
			else Debug.Log("Enabling log level setting mode");
			enabled = !enabled;
		}

		l_pressed = l;

		// if (l || f || t || r || d || i || v || e)
		// 	Debug.Log("l = " + l + ", f = " + f + ", t = " + t + ", r = " + r + ", d = " + d + ", i = " + i + ", v = " + v + ", e = " + e);

		if (!enabled)
			return;

		Log.Level level = Log.Level.Disabled;

		if (f) level = Log.Level.Focused;
		if (t) level = Log.Level.Trash;
		if (r) level = Log.Level.Trace;
		if (d) level = Log.Level.Debug;
		if (i) level = Log.Level.Info;
		if (v) level = Log.Level.Event;
		if (e) level = Log.Level.Error;

		if (level == Log.Level.Disabled)
			return;

		bool enable = !ls;

		Debug.Log("Setting log level " + level + " to " + enable);
		Log.EnableLevel(level, enable);

		enabled = false;
	}
}
