﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Controllers;
using Models;
using Managers;
using WSCommunication;
using Infrastructure;
using Common;

public class SimulatedWeaponStation : MonoBehaviour
{
	public Common.OperationalMode opMode;
	public bool Force;

	[System.Serializable]
	public struct CommanderControls
	{
		public Common.OperationalMode OpMode;
		[Header("POD")]
		[Range(-180, 180)] public float trvControlAngle;
		[Range( -90,  90)] public float elvControlAngle;

		public float trvLrfJump;
		public float elvLrfJump;
		public bool measureRangeCmd;

		[Header("Optronics")]
		public Common.Camera activeCamera;

		public FOVCmds fovCmd;

		//FOCUS
		public Common.AutoManualMode focusMode;
		//Common.AutoManualMode focusModeOld;

		public Common.InOutStopCmd focusCmd;

		public short reticleX;
		public short reticleY;

		public Common.AutoManualMode irisMode;
		[Range(0, 30)]
		public ushort irisValue;
		public InOutStopCmd irisCmd;

		public Common.AutoManualMode gammaMode;
		public InOutStopCmd gammaCmd;

		public Common.AutoManualMode levelMode;
		public InOutStopCmd levelCmd;

		public Common.AutoManualMode gainMode;
		public InOutStopCmd gainCmd;

		public Polarity polarityCmd;
		public bool nucCmd;

		public short minRange;
		public short maxRange;

		public bool enableReticle;
	};

	[System.Serializable]
	public struct SystemControls
	{
		public Common.WeaponType weaponType;
		public Common.FireMode burstLength;
		public Common.RangeMode rangeMode;
		public float battleRange;
		public float manualRange;
		public bool inFiringZone;
		public bool armSw;
		public bool safeSw;
		public bool overrideSw;
	};

	[System.Serializable]
	public struct OptronicsControls
	{
		public Common.Camera activeCamera;

		public FOVCmds fovCmd;

		//FOCUS
		public Common.AutoManualMode focusMode;
		//Common.AutoManualMode focusModeOld;

		public Common.InOutStopCmd focusCmd;

		public short reticleX;
		public short reticleY;

		public Common.AutoManualMode irisMode;
		[Range(0, 30)]
		public ushort irisValue;
		public InOutStopCmd irisCmd;

		public Common.AutoManualMode gammaMode;
		public InOutStopCmd gammaCmd;

		public Common.AutoManualMode levelMode;
		public InOutStopCmd levelCmd;

		public Common.AutoManualMode gainMode;
		public InOutStopCmd gainCmd;

		public Polarity polarityCmd;
		public bool nucCmd;

		public short minRange;
		public short maxRange;

		public bool enableReticle;
	};

	[System.Serializable]
	public struct HandlesControls
	{
		[Range(-180, 180)] public float trvControlAngle;
		[Range( -90,  90)] public float elvControlAngle;
		
		public float trvLrfJump;
		public float elvLrfJump;
		public bool measureRangeCmd;
		public bool reload;
		public bool fire;
	};

	public SystemControls systemControls;
	public HandlesControls handlesControls;
	public OptronicsControls optronicsControls;
	public CommanderControls commanderControls;

	[System.Serializable]
	public struct Feedback
	{
		public float trvAngle;
		public float elvAngle;
		public float trvAngleCMDR;
		public float elvAngleCMDR;
		public float range;
		public bool freshRange;
		public uint ammoCount;
		public bool uncalibratedFov;
	};

	public Feedback feedback;

	WSCommunicationManager _wsCommManager;
	WSSimProtocol.WsStatusMsg _wsStatus;
	WSSimProtocol.WsFastStatusMsg _wsFastStatus;
	WSSimProtocol.WsCmdsMsg _wsCmds;

	//------------------------------

	void Awake()
	{
		UnityUtils.DestroyMeIfDuplicate(this.gameObject);
		DontDestroyOnLoad(transform.gameObject);
		Force = false;
	}

	//------------------------------

	void Start()
	{
		systemControls.battleRange = 750f;
		systemControls.manualRange = 500f;
	}

	//------------------------------

	WSSimProtocol.WsCmdsMsg GetWsCmds()
	{
		WSSimProtocol.WsCmdsMsg wsCmds = new WSSimProtocol.WsCmdsMsg();

		wsCmds.FireMode = (byte) systemControls.burstLength;
		wsCmds.FireCmd = (byte)((handlesControls.fire) ? 1 : 0);
		wsCmds.FocusCmd = (sbyte) optronicsControls.focusCmd;
		//wsCmds.FocusCmd = (sbyte) optronicsControls.focusCmd;
		wsCmds.ZoomCmd = (byte) optronicsControls.fovCmd;
		wsCmds.ZoomCmdCMDR = (byte) commanderControls.fovCmd;
		wsCmds.PolarityMode = (byte) optronicsControls.polarityCmd;
		wsCmds.MeasureRange = (byte)((handlesControls.measureRangeCmd) ? 1 : 0);
		wsCmds.RangeMode = (byte) systemControls.rangeMode;
		wsCmds.ReloadWeapon = (byte)((handlesControls.reload) ? 1 : 0);
		wsCmds.IrisMode = (byte) optronicsControls.irisMode;
		wsCmds.IrisCmd = (sbyte) optronicsControls.irisCmd;
		wsCmds.GammaMode = (byte) optronicsControls.gammaMode;
		wsCmds.GammaCmd = (sbyte) optronicsControls.gammaCmd;
		wsCmds.LevelMode = (byte) optronicsControls.levelMode;
		wsCmds.LevelCmd = (sbyte) optronicsControls.levelCmd;
		wsCmds.GainMode = (byte) optronicsControls.gainMode;
		wsCmds.GainCmd = (sbyte) optronicsControls.gainCmd;
		wsCmds.NucCmd = (byte) ((optronicsControls.nucCmd) ? 1 : 0);
		wsCmds.EnableReticle = (byte) ((optronicsControls.enableReticle) ? 1 : 0);
		wsCmds.JumpToLaserPos = (byte) ((handlesControls.measureRangeCmd ? 1 : 0));

		wsCmds.JumpToLaserPos = 0;

		handlesControls.reload = false;
		//handlesControls.measureRangeCmd = false;
		//optronicsControls.nucCmd = false;

		if (systemControls.burstLength == FireMode.Single)
			handlesControls.fire = false;

		return wsCmds;
	}

	//------------------------------

	bool plusOrMinus;

	void HandleKeyboardInput()
	{
		float x = Input.GetAxis("Horizontal");
		float y = Input.GetAxis("Vertical");
		bool alt = Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
		bool shift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
		bool plus = Input.GetKey(KeyCode.Plus) || Input.GetKey(KeyCode.KeypadPlus);
		bool minus = Input.GetKey(KeyCode.Minus) || Input.GetKey(KeyCode.KeypadMinus);
		bool fire = Input.GetKey(KeyCode.Space);
		bool nuc = Input.GetKey(KeyCode.N);
		bool day = Input.GetKey(KeyCode.D);
		bool bs = Input.GetKey (KeyCode.B);
		bool thermal = Input.GetKey(KeyCode.T);
		bool auto = Input.GetKey(KeyCode.A);
		bool manual = Input.GetKey(KeyCode.M);
		float inc = (alt) ? 1f : (shift) ? 0.01f : 0.1f;
		handlesControls.trvControlAngle += (x > 0) ? inc : (x < 0) ? -inc : 0;
		handlesControls.elvControlAngle += (y > 0) ? inc : (y < 0) ? -inc : 0;
		// commanderControls.trvControlAngle += (x > 0) ? inc : (x < 0) ? -inc : 0;
		// commanderControls.elvControlAngle += (y > 0) ? inc : (y < 0) ? -inc : 0;
		handlesControls.fire = fire;
		optronicsControls.nucCmd = nuc;
		var currCam = optronicsControls.activeCamera;
		optronicsControls.activeCamera = (day) ? Common.Camera.Day : (thermal) ? Common.Camera.Thermal : (bs) ? Common.Camera.Boresight : currCam;
		
		var currCamCMDR = commanderControls.activeCamera;
		commanderControls.activeCamera = (day) ? Common.Camera.Day : (thermal) ? Common.Camera.Thermal : (bs) ? Common.Camera.Boresight : currCamCMDR;

		var currFocusCmd = optronicsControls.focusCmd;
		var currFocusCmdCMDR = commanderControls.focusCmd;

		var currFovCmd = optronicsControls.fovCmd;
		var currFovCmdCMDR = commanderControls.fovCmd;

		if (shift)
			optronicsControls.focusCmd = (plus) ? InOutStopCmd.In : (minus) ? InOutStopCmd.Out : (plusOrMinus) ? InOutStopCmd.Stop : currFocusCmd;
		else
			optronicsControls.fovCmd = (plus) ? FOVCmds.In : (minus) ? FOVCmds.Out : (plusOrMinus) ? FOVCmds.Stop : currFovCmd;

		var focusMode = optronicsControls.focusMode;
		optronicsControls.focusMode = (auto) ? AutoManualMode.Auto : (manual) ? AutoManualMode.Manual : focusMode;

		var focusModeCMDR = commanderControls.focusMode;
		commanderControls.focusMode = (auto) ? AutoManualMode.Auto : (manual) ? AutoManualMode.Manual : focusModeCMDR;

		plusOrMinus = plus || minus;
	}

	//------------------------------

	void Update()
	{
		Log.Ptrace("SimulatedWeaponStation.Update()");

		if (_wsCommManager == null)
			return;

		HandleKeyboardInput();

		// get SimStatusMsg

		var simStatus = _wsCommManager.GetSimStatusMsg();

		feedback.range = simStatus.Range;
		feedback.freshRange = simStatus.FreshRange == 1;
		feedback.ammoCount = simStatus.AmmoCount;
		feedback.uncalibratedFov = (simStatus.UncalibratedFov == 0) ? false : true;

		if (simStatus.FreshRange != 0)
			systemControls.rangeMode = Common.RangeMode.Lrf;

		// get SimFastStatusMsg

		var simFastStatus = _wsCommManager.GetSimFastStatusMsg();

		feedback.trvAngle = simFastStatus.TrvEncoder;
		feedback.elvAngle = simFastStatus.ElvEncoder;

		feedback.trvAngleCMDR = simFastStatus.TrvEncoderCMDR;
		feedback.elvAngleCMDR = simFastStatus.ElvEncoderCMDR;

		// build phony WsStatusMsg

		_wsStatus.OpMode = (byte) opMode;
		_wsStatus.OpModeCMDR = (byte) commanderControls.OpMode;

		_wsStatus.SelectedWeapon = (byte) systemControls.weaponType;

		if (systemControls.rangeMode == Common.RangeMode.Battle)
			_wsStatus.Range = (ushort) systemControls.battleRange;
		else if (systemControls.rangeMode == Common.RangeMode.Manual)
			_wsStatus.Range = (ushort) systemControls.manualRange;
		else
			_wsStatus.Range = (ushort) feedback.range;

		_wsStatus.ActiveCamera = (byte) optronicsControls.activeCamera;
		_wsStatus.ActiveCameraCMDR = (byte) commanderControls.activeCamera;

		Log.Periodic("SimulatedWeaponStation.Update(): Setting phony WSStatusMsg");

		_wsStatus.ReticlePosX = optronicsControls.reticleX;
		_wsStatus.ReticlePosY = optronicsControls.reticleY;

		_wsStatus.MinRange = optronicsControls.minRange;
		_wsStatus.MaxRange = optronicsControls.maxRange;

		_wsStatus.OutFiringZone = (byte)((systemControls.inFiringZone) ? 1 : 0);
		_wsStatus.Arm = (byte)((systemControls.armSw) ? 1 : 0);
		_wsStatus.Safe = (byte)((systemControls.safeSw) ? 1 : 0);
		_wsStatus.Override = (byte)((systemControls.overrideSw) ? 1 : 0);
		_wsStatus.FocusMode = (byte) optronicsControls.focusMode;

		const float DegToRad = Mathf.PI / 180f;
		var trvJmp = handlesControls.trvLrfJump * DegToRad;
		var elvJmp = handlesControls.elvLrfJump * DegToRad;		
		bool jump = handlesControls.measureRangeCmd;

		_wsStatus.TrvLrfJmpOffset = (jump) ? trvJmp : 0;
		_wsStatus.ElvLrfJmpOffset = (jump) ? elvJmp : 0;

		// Comamnder		
		var trvJmpCMDR = commanderControls.trvLrfJump * DegToRad;
		var elvJmpCMDR = commanderControls.elvLrfJump * DegToRad;
		bool jumpCMDR = commanderControls.measureRangeCmd;

		_wsStatus.TrvLrfJmpOffsetCMDR = (jump) ? trvJmp : 0;
		_wsStatus.ElvLrfJmpOffsetCMDR = (jump) ? elvJmp : 0;

		_wsCommManager.SetPhonyWSStatusMsg(_wsStatus);

		// build phony WsFastStatusMsg

		const float DegToMils = 6400f / 360f;
		_wsFastStatus.TrvEncoderPos = handlesControls.trvControlAngle * DegToMils;
		_wsFastStatus.ElvEncoderPos = handlesControls.elvControlAngle * DegToMils;
		
		_wsFastStatus.TrvEncoderPosCMDR = commanderControls.trvControlAngle * DegToMils;
		_wsFastStatus.ElvEncoderPosCMDR = commanderControls.elvControlAngle * DegToMils;

		Log.Periodic("SimulatedWeaponStation.Update(): Setting phony WSFastStatusMsg");

		_wsCommManager.SetPhonyWSFastStatusMsg(_wsFastStatus);

		// build phony WsCmdMsg

		var wsCmds = GetWsCmds();

		if (Force || !_wsCmds.Equals(wsCmds))
		{
			string status = "fire = " + wsCmds.FireCmd + ", lasing = " + wsCmds.MeasureRange;
			Log.Debug("SimulatedWeaponStation.Update(): Setting phony WSCmdsMsg, " + status);
			_wsCommManager.SetPhonyWSCmdsMsg(wsCmds);
			_wsCmds = wsCmds;
		}

		Force = false;

		Log.Disabled("SimulatedWeaponStation.Update():" +
		            " SimStatus.FovVertAngle = " + simStatus.FovVertAngle + "," +
		            " SimStatus.FovHorzAngle = " + simStatus.FovHorzAngle);
	}

	//------------------------------

	public void SetWsCommunicationManager(WSCommunicationManager wsCommManager)
	{
		Log.Debug("SimulatedWeaponStation.SetWsCommunicationManager(" + wsCommManager + ")");
		_wsCommManager = wsCommManager;
	}

	//------------------------------
}
