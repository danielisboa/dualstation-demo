﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetailDistance : MonoBehaviour
{
	public Terrain terrain;
	public float detailDistance;

	// Update is called once per frame
	void Update ()
	{
		if (terrain != null && detailDistance > 0)
			terrain.detailObjectDistance = detailDistance;		
	}
}
