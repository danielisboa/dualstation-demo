﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Managers;
using Models;
using Common;

public class Calibrator : MonoBehaviour, ISceneCalibrator
{
	public TurretModel[] turretModels;
	public Terrain terrain;

	List<OptronicsModel> optronicsModels;
	List<WeaponModel> weaponModels;
	List<LrfModel> lrfModels;

	//------------------------------

	SimCalibrationsData _simCalibrations;
	bool _newCalibration;
	bool _configured;

	//------------------------------

	void Start ()
	{
	}

	//------------------------------

	void Update ()
	{
		GetModels();

		if (!_newCalibration)
			return;

		Log.Focused("Calibrator.Update(): Updating scene settings");

		UpdateWeaponSettings();
		UpdateOptronicsSettings();
		UpdateTerrainSettings();
		UpdateLrfSettings();

		_newCalibration = false;
	}

	//------------------------------

	void GetModels()
	{
		if (_configured)
			return;

		Log.Focused("Calibrator.GetModels(): Configuring turrets");

		optronicsModels = new List<OptronicsModel>();
		weaponModels = new List<WeaponModel>();
		lrfModels = new List<LrfModel>();

		int configuredTurrets = 0;

		if (turretModels != null)
		{
			foreach (var turretModel in turretModels)
			{
				if (turretModel == null)
				{
					Log.Focused("Calibrator.GetModels(): TurretModel is null");
					continue;
				}

				configuredTurrets++;

				var optronicsModel = (OptronicsModel) turretModel.GetOptronicsModel();
				if (optronicsModel != null) optronicsModels.Add( optronicsModel);
				else Log.Focused("Calibrator.GetModels(): OptronicsModel is null");

				LrfModel lrfModel = null;
				if (optronicsModel != null) lrfModel = optronicsModel.GetLrfModel();
				if (lrfModel != null) lrfModels.Add(lrfModel);
				else Log.Focused("Calibrator.GetModels(): LrfModel is null");

				var weaponM2 = (WeaponModel) turretModel.GetWeaponModel(Common.WeaponType.Gun);
				if (weaponM2 != null) weaponModels.Add(weaponM2);
				else Log.Focused("Calibrator.GetModels(): WeaponM2 is null");

				var weaponMAG = (WeaponModel) turretModel.GetWeaponModel(Common.WeaponType.Mag);
				if (weaponMAG != null) weaponModels.Add(weaponMAG);
				else Log.Focused("Calibrator.GetModels(): WeaponMAG is null");
			}
		}

		if (configuredTurrets > 0)
			_configured = true;

		Log.Focused("Calibrator.GetModels(): Configured turrets: " + configuredTurrets);
	}

	//------------------------------

	void UpdateWeaponSettings()
	{
		Log.Focused("Calibrator.UpdateWeaponSettings()");

		if (weaponModels == null || weaponModels.Count == 0)
			return;

		Log.Focused(
			"Calibrator.UpdateWeaponSettings(): weaponModels.Count = " + weaponModels.Count +
			", WeaponCalibrationsOn = " + _simCalibrations.WeaponCalibrationsOn);

		foreach (var weaponModel in weaponModels)
		{
			if (weaponModel == null) continue;

			if (_simCalibrations.WeaponCalibrationsOn)
			{
				weaponModel.forceLimiter = _simCalibrations.FireForceLimiter;
				weaponModel.windForceScale = _simCalibrations.WindForceScale;
				Log.Focused(
					"Calibrator.UpdateWeaponSettings(): Setting parameters: " +
					"forceLimiter = " + weaponModel.forceLimiter + "(" + _simCalibrations.FireForceLimiter + "), " +
					"windForceScale = " + weaponModel.windForceScale + " (" + _simCalibrations.WindForceScale + ")");
			}
		}
	}

	//------------------------------

	void UpdateLrfSettings()
	{
		if (lrfModels == null || lrfModels.Count == 0)
			return;

		Log.Focused(
			"Calibrator.UpdateLrfSettings(): lrfModels.Count = " + lrfModels.Count +
			", LrfCalibrationsOn = " + _simCalibrations.LrfCalibrationsOn);

		foreach (var lrfModel in lrfModels)
		{
			lrfModel.showHitPointReference = _simCalibrations.LrfCalibrationsOn;
			if (!_simCalibrations.LrfCalibrationsOn)
				_simCalibrations.LrfPosX = _simCalibrations.LrfPosX = _simCalibrations.LrfRotY = 0;
			var posOffset = new Vector3(_simCalibrations.LrfPosX, _simCalibrations.LrfPosY, 0);
			var rotation = new Vector3(_simCalibrations.LrfRotX, _simCalibrations.LrfRotY, 0);
			lrfModel.transform.localPosition = posOffset;
			lrfModel.transform.localEulerAngles = rotation;
		}
	}

	//------------------------------

	void UpdateOptronicsSettings()
	{
		if (optronicsModels == null || optronicsModels.Count == 0)
			return;

		var offset = new Vector2(
			_simCalibrations.reticleOffsetX * _simCalibrations.reticleScaleX,
			_simCalibrations.reticleOffsetY * _simCalibrations.reticleScaleY);

		foreach (var optronicsModel in optronicsModels)
		{
			if (optronicsModel == null) continue;

			if (_simCalibrations.CameraCalibrationsOn)
			{
				optronicsModel.SetCalibrationMode(true);
				optronicsModel.EnableReticle(_simCalibrations.reticleOn, true);
				if (_simCalibrations.reticleOffsetsOn)
					optronicsModel.SetReticlePosition(offset.x, offset.y, true);
			}
			else
				optronicsModel.SetCalibrationMode(false);
		}
	}

	//------------------------------

	void UpdateTerrainSettings()
	{
		if (!_simCalibrations.UseTerrainDetail || terrain == null)
			return;

		terrain.detailObjectDistance = _simCalibrations.DetailDistance;
		terrain.treeBillboardDistance = _simCalibrations.BillboardDistance;
		terrain.detailObjectDensity = _simCalibrations.DetailDensity;
		terrain.basemapDistance = _simCalibrations.BaseMapDistance;

		Log.Periodic("Calibrator.UpdateTerrainSettings(): DetailDistance = " + terrain.detailObjectDistance + ", DetailDensity = " + terrain.detailObjectDensity);
	}

	//------------------------------

	public void SetCalibrationData(SimCalibrationsData data)
	{
		Log.Info("Calibrator.GetSimCalibrations(): New calibrations received");
		_simCalibrations = data;
		_newCalibration = true;
	}
}
