﻿using UnityEngine;
using Infrastructure;

public class UnityConsolePrinter : IConsolePrinter
{
	public void Print(string text)
	{
		Debug.Log(text);
	}
}
