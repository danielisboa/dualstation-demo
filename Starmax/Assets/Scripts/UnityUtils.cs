﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using Infrastructure;
using Common;

public class UnityUtils
{
	static UnityUtils _instance;
	public static UpdateEventDelegate sceneLoaded;

	//------------------------------

	static UnityUtils()
	{
		if (_instance == null)
			_instance = new UnityUtils();
		SceneManager.sceneLoaded += _instance.SceneLoaded;
		Log.Event("UnityUtils constructor");
	}

	//------------------------------

	public static void DestroyMeIfDuplicate(GameObject gameObject)
	{
		var name = gameObject.name;
		gameObject.name = "New" + name;

		if (GameObject.Find(name) != null)
			UnityEngine.Object.Destroy(gameObject);

		gameObject.name = name;
	}

	//------------------------------

	public static void LoadScene(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
	}

	//------------------------------

	void SceneLoaded(Scene scene, LoadSceneMode mode)
	{
		if (sceneLoaded != null) sceneLoaded();
	}
}
