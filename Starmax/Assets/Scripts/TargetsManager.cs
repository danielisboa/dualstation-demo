﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetsManager : MonoBehaviour
{
	public string selectedGroup;
	public int simultaneousTargets;
	public bool popupTargets;
	public float popupTime;

	public List<GameObject> _selectedTargets;
	public GameObject _currentTarget;
	string _selectedGroup;
	float _popupTimer;

	//------------------------------

	void Update()
	{
		if (selectedGroup != _selectedGroup)
		{
			_selectedGroup = selectedGroup;
			FindTargets();
		}

		if (Time.time < 0.5)
			return;

		ManageCurrentTarget();
	}

	//------------------------------

	void ManageCurrentTarget()
	{
		if (_currentTarget == null)
		{
			_currentTarget = GetNextTarget();
			_popupTimer = popupTime;
			RaiseTarget();
		}

		if (_currentTarget != null)
		{
			var controllerScript = _currentTarget.GetComponent<TargetController>();

			if (controllerScript && controllerScript.IsKilled())
			{
				DropTarget();
				_selectedTargets.Remove(_currentTarget);
				_currentTarget = null;
			}
			else if (_popupTimer <= 0 && _selectedTargets.Count > 1)
			{
				DropTarget();
				_currentTarget = null;
			}

			_popupTimer -= Time.deltaTime;
		}
	}

	//------------------------------

	void DropTarget()
	{
		var script = _currentTarget.GetComponent<TargetDrop>();
		if (script == null || script.Dropped) return;
		Debug.Log("Droping target " + _currentTarget.name);
		script.DropTarget();
	}

	//------------------------------

	void RaiseTarget()
	{
		var script = _currentTarget.GetComponent<TargetDrop>();
		if (script == null || !script.Dropped) return;
		Debug.Log("Raising target " + _currentTarget.name);
		script.RaiseTarget();
	}

	//------------------------------

	GameObject GetNextTarget()
	{
		if (_selectedTargets == null)
			return null;

		var length = _selectedTargets.Count;

		if (length == 0)
			return null;

		var target = _selectedTargets[0];
		_selectedTargets.RemoveAt(0);
		_selectedTargets.Add(target);

		return target;
	}

	//------------------------------

	List<GameObject> RandomizeList(List<GameObject> originalList)
	{
		List<GameObject> randomList = new List<GameObject>();

		while (originalList.Count > 0)
		{
			var length = originalList.Count;
			int index = (int) Mathf.Round(Random.Range(0, --length));
			randomList.Add(originalList[index]);
			originalList.RemoveAt(index);
		}

		return randomList;
	}

	//------------------------------

	void FindTargets()
	{
		var selectedTargets = GameObject.FindGameObjectsWithTag(selectedGroup);
		Debug.Log("Targets in group " + selectedGroup + ": " + selectedTargets.Length);

		_selectedTargets = new List<GameObject>(selectedTargets);

		foreach (var target in _selectedTargets)
		{
			var script = target.GetComponent<TargetDrop>();
			Debug.Log("Target  " + target.name + " script: " + script.name);

			if (script == null) continue;

			bool imediate = true;

			if (popupTargets)
				script.DropTarget(imediate);
			else
				script.RaiseTarget(imediate);
		}

		_selectedTargets = RandomizeList(_selectedTargets);
	}
}
