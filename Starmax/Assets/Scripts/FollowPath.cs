﻿using UnityEngine;
using System.Collections;

public class FollowPath : MonoBehaviour 
{
	public Color rayColor = Color.white;
	ArrayList path;

	void OnDrawGizmos()
	{
		Gizmos.color = rayColor;

		// all child objects
		var path_objs = transform.GetComponentsInChildren<Transform>();
		path = new ArrayList();

		foreach (var path_obj in path_objs) 
		{
			// se igual eh o parent e nao queremos incluir como elemento
			if (path_obj != transform)
				path.Add(path_obj);
		}

		for (var i=0; i<path.Count; i++) 
		{
			Vector3 pos = ((Transform) path[i]).position;
			if (i > 0)
			{
				Vector3 prev = ((Transform) path[i-1]).position;
				Gizmos.DrawLine(prev, pos);
				Gizmos.DrawWireSphere(pos, (float)0.3);
			}

		}
	}

}
