﻿using System;

// dummy class to bypass error in VolumetricFog Scripts
namespace UnityEngine.VR
{
	public static class VRSettings
	{
		public static bool enabled;

		static VRSettings()
		{
			enabled = false;
		}
	}
}

