﻿using UnityEngine;
using Common;

interface IPattern
{
	PantographPatterns GetPatternType();
	Vector3 GetPoint(float position);
	float GetLoopTime(PantographCalibratedSpeed speed);
	void SetAmplitude(Vector2 amplitude);
	void SetHeight(float height);
	void Init();
}
