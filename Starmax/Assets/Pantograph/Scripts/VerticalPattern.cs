﻿using UnityEngine;

public class VerticalPattern : Pattern
{
	public VerticalPattern ()
	{
		_type = Common.PantographPatterns.Vertical;
		SetAmplitude(new Vector2(1, 4));
		SetLoopTimes(15f, 30f, 60f);
	}

	override public Vector3 GetPoint(float position)
	{
		float angle = (2 * Mathf.PI) * position;
		var point = new Vector3();
		point.z = Mathf.Sin(angle) * _amplitude.y;
		point.y = _height;
		return point;
	}
}

