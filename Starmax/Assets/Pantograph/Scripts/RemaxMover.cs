﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Infrastructure;
using WSCommunication;
using Common;

public class RemaxMover : MonoBehaviour
{
	private RemaxSimulation weaponStation;

	new public Transform camera;
	public Transform controller;
	[Range(0, 1)]
	public float gain;

	PantographController _controller;
	CrossHair _cross;

	//------------------------------

	void Awake()
	{
		if ((weaponStation = new RemaxSimulation()) != null)
			weaponStation.SetupConnection (2223, "192.168.1.4", 2223);
	}

	//------------------------------

	void Start() 
	{
		if (controller)
			_controller = controller.GetComponent<PantographController> ();

		if (camera)
			_cross = camera.GetComponent<CrossHair>();

		if (weaponStation != null)
			weaponStation.SetEncoderSimulation(false);
	}

	//------------------------------

	void Update() 
	{
		if (Input.GetKeyDown (KeyCode.F))
			Screen.fullScreen = !Screen.fullScreen; 
		if (Input.GetKeyDown (KeyCode.H))
			_controller.Pattern = PantographPatterns.Horizontal;
		if (Input.GetKeyDown (KeyCode.V))
			_controller.Pattern = PantographPatterns.Vertical;
		if (Input.GetKeyDown (KeyCode.C))
			_controller.Pattern = PantographPatterns.Elipsoidal;
		if (Input.GetKeyDown (KeyCode.I))
			_controller.Pattern = PantographPatterns.Infinity;
		if (Input.GetKeyDown (KeyCode.S))
			_controller.Pattern = PantographPatterns.Square;
		if (Input.GetKeyDown (KeyCode.R))
			_cross.DrawCrosshair = !_cross.DrawCrosshair;
		if (Input.GetKeyDown (KeyCode.D))
			_controller.Dynamic = !_controller.Dynamic;
		if (Input.GetKeyDown (KeyCode.Plus))
			_controller.LoopTime -= 1;
		if (Input.GetKeyDown (KeyCode.Minus))
			_controller.LoopTime += 1;
		if (Input.GetKeyDown (KeyCode.G)) 
		{
			const float ginc = 0.05f;
			var g = gain + ((Input.GetKey (KeyCode.LeftShift)) ? ginc : -ginc);
			gain = Mathf.Clamp (g, 0, 1);
		}
				
		HandleEnconders();	
		HandleAimingPosition();
		HandleFire();
	}

	//------------------------------

	void HandleEnconders()
	{
		float trv = 0, elv = 0;
		weaponStation.GetEncodersValues (out trv, out elv);

		if (trv > 180f)
			trv = trv - 360;

		if (elv > 180f)
			elv = elv - 360;
		
		var position = camera.localPosition;
		position.x = trv * gain;
		position.z = elv * gain;
		position.x = Mathf.Clamp(position.x, -20, 20);
		position.y = Mathf.Clamp(position.y, -20, 20);
		camera.localPosition = position;
	}

	//------------------------------

	void HandleAimingPosition()
	{
		if (_cross == null)
			return;
		
		float offsetX, offsetY;
		weaponStation.GetAimingValues (out offsetX, out offsetY);

		offsetX *= Screen.width / 720f;
		offsetY *= Screen.height / 578f;

		const int offsetCorrection = 2;
		_cross.CrosshairOffset.x = offsetX + offsetCorrection;
		_cross.CrosshairOffset.y = offsetY - offsetCorrection;
	}

	//------------------------------

	void HandleFire()
	{
		if (_controller == null) return;
		bool bFire = weaponStation.GetFireCmd ();
		_controller.FirePressed = bFire;
	}
}
