﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;
using Common;

public class PantographModel : Model, IPantographModel
{
//	#region publicAttributes

	public Transform MainCamera;
	public LineRenderer Renderer;
	public Transform Target;
	public GameObject FireTarget;
	public GameObject LaserTarget;
	public PantographPatterns Pattern;
	[Range(30, 360)]
	public float TotalTime;
	[Range(1, 200)]
	public float LoopTime;
	[Range(100, 1000)]
	public uint Resolution;
	public float Height;
	public bool Invert;
	public bool Pause;
	public bool Reset;
	public bool Dynamic;

//	#endregion

	//---------------------------------------

//	#region privateAttributes

	IPattern _pattern;
	float _loopTime;
	float _delay;
	float _time;

	CrossHair _crosshair;
	ChangeMaterial _changeMaterial;

	GUIStyle _style;
	string _guiText;

	bool _start;
	bool _started;
	bool _onTarget;

	float _startTime;

	PantographCalibratedSpeed _speed;

// 	#endregion

	//---------------------------------------

// 	#region publicMethods

	public void Start()
	{
		Log.Trace("PantographModel.Start()");

		if (LoopTime < 1) LoopTime = 1;
		if (Resolution == 0) Resolution = 500;

		_startTime = float.MaxValue;

		_crosshair = MainCamera.GetComponent<CrossHair>();
		_changeMaterial = Target.GetComponent<ChangeMaterial>();

		_loopTime = LoopTime;

		ChangePattern(Pattern);

		_style = new GUIStyle();
		_style.normal.textColor = Color.black;
		_style.fontSize = 16;
		_style.fontStyle = FontStyle.Bold;
		_guiText = "Posicione o retículo\nsobre o alvo";

		if (_crosshair != null)
			_crosshair.SetMinMaxRange(0, 100);

		_started = false;
	}

	//---------------------------------------

	public void SetPattern(PantographPatterns pattern)
	{
		Log.Trace("PantographModel.Start()");
		Pattern = pattern;
	}

	//---------------------------------------

	public void SetReticlePosition(int x, int y)
	{
		Log.Ptrace("PantographModel.Start()");

		if (_crosshair == null) return;

		_crosshair.SetCrossHairPostion(x, y);
	}

	//---------------------------------------

	public void SetEncodersValue(float trv, float elv)
	{
		Log.Ptrace("PantographModel.SetEncodersValue(" + trv + ", " + elv + ")");

		if (trv > 180f) trv = trv - 360;
		if (elv > 180f) elv = elv - 360;

		var position = MainCamera.localPosition;
		const float gain = 0.5f;
		position.x = trv * gain;
		position.z = elv * gain;
		position.x = Mathf.Clamp(position.x, -20, 20);
		position.y = Mathf.Clamp(position.y, -20, 20);
		MainCamera.localPosition = position;
	}

	//---------------------------------------

	public void SetCalibratedTargetSpeed(PantographCalibratedSpeed speed)
	{
		Log.Trace("PantographModel.SetCalibratedTargetSpeed(" + speed + ")");
		_speed = speed;
	}

	//---------------------------------------

	public void ShowLaserRequest(bool show)
	{
		Log.Trace("PantographModel.ShowLaserRequest(" + show + ")");
		if (LaserTarget != null)
			LaserTarget.SetActive(show);
	}

	//---------------------------------------

	public void ShowFireRequest(bool show)
	{
		Log.Trace("PantographModel.ShowFireRequest(" + show + ")");
		if (FireTarget != null)
			FireTarget.SetActive(show);
	}

	//---------------------------------------

	public void ShowOnTargetIndication(bool show)
	{
		Log.Ptrace("PantographModel.ShowOnTargetIndication(" + show + ")");
		_changeMaterial.SelectMaterial((uint) ((show) ? 1 : 0));
	}

	//---------------------------------------

	public void StartTraining(float delay = 0)
	{
		Log.Trace("PantographModel.StartTraining()");
		ResetPattern();
		_delay = delay;
		_start = true;
	}

	//---------------------------------------

	public void StopTraining()
	{
		Log.Trace("PantographModel.StopTraining()");
		_start = _started = false;
	}

	//---------------------------------------

	public bool IsOnTarget()
	{
		return _onTarget;
	}

	//---------------------------------------

	public bool IsRunning()
	{
		return _started;
	}

	//---------------------------------------

	public float GetTrainingElapsedTime()
	{
		return GetTime() - _startTime;
	}

	//---------------------------------------

	public override void ModelUpdate()
	{
		ManagePattern();

		DetectTarget();

		ManageStart();

		ManageReset();

		if (!_started)
			return;

		var savedLoopTime = LoopTime;

		var diffTime = (_startTime - Time.time);

		if (diffTime > -2)
		{
			savedLoopTime = LoopTime;
			LoopTime = 10 * (2 + diffTime) * LoopTime;
			LoopTime = Mathf.Clamp(LoopTime, savedLoopTime, 20 * savedLoopTime);
		}

		UpdateLoopTime();
		LoopTime = savedLoopTime;

		MoveTarget(_time);

		if (Dynamic) DrawLine(Renderer);
	}

	//---------------------------------------

	public void OnGUI()
	{
		// const bool debugTime = false;
		// if (debugTime)
		// {
		// 	var timeText = "running time: " + Time.time + ", start time: " + _startTime + ", training time: " + (Time.time - _startTime);
		// 	GUI.Label(new Rect(Screen.width / 2 + 25, Screen.height / 7 - 20, 100, 20), timeText, _style);
		// }
		GUI.Label(new Rect(Screen.width / 2 + 25, Screen.height / 7, 100, 20), _guiText, _style);
	}

// 	#endregion

	//---------------------------------------

// 	#region privateMethods

	void ManageStart()
	{
		if (!_start || _started)
			return;

		if (_delay > 0)
		{
			_startTime = Time.time + _delay;
			_delay = 0;
		}

		var diffTime = _startTime - Time.time;

		if (diffTime > 0)
		{
			Log.Disabled("PantographModel.ManageStart(): Starting in " + diffTime + " seconds");
			uint count = (uint) diffTime + 1;
			_guiText = "Iniciando treinamento em " + count;
		}
		else
		{
			_guiText = "";
			_started = true;
		}
	}

	//---------------------------------------

	void DetectTarget()
	{
		bool onTarget = _onTarget;
		_onTarget = _crosshair.DetectTarget();
		if (_onTarget != onTarget)
			Log.Debug("PantographModel.DetectTarget(): _onTarget = " + _onTarget);
	}

	//---------------------------------------

	void ManagePattern()
	{
		if (_pattern == null || Pattern != _pattern.GetPatternType())
			ChangePattern(Pattern);
	}

	//---------------------------------------

	void ManageReset()
	{
		if (Reset)
		{
			Reset = false;
			StopTraining();
			StartTraining(_delay);
		}
	}

	//---------------------------------------

	void ResetPattern()
	{
		Log.Trace("PantographModel.ResetPattern()");
		MoveTarget(_time = 0);
		Pause = false;
	}

	//---------------------------------------

	void UpdateLoopTime(bool updateLoopTime = true)
	{
		if (Pause) return;

		if (Invert)
			_time -= Time.deltaTime;
		else
			_time += Time.deltaTime;

		if (_time > _loopTime) _time -= _loopTime;
		if (_time < 0) _time += _loopTime;

		if (LoopTime != _loopTime)
		{
			_time *= LoopTime / _loopTime;
			_loopTime = LoopTime;
		}
	}

	//---------------------------------------

	void MoveTarget(float time)
	{
		if (Target == null) return;
		float pos = time / _loopTime;
		var newPos = _pattern.GetPoint(pos);
		newPos.y = Height + 0.1f;
		Target.localPosition = newPos;
	}

	//---------------------------------------

	void ChangePattern(PantographPatterns pattern)
	{
		Log.Trace("PantographModel.ChangePattern(" + pattern + ")");

		switch (pattern)
		{
			case PantographPatterns.Infinity:
				_pattern = new InfinityPattern();
				break;
			case PantographPatterns.Elipsoidal:
				_pattern = new ElipsoidalPattern();
				break;
			case PantographPatterns.Square:
				_pattern = new SquarePattern();
				break;
			case PantographPatterns.Horizontal:
				_pattern = new HorizontalPattern ();
				break;
			case PantographPatterns.Vertical:
				_pattern = new VerticalPattern ();
				break;
			case PantographPatterns.Sinus:
				_pattern = new SinusPattern();
				break;
			case PantographPatterns.Saw:
				_pattern = new SawPattern();
				break;
			default:
				return;
		}

		DrawLine(Renderer);
		ConfigPattern();
		ResetPattern();

		LoopTime = _pattern.GetLoopTime(_speed);
		Log.Debug("PantographModel.ChangePattern(): Looptime for " + _speed + " speed is " + LoopTime);
	}

	//---------------------------------------

	void ConfigPattern()
	{
		_pattern.SetHeight(Height);
		_pattern.Init();
	}

	//---------------------------------------

	void DrawLine(LineRenderer lineRenderer)
	{
		const uint overPoints = 10;
		var size = Resolution + overPoints;
		if (Dynamic) size /= 5;

		var positions = new Vector3[size];

		ConfigPattern();

		float offset = 0; 
		if (Dynamic) offset = _time / _loopTime;

		for (uint i = 0; i < positions.Length; i++)
		{
			float pos = i * (1f / Resolution) + offset;
			if (pos > 1) Log.Disabled("LineRenderer: pos = " + pos + ", i = " + i + " <----------------------------");
			if (pos > 1) pos -= (int) pos;
			if (pos > 1) Log.Disabled("LineRenderer: pos = " + pos + ", i = " + i + " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			positions[i] = _pattern.GetPoint(pos);
		}

		lineRenderer.positionCount = positions.Length;
		lineRenderer.SetPositions(positions);
	}

// 	#endregion
}
