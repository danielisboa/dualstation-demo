﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
	new public Transform camera;
	[Range(0, 1)]
	public float attenuator;

	void Update()
	{
		if (camera == null) return;
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		var position = camera.localPosition;
		position.x += h * attenuator;
		position.z += v * attenuator;
		camera.localPosition = position;
	}
}
