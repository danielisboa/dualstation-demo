﻿using System;
using UnityEngine;

public class InfinityPattern : Pattern
{
	public InfinityPattern()
	{
		_type = Common.PantographPatterns.Infinity;
		SetAmplitude(new Vector2(8, 4));
		SetLoopTimes(25f, 50f, 100f);
	}

	override public Vector3 GetPoint(float position)
	{
		float angle = (2 * Mathf.PI) * position;
		var point = new Vector3();
		point.x = Mathf.Sin(angle) * _amplitude.x;
		point.z = Mathf.Sin(angle * 2) * _amplitude.y;
		point.y = _height;
		return point;
	}
}

