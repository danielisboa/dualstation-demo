﻿using System;
using System.Collections.Generic;
using UnityEngine;

//---------------------------------------

public class DiamondEdge
{
	public enum Sides
	{
		NoSide,
		UpperLeft,
		UpperRight,
		LowerRight,
		LowerLeft
	}

	//---------------------------------------

	public Sides _side;
	public float _centerX;
	public float _centerY;
	public float _width;

	//---------------------------------------

	public DiamondEdge(float width, float centerX, float centerY, Sides side)
	{
		Set(width, centerX, centerY, side);
	}

	//---------------------------------------

	public void Set(float width, float centerX, float centerY, Sides side)
	{
		_side = side;
		_width = width;
		_centerX = centerX;
		_centerY = centerY;
	}

	//---------------------------------------

	public Vector3 GetPoint(float position)
	{
		Vector3 res;

		switch (_side)
		{
		case Sides.UpperLeft:
			res = GetUpperLeftPoint(position);
			break;
		case Sides.UpperRight:
			res = GetUpperRightPoint(position);
			break;
		case Sides.LowerRight:
			res = GetLowerRightPoint(position);
			break;
		case Sides.LowerLeft:
			res = GetLowerLeftPoint(position);
			break;
		default:
			throw new Exception("Invalid Side");
		}

		res.z *= 2f;

		return res;
	}

	//---------------------------------------

	Vector3 GetUpperLeftPoint(float position)
	{
		float hw = _width / 2; // half width
		float x = -hw * Pattern.LinCos(position/4);
		float y = +hw * Pattern.LinSin(position/4);
		return new Vector3(x + _centerX, 0, y + _centerY);
	}

	//---------------------------------------

	Vector3 GetUpperRightPoint(float position)
	{
		float hw = _width / 2; // half width
		float x = +hw * Pattern.LinSin(position/4);
		float y = +hw * Pattern.LinCos(position/4);
		return new Vector3(x + _centerX, 0, y + _centerY);
	}

	//---------------------------------------

	Vector3 GetLowerRightPoint(float position)
	{
		float hw = _width / 2; // half width
		float x = +hw * Pattern.LinCos(position/4);
		float y = -hw * Pattern.LinSin(position/4);
		return new Vector3(x + _centerX, 0, y + _centerY);
	}

	//---------------------------------------

	Vector3 GetLowerLeftPoint(float position)
	{
		float hw = _width / 2; // half width
		float x = -hw * Pattern.LinSin(position/4);
		float y = -hw * Pattern.LinCos(position/4);
		return new Vector3(x + _centerX, 0, y + _centerY);
	}
}

//---------------------------------------

public class SawPattern : Pattern
{
	//---------------------------------------

	DiamondEdge[] _edges;
	int _numEdges;

	//---------------------------------------

	public SawPattern()
	{
		_type = Common.PantographPatterns.Saw;
		SetAmplitude(new Vector2(12, 5));
		SetLoopTimes(100f, 120f, 140f);
	}

	//---------------------------------------

	override public void Init()
	{
		CreateEdges();
	}

	//---------------------------------------

	void CreateEdges()
	{
		var space = _amplitude.x;
		var w = _amplitude.y; // big diamond width

		// adjust dw so that it
		// is not greater than ampX
		if (w > space) w = space;

		// as space is not smaller than dw (see above), there's at least one diamond, hence the +1 at the end.
		// we remove the size of the first diamond from space (the (space - dw)). the result is what we have of
		// space left to put more diamonds, and each additional diamond uses (dw/2) of space, so we divide this
		// amount of space left for the amount of each additional diamond space
		var numBigDiamonds = Mathf.CeilToInt((space - w) / (w / 2)) + 1;
		_numEdges = numBigDiamonds * 4 + 4;

//		Debug.Log($"SawPatter: Num diamonds = {numBigDiamonds}, num edges = {_numEdges}, space = {space}, dw = {dw}");

		_edges = new DiamondEdge[_numEdges];

		var hw = w / 2; // half width
		var qw = w / 4; // a quarter width
		var c = qw * numBigDiamonds;

		int topFirst = 0;
		int topLast = _numEdges / 2 - 1;
		int bottomFirst = _numEdges / 2;
		int bottomLast = _numEdges - 1;

		// create the four edges that do not alter with the number of peaks
		_edges[topFirst] = new DiamondEdge(qw, -c, 0, DiamondEdge.Sides.UpperLeft);
		_edges[topLast] = new DiamondEdge(qw, +c, 0, DiamondEdge.Sides.UpperRight);
		_edges[bottomFirst] = new DiamondEdge(qw, +c, 0, DiamondEdge.Sides.LowerRight);
		_edges[bottomLast] = new DiamondEdge(qw, -c, 0, DiamondEdge.Sides.LowerLeft);

		c -= qw;

		for (int i = topFirst + 1; i < topLast; i++)
		{
			var topSide = (i % 2 == 0) ? DiamondEdge.Sides.UpperRight : DiamondEdge.Sides.UpperLeft;
			_edges[i] = new DiamondEdge(hw, -c, qw/2, topSide);
			var bottomSide = (i % 2 == 0) ? DiamondEdge.Sides.LowerLeft : DiamondEdge.Sides.LowerRight;
			_edges[i + bottomFirst] = new DiamondEdge(hw, +c, -qw/2, bottomSide);
			c -= (i % 2 == 0) ? hw : 0;
		}

//		for (int i = 0; i < _numEdges; i++)
//		{
//			var w = _edges[i]._width;
//			var x = _edges[i]._centerX;
//			var y = _edges[i]._centerY;
//			var s = _edges[i]._side;
//			Debug.Log($"SawPatter: Edge[{i}]: w = {w} | p = ({x}, {y}) | s = {s}");
//		}
	}

	//---------------------------------------

	override public Vector3 GetPoint(float position)
	{
		position -= (int) position;
		float pos = position * _numEdges;
		int index = (int) pos;
		pos -= index;

		var point = _edges[index].GetPoint(pos);

//		Debug.Log($"SawPatter: Edge[{index}]({pos}) = {point}");

		return point;
	}
}

//---------------------------------------
