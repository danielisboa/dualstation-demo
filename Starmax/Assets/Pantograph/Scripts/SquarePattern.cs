﻿using System;
using UnityEngine;

public class SquarePattern : Pattern
{
	float[] _intervals;
	float _intX;
	float _intY;

	public SquarePattern()
	{
		_type = Common.PantographPatterns.Square;
		SetLoopTimes(30f, 40f, 50f);
	}

	override public void SetAmplitude(Vector2 amplitude)
	{
		_amplitude = amplitude;

		_intervals = new float[6];
		float total = _amplitude.x * 2 + _amplitude.y * 2;
		_intX = _amplitude.x / total;
		_intY = _amplitude.y / total;
		_intervals[0] = 0;
		_intervals[1] = _intX / 2;
		_intervals[2] = _intervals[1] + _intY;
		_intervals[3] = _intervals[2] + _intX;
		_intervals[4] = _intervals[3] + _intY;
		_intervals[5] = _intervals[4] + _intX / 2;
	}

	override public Vector3 GetPoint(float position)
	{
		var point = new Vector3();
		uint i = 0;

		if (_intervals == null)
			return point;

		position -= (uint) position;

		if (position < _intervals[i])
		{
			position += 1 + (uint) -position;
		}
		// no else if here
		if (position < _intervals[++i])
		{
			float pos = position - _intervals[i -1];
			float scale = pos / (_intX / 2);
			point.x = _amplitude.x * scale;
			point.z = _amplitude.y;
		}
		else if (position < _intervals[++i])
		{
			float pos = position - _intervals[i -1];
			float scale = pos / _intY;
			scale = 1 - scale * 2;
			point.x = _amplitude.x;
			point.z = _amplitude.y * scale;
		}
		else if (position < _intervals[++i])
		{
			float pos = position - _intervals[i -1];
			float scale = pos / _intX;
			scale = 1 - scale * 2;
			point.x = _amplitude.x * scale;
			point.z = -_amplitude.y;
		}
		else if (position < _intervals[++i])
		{
			float pos = position - _intervals[i -1];
			float scale = pos / _intY;
			scale = -1 + scale * 2;
			point.x = -_amplitude.x;
			point.z = _amplitude.y * scale;
		}
		else if (position < _intervals[++i])
		{
			float pos = position - _intervals[i -1];
			float scale = 1 - pos / (_intX / 2);
			point.x = _amplitude.x * -scale;
			point.z = _amplitude.y;
		}

		point.y = _height;
		return point;
	}
}

