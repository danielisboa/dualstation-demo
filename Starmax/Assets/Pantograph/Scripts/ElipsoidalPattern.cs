﻿using UnityEngine;

public class ElipsoidalPattern : Pattern
{
	public ElipsoidalPattern()
	{
		_type = Common.PantographPatterns.Elipsoidal;
		SetAmplitude(new Vector2(6, 3));
		SetLoopTimes(25f, 40f, 80f);
	}

	override public Vector3 GetPoint(float position)
	{
		float angle = (2 * Mathf.PI) * position;
		var point = new Vector3();
		point.x = Mathf.Cos(angle) * _amplitude.x;
		point.z = Mathf.Sin(angle) * _amplitude.y;
		point.y = _height;
		return point;
	}
}

