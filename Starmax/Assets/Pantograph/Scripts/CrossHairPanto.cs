﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossHairPanto : MonoBehaviour
{
	public bool DrawCrosshair;
	public Texture2D CrosshairTexture;
	public Vector2 CrosshairOffset;
	public bool AimWeapon;
	public Transform Weapon;

	Camera _camera;
	Vector3 _screenCenter;
	Vector3 _crosshairPos;

	void Start()
	{
		_camera = GetComponent<Camera>();
	}

	void Update()
	{
		UpdateScreenCenter();
		UpdateCrosshairPosition();
		AimWeaponToReticle();
		DrawGizmos();
	}

	void UpdateScreenCenter()
	{
		_screenCenter.x = (Screen.width)  / 2;
		_screenCenter.y = (Screen.height) / 2;
		_screenCenter.z = _camera.nearClipPlane;
	}

	void UpdateCrosshairPosition()
	{
		_crosshairPos = CrosshairOffset;
		_crosshairPos += _screenCenter;
	}

	public bool DetectTarget()
	{
		float fov = _camera.fieldOfView;
		float vAngle = (fov / _screenCenter.y) * CrosshairOffset.y / 2;
		float hAngle = (fov / _screenCenter.x) * CrosshairOffset.x;
		var position = _camera.ScreenToWorldPoint(_screenCenter);
		var direction = transform.TransformDirection(Vector3.forward);
		direction = Quaternion.Euler(vAngle, hAngle, 0) * direction;
		Debug.DrawRay(position, direction * 50, Color.yellow);

		RaycastHit hitInfo;

		if (!Physics.Raycast(position, direction, out hitInfo, 50))
		{
			//Debug.Log("No hit");
			return false;
		}

		if (hitInfo.transform.tag != "Target")
		{
			//Debug.Log("Hit on " + hitInfo.transform.name + " (tag: " + hitInfo.transform.tag + ")");
			return false;
		}

		return true;
	}

	void DrawGizmos()
	{
		float fov = _camera.fieldOfView;
		float vAngle = (fov / _screenCenter.y) * CrosshairOffset.y / 2;
		float hAngle = (fov / _screenCenter.x) * CrosshairOffset.x;
		var position = _camera.ScreenToWorldPoint(_screenCenter);
		var direction = transform.TransformDirection(Vector3.forward);
		direction = Quaternion.Euler(vAngle, hAngle, 0) * direction;
		Debug.DrawRay(position, direction * 50, Color.yellow);
	}
	
	void AimWeaponToReticle()
	{
		if (!AimWeapon) return;
		float fov = _camera.fieldOfView;
		float vAngle = (fov / _screenCenter.y) * CrosshairOffset.y / 2;
		float hAngle = (fov / _screenCenter.x) * CrosshairOffset.x;
		var position = _camera.ScreenToWorldPoint(_screenCenter);
		var direction = transform.TransformDirection(Vector3.forward);
		direction = Quaternion.Euler(vAngle, hAngle, 0) * direction;

		RaycastHit hitInfo;
		if (!Physics.Raycast(position, direction, out hitInfo, 50)) return;
		Weapon.LookAt(hitInfo.point);
	}

	void OnGUI()
	{
		if (!DrawCrosshair) return;
		var w = CrosshairTexture.width;
		var h = CrosshairTexture.height;
		Rect rect = new Rect(_crosshairPos.x - w / 2, _crosshairPos.y - h / 2 -1, w, h);;
		GUI.DrawTexture(rect, CrosshairTexture);
	}
}
