﻿using System;
using UnityEngine;
using Common;

public abstract class Pattern : IPattern
{
	protected PantographPatterns _type;
	protected Vector2 _amplitude;
	protected float _height;

	float _slowSpeed;
	float _normalSpeed;
	float _fastSpeed;

	//-------------------------------------

	public Pattern()
	{
		Log.Trace("Pattern.Pattern()");

		_type = PantographPatterns.NoPattern;
		_height = 0;

		_slowSpeed = 8f;
		_normalSpeed = 15f;
		_fastSpeed = 22f;
	}

	//-------------------------------------

	public PantographPatterns GetPatternType()
	{
		return _type;
	}

	//-------------------------------------

	virtual public void Init()
	{
	}

	//-------------------------------------

	virtual public void SetAmplitude(Vector2 amplitude)
	{
		Log.Trace("Pattern.SetAmplitude(" + amplitude + ")");
		_amplitude = amplitude;
	}

	//-------------------------------------

	virtual public void SetHeight(float height)
	{
		Log.Trace("Pattern.SetHeight(" + height + ")");
		_height = height;
	}

	//-------------------------------------

	virtual public float GetLoopTime(PantographCalibratedSpeed speed)
	{
		Log.Trace("Pattern.GetLoopTime(" + speed + ")");

		float loopTime = 15f;

		switch (speed)
		{
			case PantographCalibratedSpeed.Slow:
				loopTime = _slowSpeed;
				break;
			case PantographCalibratedSpeed.Normal:
				loopTime = _normalSpeed;
				break;
			case PantographCalibratedSpeed.Fast:
				loopTime = _fastSpeed;
				break;
		}

		return loopTime;
	}

	//-------------------------------------

	public static float LinSin(float pos)
	{
		if (pos < 0.25f) return pos * 4;
		if (pos < 0.75f) return 1f - (pos - 0.25f) * 4;
		return -1 + (pos - 0.75f) * 4;
	}

	//-------------------------------------

	public static float LinCos(float pos)
	{
		if (pos < 0.5f) return 1 - pos * 4;
		return -1 + (pos - 0.5f) * 4;
	}

	//-------------------------------------

	public void SetLoopTimes(float fastSpeed, float normalSpeed, float slowSpeed)
	{
		Log.Trace("Pattern.SetLoopTimes(" + fastSpeed + ", " + normalSpeed + ", " + slowSpeed + ")");

		_slowSpeed = slowSpeed;
		_normalSpeed = normalSpeed;
		_fastSpeed = fastSpeed;
	}

	//-------------------------------------

	abstract public Vector3 GetPoint(float pos);
}
