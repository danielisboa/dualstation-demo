﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

//---------------------------------------

public class PantographController : MonoBehaviour
{
	public UnityEngine.Camera MainCamera;
	public LineRenderer Renderer;
	public Transform Target;
	public Transform FireTarget;
	public PantographPatterns Pattern;
	[Range(30, 360)]
	public float TotalTime;
	[Range(1, 120)]
	public float LoopTime;
	[Range(20, 180)]
	public float FireTime;
	[Range(3, 10)]
	public float FireDuration;
	public Vector2 Amplitude;
	[Range(100, 1000)]
	public uint Resolution;
	public float Height;
	public uint StartDelay;
	public bool Invert;
	public bool Pause;
	public bool Reset;
	public bool Dynamic;
	public bool FirePressed;

	IPattern _pattern;
	float _fireDuration;
	float _fireTime;
	float _loopTime;
	float _delay;
	float _time;

	CrossHair _crosshair;
	ChangeMaterial _changeMaterial;

	GUIStyle _style;
	string _guiText;

	bool _started;
	bool _onTarget;
	bool _overrideStart;

	uint _numFiresReq;
	uint _numFiresOk;

	float _elapsedTime;
	float _timeOnTarget;

	//---------------------------------------

	void Start()
	{
		if (LoopTime < 1) LoopTime = 1;
		if (Resolution == 0) Resolution = 500;

		_crosshair = MainCamera.GetComponent<CrossHair>();
		_changeMaterial = Target.GetComponent<ChangeMaterial>();

		_loopTime = LoopTime;
		_delay = StartDelay;

		ChangePattern(Pattern);
		DrawLine(Renderer);

		_style = new GUIStyle();
		_style.normal.textColor = Color.black;
		_style.fontSize = 16;
		_style.fontStyle = FontStyle.Bold;
		_guiText = "Posicione o retículo\nsobre o alvo";

		_started = false;
	
		_fireTime = FireTime;
		_fireDuration = FireDuration;
	}

	//---------------------------------------

	void OnGUI()
	{
		GUI.Label(new Rect(Screen.width / 2 + 25, Screen.height / 7, 100, 20), _guiText, _style);
	}

	//---------------------------------------

	void Update()
	{
		ManagePattern();
		ManageReset();

		DetectTarget();

		ManageStart();

		if (!_started)
			return;

		var savedLoopTime = LoopTime;

		var diffTime = (_delay - Time.time);

		if (diffTime > 0)
		{
			int count = (int) (_delay - Time.time) + 1;
			_guiText = "Iniciando treinamento em " + count;
			return;
		}
		else if (diffTime > -2)
		{
			savedLoopTime = LoopTime;
			LoopTime = 10 * (2 + diffTime) * LoopTime;
			LoopTime = Mathf.Clamp(LoopTime, savedLoopTime, 20 * savedLoopTime);
		}

		TotalTime -= Time.deltaTime;

		if (TotalTime < 0)
		{
			TotalTime = 0;
			Pause = true;
		}
	
		UpdateLoopTime();
		LoopTime = savedLoopTime;

		MoveTarget(_time);

		ManageFireTarget();

		ManageStatistics();

		if (Dynamic) DrawLine(Renderer);
	}

	//---------------------------------------

	void ManageStatistics()
	{
		var deltaTime = Time.deltaTime;
		int time = Mathf.CeilToInt (TotalTime);
		string tmStr = "";
		if (time > 0)
		{
			_elapsedTime += deltaTime;
			if (_onTarget) _timeOnTarget += deltaTime;
			tmStr = "Tempo restante: " + time + "\n";
		}

		var accuracy = Mathf.FloorToInt(100f * _timeOnTarget / _elapsedTime);
		_guiText = tmStr + "Acompanhamento: " + accuracy + "%\nDisparos: " + _numFiresOk + "/" + _numFiresReq;
	}

	//---------------------------------------

	void ManageFireTarget()
	{
		if (Pause) return;
		if (!FireTarget) return;

		_fireTime -= Time.deltaTime;

		bool active = _fireTime <= 0;

		if (FirePressed)
		{
			if (active && _onTarget)
			{
				_numFiresOk++;
				_fireTime = FireTime;
				active = false;
			}
			FirePressed = false;
		}

		var go = FireTarget.gameObject;
	
		if (active && !go.activeSelf)
			_numFiresReq++;

		go.SetActive(active);

		if (active)
			_fireDuration -= Time.deltaTime;
		else
			_fireDuration = FireDuration;

		if (_fireDuration <= 0)
			_fireTime = FireTime;
	}

	//---------------------------------------

	void ManageStart()
	{
		if (Time.time < 2) return;
		if (_started) return;
		_started = _onTarget;
//		_started = true; // TODO: REMOVE
		if (_started) ResetPattern();
	}

	//---------------------------------------

	bool DetectTarget()
	{
		_onTarget = _crosshair.DetectTarget();
		_changeMaterial.SelectMaterial((uint) ((_onTarget) ? 1 : 0));
		return _onTarget;
	}

	//---------------------------------------

	void ManagePattern()
	{
		if (_pattern == null || Pattern != _pattern.GetPatternType())
		{
			ChangePattern(Pattern);
			DrawLine(Renderer);
		}
	}

	//---------------------------------------

	void ManageReset()
	{
		if (Reset)
		{
			Reset = false;
			DrawLine(Renderer);
			ResetPattern();
		}
	}

	//---------------------------------------

	void ResetPattern()
	{
		_delay = Time.time + StartDelay;
		MoveTarget(_time = 0);
		_fireTime = FireTime;
		Pause = false;
	}

	//---------------------------------------

	void UpdateLoopTime(bool updateLoopTime = true)
	{
		if (Pause) return;

		if (Invert)
			_time -= Time.deltaTime;
		else
			_time += Time.deltaTime;

		if (_time > _loopTime) _time -= _loopTime;
		if (_time < 0) _time += _loopTime;

		if (LoopTime != _loopTime)
		{
			_time *= LoopTime / _loopTime;
			_loopTime = LoopTime;
		}
	}

	//---------------------------------------

	void MoveTarget(float time)
	{
		if (Target == null) return;
		float pos = time / _loopTime;
		var newPos = _pattern.GetPoint(pos);
		newPos.y = Height + 0.1f;
		Target.localPosition = newPos;
	}

	//---------------------------------------

	void ChangePattern(PantographPatterns pattern)
	{
		switch (pattern)
		{
			case PantographPatterns.Infinity:
				_pattern = new InfinityPattern();
				break;
			case PantographPatterns.Elipsoidal:
				_pattern = new ElipsoidalPattern();
				break;
			case PantographPatterns.Square:
				_pattern = new SquarePattern();
				break;
			case PantographPatterns.Horizontal:
				_pattern = new HorizontalPattern ();
				break;
			case PantographPatterns.Vertical:
				_pattern = new VerticalPattern ();
				break;
			case PantographPatterns.Sinus:
				_pattern = new SinusPattern();
				break;
			case PantographPatterns.Saw:
				_pattern = new SawPattern();
				break;
			default:
				return;
		}

		ConfigPattern();
		ResetPattern();
	}

	//---------------------------------------

	void ConfigPattern()
	{
		_pattern.SetAmplitude(Amplitude);
		_pattern.SetHeight(Height);
		_pattern.Init();
	}

	//---------------------------------------

	void DrawLine(LineRenderer lineRenderer)
	{
		const uint overPoints = 10;
		var size = Resolution + overPoints;
		if (Dynamic) size /= 5;

		var positions = new Vector3[size];

		ConfigPattern();

		float offset = 0; 
		if (Dynamic) offset = _time / _loopTime;
			
		for (uint i = 0; i < positions.Length; i++)
		{
			float pos = i * (1f / Resolution) + offset;
			if (pos > 1) Log.Disabled("LineRenderer: pos = " + pos + ", i = " + i + " <----------------------------");
			if (pos > 1) pos -= (int) pos;
			if (pos > 1) Log.Disabled("LineRenderer: pos = " + pos + ", i = " + i + " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			positions[i] = _pattern.GetPoint(pos);
		}

		lineRenderer.positionCount = positions.Length;
		lineRenderer.SetPositions(positions);
	}

	public void HandleRcvdMsg(ConfigStruct stConfig)
	{
		if (stConfig.bLoadScene) 
			return;

		if (stConfig.bLoadTargetSpeed) {
			var tmp = 120f - ((float)stConfig.nTargetSpeed * 12f);
			if (tmp == 0)
				LoopTime = 1f;
			else
				LoopTime = tmp;
		}

		if (stConfig.bLoadBasicConfig) 
		{
			TotalTime = (float)stConfig.nTotalTime;
			FireTime = (float)stConfig.nFireTime;
			FireDuration = (float)stConfig.nFireDuration;
		}
	}
}

//---------------------------------------

