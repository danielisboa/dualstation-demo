﻿using UnityEngine;
using Common;

public class HorizontalPattern : Pattern
{
	public HorizontalPattern()
	{
		_type = Common.PantographPatterns.Horizontal;
		SetAmplitude(new Vector2(5, 1));
		SetLoopTimes(13f, 25f, 60f);
	}

	override public Vector3 GetPoint(float position)
	{
		float angle = (2 * Mathf.PI) * position;
		var point = new Vector3();
		point.x = Mathf.Cos(angle) * _amplitude.x;
		//point.z = Mathf.Sin(angle) * _amplitude.y;
		point.y = _height;
		return point;
	}
}


