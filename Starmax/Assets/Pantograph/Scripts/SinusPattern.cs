﻿using System;
using UnityEngine;

public class SinusPattern : Pattern
{
	public SinusPattern()
	{
		_type = Common.PantographPatterns.Sinus;
		SetLoopTimes(30f, 40f, 50f);
	}

	override public Vector3 GetPoint(float position)
	{
		int numsinus = 8;
		float angle = (2 * Mathf.PI) * position;
		var point = new Vector3();
		point.x = LinSin(position) * _amplitude.x;
		point.z = Mathf.Sin(angle * numsinus) * _amplitude.y;
		point.y = _height;
		return point;
	}
}
