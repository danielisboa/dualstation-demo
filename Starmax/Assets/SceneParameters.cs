﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using VolumetricFogAndMist;

public class SceneParameters : MonoBehaviour 
{
	public GameObject _dayCamera;
	VolumetricFog _fogDayCameraScript;

	[System.Serializable]
	public struct VolumetricFogControls
	{
		public float Density;
		public float Scale;
		public float Distance;
		public int Height;
		public int BaseHeight;
		public float WindSpeed;
	};

	public VolumetricFogControls fogControls;

	void Start () 
	{
		if (_dayCamera != null) {
			_fogDayCameraScript = _dayCamera.GetComponent<VolumetricFog> ();

			if (_fogDayCameraScript) 
				_fogDayCameraScript.enabled = false;
		}
	}
	
	void Update () 
	{
		_fogDayCameraScript.density = fogControls.Density;
		_fogDayCameraScript.noiseScale = fogControls.Scale;
		_fogDayCameraScript.height = fogControls.Height;
		_fogDayCameraScript.baselineHeight = fogControls.BaseHeight;
		_fogDayCameraScript.speed = fogControls.WindSpeed;
		_fogDayCameraScript.distance = fogControls.Distance;
	}
}
