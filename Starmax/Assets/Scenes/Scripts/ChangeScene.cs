﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScene : MonoBehaviour 
{
	public GameObject PantographTraining;

	LoadOnClick _loader;
	ConfigComm _configComm;
	PantographController _pantographController;

	// Use this for initialization
	void Start()
	{
		ConfigComm.Debug = true;
		_configComm = ConfigComm.Instance();
		_configComm.Flush();
		_configComm.Connect(3000);
		_loader = gameObject.GetComponent<LoadOnClick> ();
		if (PantographTraining != null)
			_pantographController = PantographTraining.GetComponent<PantographController> ();
	}
	
	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.F))
			Screen.fullScreen = !Screen.fullScreen; 
		
		if (!_loader)
			return;

		int sceneIdx = -1;

		// startup screen
		if (Input.GetKey (KeyCode.Alpha0))
			sceneIdx = 0;
		// pantograph horizontal
		if (Input.GetKey (KeyCode.Alpha1))
			sceneIdx = 1;
		// pantograph vertical
		if (Input.GetKey (KeyCode.Alpha2))
			sceneIdx = 2;
		// pantograph saw
		if (Input.GetKey (KeyCode.Alpha3))
			sceneIdx = 3;
		// pantograph elipse
		if (Input.GetKey (KeyCode.Alpha4))
			sceneIdx = 4;
		// pantograph infinity
		if (Input.GetKey (KeyCode.Alpha5))
			sceneIdx = 5;
		// static targets
		if (Input.GetKey (KeyCode.Alpha6))
			sceneIdx = 6;
		// moving targets
		if (Input.GetKey (KeyCode.Alpha7))
			sceneIdx = 7;
		// popUp targets
		if (Input.GetKey (KeyCode.Alpha8))
			sceneIdx = 8;

		while (_configComm.HasMessage())
		{
			ConfigStruct stConfig = new ConfigStruct();
			_configComm.GetNextMsg(out stConfig);
			Debug.Log ("Received nSceneIndex = " + stConfig.nSceneIndex + ", bLoadScene = " + stConfig.bLoadScene);
			if (stConfig.bLoadScene) {
				sceneIdx = stConfig.nSceneIndex;
				Debug.Log ("Loading idx: " + sceneIdx);
				_loader.LoadScene (sceneIdx);
			}

			Debug.Log ("Calling HandleRcvdMsg");
			_pantographController.HandleRcvdMsg (stConfig);
			return;
		}

		if (sceneIdx == -1)
			return;

		Debug.Log ("Loading idx: " + sceneIdx);
		_loader.LoadScene (sceneIdx);
	}

	void OnApplicationQuit()
	{
		_configComm.Close();
	}

	void ParseRcvdJsonMsg(ConfigStruct stConfig)
	{
		
	}
}
