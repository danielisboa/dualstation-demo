﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;
using Infrastructure;
using VolumetricFogAndMist;

public class EnvironmentModel : Model, IEnvironmentModel
{
	public Light[] _sceneLights;
	List<DayLightModel> _lightModels;
	public GameObject _dayCamera;

	public Transform closeRain;
	public Transform farRain;
	//public Transform middleRain;

	float _sceneLightValue;
	float _farRainPosition;
	bool _rainEnabled;

	[Range(0.5f, 10f)]
	public float ThermalScaler;

	public Material SkyboxDayNoRain;
	public Material SkyboxDayWithRain;
	public Material SkyboxNight;

	Material _currentSkybox;

	Color32 _lightColor;
	Color _ambienteColor;
	Color32 _fogColor;

	bool _enableThermalLight;
	bool _nightEnabled;

	AudioSource _rainAudio;

	VolumetricFog _cameraVolFogScript;
	float _maxFogDensity;
	float _fogDensity;
	bool _volumetricFog;

	VolumetricFogPreT _cameraVolFogPreTScript;

	protected struct AmbientConfig
	{
		public float lightValue;
		public Color ambientColor;
		public Color32 fogColor;
		public Color32 lightColor;
	}

	public enum AmbientEnum
	{
		NoSkyBox = 0xFF,
		DayClouds = 0,
		DayRain = 1,
		Night,
	}

	protected AmbientConfig[] ambientConfig;
	AmbientEnum currentAmbient;

	int _rainSoundDelay;
	//-----------------------------------

	public void Awake()
	{
		_lightModels = new List<DayLightModel> ();

		var lightsCount = (_sceneLights != null) ? _sceneLights.Length : 0;

		for (int i = 0; i < lightsCount; i++)
			_lightModels.Add(new DayLightModel(_sceneLights[i]));

		_farRainPosition = 180f;//943;

		ambientConfig = new AmbientConfig[3];

		ambientConfig [(int) AmbientEnum.DayClouds].lightValue = 100f;
		ambientConfig [(int) AmbientEnum.DayClouds].ambientColor = new Color (0.94f, 0.98f, 1f, 255f);
		ambientConfig [(int) AmbientEnum.DayClouds].fogColor = new Color32 (208, 227, 245, 255); /*(82, 80, 67, 255); */
		ambientConfig [(int) AmbientEnum.DayClouds].lightColor = new Color32 (101, 101, 101, 255);

		ambientConfig [(int) AmbientEnum.DayRain].lightValue   = 50f; //1.19f;
		ambientConfig [(int) AmbientEnum.DayRain].ambientColor = new Color (0.87f, 0.86f, 0.70f, 255f);
		ambientConfig [(int) AmbientEnum.DayRain].fogColor     = new Color32 (66, 60, 39, 255);
		ambientConfig [(int) AmbientEnum.DayRain].lightColor   = new Color32 (168, 168, 168, 255);     

		ambientConfig [(int) AmbientEnum.Night].lightValue   = 2f; //0.2f;
		ambientConfig [(int) AmbientEnum.Night].ambientColor = new Color (0.0722576f, 0.08864592f, 0.1068965f, 255f);
		ambientConfig [(int) AmbientEnum.Night].fogColor     = new Color32 (23, 26, 52, 255);                      
		ambientConfig [(int) AmbientEnum.Night].lightColor   = new Color32 (59, 59, 59, 255);      

		if (closeRain != null)
			_rainAudio = closeRain.GetComponent<AudioSource> ();
		_rainSoundDelay = 0;

		if (_dayCamera != null) {
			_cameraVolFogScript = _dayCamera.GetComponent<VolumetricFog> ();
			_cameraVolFogPreTScript = _dayCamera.GetComponent<VolumetricFogPreT> ();

			if (_cameraVolFogScript)
			{
				_cameraVolFogScript.enabled = false;
				_maxFogDensity = _cameraVolFogScript.density;
			}
		}

		_volumetricFog = false;
		_fogDensity = 0;
	
		UpdateSettings ();
	}

	//-----------------------------------

	public override void ModelUpdate()
	{
		UpdateLightModels ();
		UpdateSkyboxSettings ();
		UpdateRain ();
		UpdateSettings ();
	}

	//-----------------------------------

	void Start()
	{
		HandleAmbient ();
	}

	//-----------------------------------

	public void SetDayLightValue(float value)
	{
		if (_enableThermalLight)
			return;
		
		_sceneLightValue = value;

		const float DarknessLevel = 15;

		if (value < DarknessLevel)
			_nightEnabled = true;
		else
			_nightEnabled = false;
	
		HandleAmbient ();
	}

	//-----------------------------------

	public void SetRainValue(float value)
	{
	}

	//-----------------------------------

	public void SetFogValue(float value)
	{
		_fogDensity = value;
		_volumetricFog = value > 0;
	}

	//-----------------------------------

	void UpdateLightModels()
	{
		//Log.Debug ("((( UpdateLightModels: " + _sceneLightValue);
		if (_lightModels == null)
			return;
		foreach (var lightModel in _lightModels) {
			if (lightModel != null) {
				lightModel.SetIntensity (_sceneLightValue);
				lightModel.SetColor (_lightColor);
			}
		}
	}

	//-----------------------------------

	public void SetFarRainPosition(float value)
	{
		_farRainPosition = value;
	}

	//-----------------------------------

	public void EnableRain(bool flag)
	{
		_rainEnabled = flag;	
		HandleAmbient ();
	}

	//-----------------------------------

	public void EnablerThermalLight(bool flag)
	{
		_enableThermalLight = flag;
	}

	//-----------------------------------

	void UpdateRain()
	{
		if (!closeRain || !farRain)
			return;

		_rainSoundDelay++;

		if (_rainEnabled) {
			closeRain.gameObject.SetActive (true);
			farRain.gameObject.SetActive (true);
			if (_rainSoundDelay == 18) _rainAudio.enabled = true;
			//middleRain.gameObject.SetActive (true);
			farRain.localPosition = new Vector3 (farRain.localPosition.x, farRain.localPosition.y, _farRainPosition);
		} else {
			closeRain.gameObject.SetActive (false);
			farRain.gameObject.SetActive (false);
			_rainAudio.enabled = false;
			//middleRain.gameObject.SetActive (false);
		}
	}

	//-----------------------------------

	void UpdateSkyboxSettings()
	{
		RenderSettings.skybox = _currentSkybox;
		RenderSettings.ambientSkyColor = _ambienteColor;
		RenderSettings.fogColor = _fogColor;
	}

	//-----------------------------------

	protected void UpdateSettings()
	{
		int ambient = (int) currentAmbient;
		float scale = 1f;

		// termal camera selected
		if (_enableThermalLight) {
			ambient = (int) AmbientEnum.DayClouds;
			scale = ThermalScaler;
		}
		else
		{
			if (_cameraVolFogScript)
			{
				_cameraVolFogScript.enabled = _volumetricFog;
				// TODO: TEST USE OF DENSITY
//				_cameraVolFogScript.density = _maxFogDensity * (_fogDensity / 100f);
			}

			if (_cameraVolFogPreTScript)
				_cameraVolFogPreTScript.enabled = _volumetricFog;
		}

		Log.Disabled ("Update settings: Scale: " + scale + " - _enableThmrmlLight: " + _enableThermalLight); 

		if (ambientConfig != null && ambientConfig.Length > ambient) {
			_sceneLightValue = ambientConfig [ambient].lightValue * scale;
			_ambienteColor = ambientConfig [ambient].ambientColor;
			_fogColor = ambientConfig [ambient].fogColor;
			_lightColor = ambientConfig [ambient].lightColor;
		}

		UpdateSkyboxSettings ();
		UpdateLightModels ();
	}

	//-----------------------------------

	public void SetCameraType(Common.Camera camera)
	{
		if (camera == Common.Camera.Thermal)
			_enableThermalLight = true;
		else
			_enableThermalLight = false;

		UpdateSettings ();
	}

	//-----------------------------------

	void HandleAmbient()
	{
		if (!_nightEnabled) {
			if (!_rainEnabled) {
				currentAmbient = AmbientEnum.DayClouds;
				_currentSkybox = SkyboxDayNoRain;
			} else {
				currentAmbient = AmbientEnum.DayRain;
				_currentSkybox = SkyboxDayWithRain;
			}
		} else {
			currentAmbient = AmbientEnum.Night;
			_currentSkybox = SkyboxNight;
		}

		UpdateSettings ();
	}
}