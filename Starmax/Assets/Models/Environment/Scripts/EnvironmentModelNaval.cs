﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentModelNaval : EnvironmentModel
{
	public new void Awake()
	{
		Log.Debug("EnvironmentModelNaval.Awake()");
		base.Awake();

		ambientConfig [(int) AmbientEnum.DayClouds].lightValue = 100f;
		ambientConfig [(int) AmbientEnum.DayClouds].ambientColor = new Color (0.2f, 0.2f, 0.2f, 255f);
		ambientConfig [(int) AmbientEnum.DayClouds].fogColor = new Color32 (82, 80, 67, 255);
		ambientConfig [(int) AmbientEnum.DayClouds].lightColor = new Color32 (160, 160, 150, 255);

		UpdateSettings();
	}
}
