﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;
using UnityStandardAssets.ImageEffects;

public class DayLightModel
{
	Light _light;
	float _maxValue;

	//------------------------------------------

	public DayLightModel(Light light)
	{
		_maxValue = light.intensity;
		_light = light;
		Log.Debug ("DayLightModel: light max Value: " + _maxValue);
	}

	//------------------------------------------

	public void SetIntensity(float value)
	{
		float intensity =  _maxValue * value / 100f;
		_light.intensity = intensity;
	}

	//------------------------------------------

	public void SetColor(Color32 color)
	{
		_light.color = color;
	}

	//------------------------------------------
}
