﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetDrop : MonoBehaviour
{
	public bool Drop;
	public bool Raise;
	public bool Imediate;
	public bool Dropped;

	public Transform targetObject;
	public Transform postsObject;
	public Transform baseObject;

	GameObject _rotationBase;
	Transform _originalTargetParent;
	Transform _originalPostsParent;
	float _rotationStep;
	uint _rotationSteps;
	bool _dropped;
	bool _invalid;

	//------------------------------

	void Start()
	{
		_invalid = !targetObject || !baseObject;

		if (_invalid) return;

		_rotationBase = new GameObject("RotationBase");
		_rotationBase.transform.parent = baseObject;
		_rotationBase.transform.localScale = new Vector3(1, 1, 1);
		_rotationBase.transform.localPosition = new Vector3(0,0,0);
		_rotationBase.transform.localRotation = baseObject.transform.localRotation;
		_rotationBase.transform.parent = baseObject.transform.parent;

		_originalTargetParent = targetObject.parent;

		if (postsObject != null)
			_originalPostsParent = postsObject.parent;

		_rotationSteps = 0;
		_dropped = false;
	}

	//------------------------------

	void Update()
	{
		if (_invalid) return;

		if (Drop) DropTarget(Imediate);
		if (Raise) RaiseTarget(Imediate);

		Dropped = _dropped;
	}

	//------------------------------

	void FixedUpdate()
	{
		PerformRotation();
	}

	//------------------------------

	void PerformRotation()
	{
		if (_rotationSteps > 0)
		{
			_rotationBase.transform.Rotate(new Vector3(_rotationStep, 0, 0));
			_rotationSteps--;
		}
	}

	//------------------------------

	public void RaiseTarget(bool imediate = false)
	{
		if (Drop)
			return;

		Raise = true;

		if (_dropped)
		{
			SetSteps(imediate);

			if (imediate)
				PerformRotation();

			_dropped = false;
		}
		else if (_rotationSteps == 0)
		{
			targetObject.parent = _originalTargetParent;
			if (postsObject != null)
				postsObject.parent = _originalPostsParent;
			Raise = false;
		}
	}

	//------------------------------

	public void DropTarget(bool imediate = false)
	{
		if (Raise)
			return;

		Drop = true;

		if (!_dropped)
		{
			targetObject.parent = _rotationBase.transform;

			if (postsObject != null)
				postsObject.parent = _rotationBase.transform;

			SetSteps(imediate);

			if (imediate)
				PerformRotation();

			_dropped = true;
		}
		else if (_rotationSteps == 0)
		{
			Drop = false;
		}
	}

	//------------------------------

	public bool IsRaised()
	{
		return (Raise || !_dropped);
	}

	//------------------------------

	public bool IsDropped()
	{
		return (Drop || _dropped);
	}

	//------------------------------

	void SetSteps(bool imediate)
	{
		float dir = (_dropped) ? 1 : -1;
		_rotationSteps = (imediate) ? 1u : 60u;
		_rotationStep = dir * 90f / _rotationSteps;
	}
}
