﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTargetHorizontal : MonoBehaviour 
{
	public bool debug;
	public bool invertDirection;
	public bool randomizeSpeed;
	[Range(0, 2)]
	public float timeDelay;
	[Range(0, 300)]
	public uint amplitude;
	[Range(0, 100)]
	public float speed;
	public GameObject moveableObject;

	float _initPos;
	float _speed;
	float _time;
	float _sint;
	float _step;
	float _pos;
	float _spd;

	//------------------------------

	void Start ()
	{
		_pos = 0;
		_sint = 0;
		_time = 0;
		_speed = 0;

		if (moveableObject == null)
		{
			enabled = false;
			_initPos = 0;
		}
		else
			_initPos = moveableObject.transform.localPosition.x;
	}

	//------------------------------

	void FixedUpdate()
	{
		if (moveableObject == null)
			return;

		if (Time.time < timeDelay)
			return;

		if (speed == 0 || amplitude == 0)
			return;

		const float BreakFactor = 4;
		const float LoopTime = 2 * Mathf.PI / BreakFactor;

		if (_speed != speed)
		{
			_spd = speed / 3.6f * ((invertDirection) ? -1 : 1);
			float range = _spd * 0.1f;
			_spd += (randomizeSpeed) ? Random.Range(-1, +1) * range : 0;

			float signal = (_step != 0) ? Mathf.Abs(_step) / _step : 1;
			_step = _spd * Time.fixedDeltaTime * signal;
			_time = (invertDirection) ? (LoopTime / 2) : 0f;

			if (debug) Debug.Log("SPEED CHANGE -> spd: " + _spd + ", step: " + _step + ", sint: " + _sint + ", signal: " + signal + ", _initPos: " + _initPos);
			_speed = speed;
		}

		float amp = amplitude / 2f;
		var pos = moveableObject.transform.localPosition;

		bool normal = (_step > 0 && _pos < amp) || (_step < 0 && _pos > -amp);

		if (normal)
		{
			_pos += _step;
			pos.x = _pos + _initPos;
			if (debug) Debug.Log("NORMAL -> pos.x: " + pos.x + ", _pos: " + _pos + ", _step: " + _step);
		}

		bool edge =
			(_step > 0 && _pos >= +amp && _sint < 0.5) ||
			(_step < 0 && _pos <= -amp && _sint < 1.0);

		if (edge)
		{
			var sinAmp = Mathf.Abs(_spd) / BreakFactor;
			_time += Time.fixedDeltaTime / 2f;
			_sint = _time / LoopTime;
			var off = (sinAmp) * Mathf.Sin(_sint * Mathf.PI * 2);
			pos.x = _pos + off + _initPos;
			if (debug) Debug.Log("EDGE -> pos.x: " + pos.x + ", _pos: " + _pos + ", off: " + off + ", _sint: " + _sint);
		}

		bool invert =
			(_step > 0 && _pos >= +amp && _sint >= 0.5) ||
			(_step < 0 && _pos <= -amp && _sint >= 1.0);

		while (_time > LoopTime)
			_time -= LoopTime;

		while (_sint > 1)
			_sint -= 1;

		if (invert)
		{
			_step *= -1;
			if (debug) Debug.Log("INVERT -> _step: " + _step + ", _sint: " + _sint);
		}

		moveableObject.transform.localPosition = pos;
	}
}
