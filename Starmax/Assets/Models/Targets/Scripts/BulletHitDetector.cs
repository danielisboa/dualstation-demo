﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHitDetector : MonoBehaviour
{
	public GameObject BulletTexture;
	public bool SimulateHit;

	GameObject _lastBullet;
	List<GameObject> _holes;
	Transform _holesParent;

	float _cleanupInterval;
	float _cleanupCounter;

	uint _maximumHits;
	bool _killed;
	uint _hits;
	bool _hit;

	//------------------------------

	void Start()
	{
		_cleanupInterval = 0.7f; // seconds

		_maximumHits = 0;
		_killed = false;
		_hit = false;
		_hits = 0;

		_holes = new List<GameObject>();
		_holesParent = transform.Find("BulletHoles");

		if (_holesParent == null)
			_holesParent = transform;
	}

	//------------------------------

	void Update()
	{
		if (SimulateHit)
		{
			++_hits;
			SimulateHit = false;
			if (_maximumHits > 0 && _hits >= _maximumHits)
				_killed = true;
			_hit = true;
		}

		CleanupHoles();
	}

	//------------------------------

	public void SetMaximumHits(uint maximumHits)
	{
		_maximumHits = maximumHits;
	}

	//------------------------------

	public uint GetHits()
	{
		return _hits;
	}

	//------------------------------

	public void Reset()
	{
		_lastBullet = null;
		_killed = false;
		_hits = 0;
	}

	//------------------------------

	public bool IsHit()
	{
		var hit = _hit;
		_hit = false;
		return hit;
	}

	//------------------------------

	public bool IsKilled()
	{
		return _killed;
	}
	
	//------------------------------

	public bool AddHit(GameObject bullet)
	{
		if (_killed || bullet.tag != "Bullet")
			return false;

		if (bullet == _lastBullet)
			return false;

		_lastBullet = bullet;

		_hit = true;
		_hits++;

		if (_maximumHits > 0 && _hits >= _maximumHits)
			_killed = true;

		if (_killed)
			Log.Debug("BulletHitDetector.OnCollisionEnter(): Target killed");

		return true;
	}

	//------------------------------

	void OnCollisionEnter(Collision collision)
	{
		Log.Debug("BulletHitDetector.OnCollisionEnter(): target hit by gameObject " + collision.gameObject.name + " with tag " + collision.gameObject.tag);

		if (!AddHit(collision.gameObject))
		{
			Log.Debug("BulletHitDetector.OnCollisionEnter(): target hit by gameObject " + collision.gameObject.name + " ignored");
			return;
		}

		Log.Debug("BulletHitDetector.OnCollisionEnter(): target hit " + _hits + " by gameObject " + collision.gameObject.name);

		AddBulletContact(collision.contacts[0]);

		Log.Debug("BulletHitDetector.OnCollisionEnter(): Target has now " + _holes.Count + " holes");

		GameObject.Destroy(collision.gameObject);
	}

	//------------------------------

	public void AddBulletContact(ContactPoint contact)
	{
		if (BulletTexture == null)
			return;

		var newHole = Instantiate(BulletTexture, contact.point, Quaternion.FromToRotation(Vector3.up, contact.normal * -1));

		newHole.transform.parent = _holesParent;

		var pos = newHole.transform.localPosition;
		pos.z = 0.02f;

		newHole.transform.localPosition = pos;

		_holes.Insert(0, newHole);
	}

	//------------------------------

	void CleanupHoles()
	{
		_cleanupCounter += Time.deltaTime;

		if (_cleanupCounter < _cleanupInterval)
			return;

		_cleanupCounter -= _cleanupInterval;

		Log.Disabled("BulletHitDetector.CleanupHoles(): Before cleanup, target was with " + _holes.Count + " holes");

		for (int i = 0; i < _holes.Count; i++)
			RemoveHolesBellow(_holesParent, _holes[i].transform);

		if (_holes.Count > 100)
		{
			for (int i = 100; i < _holes.Count; i++)
			{
				var hole = _holes[i];
				_holes.RemoveAt(i);
				GameObject.Destroy(hole);
			}
		}

		Log.Disabled("BulletHitDetector.CleanupHoles(): After cleanup, target has now " + _holes.Count + " holes");
	}

	//------------------------------

	void RemoveHolesBellow(Transform holesParent, Transform referenceHole)
	{
		var pos = referenceHole.position;

		for (int i = 0; i < holesParent.childCount; i++)
		{
			var child = holesParent.GetChild(i);
			if (child.Equals(referenceHole))
				continue;

			var diff = child.position - pos;
			var fdiff = Mathf.Abs(diff.x) + Mathf.Abs(diff.y) + Mathf.Abs(diff.z);

			const float MaxDiff = 0.05f;

			if (fdiff <= MaxDiff)
			{
				Log.Disabled("BulletHitDetector.RemoveHolesBellow(): Removing hole at position " + child.position);
				_holes.Remove(child.gameObject);
				GameObject.Destroy(child.gameObject);
			}
		}
	}
}
