﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;

public class TargetModel : Model, ITargetModel
{
	public GameObject HitObject;
	public bool Mobile;

	TargetDrop _dropTarget;
	BulletHitDetector _hitDetector;
	ChangeMaterial _materialChanger;
	MoveTargetHorizontal _targetMover;
	float _materialChangeTime;

	//------------------------------

	void Start()
	{
		Log.Trace("TargetModel().Start()");

		_dropTarget = GetComponent<TargetDrop>();
		_targetMover = GetComponent<MoveTargetHorizontal>();
		_materialChanger = GetComponent<ChangeMaterial>();

		if (HitObject != null)
			_hitDetector = HitObject.GetComponent<BulletHitDetector>();
	}

	//------------------------------

	public override void ModelUpdate()
	{
		_materialChangeTime = ManageMaterialChange(_materialChangeTime);
	}

	//------------------------------

	float ManageMaterialChange(float time)
	{
		if (IsDropped()) time = 0;
		if (_materialChanger == null) return 0;
		uint sel = (time <= 0) ? 0u : 1u;
		_materialChanger.SelectMaterial(sel);
		time -= Time.deltaTime;
		if (time < 0) time = 0;
		return time;
	}

	//------------------------------

	public void SetHitIndication(float time)
	{
		_materialChangeTime = ManageMaterialChange(time);
		Log.Debug("TargetModel().SetHitIndication(): time = " + time + ", _materialChangeTime = " + _materialChangeTime);
	}

	//------------------------------

	public bool IsMobile()
	{
		return Mobile;
	}

	//------------------------------

	public bool IsHit()
	{
		if (_hitDetector == null) return false;
		return _hitDetector.IsHit();
	}

	//------------------------------

	public uint GetHitsCount()
	{
		if (_hitDetector == null) return 0;
		return _hitDetector.GetHits();
	}

	//------------------------------

	public void ResetHitsCount()
	{
		Log.Trace("TargetModel().ResetHitsCount()");
		if (_hitDetector != null) _hitDetector.Reset();
	}

	//------------------------------

	public void SetEnabled(bool enabled)
	{
		Log.Trace("TargetModel().SetEnabled(" + enabled + ")");
		if (!enabled) Drop(true);
		if (_hitDetector != null) _hitDetector.enabled = enabled;
		if (_targetMover != null) _targetMover.enabled = enabled;
		gameObject.SetActive(enabled);
	}

	//------------------------------

	public void Drop(bool imediate)
	{
		Log.Trace("TargetModel().Drop(" + imediate + ")");
		if (_hitDetector != null) _hitDetector.enabled = false;
		if (_targetMover != null) _targetMover.enabled = false;
		if (_dropTarget != null) _dropTarget.DropTarget(imediate);
		_materialChangeTime = ManageMaterialChange(0);
	}

	//------------------------------

	public void Raise(bool imediate)
	{
		Log.Trace("TargetModel().Raise(" + imediate + ")");
		if (_hitDetector != null) _hitDetector.enabled = true;
		if (_targetMover != null) _targetMover.enabled = true;
		if (_dropTarget != null) _dropTarget.RaiseTarget(imediate);
	}

	//------------------------------

	public bool IsDropped()
	{
		return (_dropTarget != null) ? _dropTarget.IsDropped() : false;
	}

	//------------------------------

	public bool IsRaised()
	{
		return (_dropTarget != null) ? _dropTarget.IsRaised() : true;
	}
}
