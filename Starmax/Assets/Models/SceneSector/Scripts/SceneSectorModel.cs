﻿using System.Collections;
using System.Collections.Generic;
using Infrastructure;
using UnityEngine;
using Models;

public class SceneSectorModel : MonoBehaviour, ISceneSectorModel
{
	public GameObject vehiclePosition;
	public GameObject trajectory;
	public GameObject targetSector;
	public Transform lookAtObject;

	List<GameObject> _targets;

	//------------------------------

	public IGameObject GetVehiclePosition()
	{
		return new GameObjectWrapper(vehiclePosition);
	}

	//------------------------------

	public IGameObject GetTrajectory()
	{
		return new GameObjectWrapper(trajectory);
	}

	//------------------------------

	public List<IModel> GetTargets()
	{
		if (_targets == null)
			FindTargets();

		List<IModel> targetModels = new List<IModel>();

		foreach(GameObject target in _targets)
		{
			target.SetActive(true);
			var lookAt = target.GetComponent<LookAt>();
			if (lookAt != null) lookAt.lookAtObject = lookAtObject;
			targetModels.Add(target.GetComponent<TargetModel>());
		}

		Log.Info("SceneSectorModel.GetTargets(): targetModels.Count = " + targetModels.Count);

		return targetModels;
	}

	//------------------------------

	void Awake()
	{
		FindTargets();
	}

	//------------------------------

	void FindTargets()
	{
		_targets = new List<GameObject>();

		if (targetSector == null)
			return;

		foreach(Transform child in targetSector.transform)
		{
			var target = child.gameObject;
			var targetModel = target.GetComponent<TargetModel>();

			if (target.activeSelf && targetModel != null)
			{
				_targets.Add(target);
			}

			target.SetActive(false);
		}

		Log.Info("SceneSectorModel.FindTargets(): " + gameObject.name + " has " + _targets.Count + " targets");
	}
}
