﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour
{
	public GameObject HitObject;
	[Range(0, 200)]
	public uint MaximumHits;
	public bool AutoDrop;

	TargetDrop _dropTarget;
	BulletHitDetector _hitDetector;
	ChangeMaterial _materialChanger;
	float _materialChangeTime;

	//------------------------------

	void Start()
	{
		_dropTarget = GetComponent<TargetDrop>();
		_materialChanger = GetComponent<ChangeMaterial>();

		if (HitObject)
			_hitDetector = HitObject.GetComponent<BulletHitDetector>();

		if (_hitDetector)
			_hitDetector.SetMaximumHits(MaximumHits);
	}

	//------------------------------

	void Update()
	{
		if (_hitDetector == null)
			return;
		
		if (IsHit())
			_materialChangeTime = .25f;

		if (!IsKilled() && _materialChanger)
		{
			uint sel = (_materialChangeTime <= 0) ? 0u : 1u;
			_materialChanger.SelectMaterial(sel);
			_materialChangeTime -= Time.deltaTime;
		}

		if (IsKilled() && AutoDrop)
			_dropTarget.DropTarget();
	}

	//------------------------------

	public bool IsHit()
	{
		return _hitDetector.IsHit();
	}

	//------------------------------

	public bool IsKilled()
	{
		return _hitDetector.IsKilled();
	}	
}
