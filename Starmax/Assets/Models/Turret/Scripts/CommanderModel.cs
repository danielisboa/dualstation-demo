﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;

public class CommanderModel : Model, ICommanderModel
{
	public Transform  traverseAxisCMDR;
	public Transform  elevationAxisCMDR;
	public GameObject traverseGyro;
	public GameObject elevationGyro;
	public GameObject optronicsModel;

	float _trvRotation;
	float _elvRotation;

	//------------------------------

	ModelType GetModel<ModelType>(GameObject model) where ModelType:IModel
	{
		if (model == null) return default(ModelType);
		return model.GetComponent<ModelType>();
	}

	//------------------------------

	public IModel GetTrvGyroModel()
	{
		return GetModel<GyroModel>(traverseGyro);
	}

	//------------------------------

	public IModel GetElvGyroModel()
	{
		return GetModel<GyroModel>(elevationGyro);
	}

	// ------------------------------

	public IModel GetOptronicsModel()
	{
		OptronicsModel model = GetModel<OptronicsModelNaval>(optronicsModel);

		if (model == null)
			model = GetModel<OptronicsModel>(optronicsModel);

		return model;
	}

	//------------------------------

	public void ApplyTrvRotation(float rotation)
	{
		_trvRotation += rotation;	
	}

	//------------------------------

	public void ApplyElvRotation(float rotation)
	{
		_elvRotation += rotation;
	}

	//------------------------------

	public override void ModelUpdate()
	{
		Log.Print(Log.Level.Disabled, "CommanderModel.ModelUpdate(): Trv rotation CMDR = " + _trvRotation + ", Elv rotation CMDR = " + _elvRotation);		
		traverseAxisCMDR.Rotate(_trvRotation * new Vector3(0, 1, 0));
		elevationAxisCMDR.Rotate(_elvRotation * new Vector3(0, 0, 1));
		_trvRotation = _elvRotation = 0;		
	}

	//------------------------------

	public float GetTrvAngle()
	{
		return traverseAxisCMDR.localRotation.eulerAngles.y;
	}

	//------------------------------

	public float GetElvAngle()
	{
		 return 360f - elevationAxisCMDR.localRotation.eulerAngles.z;
	}
}
