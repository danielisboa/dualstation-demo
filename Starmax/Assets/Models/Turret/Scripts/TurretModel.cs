﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;
using Controllers;

public class TurretModel : Model, ITurretModel
{
	public Transform  traverseAxis;
	public Transform  elevationAxis;
	public GameObject traverseGyro;
	public GameObject elevationGyro;
	public GameObject optronicsModel;
	public GameObject optronicsModelCMDR;
	public GameObject commanderModel;
	public GameObject gunModel;
	public GameObject magModel;

	float _trvRotation;
	float _elvRotation;

	//------------------------------

	ModelType GetModel<ModelType>(GameObject model) where ModelType:IModel
	{
		if (model == null) return default(ModelType);
		return model.GetComponent<ModelType>();
	}

	//------------------------------

	public IModel GetTrvGyroModel()
	{
		return GetModel<GyroModel>(traverseGyro);
	}

	//------------------------------

	public IModel GetElvGyroModel()
	{
		return GetModel<GyroModel>(elevationGyro);
	}

	//------------------------------

	public IModel GetOptronicsModel()
	{
		OptronicsModel model = GetModel<OptronicsModelNaval>(optronicsModel);

		if (model == null)
			model = GetModel<OptronicsModel>(optronicsModel);

		return model;
	}

	//------------------------------

	public IModel GetOptronicsModelCMDR()
	{
		OptronicsModel model = GetModel<OptronicsModelNaval>(optronicsModelCMDR);

		if (model == null)
			model = GetModel<OptronicsModel>(optronicsModelCMDR);

		return model;
	}

	//------------------------------

	public IModel GetPodCommanderModel()
	{
		ICommanderModel model = GetModel<ICommanderModel>(commanderModel);

		if (model == null)
			model = GetModel<ICommanderModel>(commanderModel);

		return model;
	}

	//------------------------------

	public IModel GetWeaponModel(Common.WeaponType weaponType)
	{
		GameObject weaponModel = null;

		switch (weaponType)
		{
			case Common.WeaponType.Gun: weaponModel = gunModel; break;
			case Common.WeaponType.Mag: weaponModel = magModel; break;
			default:
				Log.Error("TurretModel.GetWeaponModel(): Unknown weaponType " + weaponType);
				break;
		}

		if (weaponModel == null)
			Log.Error("TurretModel.GetWeaponModel(): WeaponModel for weaponType " + weaponType + " is invalid");

		return GetModel<WeaponModel>(weaponModel);
	}

	//------------------------------

	public void ApplyTrvRotation(float rotation)
	{
		_trvRotation += rotation;
	}

	//------------------------------

	public void ApplyElvRotation(float rotation)
	{
		_elvRotation += rotation;
	}

	//------------------------------

	public override void ModelUpdate()
	{
		Log.Print(Log.Level.Disabled, "TurretModel.ModelUpdate(): Trv rotation = " + _trvRotation + ", Elv rotation = " + _elvRotation);
		traverseAxis.Rotate(_trvRotation * new Vector3(0, 1, 0));
		elevationAxis.Rotate(_elvRotation * new Vector3(1, 0, 0));
		_trvRotation = _elvRotation = 0;
	}

	//------------------------------

	public float GetTrvAngle()
	{
		return traverseAxis.localRotation.eulerAngles.y;
	}

	//------------------------------

	public float GetElvAngle()
	{
		 return 360f - elevationAxis.localRotation.eulerAngles.x;
	}
}
