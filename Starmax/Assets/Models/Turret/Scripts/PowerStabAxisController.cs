﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//------------------------------

public class PowerStabAxisController : MonoBehaviour
{
	[Range(-180, 180)]
	public float ControlAngle;
	public GyroMeasurements Gyro;
	public bool Stabilization = false;
	[Range(-180, 180)]
	public float Test;

	public float EncoderValue { get; private set; }

	//------------------------------

	float _stabRef;
	float _lastControlAngle;

	float _ctrlAngMul;
	Vector3 _filterVec;

	//------------------------------

	void Start ()
	{
		ControlAngle = 0;
		_lastControlAngle = 0;
		_stabRef = Gyro.Angle;

		switch (Gyro.MeasuringAxis)
		{
		case GyroMeasurements.Axis.Traverse:
			_filterVec = new Vector3(0, 1, 0);
			_ctrlAngMul = 1;
			break;
		case GyroMeasurements.Axis.Elevation:
			_filterVec = new Vector3(1, 0, 0);
			_ctrlAngMul = -1;
			break;
		default:
			_filterVec = new Vector3(0, 0, 0);
			_ctrlAngMul = 0;
			break;
		}
	}

	//------------------------------

	// Update is called once per frame
	void Update()
	{
		PowerStab();
	}

	//------------------------------

	void PowerStab()
	{
		var gyroAngle = Gyro.MeasureAngle();
		var ctrlAngle = ControlAngle * _ctrlAngMul;

		if (!Stabilization)
			_stabRef = gyroAngle - _lastControlAngle;

		var stabAngle = gyroAngle - _stabRef - ctrlAngle;

		var rotation = -stabAngle * _filterVec;
		transform.Rotate(rotation);

		_lastControlAngle = ctrlAngle;
		//Debug.Log(transform.localRotation.eulerAngles);
		EncoderValue = 	transform.localRotation.eulerAngles.x +
						transform.localRotation.eulerAngles.y +
						transform.localRotation.eulerAngles.z;
	}
}

//------------------------------
