﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ThermalCameraFinalRenderBehaviour : MonoBehaviour {

	public bool HotWhite;

	public GameObject thermalLightObject;

	private Light thermalLight;
	public Camera TerrainCamera;

	public Material CombineCameraMaterial;

	private RenderTexture TerrainTarget;
	private RenderTexture TerrainDepthTarget;

	public Camera MachineCamera;

	private RenderTexture MachineTarget;
	private RenderTexture MachineDephtTarget;

	private RenderTexture TemporaryDest;

	private Camera thisCamera;

	private Dictionary<int, bool> lightDict;
	private Dictionary<int, GameObject> gameDict;

	private List<int> gameIdList;
	private bool isThermalCameraActive;

	// Use this for initialization
	void Start () {
		isThermalCameraActive = false;
		gameDict = new Dictionary<int, GameObject>();
		lightDict = new Dictionary<int, bool>();
		gameIdList = new List<int>();

		thisCamera = GetComponent<Camera>();
		foreach( var light in Resources.FindObjectsOfTypeAll<Light>() ){
			lightDict.Add(light.gameObject.GetInstanceID(), light.enabled);
			gameDict.Add(light.gameObject.GetInstanceID(), light.gameObject);
			gameIdList.Add(light.gameObject.GetInstanceID());
		}

		thermalLight = thermalLightObject.GetComponent<Light>();
	}

	/// <summary>
	/// OnRenderImage is called after all rendering is complete to render image.
	/// </summary>
	/// <param name="src">The source RenderTexture.</param>
	/// <param name="dest">The destination RenderTexture.</param>
	void OnRenderImage(RenderTexture src, RenderTexture dest)
	{

		if(isThermalCameraActive != (thisCamera.depth == 10)){
			isThermalCameraActive = (thisCamera.depth == 10);

			if(isThermalCameraActive){
				foreach(int objectId in gameIdList){
					lightDict[objectId] = gameDict[objectId].GetComponent<Light>().enabled;
					gameDict[objectId].GetComponent<Light>().enabled = false;
				}
				thermalLight.enabled = true;
			} else {

				foreach(int objectId in gameIdList){
					gameDict[objectId].GetComponent<Light>().enabled = lightDict[objectId];
				}
				thermalLight.enabled = false;
			}
		}
		
		if(TemporaryDest == null){
			TemporaryDest = RenderTexture.GetTemporary(src.width, src.height, src.depth, src.format);
		}

		if(TerrainTarget == null){
			TerrainTarget = RenderTexture.GetTemporary(src.width, src.height, src.depth, src.format);
			Debug.Log("Texture Format: " + src.format);
		}

		if(MachineTarget == null){
			MachineTarget = RenderTexture.GetTemporary(src.width, src.height, src.depth, src.format);
			Debug.Log("Texture Format: " + src.format);
		}

		if(TerrainDepthTarget == null){
			TerrainDepthTarget = RenderTexture.GetTemporary(src.width, src.height, src.depth, RenderTextureFormat.Depth);
		}

		TerrainCamera.depthTextureMode = DepthTextureMode.Depth;
		TerrainCamera.targetTexture = TerrainTarget;
		TerrainCamera.Render();

		MachineCamera.depthTextureMode = DepthTextureMode.Depth;
		MachineCamera.targetTexture = MachineTarget;
		MachineCamera.Render();
		
		float HotWhiteVal = HotWhite ? 1.0f : 0.0f;
		CombineCameraMaterial.SetFloat("_HotWhite", HotWhiteVal);
		CombineCameraMaterial.SetTexture("_SecondCamera", MachineTarget);

		Graphics.Blit(TerrainTarget, dest, CombineCameraMaterial);
		
	}
	
	// Update is called once per frame
	void Update () {
		TerrainCamera.nearClipPlane =  thisCamera.nearClipPlane;
		TerrainCamera.fieldOfView =  thisCamera.fieldOfView;

		int machineCullingMask = MachineCamera.cullingMask;
		MachineCamera.CopyFrom(TerrainCamera);
		MachineCamera.cullingMask = machineCullingMask;
	}
}
