﻿Shader "Unlit/CombineCamerasShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			sampler2D _SecondCamera;
			uniform float _HotWhite;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 colSecond = tex2D(_SecondCamera, i.uv);
				fixed4 outCol = col;
				if(col.r > colSecond.r) outCol = colSecond;
				outCol.r = outCol.b;
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				if(_HotWhite != 0.){
					outCol.r = 1. - outCol.r;
					outCol.g = 1. - outCol.g;
					outCol.b = 1. - outCol.b;
				}
				return outCol;
			}
			ENDCG
		}
	}
}
