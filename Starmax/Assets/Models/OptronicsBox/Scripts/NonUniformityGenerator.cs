﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class NonUniformityGenerator : MonoBehaviour
{
	public Canvas canvas;
	public RawImage rawImagePrefab;
	public Texture2D[] nucPatterns;

	[Range(1, 300)]
	public int period;
	[Range(0, 1)]
	public float maxAlpha;

	public bool NUC;

	public bool Enable
	{
		set
		{
			canvas.enabled = value;
		}
	}

	//-------------------------------------

	List<RawImage> _nuOverlays;
	Texture2D _selectedPattern;
	Texture2D _screenShot;
	bool _performingNUC;
	int _width;
	int _height;

	//-------------------------------------

	void Start ()
	{
		SetWidthHeight();

		_nuOverlays = new List<RawImage>();

		if (period <= 0)
			period = 1000000000;

		if (nucPatterns != null)
			_selectedPattern = nucPatterns[0];

		StartCoroutine(SelectPattern(5));
		StartCoroutine(ApplyNucPattern());
	}

	//-------------------------------------

	void Update()
	{
		SetWidthHeight();
		if (NUC) PerformNUC();
	}

	//-------------------------------------

	void SetWidthHeight()
	{
		_width = Screen.width;
		_height = Screen.height;
	}
	
	//-------------------------------------

	IEnumerator SelectPattern(int factor)
	{
		while (true)
		{
			if (nucPatterns != null)
			{
				int index = Random.Range(0, nucPatterns.Length);
				_selectedPattern = nucPatterns[index];
				Log.Trash("NonUniformityGenerator.SelectPattern(): selected pattern " + index);
			}
			yield return new WaitForSeconds(period * factor);
		}
	}

	//-------------------------------------

	IEnumerator ApplyNucPattern()
	{
		Log.Trace("NonUniformityGenerator.ApplyNucPattern(" + period + ")");

		while (true)
		{
			yield return new WaitForSeconds(period);
			yield return new WaitForEndOfFrame();

			if (_performingNUC)
				continue;

			Log.Trash("NonUniformityGenerator.ApplyNucPattern()");

			var texture = (_selectedPattern != null) ? _selectedPattern : TakeScreenshot();
			var rawImage = CreateRawImage(texture);
			_nuOverlays.Add(rawImage);

			var canvasGroup = canvas.GetComponent<CanvasGroup>();
			if (canvasGroup && canvasGroup.alpha < maxAlpha)
				canvasGroup.alpha += 0.01f;

			CleanupRawImages(50);

			_screenShot = TakeScreenshot();
		}
	}

	//-------------------------------------

	RawImage CreateRawImage(Texture2D texture)
	{
		var rawImage = Instantiate<RawImage>(rawImagePrefab, canvas.transform);
		rawImage.gameObject.SetActive(true);
		rawImage.texture = texture;
		rawImage.SetNativeSize();
		return rawImage;
	}

	//-------------------------------------

	Texture2D TakeScreenshot()
	{
		var texture = new Texture2D(_width, _height, TextureFormat.RGB24, true);
		texture.ReadPixels(new Rect(0, 0, _width, _height), 0, 0);
		texture.Apply();
		return texture;
	}

	//-------------------------------------

	void CleanupRawImages(int maxCount)
	{
		Log.Trace("NonUniformityGenerator.CleanupRawImages(" + maxCount + "): rawImages.Count = " + _nuOverlays.Count);
		if (_nuOverlays.Count >= maxCount)
		{
			var removeCount = _nuOverlays.Count - maxCount;
			for (int i = 0; i < removeCount; i++)
				Destroy(_nuOverlays[i].gameObject);
			_nuOverlays.RemoveRange(0, removeCount);
		}
	}

	//-------------------------------------

	IEnumerator DoNUC()
	{
		_performingNUC = true;
		CleanupRawImages(0);
		var canvasGroup = canvas.GetComponent<CanvasGroup>();
		if (canvasGroup) canvasGroup.alpha = 1f;
		var rawImage = CreateRawImage(_screenShot);
		yield return new WaitForSeconds(1);
		Destroy(rawImage);
		if (canvasGroup) canvasGroup.alpha = 0f;
		_performingNUC = false;
	}

	//-------------------------------------

	public void PerformNUC()
	{
		Log.Trace("NonUniformityGenerator.PerformNUC()");
		StartCoroutine(DoNUC());
		NUC = false;
	}
}
