﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;
using Common;

public class OptronicsModel : Model, IOptronicsModel
{
	public EnvironmentModel environmentModel;
	public GameObject dayCamera;
	public GameObject thermalCamera;
	public GameObject boresightCamera;
	public GameObject lrf;

	public bool reticleEnabled;

	CameraModel[] _cameraModels;
	CameraModel	_currCamera;

	LrfModel _lrfModel;

	NonUniformityGenerator _nuGenerator;
	Canvas _nuCanvas;

	CrossHair _crossHair;
	bool _drawCrossHair;
	float _targetRange;

	bool _calibrationMode;
	string _realReticlePos;
	Common.Camera _camera;

	//----------------------------------------

	void Start()
	{
		_cameraModels = new CameraModel[(int)Common.Camera.NumCameras];

		_cameraModels [(int)Common.Camera.Day] = dayCamera.GetComponent<CameraModel>();
		_cameraModels [(int)Common.Camera.Thermal] = thermalCamera.GetComponent<CameraModel>();
		_cameraModels [(int)Common.Camera.Boresight] = boresightCamera.GetComponent<CameraModel>();

		_cameraModels[(int) Common.Camera.Thermal].SetActive(false);
		_cameraModels[(int) Common.Camera.Boresight].SetActive(false);

		_camera = Common.Camera.Day;
		_currCamera = _cameraModels [(int) _camera];

		lrf = null; // TODO: CHECK THIS

		if (lrf != null && lrf.activeSelf)
			_lrfModel = lrf.GetComponent<LrfModel>();

		_crossHair = gameObject.GetComponent<CrossHair>();

		_nuGenerator = gameObject.GetComponent<NonUniformityGenerator>();

		if (_nuGenerator)
			_nuCanvas = _nuGenerator.canvas;

		if (_nuCanvas)
			_nuCanvas.gameObject.SetActive(false);

		if (_crossHair)
			_crossHair.SetCamera(_currCamera.GetCamera());

	}

	//------------------------------------------------

	void OnGUI()
	{
		var style = new GUIStyle();
		style.normal.textColor = Color.white;
		style.fontSize = 16;
		style.fontStyle = FontStyle.Bold;
		GUI.backgroundColor = Color.black;
		GUI.Label(new Rect(Screen.width / 2 + 25, Screen.height / 7 - 20, 100, 20), _realReticlePos, style);
	}

	//----------------------------------------

	public override void ModelUpdate()
	{
		UpdateCameraModel();
	}

	//----------------------------------------

	// update current camera model
	void UpdateCameraModel()
	{
		if (_currCamera == null)
			return;

		_currCamera.UpdateCamera();

		const bool bForceReticleSetting = true;
		EnableReticle(reticleEnabled, bForceReticleSetting);
	}

	//----------------------------------------

	public LrfModel GetLrfModel()
	{
		return _lrfModel;
	}

	//----------------------------------------

	public void SetCalibrationMode(bool enable)
	{
		_calibrationMode = enable;
	}

	//----------------------------------------

	public void SetActiveCamera(Common.Camera camera)
	{
		if (camera == _camera)
			return;

		_camera = camera;

		if (environmentModel != null)
			environmentModel.SetCameraType (camera);

		if (_currCamera != null)
			_currCamera.SetActive (false);

		_currCamera = _cameraModels [(int)camera];

		if (_currCamera == null)
			return;

		_currCamera.SetActive (true);

		if (_nuCanvas)
			_nuCanvas.gameObject.SetActive(camera == Common.Camera.Thermal);

		if (_crossHair) {
			_crossHair.SetCamera (_currCamera.GetCamera ());

			if (camera == Common.Camera.Boresight)
				_crossHair.DrawCrosshair = true;
			else
				_crossHair.DrawCrosshair = false;
		}
	}

	//----------------------------------------

	public void SetCameraFOVAngle(float fov)
	{
		if (_currCamera == null)
			return;

		//Log.Debug ("OptronicsModel: SetCamFOV value = " + fov);

		_currCamera.SetFovAngle (fov);
	}

	//----------------------------------------

	public void SetFocusValue(float value)
	{
		if (_currCamera == null)
			return;

		_currCamera.SetBlurLevel (value);
	}

	//----------------------------------------

	public void SetReticlePosition(float x, float y, bool force = false)
	{
		if (_calibrationMode && !force)
		{
			_realReticlePos = "Reticle pos = " + new Vector2(x, y);
			return;
		}

		// keep reticle in the center
		if (_camera == Common.Camera.Boresight) {
			x = 0;
			y = 0;
		}

		if (_crossHair != null)
			_crossHair.SetCrossHairPostion (x, y);
	}

	//----------------------------------------

	public void SetRange(float range)
	{
	}

	//----------------------------------------

	public void SetIrisValue(float value)
	{
		if (_currCamera == null)
			return;

		_currCamera.SetBloomValue (value);
	}

	//----------------------------------------

	public virtual void SetPolarityCmd(Polarity cmd)
	{
		if (_currCamera == null)
			return;

		_currCamera.SetPolarity (cmd);
	}

	//----------------------------------------

	public void SetRangeLimits(short minRange, short maxRange)
	{
		if (_lrfModel != null)
		{
			_lrfModel.minRange = (uint) minRange;
			_lrfModel.maxRange = (uint) maxRange;
		}

		if (_crossHair != null)
			_crossHair.SetMinMaxRange(minRange, maxRange);
	}

	//----------------------------------------

	public void MeasureRangeCmd()
	{
		_targetRange = 0;

		if (_lrfModel != null)
			_targetRange = _lrfModel.range;
		else if (_crossHair != null)
			_targetRange = _crossHair.TargetRange;

		Log.Disabled ("MODEL :: NEW RANGE CMD: " + _targetRange);
	}

	//----------------------------------------

	public float GetTargetRange()
	{
		return _targetRange;
	}

	//----------------------------------------

	public void SetNucCmd(bool cmd)
	{
		if (_nuGenerator == null)
			return;

		if (cmd == true)
			_nuGenerator.PerformNUC();
	}

	//----------------------------------------

	public void SetGammaValue(float value)
	{
		if (_currCamera == null)
			return;

		_currCamera.SetGammaValue (value);
	}

	//----------------------------------------

	public void SetLevelValue(float value)
	{
		if (_currCamera == null)
			return;

		_currCamera.SetLevelValue (value);
	}

	//----------------------------------------

	public void SetGainValue(float value)
	{
		if (_currCamera == null)
			return;

		_currCamera.SetGainValue (value);
	}

	//----------------------------------------

	public void EnableReticle(bool enable, bool force = false)
	{
		// if (!force) return; // TODO: REMOVE THIS

		Log.Info("OptronicsModel.EnableReticle(" + enable + ", " + force + "): _crossHair = " + _crossHair);

		if (_crossHair == null)
			return;

		if (_calibrationMode && !force)
			return;

		// boresight camera always show reticle
		if (_camera == Common.Camera.Boresight)
			enable = true;

		_crossHair.DrawCrosshair = enable;
	}

	//----------------------------------------

	public float GetCurrentFov()
	{
		return _currCamera.GetCurrentFov ();
	}

	//----------------------------------------

	public byte GetPolarityMode()
	{
		return 0;//(byte)_currCamera.GetPolarityMode ();
	}

	public Common.Camera GetCurrentCamera(){
		return _camera;
	}

}
