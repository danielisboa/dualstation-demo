using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//------------------------------

public class OptronicsController : MonoBehaviour
{
	public enum CamerasEnum
	{
		Day = 0,
		Thermal = 1,
		Boresight = 2,
		NumOfCameras,
		None
	}

	//------------------------------

	CameraController _dayCamera;
	CameraController _thermalCamera;
	CameraController[] _cameraControllers;

	CamerasEnum _activeCamera;
	FieldOfViewEnum _activeFieldOfView;
	ZoomCmdEnum _zoomCommand;
	FocusCmdEnum _focusCommand;
	FocusModeEnum _focusMode;

	//------------------------------

	public CamerasEnum ActiveCamera;
	public FieldOfViewEnum ActiveFieldOfView;
	public ZoomCmdEnum ZoomCommand;
	public FocusCmdEnum FocusCommand;
	public FocusModeEnum FocusMode;
	public int MeasuredRange;
	public bool MeasureRange;

	//------------------------------

	public CameraController CurrentCamera
	{
		get { return GetController(_activeCamera); }
	}

	//------------------------------

	void Start()
	{
		// list must contain capacity to hold all used cameras
		_cameraControllers = new CameraController[(int) CamerasEnum.NumOfCameras];

		if (_cameraControllers == null)
			throw new UnityException("Error creating controllers list");

		AddCameraController(CamerasEnum.Day, "DayCamera", 42f, 15f, 4.6f, ZoomTypeEnum.Continuous, FocusModeEnum.Auto);
		AddCameraController(CamerasEnum.Thermal, "ThermalCamera", 14.3f, 14.3f, 4.6f, ZoomTypeEnum.Discrete, FocusModeEnum.Manual);
		AddCameraController(CamerasEnum.Boresight, "BoresightCamera", 14.3f, 14.3f, 4.6f, ZoomTypeEnum.Fixed, FocusModeEnum.None);

		SetActiveCamera(CamerasEnum.Day);
		SetActiveFieldOfView(FieldOfViewEnum.Narrow);

		ZoomCommand  = _zoomCommand  = ZoomCmdEnum.ZoomStop;
		FocusCommand = _focusCommand = FocusCmdEnum.FocusStop;
		FocusMode    = _focusMode    = CurrentCamera.FocusMode;
	}

	//------------------------------

	void AddCameraController(CamerasEnum cameraId, string cameraName, float vwFov, float wFov, float nFov, ZoomTypeEnum zType, FocusModeEnum fType)
	{
		if (cameraId >= CamerasEnum.NumOfCameras) return;
		_cameraControllers[(int) cameraId] = null;
		var gameObject = this.transform.Find(cameraName);
		if (gameObject == null) return;
		var camera = gameObject.GetComponent<Camera>();
		if (camera == null) return;
		var controller = new CameraController(camera, vwFov, wFov, nFov, zType, fType);
		_cameraControllers[(int) cameraId] = controller;
	}

	//------------------------------

	void Update()
	{
		UpdateOptronics (Time.deltaTime);
	}

	//------------------------------

	void NOFixedUpdate()
	{
		UpdateOptronics (Time.fixedDeltaTime);
	}

	//------------------------------

	void UpdateOptronics(float time)
	{
		if (ActiveCamera != _activeCamera)
			SetActiveCamera(ActiveCamera);
	
		if (ActiveFieldOfView != _activeFieldOfView)
			SetActiveFieldOfView(ActiveFieldOfView);
	
		if (ZoomCommand != _zoomCommand)
			SetZoomCmd(ZoomCommand);

		if (FocusCommand != _focusCommand)
			SetFocusCmd(FocusCommand);

		if (FocusMode != _focusMode)
			SetFocusMode(FocusMode);

		if (MeasureRange)
		{
			MeasuredRange = (int) CurrentCamera.AimingRange;
			MeasureRange = false;
		}

		CurrentCamera.UpdateCamera(time);

		ActiveFieldOfView = _activeFieldOfView = CurrentCamera.FieldOfView;
		FocusMode = _focusMode = CurrentCamera.FocusMode;
	}

	//------------------------------

	void SetActiveCamera(CamerasEnum camera)
	{
		if (camera == _activeCamera)
			return;
		// get current controller
		var oldController = CurrentCamera;
		// change active controller
		ActiveCamera = _activeCamera = camera;
		// now current is the new controller
		CurrentCamera.SwitchViewWith(oldController);
		SetActiveFieldOfView(CurrentCamera.FieldOfView);
		oldController.CrossHair = false;
		//CurrentCamera.CrossHair = true;
	}

	//------------------------------

	void SetActiveFieldOfView(FieldOfViewEnum fov)
	{
		CurrentCamera.FieldOfView = fov;
		ActiveFieldOfView = fov;
		_activeFieldOfView = fov;
	}

	//------------------------------

	public void SetActiveFieldOfView(float fov)
	{
		CurrentCamera.SetFieldOfView (fov);
	}

	//------------------------------

	void SetZoomCmd(ZoomCmdEnum cmd)
	{
		ZoomCommand = _zoomCommand = cmd;
		CurrentCamera.ZoomCmd(ZoomCommand);
	}

	//------------------------------

	void SetFocusCmd(FocusCmdEnum cmd)
	{
		FocusCommand = _focusCommand = cmd;
		CurrentCamera.FocusCmd(FocusCommand);
	}

	//------------------------------

	public void SetFocusMode(FocusModeEnum mode)
	{
		FocusMode = _focusMode = mode;
		CurrentCamera.FocusMode = FocusMode;
	}

	//------------------------------

	public CameraController GetController(CamerasEnum camera)
	{
		if (_cameraControllers.Length == 0)
			throw new UnityException("Controllers list is empty");

		if ((int) camera >= _cameraControllers.Length)
 			new UnityException("Requested camera index out of bounds");

		return _cameraControllers[(int) camera];
	}

	//------------------------------

	public void SetAimingPostion(int x, int y)
	{
		CurrentCamera.SetAimingPostion (x, y);
	}
}

//------------------------------
