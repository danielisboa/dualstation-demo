using UnityEngine;
using UnityStandardAssets.ImageEffects;

//------------------------------

public enum ZoomTypeEnum
{
	Fixed,
	Continuous,
	Discrete,
}

//------------------------------

public enum FocusModeEnum
{
	None = -1,
	Manual = 0,
	Auto = 1,
}

//------------------------------

public enum FieldOfViewEnum
{
	Narrow,
	Wide,
	VeryWide,
}

//------------------------------

public enum ZoomCmdEnum
{
	ZoomStop,
	ZoomOut,
	ZoomIn,
}

//------------------------------

public enum FocusCmdEnum
{
	FocusNear = -1,
	FocusStop = 0,
	FocusFar = 1,
}

//------------------------------

public class CameraController
{
	Camera _camera;

	float _narrowFov;
	float _wideFov;
	float _veryWideFov;
	float _fovRange;
	FieldOfViewEnum _fieldOfView;

	float _zoomLevel;
	float _zoomSpeed;
	float _zoomSetPoint;
	ZoomTypeEnum _zoomType;

	float _focusPos;
	float _focusSpeed;
	float _focusSetPoint;
	FocusModeEnum _focusAllowedMode;
	FocusModeEnum _focusCurrentMode;

	bool _blurEnabledDefault;
	BlurOptimized _blurScript;

	CrossHair _crossHair;

	float _deltaTime;

	//------------------------------

	public FieldOfViewEnum FieldOfView
	{
		get { return _fieldOfView; }
		set { SetFieldOfView(value); }
	}

	//------------------------------

	public FocusModeEnum FocusMode
	{
		get { return _focusCurrentMode; }
		set { SetFocusMode(value); }
	}

	//------------------------------

	public bool CrossHair
	{
		get { return ((_crossHair) ? _crossHair.DrawCrosshair : false); }
		set { if (_crossHair) _crossHair.DrawCrosshair = value; }
	}

	//------------------------------

	public float AimingRange
	{
		get { return _crossHair.TargetRange; }
	}

	//------------------------------

	public CameraController(Camera camera, float veryWideFov, float wideFov, float narrowFov, ZoomTypeEnum zoomType, FocusModeEnum focusMode)
	{
		_camera = camera;

		_narrowFov = narrowFov;
		_wideFov = wideFov;
		_veryWideFov = veryWideFov;
		_fovRange = veryWideFov - narrowFov;

		_zoomType = zoomType;
		_zoomSpeed = 8f;

		_focusAllowedMode = focusMode;
		_focusCurrentMode = focusMode;
		_focusSpeed = 0.3f;

		camera.fieldOfView = _narrowFov;
		SetFieldOfView(FieldOfViewEnum.VeryWide);

		if (camera)
		{
			_blurScript = camera.GetComponent<BlurOptimized>();
			_blurEnabledDefault = (_blurScript) ? _blurScript.enabled : false;
			_crossHair = camera.GetComponent<CrossHair>();
		}
	}

	//------------------------------

	public void UpdateCamera(float deltaTime = 0)
	{
		_deltaTime = (deltaTime <= 0) ? Time.deltaTime : deltaTime;

		AdjustFieldOfView(_zoomSetPoint);
		SetFocus(_focusSetPoint);
	}

	//------------------------------

	public void SetFieldOfView(FieldOfViewEnum fov)
	{
		switch (fov)
		{
		case FieldOfViewEnum.Narrow:
			_zoomSetPoint = _narrowFov;
			break;
		case FieldOfViewEnum.Wide:
			_zoomSetPoint = _wideFov;
			break;
		case FieldOfViewEnum.VeryWide:
			_zoomSetPoint = _veryWideFov;
			break;
		}

		AdjustFieldOfView(_zoomSetPoint);
	}

	//------------------------------

	public void ZoomCmd(ZoomCmdEnum cmd)
	{
		if (_camera == null)
			return;

		if (_zoomType != ZoomTypeEnum.Continuous)
			cmd = ZoomCmdEnum.ZoomStop;

		switch (cmd)
		{
		case ZoomCmdEnum.ZoomStop:
			_zoomSetPoint = _camera.fieldOfView;
			break;
		case ZoomCmdEnum.ZoomOut:
			_zoomSetPoint = _veryWideFov;
			break;
		case ZoomCmdEnum.ZoomIn:
			_zoomSetPoint = _narrowFov;
			break;
		}
	}

	//------------------------------

	public void FocusCmd(FocusCmdEnum cmd)
	{
		if (_focusCurrentMode == FocusModeEnum.Auto)
			cmd = FocusCmdEnum.FocusStop;

		switch (cmd)
		{
		case FocusCmdEnum.FocusStop:
			_focusSetPoint = _focusPos;
			break;
		case FocusCmdEnum.FocusNear:
			_focusSetPoint = 1f;
			break;
		case FocusCmdEnum.FocusFar:
			_focusSetPoint = 0f;
			break;
		}
	}

	//------------------------------

	public void SetFocusMode(FocusModeEnum mode)
	{
		if (_focusAllowedMode == FocusModeEnum.Manual)
			mode = FocusModeEnum.Manual;

		_focusCurrentMode = mode;

		if (mode == FocusModeEnum.Auto)
			_focusSetPoint = _zoomLevel;
	}

	//------------------------------

	public void SwitchViewWith(CameraController other)
	{
		if (other == null) return;
		if (_camera == null) return;
		_camera.depth = other._camera.depth;
		other._camera.depth = _camera.depth - 1;
		var rect = _camera.rect;
		_camera.rect = other._camera.rect;
		other._camera.rect = rect;
	}

	//------------------------------

	void SetFocus(float focus)
	{
		float focusDiff = focus - _focusPos;
		float focusStep = _focusSpeed * _deltaTime;
		focusStep = Mathf.Min(focusStep, Mathf.Abs(focusDiff));
		if (focusDiff < 0) focusStep *= -1;
		_focusPos += focusStep;
		AdjustFocus();
	}

	//------------------------------

	void AdjustFocus()
	{
		if (!_blurScript) return;
		const float blurMax = 4f;
		const float blurDeadZone = 0.2f;
		float blurLevel = Mathf.Abs(_zoomLevel - _focusPos);
		float blur = blurMax * blurLevel - blurDeadZone;
		_blurScript.blurSize = Mathf.Clamp(blur, 0, blurMax);
		_blurScript.enabled = (blur > 0) ? true : _blurEnabledDefault;
		_blurScript.blurIterations = 1;
	}

	//------------------------------

	public void SetFieldOfView(float fov)
	{
		if (fov < 1)
			fov = 1;
		_zoomSetPoint = fov;
	}

	//------------------------------

	void AdjustFieldOfView(float fov)
	{
		if (_camera == null)
			return;
		
		float currfov = _camera.fieldOfView;

		if (currfov == fov)
			return;

		if (_zoomType == ZoomTypeEnum.Continuous)
		{
			float fovDiff = fov - currfov;
			float fovStep = _zoomSpeed * _deltaTime;
			fovStep = Mathf.Min(fovStep, Mathf.Abs(fovDiff));
			if (fovDiff < 0) fovStep *= -1;
			fov = currfov + fovStep;
		}

		_camera.fieldOfView = fov;

		_zoomLevel = (fov - _narrowFov) / _fovRange;
		_zoomLevel = 1 - _zoomLevel;

		_fieldOfView = (fov <= _narrowFov) ? FieldOfViewEnum.Narrow :
			(fov <= _wideFov)   ? FieldOfViewEnum.Wide   : 
			FieldOfViewEnum.VeryWide;

		if (_focusCurrentMode == FocusModeEnum.Auto)
			_focusSetPoint = _zoomLevel;

		AdjustFocus();
	}

	//------------------------------

	public Camera GetCamera()
	{
		return _camera;
	}

	//------------------------------

	public void SetAimingPostion(int x, int y)
	{
		_crossHair.CrosshairOffset.x = x;
		_crossHair.CrosshairOffset.y = y;
	}
}

//------------------------------

