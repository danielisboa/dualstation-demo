﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LrfModel : MonoBehaviour
{
	public uint minRange;
	public uint maxRange;
	public uint range;

	public GameObject hitPointReference;
	public bool showHitPointReference;

	public Vector3 origin;

	void Start()
	{
		origin = transform.localPosition;
	}

	// Update is called once per frame
	void Update ()
	{
		RaycastHit hitInfo;
		var position = transform.position;
		var direction = transform.TransformDirection(Vector3.forward);
		range = (Physics.Raycast(position, direction, out hitInfo, maxRange)) ? (uint) Mathf.Round(hitInfo.distance) : 0;
		if (range < minRange) range = 0;
		var color = (range > 0) ? Color.blue : Color.red;
		var length = (range > 0) ? range : maxRange;
		Debug.DrawRay(position, direction * length, color);
		hitPointReference.transform.position = hitInfo.point;
		hitPointReference.SetActive(showHitPointReference);
	}
}
