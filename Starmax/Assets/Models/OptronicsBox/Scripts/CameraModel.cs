﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;
using UnityStandardAssets.ImageEffects;
using Infrastructure;


public class CameraModel : MonoBehaviour
{
	UnityEngine.Camera _cameraObject;

	public ZoomConfig _zoomConfig;
	public float zoomSpeed;
	public float focusSpeed;
	float _zoomSetPoint;

	float _focusSetPoint;

	float _deltaTime;

	// focus
	BlurOptimized _blurScript;
	bool _blurEnabledDefault;

	// iris
	Bloom _bloom;

	//polarity
	// ColorCorrectionCurves _colorCurves;
	ThermalCameraFinalRenderBehaviour _thermalBehaviour;

	float _blurLevel;
	float _currentFov;

	//Common.Polarity _polarity; 

	//----------------------------------------------

	public void Awake()
	{
		_cameraObject = gameObject.GetComponent<UnityEngine.Camera>();
		if (_cameraObject == null)
			throw new Exception ("Invalid Unity Engine Camera");

		_bloom = gameObject.GetComponent<Bloom> ();
		// _colorCurves = gameObject.GetComponent<ColorCorrectionCurves> ();

		_thermalBehaviour = gameObject.GetComponent<ThermalCameraFinalRenderBehaviour>();

		_blurScript = _cameraObject.GetComponent<BlurOptimized>();
		_blurEnabledDefault = (_blurScript) ? _blurScript.enabled : false;
	}

	//----------------------------------------------

	public UnityEngine.Camera GetCamera()
	{
		return _cameraObject;
	}

	//----------------------------------------------

	public void SetActive(bool active)
	{
		_cameraObject.depth = (active) ? 10 : 0;
	}

	//------------------------------------------------

	public void SetFovAngle(float fovAngle)
	{
		_zoomSetPoint = fovAngle;
	}

	//------------------------------------------------

	public void SetBlurLevel(float blurLevel)
	{
		_blurLevel = blurLevel;
	}

	//------------------------------------------------

	public void SetBloomValue(float value)
	{
		if (_bloom)
			_bloom.bloomIntensity = value;
	}

	//------------------------------------------------

	// public void SetPolarity(Polarity cmd)
	// {
	// 	if (!_colorCurves)
	// 		return;	//Day Camera does not have this script

	// 	if (cmd == Polarity.BlackHot) {
	// 		_colorCurves.enabled = false;
	// 		//_polarity = Polarity.BlackHot;
	// 	} else {
	// 		_colorCurves.enabled = true;
	// 		//_polarity = Polarity.WhiteHot;
	// 	}
	// }
	
	public void SetPolarity(Polarity cmd)
	{
		if (!_thermalBehaviour)
			return;	//Day Camera does not have this script

		if (cmd == Polarity.WhiteHot) {
			_thermalBehaviour.HotWhite = false;
			//_polarity = Polarity.BlackHot;
		} else {
			_thermalBehaviour.HotWhite = true;
			//_polarity = Polarity.WhiteHot;
		}
	}

	//------------------------------------------------

	public void SetNucCmd(bool cmd)
	{
		
	}

	//------------------------------------------------

	public void SetGammaValue(float value)
	{

	}

	//------------------------------------------------

	public void SetLevelValue(float value)
	{

	}

	//------------------------------------------------

	public void SetGainValue(float value)
	{

	}

	//------------------------------------------------

	public float GetCurrentFov()
	{
		return _currentFov;
	}

	//------------------------------------------------

	public void UpdateCamera()
	{
		_deltaTime = Time.deltaTime;

		if (_zoomSetPoint != 0f)
			AdjustFieldOfView(_zoomSetPoint);

		AdjustBlurLevel();
	}

	//----------------------------------------------

	void AdjustFieldOfView(float fovAngle)
	{
		if (gameObject == null)
			return;

		float currfov = _cameraObject.fieldOfView;

		if (currfov == fovAngle)
			return;

		if (_zoomConfig == ZoomConfig.Continuous)
		{
			float fovDiff = fovAngle - currfov;
			float fovStep = zoomSpeed * _deltaTime;
			fovStep = Mathf.Min(fovStep, Mathf.Abs(fovDiff));
			if (fovDiff < 0) fovStep *= -1;
			fovAngle = currfov + fovStep;
		}
		_currentFov = fovAngle;

		Log.Disabled ("currentfov: " + _currentFov);

		if (fovAngle < 14.5)
			_cameraObject.nearClipPlane = 5f;
		else
			_cameraObject.nearClipPlane = 0.7f;

		_cameraObject.fieldOfView = fovAngle;
	}

	//------------------------------------------------

	void AdjustBlurLevel()
	{
		if (!_blurScript)
			return;

		const float blurMax = 3f;

		float currBlur = _blurScript.blurSize;

		float blurDiff = _blurLevel * blurMax - currBlur;
		float blurStep = focusSpeed * _deltaTime;

		if (_zoomConfig == ZoomConfig.Continuous)
			blurStep = Mathf.Min (blurStep, Mathf.Abs (blurDiff));
		else
			blurStep = Mathf.Abs (blurDiff);

		if (blurDiff < 0) blurStep *= -1;

		Log.Disabled("CameraModel.AdjustBlurLevel(): blurLevel = " + _blurLevel + ", currBlur = " + currBlur + ", blurStep = " + blurStep);

		currBlur = Mathf.Clamp(currBlur + blurStep, 0, blurMax);

		SetBlur(currBlur);
	}

	//------------------------------------------------

	void SetBlur(float blurLevel)
	{
		if (!_blurScript || float.IsNaN(blurLevel))
			return;

		_blurScript.blurSize = blurLevel;
		_blurScript.enabled = (blurLevel > 0) ? true : _blurEnabledDefault;
		_blurScript.blurIterations = 1;

		Log.Disabled("CameraModel.SetBlur(): blurLevel = " + blurLevel + ", enabled = " + _blurScript.enabled);
	}

	//------------------------------------------------

//	public byte GetPolarityMode()
//	{
//		return _polarity;
//	}

	//------------------------------------------------
}
