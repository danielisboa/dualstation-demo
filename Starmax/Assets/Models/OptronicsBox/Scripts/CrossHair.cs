﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//------------------------------

public class CrossHair : MonoBehaviour
{
	public bool DrawCrosshair;
	public Texture2D CrosshairTexture;
	public Vector2 CrosshairOffset;
	Vector2 crosshairCorrection;
	public bool AimWeapon;
	public Transform Weapon;
	[Range(0.01f,1f)]
	public float Gain;
	[Range(-500, +500)]
	public float los;

	//------------------------------

	short _minRange;
	short _maxRange;

	LineRenderer _lineRenderer;

	//------------------------------

	Camera _camera;
	Vector3 _screenCenter;
	Vector3 _crosshairPos;
	float _targetRange;

	//------------------------------

	public float TargetRange
	{
		get
		{
			GetAimRange(out _targetRange, true);

			if (_targetRange > (float)_maxRange || _targetRange < (float)_minRange)
				_targetRange = 0f;

			if (_lineRenderer != null)
			{
				_lineRenderer.SetPosition (0, new Vector3 (los, 0, 0));
				if (_targetRange != 0)
					_lineRenderer.SetPosition(1, new Vector3(los, 0, _targetRange));

				float r = (_targetRange == 0) ? 255 : 0;
				float b = (_targetRange != 0) ? 255 : 0;
				_lineRenderer.startColor = new Color(r, 0, b);
				_lineRenderer.endColor = new Color(r, 0, b);
			}

			return _targetRange;
		}
	}

	//------------------------------

	void Start()
	{
		if (Gain == 0) 
			Gain = 1f;
		
		crosshairCorrection = new Vector2(0, 0);

		_camera = GetComponent<Camera>();
		_lineRenderer = GetComponent<LineRenderer>();
		_targetRange = 50;
	}

	//------------------------------

	void Update()
	{
		UpdateScreenCenter();
		UpdateCrosshairPosition();
		AimWeaponToReticle();
		DrawGizmos();
	}

	//------------------------------

	void UpdateScreenCenter()
	{
		_screenCenter.x = (Screen.width)  / 2;
		_screenCenter.y = (Screen.height) / 2;
		_screenCenter.z = (_camera) ? _camera.nearClipPlane : 1f;
	}

	//------------------------------

	void UpdateCrosshairPosition()
	{
		const float scale = 1.3f;
		_crosshairPos = CrosshairOffset * scale + crosshairCorrection;
	}

	//------------------------------

	void AimWeaponToReticle()
	{
		if (!AimWeapon || !Weapon) return;

		Vector3 point;
		float range;

		if (!GetAimPosRange(out point, out range))
			return;

		_targetRange = range;
		Weapon.LookAt(point);
	}

	//------------------------------

	public void SetCamera(Camera camera)
	{
		_camera = camera;
	}

	//------------------------------

	public bool DetectTarget()
	{
		float range;
		return GetAimRange(out range);
	}

	//------------------------------

	public bool GetAimPos(out Vector3 point, bool anyTarget = false)
	{
		float range;
		return GetAimPosRange(out point, out range, anyTarget);
	}

	//------------------------------

	public bool GetAimRange(out float range, bool anyTarget = false)
	{
		Vector3 point;
		return GetAimPosRange(out point, out range, anyTarget);
	}

	//------------------------------

	void GetRaycastPosDir(out Vector3 pos, out Vector3 dir)
	{
		pos = gameObject.transform.position;
		dir = gameObject.transform.TransformDirection(Vector3.forward);

		if (_camera == null)
			return;

		var t = _camera.gameObject.transform;
		dir = t.TransformDirection(Vector3.forward);

		if (!_camera.orthographic)
		{
			float fov = _camera.fieldOfView;
			float vAngle = Gain * fov * ((_crosshairPos.y / 2) / _screenCenter.y);
			float hAngle = Gain * fov * (_crosshairPos.x / _screenCenter.x);
			Log.Disabled ("RAYCAST: dir = " + dir + ", vAngle = " + vAngle + ", hAngle = " + hAngle);
			dir = Quaternion.Euler(-vAngle, hAngle, 0) * dir;
			Log.Disabled ("RAYCAST: dir = " + dir);
			pos = _camera.ScreenToWorldPoint(_screenCenter);
		}
		else
		{
			Log.Disabled ("RAYCAST: ORTOGRAPHIC");
			var crosshairPos = _crosshairPos;
			crosshairPos.y *= -1;
			pos = _camera.ScreenToWorldPoint(crosshairPos + _screenCenter);
		}
	}

	//------------------------------

	public bool GetAimPosRange(out Vector3 point, out float range, bool anyTarget = false)
	{
		Vector3 position, direction;

		range = 0;
		point = new Vector3();

		RaycastHit hitInfo;

		GetRaycastPosDir(out position, out direction);

		if (!Physics.Raycast(position, direction, out hitInfo, _maxRange))
			return false;

		if (!anyTarget && hitInfo.collider.tag != "Target")
			return false;

		range = hitInfo.distance;
		point = hitInfo.point;
		return true;
	}

	//------------------------------

	public void SetCrossHairPostion(float x, float y, bool raw = false)
	{
		if (!raw)
		{
			const int reticleOffsetX = +2;
			const int reticleOffsetY = -2;

			x += reticleOffsetX + x / 10;
			y += reticleOffsetY + Mathf.RoundToInt(y / 20);
		}

		CrosshairOffset.x = x;
		CrosshairOffset.y = y;
	}

	//------------------------------


	void DrawGizmos()
	{
		Vector3 position, direction;
		GetRaycastPosDir(out position, out direction);
		// Debug.DrawRay(position, direction * _targetRange, Color.blue);
	}

	//------------------------------

	void OnGUI()
	{
		if (!DrawCrosshair) return;
		var w = CrosshairTexture.width;
		var h = CrosshairTexture.height;
		var chPos = _crosshairPos + _screenCenter;
		Rect rect = new Rect(chPos.x - w/2, chPos.y - h/2, w, h);
		GUI.DrawTexture(rect, CrosshairTexture);
	}

	//------------------------------

	public void SetMinMaxRange(short min, short max)
	{
		_minRange = min;
		_maxRange = max;
	}
}
