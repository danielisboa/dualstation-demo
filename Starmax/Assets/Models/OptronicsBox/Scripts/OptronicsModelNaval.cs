﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;
using Common;

public class OptronicsModelNaval : OptronicsModel
{
	public override void SetPolarityCmd(Polarity cmd)
	{
		cmd = (cmd == Common.Polarity.BlackHot) ? Common.Polarity.WhiteHot : Common.Polarity.BlackHot;
		base.SetPolarityCmd(cmd);
	}
}
