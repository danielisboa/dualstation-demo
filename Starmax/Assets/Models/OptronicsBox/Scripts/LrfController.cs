﻿using UnityEngine;
using System.Collections;

public class LrfController : MonoBehaviour
{
	[Range(1, 100)]
	public int MinDistance;
	[Range(100, 20000)]
	public int MaxDistance;
	public int MeasuredRange;
	public bool ValidHit;
	public bool Acquire;

	float _lastMeasureTime;

	void Start()
	{
		if (MinDistance <= 0) MinDistance = 1;
		if (MaxDistance <= 100) MaxDistance = 100;
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.L))
			Acquire = true;

		if (Acquire)
		{
			RangeAcquire();
			Acquire = false;
		}
	}

	// Update is called once per frame
	public int RangeAcquire()
	{
		float time = Time.time;

		if ((time - _lastMeasureTime) < 1)
			return MeasuredRange;

		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		Vector3 pos = transform.position;
		Ray ray = new Ray(pos, fwd);

		RaycastHit hitInfo;
		bool hit = Physics.Raycast(ray, out hitInfo, MaxDistance);

		ValidHit = (hit && (MinDistance == 0 || hitInfo.distance > MinDistance));
		MeasuredRange = (ValidHit) ? (int) Mathf.Round(hitInfo.distance) : 0;

		_lastMeasureTime = time;

		return MeasuredRange;
	}
}
