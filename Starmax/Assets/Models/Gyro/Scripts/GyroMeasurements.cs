﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroMeasurements : MonoBehaviour
{
	public enum Axis { Traverse, Elevation }

	public Transform StabilizedReference;
	public Axis MeasuringAxis;
	[HideInInspector] public float Angle;
	[HideInInspector] public float Rate;

	// Update is called once per frame
//	void FixedUpdate ()
//	{
//		var angle = MeasureAngle();
//		Rate = (angle - Angle) / Time.fixedDeltaTime;
//		Angle = angle;
//	}

	public float MeasureAngle()
	{
		var mr = transform.rotation.eulerAngles;
		var sr = StabilizedReference.rotation.eulerAngles;
		float angle = 0;

		switch (MeasuringAxis)
		{
		case Axis.Elevation:
			angle = mr.x - sr.x;
			break;
		case Axis.Traverse:
			angle = mr.y - sr.y;
			break;
		}

		return angle;
	}
}
