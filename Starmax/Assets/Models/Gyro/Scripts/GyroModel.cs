﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;
using Common;

public class GyroModel : PhysicsModel, IGyroModel
{
	public bool logging;

	float _loggingStartTime;

	float _angle;
	float _rate;

	public Transform StabilizedReference;
	public Common.Axis MeasuringAxis;

	//------------------------------

	public override void ModelUpdate()
	{
		MeasureAngle();
	}

	//------------------------------

	void MeasureAngle()
	{
		float angle = GetCurrentAngle();

		var angleDiff = (angle - _angle);

		// correct passing throug zero
		if (angleDiff > +300) angleDiff -= 360;
		if (angleDiff < -300) angleDiff += 360;

		var rate = (angleDiff) / GetDeltaTime();

		// TODO: CHECK
		// discard abnormal values
		if (Mathf.Abs(rate / _rate) > 50)
			rate = _rate;

		_rate = rate;
		_angle = angle;

		if (logging) LogGyro();
	}

	//------------------------------

	void LogGyro()
	{
		var time = GetTime();

		if (_loggingStartTime == 0)
		{
			_loggingStartTime = time;
			Log.Logging("time (s), gyro rate (rad/s) # " + MeasuringAxis.ToString());
		}

		time -= _loggingStartTime;

		const float DegToRad = (float) Mathf.PI / 180.0f;
		float rate = _rate * DegToRad;
		float ang = _angle * DegToRad;

		Log.Logging(time + ", " + rate + " # " + MeasuringAxis.ToString());
	}

	//------------------------------

	public float GetCurrentAngle()
	{
		var gy = transform.rotation.eulerAngles;
		var sr = StabilizedReference.rotation.eulerAngles;

		float angle = 0;

		switch (MeasuringAxis)
		{
			case Axis.Elevation:
				angle = gy.x - sr.x;
				break;
			case Axis.Traverse:
				angle = gy.y - sr.y;
				break;
		}

		Log.Disabled("GyroModel.ModelUpdate(): " + MeasuringAxis.ToString() + " angle = " + angle);

		return angle;
	}

	//------------------------------

	public float GetAngle()
	{
		return _angle;
	}

	//------------------------------

	public float GetRate()
	{
		return _rate;
	}
}
