﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------

public class WeaponShooter : MonoBehaviour
{
	public Transform Barrel;
	public Transform Bullet;
	public uint Rounds;
	[Range(1,15)]
	public uint BurstLenght;
	[Range(60, 1200)]
	public uint FireRate;
	[Range(10, 2000)]
	public uint MaxTargetRange;
	[Range(1, 2000)]
	public uint ShootingRange;
	[Range(100, 10000)]
	public float FireForce;
	[Range(2, 10)]
	public float MaxHeight;
	[Range(-100,100)]
	public float ElvCorrection;

	public bool UseMouseButtons;
	public bool Fire;
	public bool reload;

	bool _needsReload;
	GameObject _bullets;
	Transform _bullet;
	Rigidbody _rb;
	uint _roundsFired;
	float _lastFireTime;
	float _fireInterval;
	AudioSource _shootingSound;

	//----------------------------------------

	void Start()
	{
		if (BurstLenght == 0) BurstLenght = 1;
		if (FireForce == 0) FireForce = 1000;
		if (MaxHeight == 0) MaxHeight = 2;
		if (MaxTargetRange == 0) MaxTargetRange = 2000;
		if (ShootingRange == 0) ShootingRange = MaxTargetRange / 2;
		_rb = GetComponent<Rigidbody>();
		_bullets = transform.Find("Bullets").gameObject;
		if (!_bullets) _bullets = new GameObject("Bullets");
		_bullets.transform.parent = transform;
		_shootingSound = GetComponent<AudioSource>();
		StopFire();
	}

	//----------------------------------------

	void FixedUpdate()
	{
		_fireInterval = 60f / FireRate;

		if (ShootingRange > MaxTargetRange)
			ShootingRange = MaxTargetRange;

		if (UseMouseButtons)
		{
			if (Input.GetMouseButtonDown(0)) Fire = true;
			if (Input.GetMouseButtonUp(0))   Fire = false;
			if (Input.GetMouseButtonUp(1))
			{
				float realRange;
				if (GetTargetRange(out realRange))
				{
					ShootingRange = (uint) realRange;
					Debug.Log("Shooting range is " + ShootingRange);
				}
			}
		}

		if (reload) Reload();

		if (Fire) StartFire();
		else StopFire();
	}

	//----------------------------------------

	public void Reload()
	{
		reload = false;
		if (Rounds <= 0) return;
		_needsReload = false;
	}

	//----------------------------------------

	public void StartFire()
	{
		if (_needsReload)
		{
			Fire = false;
			return;
		}
	
		Fire = true;

		if (_roundsFired >= BurstLenght)
			return;

		float elapsedTime = Time.time - _lastFireTime;

		if (elapsedTime < _fireInterval)
			return;

		bool jump = (_roundsFired == 0 && BurstLenght != 1);

		CreateBullet(jump);
		ApplyForce(FireForce * 10);
		PlayShootingAudio();

		if (--Rounds == 0)
		{
			_needsReload = true;
			Fire = false;
		}

		_lastFireTime = Time.time;
		_roundsFired++;
	}

	//----------------------------------------

	public void StopFire()
	{
		_roundsFired = 0;
		_lastFireTime = 0;
		Fire = false;
	}

	//----------------------------------------

	bool GetTargetPos(out Vector3 pos)
	{
		float range;
		return GetTargetRangePos(out range, out pos);
	}

	//----------------------------------------

	bool GetTargetRange(out float range)
	{
		Vector3 pos;
		return GetTargetRangePos(out range, out pos);
	}

	//----------------------------------------

	bool GetTargetRangePos(out float range, out Vector3 point)
	{
		RaycastHit hitInfo;
		var fwd = Barrel.TransformDirection(Vector3.forward);

		range = 0;
		point = new Vector3();

		if (!Physics.Raycast(Barrel.position, fwd, out hitInfo, MaxTargetRange))
			return false;

		if (hitInfo.transform.tag != "Target")
			return false;

		range = hitInfo.distance;
		point = hitInfo.point;

		return true;
	}

	//----------------------------------------

	void CreateBullet(bool jump = false)
	{
		float realRange;
		Vector3 targetPos;
		bool targetHit = false;
		if (!(targetHit = GetTargetRangePos(out realRange, out targetPos)))
			realRange = ShootingRange;

		var pos = Barrel.position;
		var rot = Barrel.rotation;

		_bullet = Instantiate(Bullet, pos, rot);
		_bullet.transform.parent = _bullets.transform;
		_rb = _bullet.GetComponent<Rigidbody>();

		var error = CreateDispersion(realRange, jump) + CreateRangeError(realRange) + CreateElvCorrection(realRange);

		if (targetHit)
		{
			_bullet.transform.LookAt(targetPos + error);
		}
		else
		{
			pos += Barrel.TransformDirection(Vector3.forward) * 2000;
			_bullet.transform.LookAt(pos + error);
		}
	}

	//----------------------------------------

	Vector3 CreateDispersion(float range, bool jump)
	{
		const float d = .5f; 
		const float j = -20f;
		float scale = range / MaxTargetRange;
		float dispVal = Mathf.Clamp(d * scale, d/2, d);
		float jumpVal = Mathf.Clamp(j * scale, j, j/10);

		float horizDisp = Random.Range(-dispVal, dispVal) / ((jump) ? 2 : 1);
		float vertDisp = (jump) ? jumpVal : Random.Range(-dispVal, dispVal);

		return new Vector3(horizDisp, vertDisp, 0);
	}

	//----------------------------------------

	Vector3 CreateRangeError(float realRange)
	{
		float rad = Mathf.PI * realRange / ShootingRange;
		rad = Mathf.Clamp(rad, 0, Mathf.PI * 1.5f);
		float maxHeight = MaxHeight * (realRange / MaxTargetRange);
		float height = Mathf.Sin(rad) * maxHeight;
		Debug.Log("RealRange = " + realRange + ", ShootingRange " + ShootingRange + ", Range error = " + (rad / Mathf.PI) + ", Height = " + height);
		return new Vector3(0, height, 0);
	}

	//----------------------------------------

	Vector3 CreateElvCorrection(float realRange)
	{
		float corr = ElvCorrection * (realRange / MaxTargetRange);
		return new Vector3(0, 0, 0);
	}

	//----------------------------------------

	void ApplyForce(float force)
	{
		if (_rb == null) return;
		_rb.isKinematic = false;
		_rb.AddRelativeForce(force * Vector3.forward);
	}

	//----------------------------------------

	void PlayShootingAudio()
	{
		if (!_shootingSound) return;
		_shootingSound.mute = false;
		_shootingSound.Play();
	}
}

//----------------------------------------
