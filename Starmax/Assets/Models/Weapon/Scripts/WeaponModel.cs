﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;
using Common;

public class WeaponModel : PhysicsModel, IWeaponModel
{
	[Range(500,2000)]
	public uint FireRate;
	public float MuzzleVelocity;
	public Transform Barrel;
	public Transform[] Bullets;
	AudioSource _shootingSound;
	public bool Tracer;
	[Range(1,10)]
	public int TracerCount;

	[Range(0, 20)]
	public float forceVariation;
	[Range(0, 1)]
	public float forceLimiter;
	[Range(1, 2000)]
	public float spinForceLimiter;

	[Range(-99, 99)]
	public float windSpeed = 0;
	[Range(-500000, 500000)]
	public float windForceScale = 0;

	uint _bulletSelection;
	uint _bulletIndex;
	uint _shotsFired;
	uint _burstLength;
	bool _fireCmd;
	Rigidbody _rb;

	float _force;

	GameObject _bulletsParent;

	float _lastFireTime;
	float _fireInterval;

	System.Random _random;

	//-----------------------------

	void Awake()
	{
		_rb = GetComponent<Rigidbody>();

		_bulletsParent = transform.Find("Bullets").gameObject;

		if (!_bulletsParent)
			_bulletsParent = new GameObject("Bullets");

		_bulletsParent.transform.parent = transform;

		_shootingSound = GetComponent<AudioSource>();

		_random = new System.Random();

		if (FireRate == 0)
			FireRate = 1000;

		if (MuzzleVelocity == 0)
			MuzzleVelocity = 887f;

		CalculateForce();
	}

	//-----------------------------

	void CalculateForce(float mass = 0)
	{
		if (mass == 0)
		{
			if (Bullets != null && Bullets.Length >= 1)
			{
				var bullet = Instantiate(Bullets[0]);
				mass = bullet.GetComponent<Rigidbody>().mass;
				GameObject.Destroy(bullet.gameObject);
			}
			else
				mass = 0.584f;
		}

		float vel = MuzzleVelocity; // m/s
		float accel = vel / Time.fixedDeltaTime;
		_force = mass * accel;

		_force *= forceLimiter;

		Log.Info("WeaponModel.Awake(): force = " + _force);
	}

	//-----------------------------

	public override void ModelUpdate()
	{
		if (_fireCmd)
			PerformFire();
		_fireCmd = false;
	}

	//------------------------------

	public uint GetFireRate()
	{
		return FireRate;
	}

	//------------------------------

	public void SelectBullet(int index)
	{
		if (Bullets != null)
			_bulletSelection = (uint) Mathf.Clamp(index, 0, Bullets.Length - 1);
		else
			_bulletSelection = 0;
	}

	//------------------------------

	public void SetTracerBullet(int interval)
	{
		interval = Mathf.Clamp(interval, 0, 10);
		Tracer = interval > 0;
		TracerCount = interval;
	}

	//------------------------------

	public void SetWindSpeed(float windSpeed)
	{
		this.windSpeed = windSpeed;
	}

	//------------------------------

	public void Fire()
	{
		_fireCmd = true;
	}

	//-----------------------------

	public void PerformFire()
	{
		var bullet = CreateBullet();
		CalculateForce(bullet.GetComponent<Rigidbody>().mass);
		ApplyForce(bullet, _force);
		PlayShootingAudio();
		_shotsFired++;
	}

	//-----------------------------

	Rigidbody CreateBullet(bool jump = false)
	{
		if (Bullets == null || Bullets.Length <= 0)
			return null;

		if (_bulletSelection >= Bullets.Length)
			_bulletSelection = 0;
		
		var bullet = Instantiate(Bullets[_bulletSelection], Barrel.position, Barrel.rotation);

		if (bullet == null)
			return null;

		var trail = bullet.GetComponent<TrailRenderer>();
		if (trail != null)
			trail.enabled = Tracer && (_shotsFired % TracerCount == 0);
		bullet.name = "Bullet" + string.Format("{0,6}", _shotsFired);

//		var error = CreateDispersion(realRange, jump) + CreateRangeError(realRange) + CreateElvCorrection(realRange);

		var forces = bullet.gameObject.AddComponent<BulletForces>() as BulletForces;
		forces.lateralForce = windSpeed * windForceScale;

		return bullet.GetComponent<Rigidbody>();
	}

	//-----------------------------

	void ApplyForce(Rigidbody bullet, float force)
	{
		if (bullet == null)
			return;

		var maxForceVariation = force * (forceVariation / 100f);
		force += maxForceVariation * (float)(_random.NextDouble() - 0.5);

		bullet.isKinematic = false;
		bullet.AddRelativeForce(force * Vector3.forward);
		bullet.AddRelativeForce(force / spinForceLimiter * Vector3.right);
	}

	//-----------------------------

	void PlayShootingAudio()
	{
		if (!_shootingSound) return;
		_shootingSound.mute = false;
		_shootingSound.Play();
	}

	//-----------------------------
}
