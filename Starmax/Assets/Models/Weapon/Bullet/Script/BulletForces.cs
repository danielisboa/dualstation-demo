﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletForces : MonoBehaviour
{
	public float lateralForce;
	Rigidbody _rigidBody;

	void Start()
	{
		_rigidBody = gameObject.GetComponent<Rigidbody>();
	}

	void Update()
	{
		if (_rigidBody == null)
			return;

		var force = new Vector3();

		if (lateralForce != 0)
			force += lateralForce * Vector3.right;

		_rigidBody.AddRelativeForce(force);

		Log.Disabled("BulletForces.Update(): adding force " + force + " to bullet");
	}
}
