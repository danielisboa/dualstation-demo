﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDestroy : MonoBehaviour
{
	public float TimeToLive;
	public Transform bulletImpactSmoke;

	void Start()
	{
		if (TimeToLive > 0)
			Destroy(this.gameObject, TimeToLive);
	}

	void OnCollisionEnter(Collision collision)
	{
		Destroy(this.gameObject);

		var contact = collision.contacts[0];
		var other = collision.collider.gameObject;

		if (other.tag == "Target")
			Log.Debug("BulletDestroy.OnCollisionEnter(): Collision on Target object, number of contacts = " + collision.contacts.Length);

		if (other.tag != "Target" && bulletImpactSmoke != null)
		{
			var smoke = Instantiate(bulletImpactSmoke);
			smoke.position = contact.point;
			GameObject.Destroy(smoke.gameObject, 1);
		}
	}
}
