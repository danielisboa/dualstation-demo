﻿using UnityEngine;
using System;

//-------------------------------------

namespace AssemblyCSharp
{
	public class Coordinates
	{
		public float X;
		public float Y;
		public float Z;
		public float Trv;
		public float Elv;
		public float Dist;

		//-------------------------------------

		public Coordinates()
		{
			SetXYZ(0, 0, 0);
		}

		//-------------------------------------

		public Coordinates(Coordinates oOther)
		{
			Copy(oOther);
		}

		//-------------------------------------

		public Coordinates Copy(Coordinates oOther)
		{
			return SetXYZ(oOther.X, oOther.Y, oOther.Z);
		}

		//-------------------------------------

		public static Coordinates operator +(Coordinates oFirst, Coordinates oSecond)
		{
			Coordinates oNew = new Coordinates();
			oNew.SetXYZ(oFirst.X + oSecond.X, oFirst.Y + oSecond.Y, oFirst.Z + oSecond.Z);
			return oNew;
		}

		//-------------------------------------

		public static Coordinates operator -(Coordinates oFirst, Coordinates oSecond)
		{
			Coordinates oNew = new Coordinates();
			oNew.SetXYZ(oFirst.X - oSecond.X, oFirst.Y - oSecond.Y, oFirst.Z - oSecond.Z);
			return oNew;
		}

		//-------------------------------------

		public static float Deg2Rad(float fDegrees)
		{
			return fDegrees * Mathf.PI / 180.0f;
		}

		//-------------------------------------

		public static float Rad2Deg(float fRadians)
		{
			return fRadians * 180.0f / Mathf.PI;
		}

		//-------------------------------------

		public Coordinates SetXYZ(float fX, float fY, float fZ)
		{
			X = fX; Y = fY; Z = fZ;
			RecalculateAngles();
			return this;
		}

		//-------------------------------------

		public Coordinates SetAngles(float fTrv, float fElv, float fDist)
		{
			Trv = fTrv; Elv = fElv; Dist = fDist;
			RecalculateXYZ();
			return this;
		}

		//-------------------------------------

		void RecalculateXYZ()
		{
			float fXYProj = Mathf.Cos(Deg2Rad(Elv)) * Dist;
			Z = Mathf.Sin(Deg2Rad(Elv)) * Dist;
			Y = Mathf.Sin(Deg2Rad(Trv)) * fXYProj;
			X = Mathf.Cos(Deg2Rad(Trv)) * fXYProj;
		}

		//-------------------------------------

		void RecalculateAngles()
		{
			float fXYProj = Mathf.Sqrt(Y * Y + X * X);
			Trv = Rad2Deg(Mathf.Atan2(Y, X));
			Elv = Rad2Deg(Mathf.Atan2(Z, fXYProj));
			Dist = Mathf.Sqrt(Z * Z + fXYProj * fXYProj);
		}

		//-------------------------------------

		void RotateAxis(ref float fVertAxis, ref float fHorzAxis, float fRotationAngle)
		{
			float fVA = fVertAxis, fHA = fHorzAxis;
			float fAngle = Mathf.Atan2(fVA, fHA) + Deg2Rad(fRotationAngle);
			float fVectSize = Mathf.Sqrt(fVA * fVA + fHA * fHA);
			fVA = Mathf.Sin(fAngle) * fVectSize;
			fHA = Mathf.Cos(fAngle) * fVectSize;
			fVertAxis = fVA;
			fHorzAxis = fHA;
		}

		//-------------------------------------

		public void RotateX(float fRotationAngle)
		{
			RotateAxis(ref Z, ref Y, fRotationAngle);
			RecalculateAngles();
		}

		//-------------------------------------

		public void RotateY(float fRotationAngle)
		{
			RotateAxis(ref Z,  ref X, fRotationAngle);
			RecalculateAngles();
		}

		//-------------------------------------

		public void RotateZ(float fRotationAngle)
		{
			RotateAxis(ref Y, ref X, fRotationAngle);
			RecalculateAngles();
		}
	}
}

//-------------------------------------
