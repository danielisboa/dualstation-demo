﻿using UnityEngine;
using System.Collections;

public class TargetCtrl : MonoBehaviour
{
	[Range (0, 1)]  public float ThrustFrequency;
	[Range (0, 50)] public float ThrustAmplitude;
	public bool Invert;

	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
		float fDisp = Amplitude(Time.time, ThrustFrequency, ThrustAmplitude);
		var vAngles = transform.position;
		vAngles.x = (Invert) ? -fDisp : fDisp;
		transform.position = vAngles;
	}

	float Amplitude(float fRunTime, float fFreq, float fMaxAmplitude)
	{
		if (fFreq <= 0) return 0f;

		float fCycleTime = 1.0f / fFreq;
		float fSinusAngle = (fRunTime / fCycleTime) * 2f * Mathf.PI;

		float fAmplitude = Mathf.Sin(fSinusAngle) * fMaxAmplitude;

		return fAmplitude;
	}
}
