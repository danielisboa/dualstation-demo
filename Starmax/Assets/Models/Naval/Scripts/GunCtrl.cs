﻿using UnityEngine;
using System.Collections;

public class GunCtrl : MonoBehaviour
{
	Transform TrvDrive;
	Transform ElvDrive;

	public bool RemoteControl;

	[Range (-20, 85)]   public float Elevation;
	[Range (-180, 180)] public float Traverse;

	float TrvDemand;
	float ElvDemand;

	// Use this for initialization
	void Start ()
	{
		TrvDrive = transform.Find("TrvDrive");
		ElvDrive = TrvDrive.Find("ElvAssy").Find("ElvDrive");
	}
	
	// Update is called once per frame
	void Update ()
	{
		float fTrv, fElv;

		if (RemoteControl)
		{
			Traverse  = fTrv = TrvDemand;
			Elevation = fElv = ElvDemand;
		}
		else
		{
			fTrv = Traverse;
			fElv = Elevation;
		}

		var vTrvAngles = TrvDrive.localEulerAngles;
		vTrvAngles.y = fTrv;
		TrvDrive.localEulerAngles = vTrvAngles;

		var vElvAngles = ElvDrive.localEulerAngles;
		vElvAngles.x = fElv;
		ElvDrive.localEulerAngles = vElvAngles;
	}

	public void PositionGun(float fTrv, float fElv)
	{
		TrvDemand = fTrv;
		ElvDemand = fElv;
	}
}
