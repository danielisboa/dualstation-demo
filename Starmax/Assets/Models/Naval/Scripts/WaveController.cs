﻿using UnityEngine;
using System.Collections;

public class WaveController : MonoBehaviour
{
	[Range (0, 2)]  public float RollFrequency;
	[Range (0, 2)]  public float PitchFrequency;
	[Range (0, 2)]  public float HeadingFrequency;
	[Range (0, 10)] public float RollAmplitude;
	[Range (0, 10)] public float PitchAmplitude;
	[Range (0, 10)] public float HeadingAmplitude;

	public float Roll;
	public float Pitch;
	public float Heading;
	public float OrigHeading;

	// Use this for initialization
	void Start ()
	{
		OrigHeading = transform.localRotation.eulerAngles.y;
		Log.Debug("WaveController.Start(): transform.localRotation.eulerAngles = " + transform.localRotation.eulerAngles);
		Log.Debug("WaveController.Start(): OrigHeading = " + OrigHeading);
	}

	float Amplitude(float fRunTime, float fFreq, float fMaxAmplitude)
	{
		if (fFreq <= 0) return 0f;

		float fCycleTime = 1.0f / fFreq;
		float fSinusAngle = (fRunTime / fCycleTime) * 2f * Mathf.PI;

		float fAmplitude = Mathf.Sin(fSinusAngle) * fMaxAmplitude;

		return fAmplitude;
	}

	// Update is called periodically at a fixed rate
	void FixedUpdate()
	{
		float fRunTime = Time.time;

		float fRoll    = Amplitude(fRunTime, RollFrequency, RollAmplitude);
		float fPitch   = Amplitude(fRunTime, PitchFrequency, PitchAmplitude);
		float fHeading = Amplitude(fRunTime, HeadingFrequency, HeadingAmplitude) + OrigHeading;

		transform.localEulerAngles = new Vector3(fRoll, fHeading, fPitch);

		Roll = -Mathf.Round(fRoll * 100f) / 100f;
		Pitch = Mathf.Round(fPitch * 100f) / 100f;
		Heading = Mathf.Round(fHeading * 100f) / 100f;
	}
}
