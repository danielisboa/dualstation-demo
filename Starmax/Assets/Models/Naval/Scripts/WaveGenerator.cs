﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveGenerator : MonoBehaviour
{
	public float scale = 10.0f;
	public float speed = 1.0f;
	public float noiseStrength = 4.0f;
	public float noiseWalk = 4.0f;

	Vector3[] baseHeight;
	Mesh mesh;

	void Start()
	{
		var meshFilter = GetComponent<MeshFilter>();

		if (!meshFilter)
			return;

		mesh = meshFilter.mesh;
		baseHeight = mesh.vertices;
	}

	// Update is called once per frame
	void Update ()
	{
		var vertices = new Vector3[baseHeight.Length];

		for (int i = 0; i < vertices.Length; i++)
		{
			var vertex = baseHeight[i];
			vertex.y += Mathf.Sin(Time.time * speed + baseHeight[i].x + baseHeight[i].y + baseHeight[i].z) * scale;
			vertex.y += Mathf.PerlinNoise(baseHeight[i].x + noiseWalk, baseHeight[i].y + Mathf.Sin(Time.time * 0.1f)) * noiseStrength;
			vertices[i] = vertex;
		}

		mesh.vertices = vertices;
		mesh.RecalculateNormals();
	}
}
