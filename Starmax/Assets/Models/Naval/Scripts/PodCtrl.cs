﻿using UnityEngine;
using System.Collections;

public class PodCtrl : MonoBehaviour
{
	public GameObject Target;
	public GameObject StabRef;
	[Range (-20, 85)]    public float Elevation;
	[Range  (-180, 180)] public float Traverse;

	GameObject Tracked;
	Transform Sphere;
	int SlerpCount;
	const int SlerpCountResetVal = 50;

	bool Tracking;
	public bool TrackTarget
	{
		set
		{
			if (value && !Tracking)
			{
				Tracked = Target;
			}
			else if (!value && Tracking)
			{
				Debug.Log("Tracking = " + Tracking + ", value = " + value);
				StabRef.transform.position = Target.transform.position;
				Tracked = StabRef;
			}

			Tracking = value;
		}
	}

	// Use this for initialization
	void Start()
	{
		Tracking = false;
		Tracked = StabRef;
		Sphere = transform.Find("Sphere");
		SlerpCount = SlerpCountResetVal;
	}
	
	// Update is called once per frame
	void Update()
	{
		if (true) // || (Tracking && Tracked != null))
		{
			StabPod();
			float fElv = Sphere.localEulerAngles.x;
			Elevation = (fElv <= 180f) ? -fElv : (360f - fElv);
			float fTrv = Sphere.localEulerAngles.y; 
			Traverse = (fTrv <= 180f) ? fTrv : fTrv - 360f;;
		}
		// else
		// {
		// 	float fElv = (Elevation > 0f) ? Elevation : 360f + Elevation;
		// 	var vAngles = Sphere.localEulerAngles;
		// 	vAngles.x = -fElv;
		// 	vAngles.y = Traverse;
		// 	Sphere.localEulerAngles = vAngles;
		// 	SlerpCount = SlerpCountResetVal;
		// }

//		ZRotation = Mathf.Round(Sphere.localEulerAngles.z * 100f) / 100f;
	}

	void StabPod()
	{
		float fSpeed = 10.0f;
		var vSphPos = Sphere.position;
		var vTgtPos = Tracked.transform.position;
		var qRotation = Quaternion.LookRotation(vTgtPos - vSphPos, Sphere.parent.up);
		if (SlerpCount-- <= 0) fSpeed = 15f;
		Sphere.rotation =  qRotation = Quaternion.Slerp(Sphere.rotation, qRotation, Time.deltaTime * fSpeed);
		if (SlerpCount < 0) SlerpCount = 0;
	}
}
