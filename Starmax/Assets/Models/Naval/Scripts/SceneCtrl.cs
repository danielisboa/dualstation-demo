﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using AssemblyCSharp;
using System;

public class SceneCtrl : MonoBehaviour
{
	GameObject Pod;
	GameObject Ship;
	PodCtrl PodCtrlScript;
	Coordinates Coord;
	float Roll;
	float Pitch;
	float Heading;
	float Trv;
	float Elv;	

	bool bTracking;

	// Use this for initialization
	void Start ()
	{
		Coord = new Coordinates();
		Coord.SetXYZ(0, 0, 0);
		Ship = GameObject.Find("Ship");
		if (Ship == null) return;
		Pod = Ship.transform.Find("Pod").gameObject;
		if (Pod == null) return;
		PodCtrlScript = Pod.GetComponent<PodCtrl>();
		Pod = Pod.transform.Find("Sphere").gameObject;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (PodCtrlScript == null) return;
		PodCtrlScript.TrackTarget = bTracking;
		var PodPos = Pod.transform.localRotation.eulerAngles;
		Trv = PodPos.y;
		Elv = -PodPos.x;
		Coord.SetAngles(Trv, Elv, 1000);
		var ShipPos = Ship.transform.localRotation.eulerAngles;
		Roll = ShipPos.z;
		Pitch = ShipPos.x;
		Heading = ShipPos.y;
		Coord.RotateX(Roll);
		Coord.RotateY(-Pitch);
		Coord.RotateZ(Heading);
	}

	void OnGUI()
	{
		GUILabel(0.5, 0.0, 100, 20, "Panoramic View");
		GUILabel(0.1, 0.5, 100, 20, "Pod Camera View");
		GUILabel(0.1, 0.3, 100, 20, "Gun Camera View");
		GUILabel(0.9, 0.3, 100, 20, "Pod Front View");
		GUILabel(0.5, 0.8, 500, 20, String.Format("E: {3:F} | R: {1:F}, P: {2:F} | S: {0:F}", Coord.Elv, Roll, Pitch, Elv));
		GUILabel(0.5, 0.9, 500, 20, String.Format("T: {6:F}, E: {7:F} | R: {3:F}, P: {4:F}, H: {5:F} | B: {0:F}, S: {1:F}, R: {2:D}", Coord.Trv, Coord.Elv, (int)Coord.Dist, Roll, Pitch, Heading, Trv, Elv));
		bTracking = GUIToggle(0.5, 0.5, 100, 20, bTracking, "Track Target");
	}

	Rect GUIRect(double fX, double fY, int nW, int nH)
	{
		int nPosX = (int) (Screen.width * fX - nW / 2.0);
		int nPosY = (int) (Screen.height * fY);
		nPosX = Mathf.Clamp(nPosX, 0, Screen.width - nW);
		nPosY = Mathf.Clamp(nPosY, 0, Screen.height - nH);
		return new Rect(nPosX, nPosY, nW, nH);
	}

	void GUILabel(double fX, double fY, int nW, int nH, string sLbl)
	{
		GUI.Label(GUIRect(fX, fY, nW, nH), sLbl);
	}

	bool GUIToggle(double fX, double fY, int nW, int nH, bool bVal, string sLbl)
	{
		return GUI.Toggle(GUIRect(fX, fY, nW, nH), bVal, sLbl);
	}

	bool GUIButton(double fX, double fY, int nW, int nH, string sLbl)
	{
		return GUI.Button(GUIRect(fX, fY, nW, nH), sLbl);
	}
}
