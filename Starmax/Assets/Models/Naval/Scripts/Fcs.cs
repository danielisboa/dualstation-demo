﻿using UnityEngine;
using System.Collections;

public class Fcs : MonoBehaviour
{
	PodCtrl PodCtrlScript;
	GunCtrl GunCtrlScript;

	public bool EnslaveGun;

	// Use this for initialization
	void Start ()
	{
		PodCtrlScript = transform.Find("Pod").gameObject.GetComponent<PodCtrl>();
		GunCtrlScript = transform.Find("Gun").gameObject.GetComponent<GunCtrl>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (PodCtrlScript == null || GunCtrlScript == null) return;

		float fTrv = PodCtrlScript.Traverse;
		float fElv = PodCtrlScript.Elevation;

		GunCtrlScript.RemoteControl = EnslaveGun;
		GunCtrlScript.PositionGun(fTrv, fElv);
	}
}
