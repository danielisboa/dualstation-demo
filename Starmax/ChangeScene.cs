﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScene : MonoBehaviour 
{
	LoadOnClick _loader;

	// Use this for initialization
	void Start () {
		_loader = gameObject.GetComponent<LoadOnClick> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.F))
			Screen.fullScreen = !Screen.fullScreen; 
		
		if (!_loader)
			return;

//		if (!Input.GetKey(KeyCode.LeftAlt))
//			return;

		int sceneIdx = -1;

		// pantograph horizontal
		if (Input.GetKey (KeyCode.Alpha0))
			sceneIdx = 0;
		// pantograph vertical
		if (Input.GetKey (KeyCode.Alpha1))
			sceneIdx = 1;
		// pantograph elipse
		if (Input.GetKey (KeyCode.Alpha2))
			sceneIdx = 2;
		// static platform
		if (Input.GetKey (KeyCode.Alpha3))
			sceneIdx = 3;
		// mobile platform
		if (Input.GetKey (KeyCode.Alpha4))
			sceneIdx = 4;
		// mobile platform
		if (Input.GetKey (KeyCode.Alpha5))
			sceneIdx = 5;

		if (sceneIdx == -1)
			return;
		
		Debug.Log ("Loading idx: " + sceneIdx);
		_loader.LoadScene (sceneIdx);
	}
}
